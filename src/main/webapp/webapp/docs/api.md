## Classes

<dl>
<dt><a href="#App">App</a></dt>
<dd></dd>
<dt><a href="#ContentReports">ContentReports</a></dt>
<dd></dd>
<dt><a href="#TrafficReports">TrafficReports</a></dt>
<dd></dd>
<dt><a href="#UserReports">UserReports</a></dt>
<dd></dd>
<dt><a href="#ViewEvents">ViewEvents</a></dt>
<dd></dd>
<dt><a href="#Create">Create</a></dt>
<dd><p>Component that display user creation box.
It includes multiple input forms for user information.
Html tags will be sanitized when validation process is triggered for user input.
Phone number will be automatically formatted when common phone number is given.
Password strength meter checks password string from a user.
Email format will be checked by email-validator module.</p></dd>
<dt><a href="#Users">Users</a></dt>
<dd><p>Component that give a placeholder for an user list.</p></dd>
<dt><a href="#Index">Index</a></dt>
<dd><p>Component that give a placeholder for dashboard for administrator.</p></dd>
<dt><a href="#ContentElementList">ContentElementList</a></dt>
<dd></dd>
<dt><a href="#ContentListElement">ContentListElement</a></dt>
<dd></dd>
<dt><a href="#ContentManagerIndex">ContentManagerIndex</a></dt>
<dd></dd>
<dt><a href="#ContentTagDisplay">ContentTagDisplay</a></dt>
<dd></dd>
<dt><a href="#ContentTags">ContentTags</a></dt>
<dd></dd>
<dt><a href="#CoordinatePicker">CoordinatePicker</a></dt>
<dd></dd>
<dt><a href="#EditContentElement">EditContentElement</a></dt>
<dd></dd>
<dt><a href="#Editor">Editor</a></dt>
<dd></dd>
<dt><a href="#FilterTable">FilterTable</a></dt>
<dd></dd>
<dt><a href="#ManageStories">ManageStories</a></dt>
<dd></dd>
<dt><a href="#MapPointSelector">MapPointSelector</a></dt>
<dd></dd>
<dt><a href="#StoryCreationProgress">StoryCreationProgress</a></dt>
<dd></dd>
<dt><a href="#StoryForm">StoryForm</a></dt>
<dd></dd>
<dt><a href="#UploadElement">UploadElement</a></dt>
<dd></dd>
<dt><a href="#UploadPreview">UploadPreview</a></dt>
<dd></dd>
<dt><a href="#About">About</a></dt>
<dd></dd>
<dt><a href="#SignIn">SignIn</a></dt>
<dd><p>Component that display log-in form in order to take username and password.</p></dd>
<dt><a href="#SignOut">SignOut</a></dt>
<dd><p>Component that nullify user information and redirect a user to main page after log-out.</p></dd>
</dl>

## Functions

<dl>
<dt><a href="#Index">Index()</a> ⇒ <code>*</code></dt>
<dd><p>Component that give a placeholder for dashboard for user who is not administrator.</p></dd>
<dt><a href="#Checkbox">Checkbox(id, label, isSelected, onCheckboxChange)</a> ⇒ <code>*</code></dt>
<dd><p>Component that holds a list of individual ContentListElement components.</p></dd>
<dt><a href="#ReadList">ReadList()</a> ⇒ <code>*</code></dt>
<dd><p>Component that display outer HTML container for user list table.</p></dd>
<dt><a href="#Table">Table(props)</a> ⇒ <code>*</code></dt>
<dd><p>Component that holds a table for user list.
It contains user create, delete and modify functions.</p></dd>
<dt><a href="#MapBox">MapBox()</a> ⇒</dt>
<dd><p>Main map container. Holds Openlayers map canvas, every component that is to be displayed on the map as well as all
states relevant to multiple components. Passes callback functions down to children components to control shared
state. Tracks and applies filter options.</p></dd>
<dt><a href="#ProtectedRoute">ProtectedRoute(props, isAllowed, alternative)</a> ⇒ <code>*</code></dt>
<dd><p>Component that perform conditional routing.</p></dd>
<dt><a href="#archiveContentElement">archiveContentElement(element, contentID, mapPointID, id, action)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Archives or unarchives a single ContentElement on the server.</p></dd>
<dt><a href="#createMapPointRequest">createMapPointRequest(storyID, zoomLevel, coordinates)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a server request to create a new MapPoint.</p></dd>
<dt><a href="#createStory">createStory(creator)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to create a new Story with the minimum number of attributes.</p></dd>
<dt><a href="#createTag">createTag(contentID, tagDesc)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to create a new Content Tag.</p></dd>
<dt><a href="#deleteContentElement">deleteContentElement(elementID, mapPointID, contentID)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to soft-delete a single ContentElement</p></dd>
<dt><a href="#deleteMapPoint">deleteMapPoint(contentID, mapPointID)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to soft-delete a single MapPoint.</p></dd>
<dt><a href="#deleteStory">deleteStory(id)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to soft-delete a single Story object</p></dd>
<dt><a href="#getAllTags">getAllTags()</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to get every available Tag.</p></dd>
<dt><a href="#getArchivedStoryList">getArchivedStoryList()</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to get a list of every Story that has a state of ARCHIVED.</p></dd>
<dt><a href="#getContentElementByID">getContentElementByID(id)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to get a single ContentElement by its ID.</p></dd>
<dt><a href="#getMapPointByID">getMapPointByID(id)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to get a single MapPoint by its ID.</p></dd>
<dt><a href="#getMapPointsByStoryID">getMapPointsByStoryID(id)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to get every MapPoint for a Story.</p></dd>
<dt><a href="#getStory">getStory(id)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to get a single Story object by its ID.</p></dd>
<dt><a href="#getStoryList">getStoryList()</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to get an array of every Story in the system.</p></dd>
<dt><a href="#removeTagFromStory">removeTagFromStory(contentID, tag)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to remove a single Tag object from a Story.</p></dd>
<dt><a href="#saveContentElement">saveContentElement(contentElement, contentID, mapPointID)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Saves a single ContentElement to the server.</p></dd>
<dt><a href="#saveImageContentElement">saveImageContentElement(contentElement, contentID, mapPointID)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to save an Image object.</p></dd>
<dt><a href="#saveMapPoint">saveMapPoint(mapPoint)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to save a MapPoint object.</p></dd>
<dt><a href="#saveStory">saveStory(story)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends as request to the server to save a single edited Story object.</p></dd>
<dt><a href="#saveTagToStory">saveTagToStory(contentID, tag)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to save a Tag object to the current Story being edited.</p></dd>
<dt><a href="#saveTextContentElement">saveTextContentElement(contentElement, contentID, mapPointID)</a> ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code></dt>
<dd><p>Sends a request to the server to save a Text ContentElement.</p></dd>
<dt><a href="#tryTokenRefresh">tryTokenRefresh()</a></dt>
<dd><p>Sends a request to the server to refresh the JWT of the current authenticated user.</p></dd>
</dl>

<a name="App"></a>

## App
**Kind**: global class  
<a name="new_App_new"></a>

### new App(props)
<p>Top-level Component in the React application. Handles routing and transitions to the major pages of the website.</p>

**Returns**: <p>the application section based on the URL.</p>  

| Param | Description |
| --- | --- |
| props | <p>props from React.js</p> |

<a name="ContentReports"></a>

## ContentReports
**Kind**: global class  
<a name="new_ContentReports_new"></a>

### new ContentReports(props)
<p>Component that displays Content Information Charts.</p>


| Param | Description |
| --- | --- |
| props | <p>props from parent component.</p> |

<a name="TrafficReports"></a>

## TrafficReports
**Kind**: global class  
<a name="new_TrafficReports_new"></a>

### new TrafficReports(props)
<p>Component that displays Network Traffic Charts.</p>


| Param | Description |
| --- | --- |
| props | <p>props from parent component</p> |

<a name="UserReports"></a>

## UserReports
**Kind**: global class  
<a name="new_UserReports_new"></a>

### new UserReports(props)
<p>Component that displays User Information Charts.</p>


| Param | Description |
| --- | --- |
| props | <p>props from parent component.</p> |

<a name="ViewEvents"></a>

## ViewEvents
**Kind**: global class  
<a name="new_ViewEvents_new"></a>

### new ViewEvents(props)
<p>Component that displays Logged event for Users, Stories and Database.</p>


| Param | Description |
| --- | --- |
| props | <p>props from parent component.</p> |

<a name="Create"></a>

## Create
<p>Component that display user creation box.
It includes multiple input forms for user information.
Html tags will be sanitized when validation process is triggered for user input.
Phone number will be automatically formatted when common phone number is given.
Password strength meter checks password string from a user.
Email format will be checked by email-validator module.</p>

**Kind**: global class  
<a name="new_Create_new"></a>

### new Create(props)

| Param | Description |
| --- | --- |
| props | <p>props from parent component</p> |

<a name="Users"></a>

## Users
<p>Component that give a placeholder for an user list.</p>

**Kind**: global class  
<a name="Index"></a>

## Index
<p>Component that give a placeholder for dashboard for administrator.</p>

**Kind**: global class  
<a name="ContentElementList"></a>

## ContentElementList
**Kind**: global class  
<a name="new_ContentElementList_new"></a>

### new exports.ContentElementList(props)
<p>Component that holds a list of individual ContentListElement components.</p>


| Param | Description |
| --- | --- |
| props | <p>props from parent component</p> |

<a name="ContentListElement"></a>

## ContentListElement
**Kind**: global class  
<a name="new_ContentListElement_new"></a>

### new exports.ContentListElement(props)
<p>Component that hold a list of individual Content Elements available on a Map Point.</p>


| Param | Description |
| --- | --- |
| props | <p>props from the ContentElementList container</p> |

<a name="ContentManagerIndex"></a>

## ContentManagerIndex
**Kind**: global class  
<a name="new_ContentManagerIndex_new"></a>

### new exports.ContentManagerIndex(props)
<p>Entry point into the content management sub-system. Handles navigation between the Manage Stories list and the Create a Story interface.</p>


| Param | Description |
| --- | --- |
| props | <p>props from App.jsx</p> |

<a name="ContentTagDisplay"></a>

## ContentTagDisplay
**Kind**: global class  
<a name="new_ContentTagDisplay_new"></a>

### new exports.ContentTagDisplay(props)
<p>Component that displays content warning tags that have been applied to the Story currently being edited.</p>


| Param | Description |
| --- | --- |
| props | <p>props received from ContentManager.jsx</p> |

<a name="ContentTags"></a>

## ContentTags
**Kind**: global class  
<a name="new_ContentTags_new"></a>

### new exports.ContentTags(props)
<p>Component that displays available content warning tags that be applied to a Story. Also handles creation of new Content tags.</p>


| Param | Description |
| --- | --- |
| props | <p>props received from ContentManager.jsx</p> |

<a name="CoordinatePicker"></a>

## CoordinatePicker
**Kind**: global class  
<a name="new_CoordinatePicker_new"></a>

### new CoordinatePicker(props)
<p>Component that handles picking a location for a single map point in a Story.</p>


| Param | Description |
| --- | --- |
| props | <p>props recieved from ContentManager.jsx</p> |

<a name="EditContentElement"></a>

## EditContentElement
**Kind**: global class  
<a name="new_EditContentElement_new"></a>

### new exports.EditContentElement(props)
<p>Component that displays a modal for editing the attributes of a single ContentElement.</p>


| Param | Description |
| --- | --- |
| props | <p>props received from ContentManager.jsx</p> |

<a name="Editor"></a>

## Editor
**Kind**: global class  
<a name="new_Editor_new"></a>

### new exports.Editor(props)
<p>Displays a WYSIWYG text editor for creation of Text Content Elements.</p>


| Param | Description |
| --- | --- |
| props | <p>props received from ContentManager.jsx</p> |

<a name="FilterTable"></a>

## FilterTable
**Kind**: global class  

* [FilterTable](#FilterTable)
    * [new exports.FilterTable(props)](#new_FilterTable_new)
    * [~archiveStoryObject()](#FilterTable..archiveStoryObject)

<a name="new_FilterTable_new"></a>

### new exports.FilterTable(props)
<p>Displays every Story saved in the database. Every Story that is not archived can be edited, archived, published,
or deleted. Archived Stories can be view by clicking a separate button. Stories can be sorted, filtered, and searched for by different criteria.</p>


| Param | Description |
| --- | --- |
| props | <p>props received from ManageStories.jsx</p> |

<a name="FilterTable..archiveStoryObject"></a>

### FilterTable~archiveStoryObject()
<p>Handles archiving and un-archiving Stories. Stories that are un-archived go into DRAFT state.</p>

**Kind**: inner method of [<code>FilterTable</code>](#FilterTable)  
<a name="ManageStories"></a>

## ManageStories
**Kind**: global class  
<a name="new_ManageStories_new"></a>

### new exports.ManageStories(props)
<p>Handles retrieval of stories to fill the FilterTable component.</p>


| Param | Description |
| --- | --- |
| props | <p>props received from ContentManagerIndex.jsx</p> |

<a name="MapPointSelector"></a>

## MapPointSelector
**Kind**: global class  
<a name="new_MapPointSelector_new"></a>

### new exports.MapPointSelector(props)
<p>Displays all of the created map points for the current Story in the Content Manager.</p>


| Param | Description |
| --- | --- |
| props | <p>props received from ContentManager.jsx</p> |

<a name="StoryCreationProgress"></a>

## StoryCreationProgress
**Kind**: global class  
<a name="new_StoryCreationProgress_new"></a>

### new exports.StoryCreationProgress(props)
<p>Displays headings for the different steps in the Story creation process. Also handles navigation between the different steps.</p>


| Param | Description |
| --- | --- |
| props | <p>props received from ContentManager.jsx</p> |

<a name="StoryForm"></a>

## StoryForm
**Kind**: global class  
<a name="new_StoryForm_new"></a>

### new exports.StoryForm(props)
<p>Displays a form to enter the initial Story details when creating or editing a Story. The fields are: Story Title, Asylum Seeker Name, Country of Origin, and Description.</p>


| Param | Description |
| --- | --- |
| props | <p>props received from ContentManager.jsx</p> |

<a name="UploadElement"></a>

## UploadElement
**Kind**: global class  
<a name="new_UploadElement_new"></a>

### new exports.UploadElement(props)
<p>A modal for uploading a new ContentElement file. Acceptable file types are images, videos, and audio files. File size limit is 100 MB.</p>


| Param | Description |
| --- | --- |
| props | <p>props received from ContentManager.jsx</p> |

<a name="UploadPreview"></a>

## UploadPreview
**Kind**: global class  
<a name="new_UploadPreview_new"></a>

### new exports.UploadPreview(props)
<p>Container for holding the file object from a file uploaded from a user's computer.</p>


| Param | Description |
| --- | --- |
| props | <p>props received from EditContentElement.jsx</p> |

<a name="About"></a>

## About
**Kind**: global class  
<a name="new_About_new"></a>

### new exports.About(props)
<p>A modal window that displays information about the Asylum Seekers website.</p>


| Param | Description |
| --- | --- |
| props | <p>received from App.jsx</p> |

<a name="SignIn"></a>

## SignIn
<p>Component that display log-in form in order to take username and password.</p>

**Kind**: global class  
<a name="new_SignIn_new"></a>

### new SignIn(props)

| Param | Description |
| --- | --- |
| props | <p>props from parent component</p> |

<a name="SignOut"></a>

## SignOut
<p>Component that nullify user information and redirect a user to main page after log-out.</p>

**Kind**: global class  
<a name="Index"></a>

## Index() ⇒ <code>\*</code>
<p>Component that give a placeholder for dashboard for user who is not administrator.</p>

**Kind**: global function  
<a name="Checkbox"></a>

## Checkbox(id, label, isSelected, onCheckboxChange) ⇒ <code>\*</code>
<p>Component that holds a list of individual ContentListElement components.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| id | <p>an id number for each permission</p> |
| label | <p>a string value for description of each permission</p> |
| isSelected | <p>a boolean value to check whether a checkbox is selected or not.</p> |
| onCheckboxChange | <p>a listener method to handle with DOM event.</p> |

<a name="ReadList"></a>

## ReadList() ⇒ <code>\*</code>
<p>Component that display outer HTML container for user list table.</p>

**Kind**: global function  
<a name="Table"></a>

## Table(props) ⇒ <code>\*</code>
<p>Component that holds a table for user list.
It contains user create, delete and modify functions.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| props | <p>props from parent component</p> |

<a name="MapBox"></a>

## MapBox() ⇒
<p>Main map container. Holds Openlayers map canvas, every component that is to be displayed on the map as well as all
states relevant to multiple components. Passes callback functions down to children components to control shared
state. Tracks and applies filter options.</p>

**Kind**: global function  
**Returns**: <p>The Openlayers canvas and parent for all components displayed on the map.</p>  
**Params**: props props from MapWindow including locked state when using generated link and the list of published stories received from the server  
<a name="ProtectedRoute"></a>

## ProtectedRoute(props, isAllowed, alternative) ⇒ <code>\*</code>
<p>Component that perform conditional routing.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| props | <p>props from parent component</p> |
| isAllowed | <p>a boolean value. 'false' redirect to alternative page</p> |
| alternative | <p>an alternative address when isAllowed value is false (unauthorized access)</p> |

<a name="archiveContentElement"></a>

## archiveContentElement(element, contentID, mapPointID, id, action) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Archives or unarchives a single ContentElement on the server.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| element | <p>the element to be archived</p> |
| contentID | <p>the containing Content contentID</p> |
| mapPointID | <p>the containing MapPoint mapPointID</p> |
| id | <p>the id of the ContentElement to be archived</p> |
| action | <p>either 'archive' or 'unarchive'</p> |

<a name="createMapPointRequest"></a>

## createMapPointRequest(storyID, zoomLevel, coordinates) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a server request to create a new MapPoint.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| storyID | <p>the storyID of the containing Story.</p> |
| zoomLevel | <p>the current zoom level of the MapPoint to be created.</p> |
| coordinates | <p>the latitude and longitude of the MapPoint</p> |

<a name="createStory"></a>

## createStory(creator) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to create a new Story with the minimum number of attributes.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| creator | <p>the user id of the user that is creating this Story</p> |

<a name="createTag"></a>

## createTag(contentID, tagDesc) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to create a new Content Tag.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| contentID | <p>the contentID of the containing Content object</p> |
| tagDesc | <p>A custom description for the new Tag</p> |

<a name="deleteContentElement"></a>

## deleteContentElement(elementID, mapPointID, contentID) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to soft-delete a single ContentElement</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| elementID | <p>the id of the ContentElement to be deleted</p> |
| mapPointID | <p>the id of the MapPoint containing the ContentElement</p> |
| contentID | <p>the id of the Story containing this ContentElement</p> |

<a name="deleteMapPoint"></a>

## deleteMapPoint(contentID, mapPointID) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to soft-delete a single MapPoint.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| contentID | <p>the contentID of the containing Story.</p> |
| mapPointID | <p>the mapPointID of the MapPoint to be deleted.</p> |

<a name="deleteStory"></a>

## deleteStory(id) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to soft-delete a single Story object</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| id | <p>the id of the Story to be deleted</p> |

<a name="getAllTags"></a>

## getAllTags() ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to get every available Tag.</p>

**Kind**: global function  
<a name="getArchivedStoryList"></a>

## getArchivedStoryList() ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to get a list of every Story that has a state of ARCHIVED.</p>

**Kind**: global function  
<a name="getContentElementByID"></a>

## getContentElementByID(id) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to get a single ContentElement by its ID.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| id | <p>the id of the ContentElement to retrieve.</p> |

<a name="getMapPointByID"></a>

## getMapPointByID(id) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to get a single MapPoint by its ID.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| id | <p>the id of the MapPoint to retrieve</p> |

<a name="getMapPointsByStoryID"></a>

## getMapPointsByStoryID(id) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to get every MapPoint for a Story.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| id | <p>the id of the Story to retrieve for</p> |

<a name="getStory"></a>

## getStory(id) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to get a single Story object by its ID.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| id | <p>the id of the Story to retrieve.</p> |

<a name="getStoryList"></a>

## getStoryList() ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to get an array of every Story in the system.</p>

**Kind**: global function  
<a name="removeTagFromStory"></a>

## removeTagFromStory(contentID, tag) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to remove a single Tag object from a Story.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| contentID | <p>the contentID of the Story that the Tag will be removed from</p> |
| tag | <p>the Tag to be removed from the containing Story</p> |

<a name="saveContentElement"></a>

## saveContentElement(contentElement, contentID, mapPointID) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Saves a single ContentElement to the server.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| contentElement | <p>the JSON object of the ContentElement to save</p> |
| contentID | <p>the containing contentID</p> |
| mapPointID | <p>the containing mapPointID</p> |

<a name="saveImageContentElement"></a>

## saveImageContentElement(contentElement, contentID, mapPointID) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to save an Image object.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| contentElement | <p>the Image object to save</p> |
| contentID | <p>the contentID of the Content containing this Image</p> |
| mapPointID | <p>the mapPointID of the MapPoint containing this Image</p> |

<a name="saveMapPoint"></a>

## saveMapPoint(mapPoint) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to save a MapPoint object.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| mapPoint | <p>the MapPoint object to save</p> |

<a name="saveStory"></a>

## saveStory(story) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends as request to the server to save a single edited Story object.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| story | <p>the Story object to save</p> |

<a name="saveTagToStory"></a>

## saveTagToStory(contentID, tag) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to save a Tag object to the current Story being edited.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| contentID | <p>the contentID of the Story to save to</p> |
| tag | <p>the Tag object to save</p> |

<a name="saveTextContentElement"></a>

## saveTextContentElement(contentElement, contentID, mapPointID) ⇒ <code>Promise.&lt;AxiosResponse.&lt;T&gt;&gt;</code>
<p>Sends a request to the server to save a Text ContentElement.</p>

**Kind**: global function  

| Param | Description |
| --- | --- |
| contentElement | <p>the Text element to save</p> |
| contentID | <p>the contentID of the containing Content object</p> |
| mapPointID | <p>the mapPointID of the containing MapPoint</p> |

<a name="tryTokenRefresh"></a>

## tryTokenRefresh()
<p>Sends a request to the server to refresh the JWT of the current authenticated user.</p>

**Kind**: global function  
