import React, {Component} from "react";
import axios from "axios";
import {Redirect} from "react-router-dom";
import {connect} from "react-redux";
// Every files under actions directory are working as libraries. (They are not components)
import * as authenticationActions from "../../actions/authentication"

/**
 * Component that nullify user information and redirect a user to main page after log-out.
 * @returns {*}
 */

class SignOut extends Component {

    handleSubmit = () => {
        this.sendRequest();
    };

    sendRequest = () => {

        const url = "http://localhost:8080/api/auth/logout";

        const config = {
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
            }
        };

        axios.get(
            url,
            config
        ).then((result) => {
            if(result.status === 200) {
                // Sign Out successfully
                // Clean localStorage in client's browser
                localStorage.removeItem("accessToken");
                localStorage.removeItem("tokenType");
                // Set isSignIn to 'false' in Redux store
                this.props.handleAuthentication(false);
                // Purge state in Redux store
                this.props.handleDeAuthentication();
                // // Nullify User information in Redux store
                // this.props.handleUserId(null);
                // // this.props.handleCreator(null);
                // this.props.handleDefaultLanguage(null);
                // this.props.handleFirstName(null);
                // this.props.handleLastName(null);
                // this.props.handlePhoneNumber(null);
                // this.props.handlePhotoPath(null);
                // // this.props.handleCreationDate(null);
                // this.props.handleUserDelete(null);
                // this.props.handleEnabled(null);
                // this.props.handleAuthorities([]);
                // this.props.handleUsername(null);
                // this.props.handleEmail(null);
                // this.props.handleAccountNonExpired(null);
                // this.props.handleCredentialsNonExpired(null);
                // this.props.handleAccountNonLocked(null);
            } else {
                // Sign Out failed
                // Lack of knowledge of sign-out failed cases
            }
        });
    };

    componentDidMount() {
        this.handleSubmit();
    };

    render() {
        return <Redirect to="/map" />
    };

}

// Redux settings below
// Before you edit this section, Please refer to connect() method of react-redux
const mapStateToProps = (state) => {
    return {
        isSignIn: state.authenticationReducer.isSignIn,
        // user_id: state.authenticationReducer.user_id,
        // // user_creator: state.authenticationReducer.user_creator,
        // user_defaultLanguage: state.authenticationReducer.user_defaultLanguage,
        // user_firstName: state.authenticationReducer.user_firstName,
        // user_lastName: state.authenticationReducer.user_lastName,
        // user_phoneNumber: state.authenticationReducer.user_phoneNumber,
        // user_photoPath: state.authenticationReducer.user_photoPath,
        // // user_creationDate: state.authenticationReducer.user_creationData,
        // user_userDelete: state.authenticationReducer.user_userDelete,
        // user_enabled: state.authenticationReducer.user_enabled,
        // user_authorities: state.authenticationReducer.user_authorities,
        // user_username: state.authenticationReducer.user_username,
        // user_email: state.authenticationReducer.user_email,
        // user_accountNonExpired: state.authenticationReducer.user_accountNonExpired,
        // user_credentialNonExpired: state.authenticationReducer.user_credentialNonExpired,
        // user_accountNonLocked: state.authenticationReducer.user_accountNonLocked,
    };
};

const mapDispatchProps = (dispatch) => {
    return {
        handleAuthentication: (isSignIn) => {dispatch(authenticationActions.authenticationAction(isSignIn))},
        handleDeAuthentication: () => {dispatch(authenticationActions.deauthenticationAction())},
        // handleUserId: (user_id) => {dispatch(authenticationActions.userIdAction(user_id))},
        // // handleCreator: (user_creator) => {dispatch(authenticationActions.creatorAction(user_creator))},
        // handleDefaultLanguage: (user_defaultLanguage) => {dispatch(authenticationActions.defaultLanguageAction(user_defaultLanguage))},
        // handleFirstName: (user_firstName) => {dispatch(authenticationActions.firstNameAction(user_firstName))},
        // handleLastName: (user_lastName) => {dispatch(authenticationActions.lastNameAction(user_lastName))},
        // handlePhoneNumber: (user_phoneNumber) => {dispatch(authenticationActions.phoneNumberAction(user_phoneNumber))},
        // handlePhotoPath: (user_photoPath) => {dispatch(authenticationActions.photoPathAction(user_photoPath))},
        // // handleCreationDate: (user_creationDate) => {dispatch(authenticationActions.creationDateAction(user_creationDate))},
        // handleUserDelete: (user_userDelete) => {dispatch(authenticationActions.userDeleteAction(user_userDelete))},
        // handleEnabled: (user_enabled) => {dispatch(authenticationActions.enabledAction(user_enabled))},
        // handleAuthorities: (user_authorities) => {dispatch(authenticationActions.authritiesAction(user_authorities))},
        // handleUsername: (user_username) => {dispatch(authenticationActions.usernameAction(user_username))},
        // handleEmail: (user_email) => {dispatch(authenticationActions.emailAction(user_email))},
        // handleAccountNonExpired: (user_accountNonExpired) => {dispatch(authenticationActions.accountNonExpiredAction(user_accountNonExpired))},
        // handleCredentialsNonExpired: (user_credentailNonExpired) => {dispatch(authenticationActions.credentialsNonExpiredAction(user_credentailNonExpired))},
        // handleAccountNonLocked: (user_accountNonLocked) => {dispatch(authenticationActions.accountNonLockedAction(user_accountNonLocked))}
    };
};

SignOut = connect(mapStateToProps, mapDispatchProps)(SignOut);

export default SignOut;