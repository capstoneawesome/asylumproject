import React from "react";
// import {Redirect, Route, Link, Switch, BrowserRouter} from "react-router-dom";
import Dashboard from "../Dashboard";
import * as authenticationActions from "../../actions/authentication";
import {connect} from "react-redux";
import "./css/account.scss";

/**
 * Component that give a placeholder for dashboard for user who is not administrator.
 * @returns {*}
 */

function Index() {

    return (
        <Dashboard/>
        // <>
        //     <div className="hero is-fullheight">
        //         <div className="hero-body asp-hero-body">
        //
        //             <div className="columns">
        //                 <div className="column is-3 ">
        //                     <aside className="menu is-hidden-mobile">
        //                         <p className="menu-label">
        //                             Account
        //                         </p>
        //                         <ul className="menu-list">
        //                             <Link to="/account">Dashboard</Link>
        //                             <Link to="/map">Map</Link>
        //                         </ul>
        //
        //                         {
        //                             props.user_authorities.includes("ROLE_SYSTEM_ADMIN")
        //                                 ?
        //                                     (
        //                                         <>
        //                                             <br/>
        //                                             <br/>
        //                                             <p className="menu-label">
        //                                                 Administration
        //                                             </p>
        //                                             <ul className="menu-list">
        //                                             <li>
        //                                             <Link to="/admin/adminboard">Adminboard</Link>
        //                                             </li>
        //                                             </ul>
        //                                         </>
        //                                     )
        //                                 :
        //                                     (
        //                                         <h2>You're not sysadmin</h2>
        //                                     )
        //                         }
        //                     </aside>
        //                 </div>
        //
        //                 <div className="column is-9">
        //
        //                     <Switch>
        //                         <Redirect exact from="/account" to="/account/dashboard" />
        //
        //                         <Route path="/account/dashboard" component={Dashboard} />
        //                     </Switch>
        //
        //                 </div>
        //
        //             </div>
        //         </div>
        //     </div>
        // </>
    );

}

// Redux settings below
// Before you edit this section, Please refer to connect() method of react-redux
const mapStateToProps = (state) => {
    return {
        isSignIn: state.authenticationReducer.isSignIn,
        user_authorities: state.authenticationReducer.user_authorities,
    };
};

const mapDispatchProps = (dispatch) => {
    // return bindActionCreator(actions, dispatch);
    return {
        handleAuthentication: (isSignIn) => {dispatch(authenticationActions.authenticationAction(isSignIn))},
        handleAuthorities: (user_authorities) => {dispatch(authenticationActions.authoritiesAction(user_authorities))},
    };
};

Index = connect(mapStateToProps, mapDispatchProps)(Index);

export default Index;