import React, {useState} from "react";
import * as authenticationActions from "../../../actions/authentication";
import {connect} from "react-redux";
import {Redirect} from "react-router";

// Redux settings below
// Before you edit this section, Please refer to connect() method of react-redux
const mapStateToProps = (state) => {
    return {
        isSignIn: state.authenticationReducer.isSignIn,
        user_id: state.authenticationReducer.user_id,
        user_defaultLanguage: state.authenticationReducer.user_defaultLanguage,
        user_firstName: state.authenticationReducer.user_firstName,
        user_lastName: state.authenticationReducer.user_lastName,
        user_authorities: state.authenticationReducer.user_authorities,
        user_username: state.authenticationReducer.user_username,
        user_email: state.authenticationReducer.user_email,
        user_photoPath: state.authenticationReducer.user_photoPath,
    };
};

const mapDispatchProps = (dispatch) => {
    // return bindActionCreator(actions, dispatch);
    return {
        handleAuthentication: (isSignIn) => {dispatch(authenticationActions.authenticationAction(isSignIn))},
        handleUserId: (user_id) => {dispatch(authenticationActions.userIdAction(user_id))},
        handleDefaultLanguage: (user_defaultLanguage) => {dispatch(authenticationActions.defaultLanguageAction(user_defaultLanguage))},
        handleFirstName: (user_firstName) => {dispatch(authenticationActions.firstNameAction(user_firstName))},
        handleLastName: (user_lastName) => {dispatch(authenticationActions.lastNameAction(user_lastName))},
        handlePhotoPath: (user_photoPath) => {dispatch(authenticationActions.photoPathAction(user_photoPath))},
        handleAuthorities: (user_authorities) => {dispatch(authenticationActions.authoritiesAction(user_authorities))},
        handleUsername: (user_username) => {dispatch(authenticationActions.usernameAction(user_username))},
        handleEmail: (user_email) => {dispatch(authenticationActions.emailAction(user_email))},
    };
};

Dashboard = connect(mapStateToProps, mapDispatchProps)(Dashboard);
// Redux settings end

export default function Dashboard(props) {

    // const [route, setRoute] = useState(null);
    //
    // const handleSignOut = () => {
    //     setRoute("/signout");
    // };

    return (
        // route === null
        //     ?
        //         <>
        //             <div id="signout" onClick={handleSignOut}>Signout <i className="fas fa-sign-out-alt" /></div>
        //             <h1>Hi {props.user_username}</h1>
        //             <h2>Your information below</h2>
        //             <div>
        //                 {props.user_id}
        //             </div>
        //             <div>
        //                 {props.user_email}
        //             </div>
        //             <div>
        //                 {props.user_firstName}
        //             </div>
        //             <div>
        //                 {props.user_lastName}
        //             </div>
        //             <div>
        //                 {props.user_authorities}
        //             </div>
        //             <div>
        //                 {props.user_photoPath}
        //             </div>
        //         </>
        //     :
        //         <Redirect to={route} />
        <></>
    );

};

