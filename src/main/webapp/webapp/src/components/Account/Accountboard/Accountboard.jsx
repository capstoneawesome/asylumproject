import React, {Component, useState} from "react";
import Avatar from "react-avatar";
import * as authenticationActions from "../../../actions/authentication";
import {connect} from "react-redux";
import ISO6391 from "iso-639-1";
import ISO6393 from "iso-639-3";

// Redux settings below
// Before you edit this section, Please refer to connect() method of react-redux
const mapStateToProps = (state) => {
    return {
        isSignIn: state.authenticationReducer.isSignIn,
        user_defaultLanguage: state.authenticationReducer.user_defaultLanguage,
        user_firstName: state.authenticationReducer.user_firstName,
        user_lastName: state.authenticationReducer.user_lastName,
        user_authorities: state.authenticationReducer.user_authorities,
        user_username: state.authenticationReducer.user_username,
        user_email: state.authenticationReducer.user_email,
        user_photoPath: state.authenticationReducer.user_photoPath,
    };
};

const mapDispatchProps = (dispatch) => {
    // return bindActionCreator(actions, dispatch);
    return {
        handleAuthentication: (isSignIn) => {dispatch(authenticationActions.authenticationAction(isSignIn))},
        handleDefaultLanguage: (user_defaultLanguage) => {dispatch(authenticationActions.defaultLanguageAction(user_defaultLanguage))},
        handleFirstName: (user_firstName) => {dispatch(authenticationActions.firstNameAction(user_firstName))},
        handleLastName: (user_lastName) => {dispatch(authenticationActions.lastNameAction(user_lastName))},
        handlePhotoPath: (user_photoPath) => {dispatch(authenticationActions.photoPathAction(user_photoPath))},
        handleAuthorities: (user_authorities) => {dispatch(authenticationActions.authoritiesAction(user_authorities))},
        handleUsername: (user_username) => {dispatch(authenticationActions.usernameAction(user_username))},
        handleEmail: (user_email) => {dispatch(authenticationActions.emailAction(user_email))},
    };
};

Accountboard = connect(mapStateToProps, mapDispatchProps)(Accountboard);
// Redux settings end

/**
 * Component that display personal user information and profile.
 * @param props props from parent component
 * @returns {*}
 */

export default function Accountboard(props) {

    const [defaultLanguage, setDefaultLanguage] = useState("en");

    const handleDefaultLanguage = (e) => {
        setDefaultLanguage({
            defaultLanguage: e.target.value,
        });
    };

    const handleLanguageSelectionMount = () => {
        return (
            ISO6391.getAllCodes().map((languageCode, index) => {
                return <option key={index} value={languageCode}>{ISO6391.getName(languageCode)}</option>;
            })
        );
    };

    return (
        <>
            <div className="hero-body">
                <section className="asp-profile section is-main-section">
                    <div className="tile is-ancestor">
                        <div className="tile is-parent">
                            <div className="card tile is-child">
                                <header className="card-header">
                                    <p className="card-header-title">
                                        <span className="icon"><i className="fas fa-user-circle"/></span>
                                        Edit Profile
                                    </p>
                                </header>
                                <div className="card-content">
                                    <form>
                                        <div className="field is-horizontal">
                                            <div className="field-label is-normal">
                                                <label className="label">Avatar</label>
                                            </div>
                                            <div className="field-body">
                                                <div className="field">
                                                    <div className="field file">
                                                        <label className="upload control">
                                                            <a className="button is-primary">
                                                                <span className="icon"><i className="fas fa-upload"/></span>
                                                                <span>Pick a file</span>
                                                            </a>
                                                            <input type="file" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div className="field is-horizontal">
                                            <div className="field-label is-normal">
                                                <label className="label">First Name</label>
                                            </div>
                                            <div className="field-body">
                                                <div className="field">
                                                    <div className="control">
                                                        <input type="text" autoComplete="on" name="firstname" defaultValue="" placeholder={props.user_firstName} className="input" required="" />
                                                    </div>
                                                    <p className="help">Required. Your first name</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="field is-horizontal">
                                            <div className="field-label is-normal">
                                                <label className="label">Last Name</label>
                                            </div>
                                            <div className="field-body">
                                                <div className="field">
                                                    <div className="control">
                                                        <input type="text" autoComplete="on" name="lastname" defaultValue="" placeholder={props.user_lastName} className="input" required="" />
                                                    </div>
                                                    <p className="help">Required. Your last name</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="field is-horizontal">
                                            <div className="field-label is-normal">
                                                <label className="label">Language</label>
                                            </div>
                                            <div className="field-body">
                                                <div className="field">
                                                    <div className="control">
                                                        <select name="language" value={defaultLanguage} onChange={handleDefaultLanguage}>
                                                            {handleLanguageSelectionMount()}
                                                        </select>
                                                    </div>
                                                    <p className="help">Required. Your default language</p>
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div className="field is-horizontal">
                                            <div className="field-label is-normal"/>
                                            <div className="field-body">
                                                <div className="field">
                                                    <div className="control">
                                                        <button type="submit" className="button is-primary">
                                                            Submit
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="tile is-parent">
                            <div className="card tile is-child">
                                <header className="card-header">
                                    <p className="card-header-title">
                                        <span className="icon"><i className="fas fa-id-badge"/></span>
                                        Profile
                                    </p>
                                </header>
                                <div className="card-content">
                                    <div className="is-user-avatar image has-max-width is-aligned-center">
                                        <div className="columns">
                                            <div className="asp-profile-avatar column">
                                                <Avatar name={props.user_firstName + " " + props.user_lastName} />
                                            </div>
                                            <div className="column">
                                                <div className="field">
                                                    <label className="label">Username</label>
                                                    <div className="control is-clearfix">
                                                        <input type="text" readOnly="" defaultValue={props.user_username} className="input is-static" />
                                                    </div>
                                                </div>
                                                <hr/>
                                                <div className="field">
                                                    <label className="label">E-mail</label>
                                                    <div className="control is-clearfix">
                                                        <input type="text" readOnly="" defaultValue={props.user_email} className="input is-static" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div className="field">
                                        <label className="label">Full Name</label>
                                        <div className="control is-clearfix">
                                            <input type="text" readOnly="" defaultValue={props.user_firstName + " " + props.user_lastName} className="input is-static" />
                                        </div>
                                    </div>
                                    <div className="field">
                                        <label className="label">Default Language</label>
                                        <div className="control is-clearfix">
                                            <input type="text" readOnly="" defaultValue={ISO6391.getName(props.user_defaultLanguage.toLowerCase())} className="input is-static" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <header className="card-header">
                            <p className="card-header-title">
                                <span className="icon"><i className="fas fa-unlock-alt"/></span>
                                Change Password
                            </p>
                        </header>
                        <div className="card-content">
                            <form>
                                <div className="field is-horizontal">
                                    <div className="field-label is-normal">
                                        <label className="label">Current password</label>
                                    </div>
                                    <div className="field-body">
                                        <div className="field">
                                            <div className="control">
                                                <input type="password" name="password_current"
                                                       autoComplete="current-password" className="input"
                                                       required="" />
                                            </div>
                                            <p className="help">Required. Your current password</p></div>
                                    </div>
                                </div>
                                <hr/>
                                    <div className="field is-horizontal">
                                        <div className="field-label is-normal">
                                            <label className="label">New password</label>
                                        </div>
                                        <div className="field-body">
                                            <div className="field">
                                                <div className="control">
                                                    <input type="password" autoComplete="new-password"
                                                           name="password" className="input" required="" />
                                                </div>
                                                <p className="help">Required. New password</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="field is-horizontal">
                                        <div className="field-label is-normal">
                                            <label className="label">Confirm password</label>
                                        </div>
                                        <div className="field-body">
                                            <div className="field">
                                                <div className="control">
                                                    <input type="password" autoComplete="new-password"
                                                           name="password_confirmation" className="input"
                                                           required="" />
                                                </div>
                                                <p className="help">Required. New password one more time</p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                        <div className="field is-horizontal">
                                            <div className="field-label is-normal"></div>
                                            <div className="field-body">
                                                <div className="field">
                                                    <div className="control">
                                                        <button type="submit" className="button is-primary">
                                                            Submit
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </>
    );

};

