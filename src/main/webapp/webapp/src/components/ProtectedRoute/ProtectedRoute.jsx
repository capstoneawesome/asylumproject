import React from "react";
import {Redirect, Route} from "react-router-dom";

/**
 * Component that perform conditional routing.
 * @param props props from parent component
 * @param isAllowed a boolean value. 'false' redirect to alternative page
 * @param alternative an alternative address when isAllowed value is false (unauthorized access)
 * @returns {*}
 */

const ProtectedRoute = ({ isAllowed, alternative, ...props }) => {
    return (
        isAllowed
            ? <Route {...props}/>
            : <Redirect to={alternative} />
    )
};

export default ProtectedRoute;