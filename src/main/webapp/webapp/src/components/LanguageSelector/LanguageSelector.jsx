import React, {Component} from "react";

class LanguageSelector extends Component {

    render() {
        return (
            <>
                <div className="select is-danger">
                    <select>
                        <option value="en">English</option>
                        <option value="ko">Korean</option>
                        <option value="es">Spanish</option>
                        <option value="tr">Turkish</option>
                    </select>
                </div>
            </>
        );
    }

}

export default LanguageSelector;