import React, {useState, useEffect, useRef} from 'react';
import {animated, useSpring, config} from 'react-spring';
import axios from 'axios';
import "./controls.scss";

/**
 * Control allowing user to generate a link which leads to a version of the map which does not contain any stories with
 * select content ratings. Intended for use by parents and teachers to control access to disturbing content.
 * @params props props from MapBox
 * @returns An animated control which generates and copies the link
 */

export default function(props){

    const linkRef = useRef();

    const url = "http://localhost:8080/stories/filtered";

    const [link, setLink] = useState("");
    const [loading, setLoading] = useState(true);

    const icon = useSpring({
        opacity: (props.linkOpen && !props.isOpen) ? 1 : 0,
        width: (props.linkOpen && !props.isOpen) ? "290px" : "0px"
    });

    const getIdList = () => {
        const idList = props.ratingList.map(rating => {
            return rating.tagId;
        });
        return idList;
    };

    const fetchLink = (idList) => {
        if(props.linkOpen && !props.isOpen){
            let config = {
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
                }
            };

            axios.post(url, idList, config)
                .then((response) => {
                    console.log(response);
                    if(response != undefined){
                        setLink(response.data.shortUrl);
                        setLoading(false);
                    }
                }, (error) => {
                    console.log(error);
                });
        }
    };

    const handleCopy = (e) => {
        const element = linkRef.current;
        element.select();
        document.execCommand("copy");
    };

    const loadingButton = <button className="button is-loading is-info" onClick={(e) => handleCopy(e)}></button>;

    const copyButton = <button className="button is-success">
                            <span className="icon">
                                <i className="fas fa-copy" ></i>
                            </span>
                        </button>;

    useEffect(() => {
        const idList = getIdList();
        fetchLink(idList);
    }, []);

    useEffect(() => {
        const idList = getIdList();
        fetchLink(idList);
    }, [props.linkOpen]);

    return(
        <animated.div className="link-box" style={icon}>
            <article className="message is-small is-success">
                <div className="message-header">
                    <p>Rated Content Link</p>
                    <button className="delete" aria-label="delete" onClick={props.closeLink}></button>
                </div>
                <div className="message-body">
                    <p className="label is-small">Use this link to visit the site with selected rated content removed: </p>
                    <nav className="level">
                        <div className="field has-addons">
                            <textarea ref={linkRef} className="textarea is-info is-small has-fixed-size" readOnly rows="1" value={link} >
                                {link}
                            </textarea>
                            {loading ? loadingButton : copyButton}
                        </div>
                    </nav>
                </div>
            </article>
        </animated.div>
    );

}