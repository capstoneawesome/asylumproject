import React, {useState, useEffect} from 'react';
import {animated, useSpring, config} from 'react-spring';
import "./controls.scss";

/**
* Animated display on map for required attributions including openlayers, OpenStreetMap contributors, and Stamen map design.
* @returns An animated info element containing attribution links
*/

export default function AttributionControl(props){

    const [open, setOpen] = useState(true);

     const icon = useSpring({
            opacity: open ? 1 : 0,
            width: open ? "430px" : "0px"
        });

    const toggleOpen = () => {

        setOpen(!open);
    };

    return(
        <div className="attribution-tag">
            <div className="tags has-addons">
            <button className="button is-info is-small" style={{zIndex: 5}} onClick={toggleOpen}>
                                 <span className="icon" >
                                     <i className="fas fa-info" ></i>
                                </span>
            </button>

              <animated.span style={icon} className="tag is-light is-info">
                   <p> Map tiles by &nbsp;<a href="https://stamen.com/" target="_blank">Stamen Design</a>, under &nbsp;
                   <a href="https://creativecommons.org/licenses/by/3.0/" target="_blank">CC BY 3.0</a>. ©
                   <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>  contributors.</p>
              </animated.span>

            </div>
        </div>
    );

}