import React, {useState, useEffect} from 'react';
import "./controls.scss";

/**
 * Control for adding content rating filters. Generates a list of tags containing rating objects and updates ratingList
 * in MapBox via callback. Disabled when MapWindow lock state is true.
 * @params props props from MapWindow, MapBox, and StoryFilterControl
 * @returns A select input and editable list of tags representing ratingFilter list
 */

export default function RatingFilter(props) {


    const [tags, setTags] = useState([]);
    const [options, setOptions] = useState();
    const [selectedRating, setSelectedRating] = useState("");

    const convertToObject = (name) => {
        let obj = {tagId: 0, tag: "unknown"};
        props.ratingList.map((rating) => {
            if(rating.tag === name){
                obj = rating;
            }
        });
        return obj;
    };

    const handleAddRating = () => {
        if(!props.locked){
            let result = props.ratingFilter.filter(rating => (rating.tag === selectedRating));

            if(result.length == 0){
                props.setRatingFilter([...props.ratingFilter, convertToObject(selectedRating)]);
            }
        }
    };

    const handleSelect = (target) => {
        setSelectedRating(target.value);
    };

    const buildTags = () => {
        let newTags = [];
        let name = "";
        let element;

        props.ratingFilter.map(rating => {
            element = <span key={rating.tagId} className="tag is-info">
                            <p>{rating.tag}</p>
                            <button className="delete is-small"
                                    value={rating.tag}
                                    onClick={(e) => handleRemoveTag(e.target.value)}>
                            </button>
                        </span>;
            newTags.push(element);
        });
        setTags(newTags);
    };

    const handleRemoveTag = (ratingName) => {
        if(!props.locked){
            let newRatings = [];

            props.ratingFilter.map((rating) => {
                if(!(rating.tag === ratingName)){
                    newRatings.push(rating);
                }
            });
            props.setRatingFilter(newRatings);
            buildTags();
        }

    };

    useEffect(() => {
        buildTags();

    }, [props.ratingFilter]);

    useEffect(() => {
        buildTags();
        props.setRatingFilter([...props.ratingFilter]);

    }, []);

    useEffect(() => {
        let ratingNames = [];
        let newOptions = [];
        let ratingName = "";
        props.ratingList.map((rating) => {

            ratingNames.push(rating.tag);
        });
        const uniqueNames = [...new Set(ratingNames)].sort();
        uniqueNames.map(rating => {
            newOptions.push(<option key={rating}>{rating}</option>);
        });
        setSelectedRating(uniqueNames[0]);
        setOptions(newOptions);

    }, [props.ratingList]);

    return(
        <div className="rating-filter">
            <p className="label is-small">Hide stories with rated content:</p>
            <div className="columns" id="rating-select">
                <div className="column is-paddingless-horizontal is-marginless-horizontal">
                    <select className="select is-small" onChange={(e) => handleSelect(e.target)}>
                        {options}
                    </select>
                </div>
                <div className="column  is-paddingless-horizontal is-marginless-horizontal">
                    <button className="button is-success is-small" onClick={handleAddRating}>Add</button>
                </div>
            </div>
            <div className="columns" id="rating-tags">
                <div className="column is-paddingless-horizontal is-marginless-horizontal">
                    <div className="tags">
                        {tags}
                    </div>
                </div>
            </div>
            <nav className="level">
                <div className="level-item">

                </div>
                <div className="level-item">
                    <button className="button is-small is-rounded is-outlined is-success" onClick={props.generateLink}>
                        Generate rated content link
                    </button>
                </div>
            </nav>
        </div>
    );
}