import React, {useEffect, useRef} from 'react';
import Control from "ol/control/Control";
import "./controls.scss";
import Zoom from "ol/control/Zoom"

/**
 * Custom control which integrates with Openlayers map api to control map zoom level.
 * @params props props from MapWindow and MapBox
 * @returns zoom control buttons that will appear on the map
 */

export default function ZoomControl(props){

   const mapObject = props.mapObject;
   const view = mapObject.getView();

   const zoomControlRef = useRef(null);

   const zoomOut = () => {
        view.animate({zoom: view.getZoom() - 1});
   };

    const zoomIn = () => {
        view.animate({zoom: view.getZoom() + 1});
    };



   useEffect(() =>{
           const zoomControl = new Zoom();
           mapObject.addControl(zoomControl);
   }, []
   );


   return(
     <div ref={zoomControlRef}>

     </div>
   );

}