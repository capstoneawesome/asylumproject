import React, {useState, useEffect} from 'react';
import "./controls.scss";
import iso3166 from "iso-3166-2/iso3166.min";

/**
 * Control for adding country filters. Generates an editable list of tags containing countries the user wants to include
 * and updates countryFilter in MapBox via callback if tags change. Default list is [].
 * @params props props from MapBox, and StoryFilterControl
 * @returns A select input and editable list of tags representing countryFilter list
 */

export default function CountryFilter(props) {


    const [tags, setTags] = useState([]);
    const [options, setOptions] = useState();
    const [selectedCountry, setSelectedCountry] = useState("");

    const handleAddCountry = () => {
        const code = iso3166.country(selectedCountry).code;

        if(!props.countryFilter.includes(code)){
            props.changeCountries([...props.countryFilter, code]);
        }
    };

    const handleSelect = (target) => {
        setSelectedCountry(target.value);
    };

    const buildTags = () => {
        let newTags = [];
        let name = "";
        let element;

        props.countryFilter.map(code => {
            name = iso3166.country(code).name;
            //alert(name);
            element = <span key={name} className="tag is-info">
                            <p>{name}</p>
                            <button className="delete is-small"
                                value={name}
                                onClick={(e) => handleRemoveTag(e.target.value)}>
                            </button>
                        </span>;
            newTags.push(element);
        });
        setTags(newTags);
    };

    const handleRemoveTag = (name) => {
        let newCountries = [];
        const code = iso3166.country(name).code;
        props.countryFilter.map((country) => {
            if(!(country === code)){
                newCountries.push(country);
            }
        });
        props.changeCountries(newCountries);
        buildTags();
    };

    useEffect(() => {
        buildTags();
        //alert("countryFilter");
    }, [props.countryFilter]);

    useEffect(() => {
        buildTags();
        props.changeCountries([...props.countryFilter]);
        //alert("countryFilter");
    }, []);

    useEffect(() => {
        let countryNames = [];
        let newOptions = [];
        let countryName = "";
        props.countryList.map((country) => {
            countryName = iso3166.country(country).name;
            countryNames.push(countryName);
        });
        const uniqueNames = [...new Set(countryNames)].sort();
        uniqueNames.map(countryName => {
            newOptions.push(<option key={countryName}>{countryName}</option>);
        });
        setSelectedCountry(uniqueNames[0]);
        setOptions(newOptions);
        //alert("countryList");
    }, [props.countryList]);

    return(
        <div className="country-filter">
            <p className="label is-small">Show stories from country:</p>
            <div className="columns" id="country-select">
                <div className="column is-paddingless-horizontal is-marginless-horizontal">
                    <select className="select is-small" onChange={(e) => handleSelect(e.target)}>
                        {options}
                    </select>
                </div>
                <div className="column  is-paddingless-horizontal is-marginless-horizontal">
                    <button className="button is-success is-small" onClick={handleAddCountry}>Add</button>
                </div>
            </div>
            <div className="columns" id="country-tags">
                <div className="column is-paddingless-horizontal is-marginless-horizontal">
                    <div className="tags">
                        {tags}
                    </div>
                </div>
            </div>
        </div>
    );
}