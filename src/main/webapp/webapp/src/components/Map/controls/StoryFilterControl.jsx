import React, {useState, useEffect} from 'react';
import {animated, useSpring, config} from 'react-spring';
import bulmaAccordion from 'bulma-extensions/bulma-accordion/dist/js/bulma-accordion.min.js';
import iconShadow from "../icons/filter_control_shadow.svg";
import iconShadowHover from "../icons/filter_control_shadow_hover.svg";
import "./controls.scss";
import LanguageFilter from "./LanguageFilter";
import CountryFilter from "./CountryFilter";
import RatingFilter from "./RatingFilter";

/**
 * Parent element for all filter controls. Represents an accordion element containing the various filter controls.
 * Only visible when a story is not open.
 * @params props props from MapBox including all filter lists
 * @returns An accordion element overlayed on the map containing all filter controls
 */

export default function StoryFilterControl(props) {

    let accordions;
    const [filterIcon, setFilterIcon] = useState(iconShadowHover);

    const controlAnimation = useSpring({
        opacity: (!props.isOpen && props.expanded) ? 1 : 0,
        transform: (!props.isOpen && props.expanded) ? "scale(1)" : "scale(0)",
        config: config.stiff

    });

    const icon = useSpring({
        opacity: props.isOpen ? 0 : 1,
        transform: props.isOpen ? "scale(0)" : "scale(1)"
    });

    useEffect(() => {
        accordions = bulmaAccordion.attach();

    },[]);



    const toggleHover = () => {
        if(filterIcon == iconShadow) {
            setFilterIcon(iconShadowHover);
        }
        else {
            setFilterIcon(iconShadow);
        }
    };

    return (
        <div className="filter-control ">
            <animated.div style={icon} className="filter-icon">
                <img className="exit-button" src={filterIcon} onMouseEnter={toggleHover} onMouseLeave={toggleHover} onClick={() => props.toggleExpand()}>

                </img>
            </animated.div>
            <animated.div style={controlAnimation} className="filter-accordion" >
                <section className="accordions" >
                    <article className="accordion is-active is-small ">

                        <div className="accordion-header toggle">
                            <p>Language</p>

                        </div>
                        <div className="accordion-body is-small">
                            <div className="accordion-content ">
                                <LanguageFilter langList={props.langList}
                                                langFilter={props.langFilter}
                                                changeLanguages={props.changeLanguages}
                                                setLangFilter={props.setLangFilter}
                                                buildLangList={props.buildLangList}>
                                </LanguageFilter>
                            </div>
                        </div>
                    </article>
                    <article className="accordion is-small">
                        <div className="accordion-header toggle">
                            <p>Country</p>
                        </div>
                        <div className="accordion-body">
                            <div className="accordion-content">
                                <CountryFilter  countryList={props.countryList}
                                                countryFilter={props.countryFilter}
                                                changeCountries={props.changeCountries}
                                                setCountryFilter={props.setCountryFilter}>
                                </CountryFilter>
                            </div>
                        </div>
                    </article>
                    <article className="accordion is-small">
                        <div className="accordion-header toggle">
                            <p>Rating</p>
                        </div>
                        <div className="accordion-body">
                            <div className="accordion-content">
                                <RatingFilter   locked={props.locked}
                                                ratingList={props.ratingList}
                                                ratingFilter={props.ratingFilter}
                                                setRatingFilter={props.setRatingFilter}
                                                generateLink={props.generateLink}
                                                locked={props.locked}>
                                </RatingFilter>
                            </div>
                        </div>
                    </article>
                </section>
            </animated.div>
        </div>
    );

}