import React, {useState, useEffect, useRef} from 'react';
import Control from "ol/control/Control";
import "./controls.scss";
import FullScreen from "ol/control/FullScreen";

/**
 * React wrapper around Openlayers control which enters and exits map fullscreen mode. Hooks into html5 fullscreen api call.
 * @params props props from MapWindow including the map object
 * @returns A reactDom div reference to be used by the Openlayer constructor to add control
 */

export default function FullscreenControl(props){

    const mapObject = props.mapObject;

    const fullscreenRef = useRef(null);

    useEffect(() =>{
            const fullscreenControl = new FullScreen();
            mapObject.addControl(fullscreenControl);
        }, []
    );


    return(
        <div ref={fullscreenRef} className="fullscreen-control-box">

        </div>
    );

}