import React, {useState, useEffect} from 'react';
import "./controls.scss";

/**
 * Control for adding language filters. Generates an editable list of tags containing languages the user wants to see and updates
 * languageList in MapBox via callback if tags change. Default list is ['eng'].
 * @params props props from MapBox, and StoryFilterControl
 * @returns A select input and editable list of tags representing languageFilter list
 */

export default function LanguageFilter(props) {

    const iso6393 = require('iso-639-3');
    const iso6393To1 = require('iso-639-3/to-1');

    const convert1To3 = (code) => {
        const converted = Object.keys(iso6393To1).find(key => iso6393To1[key] === code);
        if(converted == undefined) {
            return code;
        }
        return converted;
    };

    const codeToName = (code) => {

            const convertedCode = convert1To3(code);
            const result = iso6393.find(element => element.iso6393 === convertedCode);
            if(result === undefined) {
                return "Undetermined";
            }
            return result.name;
    };

    const nameToCode = (name) => {
        const result = iso6393.find(element => element.name === name);
        if(result === undefined) {
            return "Undetermined";
        }
        return result.iso6393;
    };

    const [tags, setTags] = useState([]);
    const [options, setOptions] = useState();
    const [selectedLang, setSelectedLang] = useState("eng");

    const handleAddLang = () => {
        const code = nameToCode(selectedLang);
        //props.changeLanguages([...props.langFilter]);
        if(!props.langFilter.includes(code)){
            props.setLangFilter([...props.langFilter, code]);
        }
        buildTags();

    };

    const handleSelect = (target) => {
        setSelectedLang(target.value);
    };

    const buildTags = () => {
        let newTags = [];
        let name = "";
        let element;

        props.langFilter.map(code => {
            name = codeToName(code);
            element = <span key={name} className="tag is-info is-small">
                            {name}
                <button className="delete is-small"
                        value={name}
                        onClick={(e) => handleRemoveTag(e.target.value)}>
                            </button>
                        </span>;
            newTags.push(element);
        });
        //props.changeLanguages([...props.langFilter]);
        setTags(newTags);
    };

    const handleRemoveTag = (name) => {
        let newLangs = [];
        const code = nameToCode(name);
        props.langFilter.map((lang) => {
            if(!(lang === code)){
                newLangs.push(lang);
            }
        });
        props.setLangFilter(newLangs);
        buildTags();
        //forceRerender(!reRender);
    };

    useEffect(() => {
        buildTags();
        props.setLangFilter([...props.langFilter]);
        //alert("langFilter");
    }, []);

    useEffect(() => {
       buildTags();
       props.buildLangList();
       //alert("langFilter");
    }, [props.langFilter]);

    useEffect(() => {
        let langNames = [];
        let newOptions = [];
        let langName = "";
        props.langList.map((lang) => {
            langName = codeToName(lang);
            langNames.push(langName);
        });
        const uniqueNames = [...new Set(langNames)].sort();
        uniqueNames.map(langName => {
            newOptions.push(<option key={langName}>{langName}</option>);
            });
        setSelectedLang(uniqueNames[0]);
        setOptions(newOptions);
        //alert("langList");
    }, []);

    return(
        <div className="language-filter">
            <p className="label is-small">Show stories with language:</p>
            <div className="columns is-paddingless-bottom is-marginless-bottom" id="language-select">
                <div className="column is-paddingless-horizontal is-marginless-horizontal is-marginless-bottom">
                    <select className="select is-small" onChange={(e) => handleSelect(e.target)}>
                        {options}
                    </select>
                </div>
                <div className="column  is-paddingless-horizontal is-paddingless-bottom is-marginless-horizontal">
                    <button className="button is-success is-small" onClick={() => handleAddLang()}>Add</button>
                </div>
            </div>
            <div className="columns is-paddingless-top is-marginless-top" id="language-tags">
                <div className="column is-paddingless-horizontal is-marginless-horizontal is-paddingless-top is-marginless-top">
                    <div className="tags">
                        {tags}
                    </div>
                </div>
            </div>
        </div>
    );
}


