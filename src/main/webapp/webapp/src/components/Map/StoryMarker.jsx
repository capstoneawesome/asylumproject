import React, {useState, useEffect, useRef} from 'react';
import {fromLonLat} from "ol/proj";
import Overlay from "ol/Overlay";
import "./map.scss";
import marker from "./icons/map_marker_shadow.svg";
import {animated, useTransition, config} from "react-spring";

/**
 * React wrapper for Openlayers Overlay object representing a story on the map. Hovering over marker opens the expanded
 * version of the marker, with story info. Clicking expanded marker opens the corresponding story. One markeris
 * generated for each story in the filtered story list.
 * @params props props from MapBox
 * @returns A marker and expanded marker representing a story on the map. Also used to open story for viewing.
 */

export default function StoryMarker(props) {

    const [expanded, setExpanded] = useState(false);

    const markerRef = useRef(null);

    const markerTransition = useTransition(expanded, null, {
        from: {transform: 'scale(0)', opacity: 0, zIndex: 10},
        enter: {transform: 'scale(1)', opacity: 1, zIndex: 10},
        leave: {transform: 'scale(0)', opacity: 0, zIndex: 10},
        config: config.stiff
    });

    const iso6393 = require('iso-639-3');
    const iso6393To1 = require('iso-639-3/to-1');

    const convert1To3 = (code) => {
        const converted = Object.keys(iso6393To1).find(key => iso6393To1[key] === code);
        if(converted == undefined) {
            return code;
        }
        return converted;
    };

    const codeToName = (code) => {

        const convertedCode = convert1To3(code);
        const result = iso6393.find(element => element.iso6393 === convertedCode);
        if(result === undefined) {
            return "Undetermined";
        }
        return result.name;
    };

    const nameToCode = (name) => {
        const result = iso6393.find(element => element.name === name);
        if(result === undefined) {
            return "Undetermined";
        }
        return result.iso6393;
    };

    const getLanguageNames = () => {
        let languageNames = props.story.languages.map((lang) => {
            return codeToName(convert1To3(lang.code)) + "  ";
        });
        return languageNames;
    };

    useEffect(() => {

            let location = [];
            if(props.story.mapPoints.length == 0){
                location = [78, -79];
                //alert(location);
            }
            else{
                location = props.story.mapPoints[0].coordinates;
                //alert(location);
            }

            const markerOverlay = new Overlay({
            id: "test",
            element: markerRef.current,
            position: fromLonLat(location),
            stopEvent: true,
            className: "story-marker",
            positioning: "bottom-center"
        });
        //alert(markerOverlay.element);
        props.mapObject.addOverlay(markerOverlay);
        return(() => {props.mapObject.removeOverlay(markerOverlay)});
        }, []
    );

    const toggleExpand = () => {
        setExpanded(expanded => !expanded);
    };

    const handleStorySelect = () => {
        toggleExpand();
        props.openStory(props.story.contentID);
    };

    if(props.open){
        return (<div ref={markerRef}></div>);
    }
    else {
        return (

            <div ref={markerRef}>
                <div className="story-marker" onMouseEnter={toggleExpand}>
                    <img className="marker-svg" src={marker} ></img>
                </div>
                {
                    markerTransition.map(({item, key, props: transition}) =>
                        item && (
                            <animated.div key={key} style={transition} className="expanded-marker-card"
                                          onMouseLeave={toggleExpand} onClick={handleStorySelect}>
                                <div className="columns is-gapless is-inline-flex">

                                    <div className="column" style={{zIndex: 5}}>
                                        <div className="story-marker-message">
                                            <div className="message-header">
                                                <p className="label is-medium is-white">{props.story.title}</p>
                                            </div>

                                            <div className="message-body">
                                                <p className="label is-4 is-info is-marginless is-paddingless">{props.story.asylumSeekerName}</p>
                                                <p className="label is-4 is-info is-marginless is-paddingless"><em className="country-label has-text-info is-4 is-info is-marginless is-paddingless">{props.story.countryFull}</em></p>
                                                <b className="label is-4 is-marginless is-paddingless">Languages:</b> <em className="language-list has-text-info is-4 is-small is-marginless is-paddingless">{getLanguageNames()}</em>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </animated.div>))
                    }

            </div>);

    }
}