import React, {useState, useEffect} from 'react';
import AudioPlayer from 'react-h5-audio-player';
import './storyview.scss';

/**
 * Audio player for audio element at current map point and language. Does not load if no audio element found for that map point and
 * language. Uses react-h5-audio-player library.
 * @params props props from MapBox
 * @returns An audio player for audio element at current map point and language
 */

export default function AudioContent(props) {

    const iso6393To1 = require('iso-639-3/to-1');

    const convert1To3 = (code) => {
        const converted = Object.keys(iso6393To1).find(key => iso6393To1[key] === code);
        if(converted == undefined) {
            return code;
        }
        return converted;
    }

    const getLang = () => {
        const converted = convert1To3(props.storyLanguage);
        return converted;
    };

    const getAudio = () => {
        let audio = [];
        const elements = props.openedStory.mapPoints[props.mapPointIndex].contentElement;
        const lang = getLang();
        const contentID = props.openedStory.contentID;
        const mapPointID = props.openedStory.mapPoints[props.mapPointIndex].id;

        elements.map((element) => {
            const converted = convert1To3(element.language);
            if(element.fileType === 'audio' && converted === lang) {
                const found = "/api/content/" + contentID + "/mapPoints/" + mapPointID + "/audio/" + element.filePath;
                audio.push(found);
            }
        });
        if(audio.length > 1){
            props.setAudio(audio[0]);
        }
        else{
            props.setAudio(audio);
        }
    };

    useEffect(() => {
        getAudio();
    }, []);

    useEffect(() => {
        getAudio();
    }, [props.mapPointIndex]);

    useEffect(() => {
        getAudio();
    }, [props.storyLanguage]);

    if(props.audio.length == 0){
        return(
            null
        );
    }
    else{
        return(
            <div className="audio-box">
                <AudioPlayer
                    src={props.audio}
                    onPlay={e => console.log("onPlay")}
                    layout={"horizontal-reverse"}
                    showJumpControls={false}
                />
            </div>

        );
    }
}