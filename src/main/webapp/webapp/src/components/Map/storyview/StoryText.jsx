import React, {useState, useEffect, useRef} from 'react';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import "./storyview.scss";

/**
 * Text container for representing text elements in the current map point and language. Concatenates multiple elements if found.
 * @params props props from MapBox,StoryBox
 * @returns A container that parses and displays text elements for the current map point and language
 */

export default function StoryText(props) {

    const [text, setText] = useState([]);

    const iso6393 = require('iso-639-3');
    const iso6393To1 = require('iso-639-3/to-1');

    const convert1To3 = (code) => {
        const converted = Object.keys(iso6393To1).find(key => iso6393To1[key] === code);
        if(converted == undefined) {
            return code;
        }
        return converted;
    }

    const getLang = () => {
        //alert(props.storyLanguage);
        const converted = convert1To3(props.storyLanguage);
        return converted;
    };

    const getText = () => {
        let text = [];

        const elements = props.openedStory.mapPoints[props.mapPointIndex].contentElement;
        const lang = getLang();
        //alert(lang);
        elements.map((element) => {
            const converted = convert1To3(element.language);
            if(element.fileType === 'text' && converted === lang) {
                const html = ReactHtmlParser(element.description);
                text.push(html);
            }
        });
        return text;

    };

    useEffect(() => {
        setText(getText());
    }, [props.mapPointIndex]);

    useEffect(() => {
        setText(getText());
    }, [props.storyLanguage]);

    return(
        <div className="story-text-area">
            {text}
        </div>

    );

}