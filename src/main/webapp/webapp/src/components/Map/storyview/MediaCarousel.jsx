
/*
MIT License for video-react component

Copyright (c) 2016 video-react

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */


import React, {useState, useEffect, useRef} from 'react';
import {animated, useSpring, config} from 'react-spring';
import { Player, ControlBar, LoadingSpinner, BigPlayButton } from 'video-react';
import "./storyview.scss";
import Carousel from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';

/**
 * Collapsible element which displays image and video elements for the current map point in a looping carousel.
 * Adapts the react-carousel component from @brainhubeu/react-carousel and video-react library
 * @params props props from MapBox
 * @returns A collapsible media carousel to display images and videos
 */

export default function MediaCarousel(props) {

    const carouselRef = useRef(null);
    const player = useRef(null);
    const [elements, setElements] = useState([]);

    const mediaAnimation = useSpring({
        opacity: (props.isOpen && props.expanded) ? 1 : 0,
        transform: (props.isOpen && props.expanded) ? "scale(1)" : "scale(0)"
    });

    const icon = useSpring({
        opacity: (props.isOpen && elements.length > 0) ? 1 : 0,
        transform: (props.isOpen && elements.length > 0) ? "scale(1)" : "scale(0)"
    });

    const getImages = () => {
        if(props.isOpen && props.openedStory.mapPoints.length > 0){
            const images = props.openedStory.mapPoints[props.mapPointIndex].contentElement.filter(element => {
                return element.fileType === "image" && !element.deleted;
            });
            if(images.length > 0){
                const contentID = props.openedStory.contentID;
                const mapPointID = props.openedStory.mapPoints[props.mapPointIndex].id;

                const urls = images.map(image => {

                    return "/api/content/" + contentID + "/mapPoints/" + mapPointID + "/images/" + image.filePath;

                });
                //setImageUrls(urls);
                return(urls);
            }
            else{
                //setImageUrls([""]);
                return([]);
            }
        }
        return([]);
    };

    const getVideos = () => {
        if(props.isOpen && props.openedStory.mapPoints.length > 0){
            const videos = props.openedStory.mapPoints[props.mapPointIndex].contentElement.filter(element => {
                return element.fileType === "video" && !element.deleted;
            });
            if(videos.length > 0){
                const contentID = props.openedStory.contentID;
                const mapPointID = props.openedStory.mapPoints[props.mapPointIndex].id;

                const urls = videos.map(video => {

                    return "/api/content/" + contentID + "/mapPoints/" + mapPointID + "/videos/" + video.filePath;

                });
                //setVideoUrls(urls);
                return(urls);
            }
            else{
                //setVideoUrls([""]);
                return([]);
            }
        }
        return([]);
    };

    const buildItems = (imageUrls, videoUrls) => {
        let count = 0;
        const elements = imageUrls.map(url => {
            //alert(url);
            return (    <div className="carousel-item" >
                            <figure className="image is-5by3">
                                <img src={url} />
                                <figcaption></figcaption>
                            </figure>
                        </div>

                    );
        });

        const videoItems = videoUrls.map(url => {
            elements.push(<div className="carousel-item">
                        <Player ref={player} src={url} >
                            <LoadingSpinner></LoadingSpinner>
                            <ControlBar autoHide={false} className="my-class"></ControlBar>
                            <BigPlayButton position="center" />
                        </Player>
                    </div>);
        });
        //alert(imageItems);



        //setImageElements(imageItems);
        //setVideoElements(videoItems);
        setElements(elements);
    };

    useEffect(() => {
        //bulmaCarousel.attach(carouselRef.current, {loop: true});
        const images = getImages();
        const videos = getVideos();
        buildItems(images, videos);
    }, [props.isOpen]);

    useEffect(() => {
        //bulmaCarousel.attach(carouselRef.current, {loop: true});
        props.setPlayerFromRef(player);
    }, [props.isOpen]);

    useEffect(() => {
        //bulmaCarousel.attach(carouselRef.current, {loop: true});
        const images = getImages();
        const videos = getVideos();
        buildItems(images, videos);

    }, [props.mapPointIndex]);

        return (
            <div>
                <animated.div style={mediaAnimation} ref={carouselRef} id="image-content" className="carousel" >
                    <Carousel className="carousel-component" arrows slides={elements} infinite>
                    </Carousel>
                </animated.div>
                <animated.div style={icon} className="media-marker" >
                    <img className="camera" src={props.button} onClick={() => {
                        //player.current.pause();
                        props.toggleExpand();
                    }}></img>
                </animated.div>
            </div>
        );
}