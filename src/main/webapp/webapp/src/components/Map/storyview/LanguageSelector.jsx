import React, {useState} from 'react';
import "./storyview.scss";

/**
 * Selector which sets the display language for the story. Contains available language options for the story. Text and
 * audio elements are automatically updated to the new language if a matching element exists.
 * @params props props from MapBox
 * @returns A language selector which controls story display language
 */

export default function LanguageSelector(props) {

    const iso6393 = require('iso-639-3');
    const iso6393To1 = require('iso-639-3/to-1');

    const buildLangList = () => {
        let codes = props.languages.map(obj => {
            return obj.code;
        });
        let langs = [];
        let options = [];
        let convertedCode = "";

        codes.map((code) => {
            convertedCode = convert1To3(code);
            const result = iso6393.find(element => element.iso6393 === convertedCode);
            if(result === undefined) {
                langs.push("Undetermined");
            }
            else {
                langs.push(result.name);
            }

        });
        langs.map((lang) => {

            options.push(<option key={lang}>{lang}</option>);


        });

        return options;
    };

    function convert1To3(code) {
        const converted = Object.keys(iso6393To1).find(key => iso6393To1[key] === code);
        if(converted == undefined) {
            return code;
        }
        return converted;
    }

    const getCode = (name) => {
        const result = iso6393.find(element => element.name === name);
        return result.iso6393;
    };

    return (
        <div className="select is-small is-rounded">
            <select defaultValue="English" onChange={(evt) => props.setStoryLanguage(getCode(evt.target.value))}>
                {buildLangList()}

            </select>
        </div>
    );

}