import React, {useState, useEffect, useRef} from 'react';
import {animated, useTransition, config} from 'react-spring';
import box from "../icons/story_box_shadow_down.svg";
import "./storyview.scss";
import Control from "ol/control/Control";
import LanguageSelector from "./LanguageSelector.jsx";
import CloseButton from "./CloseButton.jsx";
import StoryText from "./StoryText.jsx";
import Timeline from "./Timeline.jsx";
import AudioContent from "./AudioContent.jsx";

/**
 * Element containing story info, language selector, text elements, and audio element for the current story and map
 * point.
 * @params props props from MapBox
 * @returns A container that displays story info, language selector, text elements, and audio element for the current
 * story and map
 */

export default function StoryBox(props) {

    const [isLoading, setIsLoading] = useState(false);
    const [audio, setAudio] = useState([]);

    const storyboxTransition = useTransition(props.open, null, {
            from: {transform: 'translate3d(100%, 100%, 0)', opacity: 0, display: 'none'},
            enter: {transform: 'translate3d(0, 0, 0)', opacity: 1, display: 'block'},
            leave: {transform: 'translate3d(100%, 100%, 0)', opacity: 0},
            config: config.stiff
        });

    const calcTitleSize = () => {
        if(props.openedStory.title.length > 11) {
            return "is-4";
        }
        return "is-3";
    };

     return (
        storyboxTransition.map(({item, key, props: transition}) =>
                                item && (
            <animated.div key={key} style={transition} className="story-box" >
                    <img className="back-shape" src={box}></img>
                    <CloseButton closeStory={props.closeStory} prev={props.prev}></CloseButton>



                    <div className="content">
                        <p className={"title is-white " + calcTitleSize()}>{props.openedStory.title}</p>
                        <p className="subtitle">Language: </p>
                    </div>
                    <LanguageSelector languages={props.openedStory.languages} setStoryLanguage={props.setStoryLanguage}></LanguageSelector>
                    <div className="story-body" style={{height: (audio.length == 0) ? "34%" : "30%"}}>
                        <Timeline openedStory={props.openedStory} mapPointIndex={props.mapPointIndex} movePoint={props.movePoint}></Timeline>
                        <StoryText openedStory={props.openedStory} mapPointIndex={props.mapPointIndex} storyLanguage={props.storyLanguage}></StoryText>
                        <AudioContent audio={audio} setAudio={setAudio} openedStory={props.openedStory} mapPointIndex={props.mapPointIndex} storyLanguage={props.storyLanguage}></AudioContent>

                    </div>
            </animated.div>))
           );

}