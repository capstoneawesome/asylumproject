import React, {useState} from 'react';
import "./storyview.scss";
import exit from "../icons/exit_story.svg";
import exitH from "../icons/exit_story_hover1.svg";

/**
 * Custom svg element which closes the story and returns user to regular map view.
 * @params props props from MapBox (callback function to close story)
 * @returns A close button for the story
 */

export default function CloseButton(props) {

    const [exitButton, setExitButton] = useState(exit);

    const hoverExit = () => {
        if(exitButton == exit) {
            setExitButton(exitH);
        }
        else {
            setExitButton(exit);
        }
    };

    return(
        <img className="exit-button" src={exitButton} onMouseEnter={hoverExit} onMouseLeave={hoverExit} onClick={() => props.closeStory()}></img>
    );
}