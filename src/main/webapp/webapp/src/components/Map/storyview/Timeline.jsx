import React, {useState, useEffect, useRef} from 'react';
import "./storyview.scss";

/**
 * Element of the StoryBox representing map points in the story. Used to navigate from map point to map point and
 * displays which map point the user is on. Modified from bulma-steps in the bulma-extensions package by Wikiki.
 * @params props props from MapBox
 * @returns mapPoints timeline for navigating the map points of the current story
 */

export default function Timeline(props) {

    const [nodes, setNodes] = useState([]);

    const buildPoints = () => {
        const mapPoints = props.openedStory.mapPoints;
        let nodes = [];
        let index = 0;
        let cName;
        mapPoints.map((point) => {
                if(index == props.mapPointIndex){
                    cName = "step-item is-completed is-info";
                }
                else {
                    cName = "step-item";
                }
                nodes.push(
                    <li key={index} className={cName}>
                        <div className="step-marker is-danger" data-index={index} onClick={(evt) => props.movePoint(evt.target.dataset.index)}>

                        </div>
                    </li>
                );
                index++;
            });
        return nodes;
    };

    useEffect(() => {
        setNodes(buildPoints());
    }, [props.mapPointIndex]);

    return(
        <div>

            <ul className="steps is-small">
                {nodes}
            </ul>
        </div>

    );
}