import React, {useState, useEffect} from 'react';
import {animated, useSpring, config} from 'react-spring';
import bulmaQuickview from 'bulma-extensions/bulma-quickview/dist/js/bulma-quickview.min.js';
import "./storyview.scss";

/**
 * Catalogue view of published stories. Stories showing on the map are each represented in the catalogue.
 * Story can be opened from the catalogue item. Adapted from QuickView in bulma-extensions package.
 * @params props props from MapBox
 * @returns A collapsible story catalogue containing every currently visible story
 */

export default function StoryCatalogue(props){

    const [open, setOpen] = useState(false);
    const [listItems, setListItems] = useState([]);

    const buttonAnimation = useSpring({
        opacity: props.storyOpen ? 0 : 1,
        transform: props.storyOpen ? "scale(0)" : "scale(1)",
        config: config.stiff
    });

    const iso6393 = require('iso-639-3');
    const iso6393To1 = require('iso-639-3/to-1');

    const convert1To3 = (code) => {
        const converted = Object.keys(iso6393To1).find(key => iso6393To1[key] === code);
        if(converted == undefined) {
            return code;
        }
        return converted;
    };

    const codeToName = (code) => {

        const convertedCode = convert1To3(code);
        const result = iso6393.find(element => element.iso6393 === convertedCode);
        if(result === undefined) {
            return "Undetermined";
        }
        return result.name;
    };

    const nameToCode = (name) => {
        const result = iso6393.find(element => element.name === name);
        if(result === undefined) {
            return "Undetermined";
        }
        return result.iso6393;
    };

    const convertLanguages = (langs) => {

        let converted = langs.map((lang) => {
            return codeToName(lang.code);
        });
        const length = converted.length;
        if(length != 0){
            let formatted = converted.map((lang) => {
                return <small>{lang + "  "}</small>
            });
            return formatted;
        }
        else{
            return converted;
        }

    };

    const handleOpenStory = (target) => {
        props.openStory(parseInt(target.dataset.contentid));
    };

    const buildItems = () => {
        let items = [];
        let languages = [];
        props.stories.map((story) => {
            languages = convertLanguages(story.languages);
            items.push(
                <article key={story.name} className="media">

                    <div className="media-content">
                        <div className="content">

                                <h6 className="label is-medium">{story.title}</h6> <p className="label">{story.countryFull}</p>

                                <b>Languages: </b>{languages}


                        </div>

                    </div>
                    <div className="media-right">
                        <span className="icon is-medium"><i className="fas fa-compass" data-contentid={story.contentID} onClick={(e) => handleOpenStory(e.target)}></i></span>
                    </div>
                </article>
            );
        });
        setListItems(items);
    };

    const handleOpen = () => {
        setOpen(true);
        //alert(open);
    };

    const handleClose = () => {
        setOpen(false);
        //alert(open);
    };

    useEffect(() => {
        let quickviews = bulmaQuickview.attach();
    });
    useEffect(() => {
        buildItems();
    }, [props.stories]);

    return(
        <>
        <animated.div id="quickviewDefault" className="quickview" style={buttonAnimation}>
            <header className="quickview-header">
                <h2 className="title is-2">Story List</h2>
                <span className="delete" data-dismiss="quickview" onClick={handleClose}></span>
            </header>

            <div className="quickview-body">

                <div className="quickview-block">
                    <nav className="level">

                    </nav>
                    <div className="content">
                        {listItems}
                    </div>
                </div>
            </div>


        </animated.div>
            <animated.button className="button is-info is-small" id="open-catalogue" style={buttonAnimation} data-dismiss={open ? "quickview" : ""} data-show={open ? "" : "quickview"} data-target="quickviewDefault" onClick={open ? handleClose : handleOpen}>Story List
            </animated.button>
            </>
    );
}