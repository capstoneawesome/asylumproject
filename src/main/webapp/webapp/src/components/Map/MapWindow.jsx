/*
Openlayers Map API licensed under BSD 2-Clause

BSD 2-Clause License

Copyright 2005-present, OpenLayers Contributors All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Parent window for the map. Entry point when routed to the /map url. Handles api calls to get content from server.
 * Sets lock on content rating filter if navigated to using generated link.
 * @returns Parent window for the map and all its components
 */

import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import {defaults} from 'ol/control';
import XYZ from "ol/source/XYZ";
import {fromLonLat} from "ol/proj";
import * as Geocoder from 'ol-geocoder';
import 'ol-geocoder/dist/ol-geocoder.css';
import './map.scss';
import 'ol/ol.css';
import "./controls/controls.scss";
import Attribution from "ol/control/Attribution";
import axios from 'axios';
import Stargate from "../Stargate/Stargate"
import {useParams, useLocation} from "react-router";

import React, {useState, useEffect} from 'react';
import MapBox from "./MapBox";

function MapWindow(props) {

    const center = fromLonLat([0,0]);
    const zoom = 3;
    let location = useLocation();

    const getLockStatus = () => {
        if(props.filterString == undefined || props.filterString == null || props.filterString.length == 0){

            return false;
        }
        else{

            if (props.filterString.startsWith("?share") && props.filterString.substring(7) !== '') {
                return true;
            }else{
                return false;
            }
        }
    };

    const [locked, setLocked] = useState(getLockStatus());

    const [stories, setStories] = useState([]);

    const [isLoading, setIsLoading] = useState(true);
    const [isError, setIsError] = useState(false);

    let attribution = new Attribution({collapsible: false});

    const map = new Map({
        layers: [
            new TileLayer({
                source: new XYZ({
                    url: 'http://tile.stamen.com/terrain/{z}/{x}/{y}.jpg',
                    wrapX: true
                })
            })],
        target: null,
        view: new View({
            center: center,
            zoom: zoom,
            extent: [-20037508.3427892, -20037508.3427892, 20037508.3427892, 20037508.3427892],
            constrainOnlyCenter: false,
            smoothExtentConstraint: true
        }),
        controls: defaults({
            zoom: false,
            attribution : false,
            rotate : false,
            //attributionOptions: {collapsible: true, collapsed: true, label: "test attribution", collapsedLabel: "collapsed?"}
        }).extend(attribution)

    });

    const geocoder = new Geocoder('nominatim', {
        provider: 'osm',
        autoComplete: true,

        lang: 'en',
        placeholder: 'Go to location . . .',
        limit: 5,
        keepOpen: true,
        preventDefault: true
    });

    geocoder.on('addresschosen', function(e){
        const coord = e.coordinate;

        map.getView().animate({center: coord, duration: 4000});
        map.getView().animate({zoom: 3, duration: 2000}, {zoom: 9, duration: 2000});
    });

    const toggleLoading = () => {
        setIsLoading(!isLoading);
    };

    const fetchStories = () => {

        //***********************************************************************************************
        // Dummy data for offline testing
        //***********************************************************************************************
        let allStories = [{contentID: 1, name: "Mary", title: "Mary's Story", contentElements: null, mapPoints: [{contentElement: [{id: 1, language: 'en', description: <p>Miss Evelyn, Madam, from the second to the <em>eighteenth</em> year of her life, was brought up under my care, and, except when at school under my roof. I need not speak to your Ladyship of the virtues of that excellent young creature. She loved me as her father; nor was Mrs. Villars less valued by her; while to me she became so dear, that her loss was little less afflicting than that which I have since sustained of Mrs.Villars herself.</p>, length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'},
                    {id: 6, language: 'spa', description: <p>La señorita Evelyn, señora, desde el segundo hasta el decimoctavo año de su vida, fue criada bajo mi cuidado y, excepto cuando estaba en la escuela bajo mi techo. No necesito hablar con su señoría de las virtudes de esa excelente criatura joven. Ella me amaba como su padre; ni la señora Villars era menos valorada por ella; mientras que para mí se hizo tan querida, que su pérdida fue un poco menos dolorosa que la que desde entonces he sufrido de la Sra. Villars.</p>, length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                                                                                                                                coordinates: [-98.43613, 18.90815],
                                                                                                                                zoomLevel: 12,
                                                                                                                                story: 1},
                                                                                                                            {contentElement: [{id: 2, language: 'en', description: "At that period of her life we parted; her mother, then married to Monsieur Duval, sent for her to Paris. How often have I since regretted that I did not accompany her thither! Protected and supported by me, the misery and disgrace which awaited her might perhaps have been avoided. But, to be brief-Madame Duval, at the instigation of her husband, earnestly, or rather tyrannically, endeavoured to effect a union between Miss Evelyn and one of his nephews. And, when she found her power inadequate to her attempt, enraged at her non-compliance, she treated her with the grossest unkindness, and threatened her with poverty and ruin.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                                                                                                                                coordinates: [-103.41898, 25.54389],
                                                                                                                                zoomLevel: 13,
                                                                                                                                story: 1},
                                                                                                                            {contentElement: [{id: 3, language: 'en', description: "Some other gentlemen came in. The conversation concerning the person whose character Dr. Johnson had treated so slightingly, as he did not know his merit, was resumed. Mrs. Thrale said, 'You think so of him, Sir, because he is quiet, and does not exert himself with force. You'll be saying the same thing of Mr. ***** there, who sits as quiet--.' This was not well-bred; and Johnson did not let it pass without correction. 'Nay, Madam, what right have you to talk thus? Both Mr. ***** and I have reason to take it ill. You may talk so of Mr. *****; but why do you make me do it? Have I said anything against Mr. *****? You have set him, that I might shoot him: but I have not shot him.'", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                                                                                                                            coordinates: [-81.43613, 26.3],
                                                                                                                                zoomLevel: 10,
                                                                                                                            story: 1},
                                                                                                                            {contentElement: [{id: 4, language: 'en', description: "Mr. Thrale appeared very lethargick to-day. I saw him again on Monday evening, at which time he was not thought to be in immediate danger; but early in the morning of Wednesday, the 4th, he expired. Johnson was in the house, and thus mentions the event: 'I felt almost the last flutter of his pulse, and looked for the last time upon the face that for fifteen years had never been turned upon me but with respect and benignity.' Upon that day there was a Call of The LITERARY CLUB; but Johnson apologised for his absence by the following note:--", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                                                                                                                            coordinates: [-70.2, 11.6],
                                                                                                                                zoomLevel: 13,
                                                                                                                            story: 1},
                                                                                                                            {contentElement: [{id: 5, language: 'en', description: "Mr. Thrale's death was a very essential loss to Johnson, who, although he did not foresee all that afterwards happened, was sufficiently convinced that the comforts which Mr. Thrale's family afforded him, would now in a great measure cease. He, however, continued to shew a kind attention to his widow and children as long as it was acceptable; and he took upon him, with a very earnest concern, the office of one of his executors, the importance of which seemed greater than usual to him, from his circumstances having been always such, that he had scarcely any share in the real business of life. His friends of THE CLUB were in hopes that Mr. Thrale might have made a liberal provision for him for his life, which, as Mr. Thrale left no son, and a very large fortune, it would have been highly to his honour to have done; and, considering Dr. Johnson's age, could not have been of long duration; but he bequeathed him only two hundred pounds, which was the legacy given to each of his executors. I could not but be somewhat diverted by hearing Johnson talk in a pompous manner of his new office, and particularly of the concerns of the brewery, which it was at last resolved should be sold. Lord Lucan tells a very good story, which, if not precisely exact, is certainly characteristical: that when the sale of Thrale's brewery was going forward, Johnson appeared bustling about, with an ink-horn and pen in his button-hole, like an excise-man; and on being asked what he really considered to be the value of the property which was to be disposed of, answered, 'We are not here to sell a parcel of boilers and vats, but the potentiality of growing rich, beyond the dreams of avarice.'", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                                                                                                                            coordinates: [-74.43613, 40.90815],
                                                                                                                            zoomLevel: 10,
                                                                                                                                story: 1}],
                        position: [-98.43613, 18.90815], zoom: [12], languages: [{code: "eng"}, {code: "es"} , {code: "ncx"}], countryFull: "Mexico", countryOfOrigin: "MX", tags: [{tagId: 1392, tag: "Abuse(mental, emotional, verbal)"}, {tagId: 1403,tag: "Death or dying"}, {tagId: 1686, tag: "Profanity"}]},

            {contentID: 2, name: "Ronald", title: "Ronald's Story", mapPoints: [{contentElement: [{id: 1, language: 'en', description: "Miss Evelyn, Madam, from the second to the eighteenth year of her life, was brought up under my care, and, except when at school under my roof. I need not speak to your Ladyship of the virtues of that excellent young creature. She loved me as her father; nor was Mrs. Villars less valued by her; while to me she became so dear, that her loss was little less afflicting than that which I have since sustained of Mrs. Villars herself.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'},
                        {id: 6, language: 'spa', description: "La señorita Evelyn, señora, desde el segundo hasta el decimoctavo año de su vida, fue criada bajo mi cuidado y, excepto cuando estaba en la escuela bajo mi techo. No necesito hablar con su señoría de las virtudes de esa excelente criatura joven. Ella me amaba como su padre; ni la señora Villars era menos valorada por ella; mientras que para mí se hizo tan querida, que su pérdida fue un poco menos dolorosa que la que desde entonces he sufrido de la Sra. Villars.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                    coordinates: [-98.43613, 18.90815],
                    zoomLevel: 12,
                    story: 1},
                    {contentElement: [{id: 2, language: 'en', description: "At that period of her life we parted; her mother, then married to Monsieur Duval, sent for her to Paris. How often have I since regretted that I did not accompany her thither! Protected and supported by me, the misery and disgrace which awaited her might perhaps have been avoided. But, to be brief-Madame Duval, at the instigation of her husband, earnestly, or rather tyrannically, endeavoured to effect a union between Miss Evelyn and one of his nephews. And, when she found her power inadequate to her attempt, enraged at her non-compliance, she treated her with the grossest unkindness, and threatened her with poverty and ruin.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-103.41898, 25.54389],
                        zoomLevel: 13,
                        story: 1},
                    {contentElement: [{id: 3, language: 'en', description: "Some other gentlemen came in. The conversation concerning the person whose character Dr. Johnson had treated so slightingly, as he did not know his merit, was resumed. Mrs. Thrale said, 'You think so of him, Sir, because he is quiet, and does not exert himself with force. You'll be saying the same thing of Mr. ***** there, who sits as quiet--.' This was not well-bred; and Johnson did not let it pass without correction. 'Nay, Madam, what right have you to talk thus? Both Mr. ***** and I have reason to take it ill. You may talk so of Mr. *****; but why do you make me do it? Have I said anything against Mr. *****? You have set him, that I might shoot him: but I have not shot him.'", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-81.43613, 26.3],
                        zoomLevel: 10,
                        story: 1},
                    {contentElement: [{id: 4, language: 'en', description: "Mr. Thrale appeared very lethargick to-day. I saw him again on Monday evening, at which time he was not thought to be in immediate danger; but early in the morning of Wednesday, the 4th, he expired. Johnson was in the house, and thus mentions the event: 'I felt almost the last flutter of his pulse, and looked for the last time upon the face that for fifteen years had never been turned upon me but with respect and benignity.' Upon that day there was a Call of The LITERARY CLUB; but Johnson apologised for his absence by the following note:--", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-70.2, 11.6],
                        zoomLevel: 13,
                        story: 1},
                    {contentElement: [{id: 5, language: 'en', description: "Mr. Thrale's death was a very essential loss to Johnson, who, although he did not foresee all that afterwards happened, was sufficiently convinced that the comforts which Mr. Thrale's family afforded him, would now in a great measure cease. He, however, continued to shew a kind attention to his widow and children as long as it was acceptable; and he took upon him, with a very earnest concern, the office of one of his executors, the importance of which seemed greater than usual to him, from his circumstances having been always such, that he had scarcely any share in the real business of life. His friends of THE CLUB were in hopes that Mr. Thrale might have made a liberal provision for him for his life, which, as Mr. Thrale left no son, and a very large fortune, it would have been highly to his honour to have done; and, considering Dr. Johnson's age, could not have been of long duration; but he bequeathed him only two hundred pounds, which was the legacy given to each of his executors. I could not but be somewhat diverted by hearing Johnson talk in a pompous manner of his new office, and particularly of the concerns of the brewery, which it was at last resolved should be sold. Lord Lucan tells a very good story, which, if not precisely exact, is certainly characteristical: that when the sale of Thrale's brewery was going forward, Johnson appeared bustling about, with an ink-horn and pen in his button-hole, like an excise-man; and on being asked what he really considered to be the value of the property which was to be disposed of, answered, 'We are not here to sell a parcel of boilers and vats, but the potentiality of growing rich, beyond the dreams of avarice.'", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-74.43613, 40.90815],
                        zoomLevel: 10,
                        story: 1}],
                position: [-60.15, -1.5], zoom: [11], languages: [{code: "eng"}, {code: "por"}, {code: "yrl"}], countryFull: "Brazil", countryOfOrigin: "BR", tags: [{tagId: 1403,tag: "Death or dying"},{tagId: 1686, tag: "Profanity"}]},
            {contentID: 3, name: "Sami", title: "Sami's Story", mapPoints: [{contentElement: [{id: 1, language: 'en', description: "Miss Evelyn, Madam, from the second to the eighteenth year of her life, was brought up under my care, and, except when at school under my roof. I need not speak to your Ladyship of the virtues of that excellent young creature. She loved me as her father; nor was Mrs. Villars less valued by her; while to me she became so dear, that her loss was little less afflicting than that which I have since sustained of Mrs. Villars herself.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'},
                        {id: 6, language: 'spa', description: "La señorita Evelyn, señora, desde el segundo hasta el decimoctavo año de su vida, fue criada bajo mi cuidado y, excepto cuando estaba en la escuela bajo mi techo. No necesito hablar con su señoría de las virtudes de esa excelente criatura joven. Ella me amaba como su padre; ni la señora Villars era menos valorada por ella; mientras que para mí se hizo tan querida, que su pérdida fue un poco menos dolorosa que la que desde entonces he sufrido de la Sra. Villars.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                    coordinates: [-98.43613, 18.90815],
                    zoomLevel: 12,
                    story: 1},
                    {contentElement: [{id: 2, language: 'en', description: "At that period of her life we parted; her mother, then married to Monsieur Duval, sent for her to Paris. How often have I since regretted that I did not accompany her thither! Protected and supported by me, the misery and disgrace which awaited her might perhaps have been avoided. But, to be brief-Madame Duval, at the instigation of her husband, earnestly, or rather tyrannically, endeavoured to effect a union between Miss Evelyn and one of his nephews. And, when she found her power inadequate to her attempt, enraged at her non-compliance, she treated her with the grossest unkindness, and threatened her with poverty and ruin.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-103.41898, 25.54389],
                        zoomLevel: 13,
                        story: 1},
                    {contentElement: [{id: 3, language: 'en', description: "Some other gentlemen came in. The conversation concerning the person whose character Dr. Johnson had treated so slightingly, as he did not know his merit, was resumed. Mrs. Thrale said, 'You think so of him, Sir, because he is quiet, and does not exert himself with force. You'll be saying the same thing of Mr. ***** there, who sits as quiet--.' This was not well-bred; and Johnson did not let it pass without correction. 'Nay, Madam, what right have you to talk thus? Both Mr. ***** and I have reason to take it ill. You may talk so of Mr. *****; but why do you make me do it? Have I said anything against Mr. *****? You have set him, that I might shoot him: but I have not shot him.'", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-81.43613, 26.3],
                        zoomLevel: 10,
                        story: 1},
                    {contentElement: [{id: 4, language: 'en', description: "Mr. Thrale appeared very lethargick to-day. I saw him again on Monday evening, at which time he was not thought to be in immediate danger; but early in the morning of Wednesday, the 4th, he expired. Johnson was in the house, and thus mentions the event: 'I felt almost the last flutter of his pulse, and looked for the last time upon the face that for fifteen years had never been turned upon me but with respect and benignity.' Upon that day there was a Call of The LITERARY CLUB; but Johnson apologised for his absence by the following note:--", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-70.2, 11.6],
                        zoomLevel: 13,
                        story: 1},
                    {contentElement: [{id: 5, language: 'en', description: "Mr. Thrale's death was a very essential loss to Johnson, who, although he did not foresee all that afterwards happened, was sufficiently convinced that the comforts which Mr. Thrale's family afforded him, would now in a great measure cease. He, however, continued to shew a kind attention to his widow and children as long as it was acceptable; and he took upon him, with a very earnest concern, the office of one of his executors, the importance of which seemed greater than usual to him, from his circumstances having been always such, that he had scarcely any share in the real business of life. His friends of THE CLUB were in hopes that Mr. Thrale might have made a liberal provision for him for his life, which, as Mr. Thrale left no son, and a very large fortune, it would have been highly to his honour to have done; and, considering Dr. Johnson's age, could not have been of long duration; but he bequeathed him only two hundred pounds, which was the legacy given to each of his executors. I could not but be somewhat diverted by hearing Johnson talk in a pompous manner of his new office, and particularly of the concerns of the brewery, which it was at last resolved should be sold. Lord Lucan tells a very good story, which, if not precisely exact, is certainly characteristical: that when the sale of Thrale's brewery was going forward, Johnson appeared bustling about, with an ink-horn and pen in his button-hole, like an excise-man; and on being asked what he really considered to be the value of the property which was to be disposed of, answered, 'We are not here to sell a parcel of boilers and vats, but the potentiality of growing rich, beyond the dreams of avarice.'", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-74.43613, 40.90815],
                        zoomLevel: 10,
                        story: 1}],
                position: [36.06694, 32.73564], zoom: [12], languages: [{code: "eng"}, {code: "ara"}, {code: "apc"}], countryFull: "Syria", countryOfOrigin: "SY", tags: [{tagId: 1686, tag: "Profanity"}]},
            {contentID: 4, name: "Danylo", title: "Danylo's Story", mapPoints: [{contentElement: [{id: 1, language: 'en', description: "Miss Evelyn, Madam, from the second to the eighteenth year of her life, was brought up under my care, and, except when at school under my roof. I need not speak to your Ladyship of the virtues of that excellent young creature. She loved me as her father; nor was Mrs. Villars less valued by her; while to me she became so dear, that her loss was little less afflicting than that which I have since sustained of Mrs. Villars herself.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'},
                        {id: 6, language: 'spa', description: "La señorita Evelyn, señora, desde el segundo hasta el decimoctavo año de su vida, fue criada bajo mi cuidado y, excepto cuando estaba en la escuela bajo mi techo. No necesito hablar con su señoría de las virtudes de esa excelente criatura joven. Ella me amaba como su padre; ni la señora Villars era menos valorada por ella; mientras que para mí se hizo tan querida, que su pérdida fue un poco menos dolorosa que la que desde entonces he sufrido de la Sra. Villars.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                    coordinates: [-98.43613, 18.90815],
                    zoomLevel: 12,
                    story: 1},
                    {contentElement: [{id: 2, language: 'en', description: "At that period of her life we parted; her mother, then married to Monsieur Duval, sent for her to Paris. How often have I since regretted that I did not accompany her thither! Protected and supported by me, the misery and disgrace which awaited her might perhaps have been avoided. But, to be brief-Madame Duval, at the instigation of her husband, earnestly, or rather tyrannically, endeavoured to effect a union between Miss Evelyn and one of his nephews. And, when she found her power inadequate to her attempt, enraged at her non-compliance, she treated her with the grossest unkindness, and threatened her with poverty and ruin.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-103.41898, 25.54389],
                        zoomLevel: 13,
                        story: 1},
                    {contentElement: [{id: 3, language: 'en', description: "Some other gentlemen came in. The conversation concerning the person whose character Dr. Johnson had treated so slightingly, as he did not know his merit, was resumed. Mrs. Thrale said, 'You think so of him, Sir, because he is quiet, and does not exert himself with force. You'll be saying the same thing of Mr. ***** there, who sits as quiet--.' This was not well-bred; and Johnson did not let it pass without correction. 'Nay, Madam, what right have you to talk thus? Both Mr. ***** and I have reason to take it ill. You may talk so of Mr. *****; but why do you make me do it? Have I said anything against Mr. *****? You have set him, that I might shoot him: but I have not shot him.'", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-81.43613, 26.3],
                        zoomLevel: 10,
                        story: 1},
                    {contentElement: [{id: 4, language: 'en', description: "Mr. Thrale appeared very lethargick to-day. I saw him again on Monday evening, at which time he was not thought to be in immediate danger; but early in the morning of Wednesday, the 4th, he expired. Johnson was in the house, and thus mentions the event: 'I felt almost the last flutter of his pulse, and looked for the last time upon the face that for fifteen years had never been turned upon me but with respect and benignity.' Upon that day there was a Call of The LITERARY CLUB; but Johnson apologised for his absence by the following note:--", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-70.2, 11.6],
                        zoomLevel: 13,
                        story: 1},
                    {contentElement: [{id: 5, language: 'en', description: "Mr. Thrale's death was a very essential loss to Johnson, who, although he did not foresee all that afterwards happened, was sufficiently convinced that the comforts which Mr. Thrale's family afforded him, would now in a great measure cease. He, however, continued to shew a kind attention to his widow and children as long as it was acceptable; and he took upon him, with a very earnest concern, the office of one of his executors, the importance of which seemed greater than usual to him, from his circumstances having been always such, that he had scarcely any share in the real business of life. His friends of THE CLUB were in hopes that Mr. Thrale might have made a liberal provision for him for his life, which, as Mr. Thrale left no son, and a very large fortune, it would have been highly to his honour to have done; and, considering Dr. Johnson's age, could not have been of long duration; but he bequeathed him only two hundred pounds, which was the legacy given to each of his executors. I could not but be somewhat diverted by hearing Johnson talk in a pompous manner of his new office, and particularly of the concerns of the brewery, which it was at last resolved should be sold. Lord Lucan tells a very good story, which, if not precisely exact, is certainly characteristical: that when the sale of Thrale's brewery was going forward, Johnson appeared bustling about, with an ink-horn and pen in his button-hole, like an excise-man; and on being asked what he really considered to be the value of the property which was to be disposed of, answered, 'We are not here to sell a parcel of boilers and vats, but the potentiality of growing rich, beyond the dreams of avarice.'", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-74.43613, 40.90815],
                        zoomLevel: 10,
                        story: 1}],
                position: [55.44, -4.67], zoom: [13], languages: [{code: "eng"}, {code: "acf"}, {code: "fra"}], countryFull: "Seychelles", countryOfOrigin: "SC", tags: []},
            {contentID: 5, name: "Parmata", title: "Parmata's Story", mapPoints: [{contentElement: [{id: 1, language: 'en', description: "Miss Evelyn, Madam, from the second to the eighteenth year of her life, was brought up under my care, and, except when at school under my roof. I need not speak to your Ladyship of the virtues of that excellent young creature. She loved me as her father; nor was Mrs. Villars less valued by her; while to me she became so dear, that her loss was little less afflicting than that which I have since sustained of Mrs. Villars herself.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'},
                        {id: 6, language: 'spa', description: "La señorita Evelyn, señora, desde el segundo hasta el decimoctavo año de su vida, fue criada bajo mi cuidado y, excepto cuando estaba en la escuela bajo mi techo. No necesito hablar con su señoría de las virtudes de esa excelente criatura joven. Ella me amaba como su padre; ni la señora Villars era menos valorada por ella; mientras que para mí se hizo tan querida, que su pérdida fue un poco menos dolorosa que la que desde entonces he sufrido de la Sra. Villars.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                    coordinates: [-98.43613, 18.90815],
                    zoomLevel: 12,
                    story: 1},
                    {contentElement: [{id: 2, language: 'en', description: "At that period of her life we parted; her mother, then married to Monsieur Duval, sent for her to Paris. How often have I since regretted that I did not accompany her thither! Protected and supported by me, the misery and disgrace which awaited her might perhaps have been avoided. But, to be brief-Madame Duval, at the instigation of her husband, earnestly, or rather tyrannically, endeavoured to effect a union between Miss Evelyn and one of his nephews. And, when she found her power inadequate to her attempt, enraged at her non-compliance, she treated her with the grossest unkindness, and threatened her with poverty and ruin.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-103.41898, 25.54389],
                        zoomLevel: 13,
                        story: 1},
                    {contentElement: [{id: 3, language: 'en', description: "Some other gentlemen came in. The conversation concerning the person whose character Dr. Johnson had treated so slightingly, as he did not know his merit, was resumed. Mrs. Thrale said, 'You think so of him, Sir, because he is quiet, and does not exert himself with force. You'll be saying the same thing of Mr. ***** there, who sits as quiet--.' This was not well-bred; and Johnson did not let it pass without correction. 'Nay, Madam, what right have you to talk thus? Both Mr. ***** and I have reason to take it ill. You may talk so of Mr. *****; but why do you make me do it? Have I said anything against Mr. *****? You have set him, that I might shoot him: but I have not shot him.'", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-81.43613, 26.3],
                        zoomLevel: 10,
                        story: 1},
                    {contentElement: [{id: 4, language: 'en', description: "Mr. Thrale appeared very lethargick to-day. I saw him again on Monday evening, at which time he was not thought to be in immediate danger; but early in the morning of Wednesday, the 4th, he expired. Johnson was in the house, and thus mentions the event: 'I felt almost the last flutter of his pulse, and looked for the last time upon the face that for fifteen years had never been turned upon me but with respect and benignity.' Upon that day there was a Call of The LITERARY CLUB; but Johnson apologised for his absence by the following note:--", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-70.2, 11.6],
                        zoomLevel: 13,
                        story: 1},
                    {contentElement: [{id: 5, language: 'en', description: "Mr. Thrale's death was a very essential loss to Johnson, who, although he did not foresee all that afterwards happened, was sufficiently convinced that the comforts which Mr. Thrale's family afforded him, would now in a great measure cease. He, however, continued to shew a kind attention to his widow and children as long as it was acceptable; and he took upon him, with a very earnest concern, the office of one of his executors, the importance of which seemed greater than usual to him, from his circumstances having been always such, that he had scarcely any share in the real business of life. His friends of THE CLUB were in hopes that Mr. Thrale might have made a liberal provision for him for his life, which, as Mr. Thrale left no son, and a very large fortune, it would have been highly to his honour to have done; and, considering Dr. Johnson's age, could not have been of long duration; but he bequeathed him only two hundred pounds, which was the legacy given to each of his executors. I could not but be somewhat diverted by hearing Johnson talk in a pompous manner of his new office, and particularly of the concerns of the brewery, which it was at last resolved should be sold. Lord Lucan tells a very good story, which, if not precisely exact, is certainly characteristical: that when the sale of Thrale's brewery was going forward, Johnson appeared bustling about, with an ink-horn and pen in his button-hole, like an excise-man; and on being asked what he really considered to be the value of the property which was to be disposed of, answered, 'We are not here to sell a parcel of boilers and vats, but the potentiality of growing rich, beyond the dreams of avarice.'", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-74.43613, 40.90815],
                        zoomLevel: 10,
                        story: 1}],
                position: [108.3, -6.43], zoom: [14], languages: [{code: "eng"}, {code: "ind"}, {code: "sun"}], countryFull: "Indonesia", countryOfOrigin: "ID", tags: [{tagId: 1405, tag: "Graphic injury"}]},
            {contentID: 6, name: "Sakina", title: "Sakina's Story", mapPoints: [{contentElement: [{id: 1, language: 'en', description: "Miss Evelyn, Madam, from the second to the eighteenth year of her life, was brought up under my care, and, except when at school under my roof. I need not speak to your Ladyship of the virtues of that excellent young creature. She loved me as her father; nor was Mrs. Villars less valued by her; while to me she became so dear, that her loss was little less afflicting than that which I have since sustained of Mrs. Villars herself.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'},
                        {id: 6, language: 'spa', description: "La señorita Evelyn, señora, desde el segundo hasta el decimoctavo año de su vida, fue criada bajo mi cuidado y, excepto cuando estaba en la escuela bajo mi techo. No necesito hablar con su señoría de las virtudes de esa excelente criatura joven. Ella me amaba como su padre; ni la señora Villars era menos valorada por ella; mientras que para mí se hizo tan querida, que su pérdida fue un poco menos dolorosa que la que desde entonces he sufrido de la Sra. Villars.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                    coordinates: [-98.43613, 18.90815],
                    zoomLevel: 12,
                    story: 1},
                    {contentElement: [{id: 2, language: 'en', description: "At that period of her life we parted; her mother, then married to Monsieur Duval, sent for her to Paris. How often have I since regretted that I did not accompany her thither! Protected and supported by me, the misery and disgrace which awaited her might perhaps have been avoided. But, to be brief-Madame Duval, at the instigation of her husband, earnestly, or rather tyrannically, endeavoured to effect a union between Miss Evelyn and one of his nephews. And, when she found her power inadequate to her attempt, enraged at her non-compliance, she treated her with the grossest unkindness, and threatened her with poverty and ruin.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-103.41898, 25.54389],
                        zoomLevel: 13,
                        story: 1},
                    {contentElement: [{id: 3, language: 'en', description: "Some other gentlemen came in. The conversation concerning the person whose character Dr. Johnson had treated so slightingly, as he did not know his merit, was resumed. Mrs. Thrale said, 'You think so of him, Sir, because he is quiet, and does not exert himself with force. You'll be saying the same thing of Mr. ***** there, who sits as quiet--.' This was not well-bred; and Johnson did not let it pass without correction. 'Nay, Madam, what right have you to talk thus? Both Mr. ***** and I have reason to take it ill. You may talk so of Mr. *****; but why do you make me do it? Have I said anything against Mr. *****? You have set him, that I might shoot him: but I have not shot him.'", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-81.43613, 26.3],
                        zoomLevel: 10,
                        story: 1},
                    {contentElement: [{id: 4, language: 'en', description: "Mr. Thrale appeared very lethargick to-day. I saw him again on Monday evening, at which time he was not thought to be in immediate danger; but early in the morning of Wednesday, the 4th, he expired. Johnson was in the house, and thus mentions the event: 'I felt almost the last flutter of his pulse, and looked for the last time upon the face that for fifteen years had never been turned upon me but with respect and benignity.' Upon that day there was a Call of The LITERARY CLUB; but Johnson apologised for his absence by the following note:--", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-70.2, 11.6],
                        zoomLevel: 13,
                        story: 1},
                    {contentElement: [{id: 5, language: 'en', description: "Mr. Thrale's death was a very essential loss to Johnson, who, although he did not foresee all that afterwards happened, was sufficiently convinced that the comforts which Mr. Thrale's family afforded him, would now in a great measure cease. He, however, continued to shew a kind attention to his widow and children as long as it was acceptable; and he took upon him, with a very earnest concern, the office of one of his executors, the importance of which seemed greater than usual to him, from his circumstances having been always such, that he had scarcely any share in the real business of life. His friends of THE CLUB were in hopes that Mr. Thrale might have made a liberal provision for him for his life, which, as Mr. Thrale left no son, and a very large fortune, it would have been highly to his honour to have done; and, considering Dr. Johnson's age, could not have been of long duration; but he bequeathed him only two hundred pounds, which was the legacy given to each of his executors. I could not but be somewhat diverted by hearing Johnson talk in a pompous manner of his new office, and particularly of the concerns of the brewery, which it was at last resolved should be sold. Lord Lucan tells a very good story, which, if not precisely exact, is certainly characteristical: that when the sale of Thrale's brewery was going forward, Johnson appeared bustling about, with an ink-horn and pen in his button-hole, like an excise-man; and on being asked what he really considered to be the value of the property which was to be disposed of, answered, 'We are not here to sell a parcel of boilers and vats, but the potentiality of growing rich, beyond the dreams of avarice.'", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-74.43613, 40.90815],
                        zoomLevel: 10,
                        story: 1}],
                position: [59.61028, 42.45306], zoom: [10], languages: [{code: "eng"}, {code: "rus"}, {code: "kaa"}], countryFull: "Uzbekistan", countryOfOrigin: "UZ", tags: [{tagId: 1399, tag: "Abuse(Physical, Sexual)"}, {tagId: 1686, tag: "Profanity"}]},
            {contentID: 7, name: "Akano", title: "Akano's Story", mapPoints: [{contentElement: [{id: 1, language: 'en', description: "Miss Evelyn, Madam, from the second to the eighteenth year of her life, was brought up under my care, and, except when at school under my roof. I need not speak to your Ladyship of the virtues of that excellent young creature. She loved me as her father; nor was Mrs. Villars less valued by her; while to me she became so dear, that her loss was little less afflicting than that which I have since sustained of Mrs. Villars herself.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'},
                        {id: 6, language: 'spa', description: "La señorita Evelyn, señora, desde el segundo hasta el decimoctavo año de su vida, fue criada bajo mi cuidado y, excepto cuando estaba en la escuela bajo mi techo. No necesito hablar con su señoría de las virtudes de esa excelente criatura joven. Ella me amaba como su padre; ni la señora Villars era menos valorada por ella; mientras que para mí se hizo tan querida, que su pérdida fue un poco menos dolorosa que la que desde entonces he sufrido de la Sra. Villars.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                    coordinates: [-98.43613, 18.90815],
                    zoomLevel: 12,
                    story: 1},
                    {contentElement: [{id: 2, language: 'en', description: "At that period of her life we parted; her mother, then married to Monsieur Duval, sent for her to Paris. How often have I since regretted that I did not accompany her thither! Protected and supported by me, the misery and disgrace which awaited her might perhaps have been avoided. But, to be brief-Madame Duval, at the instigation of her husband, earnestly, or rather tyrannically, endeavoured to effect a union between Miss Evelyn and one of his nephews. And, when she found her power inadequate to her attempt, enraged at her non-compliance, she treated her with the grossest unkindness, and threatened her with poverty and ruin.", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-103.41898, 25.54389],
                        zoomLevel: 13,
                        story: 1},
                    {contentElement: [{id: 3, language: 'en', description: "Some other gentlemen came in. The conversation concerning the person whose character Dr. Johnson had treated so slightingly, as he did not know his merit, was resumed. Mrs. Thrale said, 'You think so of him, Sir, because he is quiet, and does not exert himself with force. You'll be saying the same thing of Mr. ***** there, who sits as quiet--.' This was not well-bred; and Johnson did not let it pass without correction. 'Nay, Madam, what right have you to talk thus? Both Mr. ***** and I have reason to take it ill. You may talk so of Mr. *****; but why do you make me do it? Have I said anything against Mr. *****? You have set him, that I might shoot him: but I have not shot him.'", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-81.43613, 26.3],
                        zoomLevel: 10,
                        story: 1},
                    {contentElement: [{id: 4, language: 'en', description: "Mr. Thrale appeared very lethargick to-day. I saw him again on Monday evening, at which time he was not thought to be in immediate danger; but early in the morning of Wednesday, the 4th, he expired. Johnson was in the house, and thus mentions the event: 'I felt almost the last flutter of his pulse, and looked for the last time upon the face that for fifteen years had never been turned upon me but with respect and benignity.' Upon that day there was a Call of The LITERARY CLUB; but Johnson apologised for his absence by the following note:--", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-70.2, 11.6],
                        zoomLevel: 13,
                        story: 1},
                    {contentElement: [{id: 5, language: 'en', description: "Mr. Thrale's death was a very essential loss to Johnson, who, although he did not foresee all that afterwards happened, was sufficiently convinced that the comforts which Mr. Thrale's family afforded him, would now in a great measure cease. He, however, continued to shew a kind attention to his widow and children as long as it was acceptable; and he took upon him, with a very earnest concern, the office of one of his executors, the importance of which seemed greater than usual to him, from his circumstances having been always such, that he had scarcely any share in the real business of life. His friends of THE CLUB were in hopes that Mr. Thrale might have made a liberal provision for him for his life, which, as Mr. Thrale left no son, and a very large fortune, it would have been highly to his honour to have done; and, considering Dr. Johnson's age, could not have been of long duration; but he bequeathed him only two hundred pounds, which was the legacy given to each of his executors. I could not but be somewhat diverted by hearing Johnson talk in a pompous manner of his new office, and particularly of the concerns of the brewery, which it was at last resolved should be sold. Lord Lucan tells a very good story, which, if not precisely exact, is certainly characteristical: that when the sale of Thrale's brewery was going forward, Johnson appeared bustling about, with an ink-horn and pen in his button-hole, like an excise-man; and on being asked what he really considered to be the value of the property which was to be disposed of, answered, 'We are not here to sell a parcel of boilers and vats, but the potentiality of growing rich, beyond the dreams of avarice.'", length: 99, filePath: "", fileSize: "", fileType: 'text', state: 'published'}],
                        coordinates: [-74.43613, 40.90815],
                        zoomLevel: 10,
                        story: 1}],
                position: [7.99111, 16.97333], zoom: [12], languages: [{code: "eng"}, {code: "ha"}, {code: "fra"}], countryFull: "Niger", countryOfOrigin: "NE", tags: [{tagId: 1399, tag: "Abuse(Physical, Sexual)"}, {tagId: 1405, tag: "Graphic injury"}]}
        ];/*

        //setStories(allStories);

        //toggleLoading();*/
        //********************************************************************************************************************************************************
        let url = "";
        if(locked){
            url = "http://localhost:8080/api/content/filtered/" + props.filterString.substring(7);
        }
        else{
            url = "http://localhost:8080/api/content/stories/published";
        }
        let config = {
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
            }
        };

        axios.get(url, config)
            .then((response) => {
                console.log(response);
                if(response != undefined && response.data != undefined){
                    setStories(response.data);
                }
                else{
                    setStories(allStories);
                }

                toggleLoading();
            }, (error) => {
                console.log(error);
                setIsError(true);
            });
    };

    useEffect(() => {
        fetchStories();

        //fetchOpenedStory();
    }, []);

    if(isError) {
        return (
            <button className="button is-large is-danger">Something went wrong!</button>
        );
    }
    else if(isLoading) {
        return (
            <div className="pageloader is-info" style={{position: "fixed",
                width: "100vw",
                height: "100vh",
                top: "100vh",
                bottom: "0",
                left: "0",
                right: "0",
                zIndex: "1",
                overflow: "hidden"}}><span className="title">Loading the Map</span></div>
        );
    }
    else {
        return (
            <>
                <MapBox geocoder={geocoder}
                        mapObject={map}
                        fullStoryList={stories}
                        storyList={stories}
                        filterSting={props.filterString.substring(7)}
                        locked={locked}>
                </MapBox>
                <Stargate/>
            </>
        );
    }

}

export default MapWindow;