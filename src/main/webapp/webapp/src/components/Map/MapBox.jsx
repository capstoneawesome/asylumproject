import React, {useState, useEffect, useLayoutEffect, useRef} from 'react';
import ZoomControl from "./controls/ZoomControl";
import "./map.scss";
import StoryMarker from "./StoryMarker";
import {fromLonLat} from "ol/proj";
import FullscreenControl from "./controls/FullscreenControl";
import StoryBox from "./storyview/StoryBox";
import AttributionControl from "./controls/AttributionControl";
import MediaCarousel from "./storyview/MediaCarousel";
import cameraExpand from "./icons/media_expand_combined_shadow.svg";
import cameraContract from "./icons/media_contract_combined_shadow.svg";
import StoryFilterControl from "./controls/StoryFilterControl";
import "./controls/controls.scss";
import StoryCatalogue from "./storyview/StoryCatalogue";
import {animated, useSpring} from "react-spring";
import LinkGenerator from "./controls/LinkGenerator";

/**
 * Main map container. Holds Openlayers map canvas, every component that is to be displayed on the map as well as all
 * states relevant to multiple components. Passes callback functions down to children components to control shared
 * state. Tracks and applies filter options.
 * @params props props from MapWindow including locked state when using generated link and the list of published stories received from the server
 * @returns The Openlayers canvas and parent for all components displayed on the map.
 */

function MapBox(props){

    let story = {};
    //alert(props.filterSting);

    const [linkOpen, setLinkOpen] = useState(false);
    const setInitialFilter = () => {
        return([]);
    };

    const [isOpen, setIsOpen] = useState(false);
    const [openedStory, setOpenedStory] = useState({name: "undefined"});
    const [prevLocation, setPrevLocation] = useState([]);
    const [storyLanguage, setStoryLanguage] = useState("eng");
    const [mapPointIndex, setMapPointIndex] = useState(0);
    const [filtered, setFiltered] = useState([]);
    const [langFilter, setLangFilter] = useState(["eng"]);
    const [countryFilter, setCountryFilter] = useState([]);
    const [ratingFilter, setRatingFilter] = useState(setInitialFilter());
    const [expanded, setExpanded] = useState(false);
    const [filterExpanded, setFilterExpanded] = useState(false);
    const [button, setButton] = useState(cameraExpand);
    const [player, setPlayer] = useState(null);

    const [storyMarkers, setStoryMarkers] = useState([]);

    const mediaAnimation = useSpring({
        opacity: (isOpen && expanded) ? 1 : 0,
        transform: (isOpen && expanded) ? "scale(1)" : "scale(0)"
    });

    const carouselRef = useRef(null);

    const buildLangList = () => {
        let list = [];
        props.storyList.map(story => {
            story.languages.map(lang => list.push(lang.code));
            });
        return list;
    };

    const buildCountryList = () => {
        let list = [];
        props.storyList.map(story => {
            list.push(story.countryOfOrigin.toUpperCase());
        });
        return list;
    };

    const buildRatingList = () => {
        let list = [];
        props.storyList.map(story => {
            story.tags.map(tag => list.push(tag));
        });
        return list;
    };

    const [langList, setLangList] = useState(buildLangList());
    const [countryList, setCountryList] = useState(buildCountryList());
    const [ratingList, setRatingList] = useState(buildRatingList());

    const setPlayerFromRef = (playerRef) => {
        setPlayer(playerRef.current);
    };

    const toggleOpen = () => {
        //alert(isOpen);
        setIsOpen(!isOpen);
        //alert(isOpen);
    };

    const toggleFilter = () => {
        setFilterExpanded(!filterExpanded);
    };

    const fetchOpenedStory = (story) => {
        //alert(story);
        setOpenedStory(story);
    };

    const openStory = (id) => {
        //alert(id);
        story = props.storyList.find(story => story.contentID === id);
        //alert(story);
        fetchOpenedStory(story);
        const view = props.mapObject.getView();
        setPrevLocation([view.getCenter(), view.getZoom()]);
        view.animate({center: fromLonLat(story.mapPoints[0].coordinates), duration: 2500});
        view.animate({zoom: story.mapPoints[0].zoomLevel , duration: 2500});
        //alert([story.mapPoints[0].coordinates, story.mapPoints[0].zoomLevel]);
        setTimeout(() => {   toggleOpen(); }, 2500);



    };

    const toggleExpand = () => {
        if(button == cameraExpand) {
            setButton(cameraContract);
        }
        else {
            setButton(cameraExpand);
        }
        if(player != null){
            player.pause();
        }

        setExpanded(!expanded);
    };

    const closeExpand = () => {
        if(player != null){
            player.pause();
        }
        setButton(cameraExpand);
        setExpanded(false);
    }

    const closeStory = (prev) => {
      toggleOpen();
      closeExpand();
      const view = props.mapObject.getView();
      view.animate({center: prevLocation[0], duration: 2500});
      view.animate({zoom: prevLocation[1], duration: 2500});
      setMapPointIndex(0);
    };

    const movePoint = (index) => {
        closeExpand();
        setMapPointIndex(index);
        const nextPoint = openedStory.mapPoints[index];
        const view = props.mapObject.getView();
        const out = view.getZoom();
        view.animate({zoom: out-5, duration: 2500}, {zoom: nextPoint.zoomLevel, duration: 2500});
        view.animate({center: fromLonLat(nextPoint.coordinates), duration: 5000});

    };

    const buildMarkers = () => {
        let markers = [];
        filtered.map((story) => {
            markers.push(
                <div key={story.contentID.toString()}>
                    <StoryMarker
                                 mapObject={props.mapObject}
                                 story={story}
                                 openStory={openStory}
                                 open={isOpen}>
                    </StoryMarker>
                </div>);


        });
        setStoryMarkers(markers);
    };

    const iso6393To1 = require('iso-639-3/to-1');

    const convert1To3 = (code) => {
        const converted = Object.keys(iso6393To1).find(key => iso6393To1[key] === code);
        if(converted == undefined) {
            return code;
        }
        return converted;
    };

    const applyFilters = () => {
        let filteredByLang = [];
        props.storyList.map(story => {
            let flag = false;
            langFilter.map(lang => {
                if(!flag){
                    let converted = story.languages.map(unconverted => {
                        let unconvertedCode = unconverted.code;
                        return convert1To3(unconvertedCode)
                    });
                    if(converted.includes(lang)){
                        flag = true;
                    };
                }
            });
            //flag = true; // force stories through --DEBUG ONLY
            if(flag || langFilter.length == 0){
                filteredByLang.push(story);
            }
        });
        let filteredByCountry = [];
        filteredByLang.map((story) => {
            if(countryFilter.length == 0){
                filteredByCountry.push(story);
            }
            else if(countryFilter.includes(story.countryOfOrigin)){
                filteredByCountry.push(story);
            }
        });
        let filteredByRating = [];
        filteredByCountry.map((story) => {
            let flag = false;
                story.tags.map((storyTag) => {
                    let result = ratingFilter.filter(tag => (tag.tagId === storyTag.tagId));
                    if(result.length > 0){
                        flag = true;
                    }
                });
                if(!flag){
                    filteredByRating.push(story);
                }

        });
        setFiltered(filteredByRating);
    };

    const changeLanguages = (languages) => {
        setLangFilter(languages);
    };

    const changeCountries = (countries) => {
        setCountryFilter(countries);
    };

    const generateLink = () => {
        setLinkOpen(true);
    };

    const closeLink = () => {
        setLinkOpen(false);
    };

    useEffect(() => {
        props.mapObject.addControl(props.geocoder);
        //props.mapObject.addControl(new Attribution());
        props.mapObject.setTarget('map');
        return(() => {setStoryMarkers([])});
        }, []);

    useEffect(() => {
        applyFilters();
        return(() => {setStoryMarkers([])});
    }, [langFilter]);

    useEffect(() => {
        applyFilters();
        return(() => {setStoryMarkers([])});
    }, [countryFilter]);

    useEffect(() => {
        applyFilters();
        return(() => {setStoryMarkers([])});
    }, [ratingFilter]);

    useEffect(() => {
        //setFiltered(props.applyFilters());
        //applyFilters();
        buildMarkers();
        return(() => {setStoryMarkers([])});
    }, [filtered]);


        if(!isOpen){
            return (<>
                <div id='map' className="map-box">
                    <StoryCatalogue
                        storyOpen={isOpen}
                        stories={filtered}
                        openStory={openStory}
                    ></StoryCatalogue>
                    <MediaCarousel isOpen={isOpen}
                                   expanded={expanded}
                                   button={button}
                                   setExpanded={setExpanded}
                                   setButton={setButton}
                                   toggleExpand={toggleExpand}
                                   setPlayerFromRef={setPlayerFromRef}
                                   openedStory={openedStory}
                                   mapPointIndex={mapPointIndex}>
                    </MediaCarousel>
                    <ZoomControl id="zoom" mapObject={props.mapObject}></ZoomControl>
                    <FullscreenControl mapObject={props.mapObject}></FullscreenControl>
                    <StoryFilterControl locked={props.locked}
                                        isOpen={isOpen}
                                        expanded={filterExpanded}
                                        toggleExpand={toggleFilter}
                                        langList={langList}
                                        countryList={countryList}
                                        ratingList={ratingList}
                                        langFilter={langFilter}
                                        countryFilter={countryFilter}
                                        ratingFilter={ratingFilter}
                                        setLangFilter={setLangFilter}
                                        setCountryFilter={setCountryFilter}
                                        setRatingFilter={setRatingFilter}
                                        changeLanguages={changeLanguages}
                                        changeCountries={changeCountries}
                                        buildLangList={buildLangList}
                                        generateLink={generateLink}
                    >
                    </StoryFilterControl>

                    {storyMarkers}

                    <StoryBox className="story-box"
                              mapObject={props.mapObject}
                              story={story}
                              closeStory={closeStory}
                              open={isOpen}
                              openedStory={openedStory}
                              storyLanguage={storyLanguage}
                              setStoryLanguage={setStoryLanguage}
                              mapPointIndex={mapPointIndex}
                              movePoint={movePoint}>
                    </StoryBox>
                    <LinkGenerator linkOpen={linkOpen} closeLink={closeLink} ratingList={ratingList}>

                    </LinkGenerator>
                    <AttributionControl/>
                </div>
            </>);
        }
        else {
            return (<>


                <div id='map' className="map-box">
                    <StoryCatalogue
                        storyOpen={isOpen}
                        stories={filtered}
                        openStory={openStory}
                    ></StoryCatalogue>
                    <MediaCarousel isOpen={isOpen}
                                             expanded={expanded}
                                             button={button}
                                             setExpanded={setExpanded}
                                             setButton={setButton}
                                             toggleExpand={toggleExpand}
                                             setPlayerFromRef={setPlayerFromRef}
                                             openedStory={openedStory}
                                             mapPointIndex={mapPointIndex}>
                    </MediaCarousel>
                    <ZoomControl id="zoom" mapObject={props.mapObject}></ZoomControl>
                    <FullscreenControl mapObject={props.mapObject}></FullscreenControl>
                    <StoryFilterControl locked={props.locked}
                                        isOpen={isOpen}
                                        expanded={filterExpanded}
                                        toggleExpand={toggleFilter}
                                        langList={langList}
                                        countryList={countryList}
                                        ratingList={ratingList}
                                        langFilter={langFilter}
                                        countryFilter={countryFilter}
                                        ratingFilter={ratingFilter}
                                        setLangFilter={setLangFilter}
                                        setCountryFilter={setCountryFilter}
                                        setRatingFilter={setRatingFilter}
                                        changeLanguages={changeLanguages}
                                        changeCountries={changeCountries}
                                        buildLangList={buildLangList}
                                        generateLink={generateLink}
                    >
                    </StoryFilterControl>



                    <StoryBox className="story-box"
                              mapObject={props.mapObject}
                              story={story}
                              closeStory={closeStory}
                              open={isOpen}
                              openedStory={openedStory}
                              storyLanguage={storyLanguage}
                              setStoryLanguage={setStoryLanguage}
                              mapPointIndex={mapPointIndex}
                              movePoint={movePoint}>
                    </StoryBox>
                    <LinkGenerator linkOpen={linkOpen} closeLink={closeLink} ratingList={ratingList}>

                    </LinkGenerator>
                    <AttributionControl/>
                </div>
            </>);
        }

}

export default MapBox;

