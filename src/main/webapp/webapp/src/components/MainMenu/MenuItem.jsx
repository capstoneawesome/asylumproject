import React, {useState, useEffect, useRef} from 'react';
import {animated, useSpring, useChain, useTrail, config} from 'react-spring';
import {useHistory} from 'react-router';
import "./mainmenu.scss";

/**
 * Represents an navigation link and item label on the new menu.
 * @params props props from NewMenu
 * @returns A menu label with site routing based on props
 */

export default function MenuItem(props) {

    const [hover, setHover] = useState(false);

    const itemSpring = useSpring({
        opacity: props.open ? 1 : 0,
        width: props.open ? "280px" : "0px",
        config: config.gentle
    });

    let history = useHistory();

    const handleClick = () => {
        console.log(props.path);
        if (props.path === '/about'){
           props.setAboutModalOpen(!props.aboutModalOpen);
        }else{
            history.push(props.path);
            props.closeMenu();
        }
    };

    const toggleHover = () => {
        setHover(!hover);
    };

    return (
        <div className="menu-item" style={props.style} onMouseEnter={toggleHover} onMouseLeave={toggleHover}>
            <animated.div style={itemSpring} className="columns is-mobile" id="menu-item-columns"  onClick={handleClick}>
                <div className="column is-one-third is-paddingless is-marginless" id="menu-item-head-div">
                    <h1  id={hover ? "head-text-hover" : "head-text-no-hover"}>{props.label[0]}</h1>
                </div>
                <div className="column is-paddingless is-marginless" id="menu-item-tail-div">
                    <h1  id={hover ? "tail-text-hover" : "tail-text-no-hover"}>{props.label.slice(1)}</h1>
                </div>
            </animated.div>
        </div>

    );
}