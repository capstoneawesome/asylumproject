import {NavLink} from "react-router-dom";
import React from "react";


export default function UserDisplay(props) {

    return (
        <div className="user-box">
            <NavLink to="/signin" className="main-menu-link">Sign In</NavLink>
            <NavLink to="/signout" className="main-menu-link">Sign Out</NavLink>
        </div>
    );
}