import React, {useState, useEffect, useRef} from 'react';
import burger_button, {ReactComponent as Burger} from "./burger_button.svg";
import "./mainmenu.scss";
import Control from "ol/control/Control";
import {animated, useTrail} from "react-spring";
import useRouter from "../../customHooks/useRouter";
import {BrowserRouter, Redirect, Route, Switch, NavLink} from "react-router-dom";

import Admin from "../Admin/Index";
import Intro from "../Intro/Intro";
import AboutUs from "../AboutUs/AboutUs";
import SignIn from "../SignIn/SignIn";
import SignOut from "../SignOut/SignOut";
import UserDisplay from "./UserDisplay";

export default function MainMenu(props) {

    const menuItems = [{label: "Intro", path: "/intro"},
        {label: "Map", path: "/map"},
        {label: "Content Manager", path: "/contentmanager"},
        {label: "About Us", path: "/aboutus"},
        {label: "Admin", path: "/admin"}];

    const [open, setOpen] = useState(false);

    const trail = useTrail(menuItems.length, {
        transform: open ? 'translate3d(-10%, 0, 0)' : 'translate3d(130%, -100%, 0)'
    });

    const toggleMenu = () => {
        setOpen(() => !open);
    };

    return(
        <>
            {/*<div>*/}
            {/*    <NavLink to="/signin" className="main-menu-link">DEBUG--Sign In</NavLink>*/}
            {/*    <NavLink to="/signout" className="main-menu-link">DEBUG--Sign Out</NavLink>*/}
                <Burger className="burger-icon" onClick={toggleMenu}></Burger>
            {/*</div>*/}
            <div className="main-menu-container">
                {trail.map((animation, index) => (
                    < animated.div key={menuItems[index].label} className="main-menu-item" style={animation}>
                        <NavLink to={menuItems[index].path} className="main-menu-link" onClick={toggleMenu}>{menuItems[index].label}</NavLink>
                    </animated.div>
                ))}
            </div>
        </>
    );

}