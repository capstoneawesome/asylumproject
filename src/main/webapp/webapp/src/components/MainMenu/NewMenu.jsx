import React, {useState, useEffect, useRef} from 'react';
import {animated, useSpring, useChain, useTrail, config} from 'react-spring';
import menuIcon from "./Menu_Button_Orange.svg";
import {useHistory} from 'react-router';
import "./mainmenu.scss";
import menubar from "./MenuBarLeanLeftTest.svg";
import MenuItem from "./MenuItem";
import * as authenticationActions from "../../actions/authentication";
import {useSelector} from "react-redux";
import App from "../../App";

/**
 * The main navigation menu for the application. Contains navigation items for all main pages.
 * @returns Animated navigation menu and blur underlay
 */

export default function NewMenu(props) {

    const perms = useSelector(state => state.authenticationReducer.user_authorities);

    const checkAdmin = () => {
        if(perms != null && perms != undefined){
            return perms.includes("ROLE_SYSTEM_ADMIN");
        }
        return false;
    };

    const checkCM = () => {
        if(perms != null && perms != undefined){
            return perms.includes("ROLE_CONTENT_CURATOR");
        }
        return false;
    };

    const [open, setOpen] = useState(false);
    const [isAdmin, setIsAdmin] = useState(checkAdmin());
    const [isCM, setIsCM] = useState(checkCM());
    let history = useHistory();

    /*const trail = useTrail(menuItems.length, {
        transform: open ? 'translate3d(-10%, 0, 0)' : 'translate3d(130%, -100%, 0)'
    });*/
    const mainBarSpring = useSpring({
        height: open ? "625px" : "0px",
        config: config.gentle
        //width: open ? "625px" : "0px"
    });

    const toggleMenu = () => {
        setOpen(() => !open);
    };

    const aboutItem = {label: "About", style: {top: "170px", left: "88px"}, path: "/about"};
    const mapItem = {label: "Map", style: {top: "240px", left: "105px"}, path: "/map"};
    //const catalogueItem = {label: "Stories", style: {top: "310px", left: "9.5vw"}, path: "/catalogue"};
    const resourcesItem = {label: "Resources", style: {top: "310px", left: "120px"}, path: "/resources"};
    const faqItem = {label: "FAQ", style: {top: "380px", left: "138px"}, path: "/faq"};

    const goToCM = () => {
        console.log("goToCM");
        history.push("/contentmanager");
        toggleMenu();
    };

    const goToAdmin = () => {
        console.log("goToAdmin");
        history.push("/admin");
        toggleMenu();
    };

    const cmButton = () => {
        if(isCM){
            return <button className="button is-info is-small has-tooltip-bottom" id="content-creator-link" data-tooltip="Content Management" onClick={goToCM}>
                    <span className="icon is-medium">
                        <i className="fas fa-pen-fancy"></i>
                    </span>
            </button>;
        }
        else{
            return null;
        }
    };

    const adminButton = () => {
        if(isAdmin){
            return <button className="button is-danger is-small has-tooltip-bottom" id="admin-link" data-tooltip="System Admin" onClick={goToAdmin}>
                    <span className="icon is-medium">
                        <i className="fas fa-cogs"></i>
                    </span>
            </button>
        }
        else{
            return null;
        }
    };

    const maintainAdmin = () => {
        if(perms != null && perms != undefined){
            setIsAdmin(perms.includes("ROLE_SYSTEM_ADMIN"));
        }
        else{
            setIsAdmin(false);
        }
    };

    const maintainCM = () => {
        if(perms != null && perms != undefined){
            setIsCM(perms.includes("ROLE_CONTENT_CURATOR"));
        }
        else{
            setIsCM(false);
        }
    };

    useEffect(() => {
        maintainAdmin();
        maintainCM();
    }, [open]);

    return(
        <>
            <div>
                <img className="menu-icon" src={menuIcon} onClick={toggleMenu}></img>
            </div>
            <div className={open ? "main-menu-box-blurred" : "main-menu-box-unblurred"}>

            </div>
            <animated.div style={mainBarSpring} className="main-menu is-marginless is-paddingless">
                <img className="menu-image is-marginless is-paddingless" src={menubar}></img>
                <MenuItem open={open} closeMenu={toggleMenu} label={aboutItem.label} style={aboutItem.style} path={aboutItem.path} setAboutModalOpen={props.setAboutModalOpen} aboutModalOpen={props.aboutModalOpen}></MenuItem>
                <MenuItem open={open} closeMenu={toggleMenu} label={mapItem.label} style={mapItem.style} path={mapItem.path} ></MenuItem>
                <MenuItem open={open} closeMenu={toggleMenu} label={resourcesItem.label} style={resourcesItem.style} path={resourcesItem.path} ></MenuItem>
                <MenuItem open={open} closeMenu={toggleMenu} label={faqItem.label} style={faqItem.style} path={faqItem.path} ></MenuItem>
                {cmButton()}
                {adminButton()}
            </animated.div>
        </>
    );
}