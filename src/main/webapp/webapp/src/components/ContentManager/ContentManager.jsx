import React, {useEffect, useRef, useState} from "react";
import swal from 'sweetalert';
import {useToasts} from 'react-toast-notifications';
import {UploadElement} from "./UploadElement";
import {StoryForm} from "./StoryForm";
import {ContentTags} from "./ContentTags";
import {ContentElementList} from "./ContentElementList";
import {Editor} from './Editor';
import {StoryCreationProgress} from './StoryCreationProgress';
import {createMapPointRequest} from "../../libraries/createMapPointRequest";
import './sass/contenttags.scss';
import './sass/manageMappoints.scss';
import {useHistory, useParams} from 'react-router-dom';
import {animated, useTransition} from "react-spring";
import CoordinatePicker from "./CoordinatePicker";
import {EditContentElement} from "./EditContentElement";
import langCodes from 'iso-639-1';
import {getStory} from "../../libraries/getStory";
import {saveStory} from "../../libraries/saveStory";
import {saveMapPoint} from "../../libraries/saveMapPoint";
import {getMapPointByID} from "../../libraries/getMapPointByID";
import {connect} from "react-redux";
import {deleteMapPoint} from "../../libraries/deleteMapPoint";
import axios from "axios";
import {getAllTags} from "../../libraries/getAllTags";
import {createTag} from "../../libraries/createTag";
import {saveTagToStory} from "../../libraries/saveTagToStory";
import {ContentTagDisplay} from "./ContentTagDisplay";
import {MapPointSelector} from "./MapPointSelector";
import {createStory} from "../../libraries/createStory";
import {getMapPointsByStoryID} from "../../libraries/getMapPointsByStoryID";

/**
 * This Component handles the Story creation process. Changes to the Story's information in the interface is saved automatically for most actions.
 * @param props props from ContentManagerIndex.jsx
 * @returns {*}
 * @constructor
 */
export default function ContentManager(props) {

    const {addToast} = useToasts();
    const history = useHistory();

    const [story, setStory] = useState(null);
    const [uploadModalState, setUploadModalState] = useState(false);
    const [editElementModalState, setEditElementModalState] = useState(false);
    const [editableContentElement, setEditableContentElement] = useState(null);
    const [editableTextElement, setEditableTextElement] = useState(null);
    const [editorSubmitted, setEditorSubmitted] = useState(false);
    const [activeStep, setActiveStep] = useState(1);
    const [currentMappointCoordinates, setCurrentMappointCoordinates] = useState([]);
    const [currentMappointZoomLevel, setCurrentMappointZoomLevel] = useState(0);
    const [contentElementsDisplay, setContentElementsDisplay] = useState({display: 'none'});
    const [mappointSelectorDisplay, setMappointSelectorDisplay] = useState({display: 'flex'});
    const [mapPointAddDeleteDisplay, setMapPointAddDeleteDisplay] = useState({display: 'none'});
    const [contentElementListLanguage, setContentElementListLanguage] = useState('');
    const [contentElementListLoading, setContentElementListLoading] = useState(false);
    const [storyReadyForReview, setStoryReadyForReview] = useState(false);
    const [loadingResources, setLoadingResources] = useState(false);
    const [selectedElementsType, setSelectedElementsType] = useState('all');
    const [elementsList, setElementsList] = useState(null);
    let [filteredElements, setFilteredElements] = useState(null);
    let [contentTags, setContentTags] = useState([]);
    let [customTag, setCustomTag] = useState('');
    let [availableTag, setAvailableTag] = useState();
    let [availableTags, setAvailableTags] = useState([]);

    let currentMapPoint = useRef(null);
    const [mapPointList, setMapPointList] = useState(null);
    const [mapPointPageClick, setMapPointPageClick] = useState(false);

    let scroll = React.createRef();
    let mapScroll = React.createRef();

    let {storyID} = useParams();

    const [storyTitle, setStoryTitle] = useState(story !== null ? story.title : '');
    const [asylumSeekerName, setAsylumSeekerName] = useState(story !== null ? story.asylumSeekerName : '');
    const [countryOfOrigin, setCountryOfOrigin] = useState(story !== null ? story.countryOfOrigin : "US");
    const [storyDescription, setStoryDescription] = useState(story != null ? story.description : "");

    console.log("story", story);
    useEffect(() => {
        console.log("storyID" , storyID);
        if (storyID !== null && storyID !== undefined) {
            setLoadingResources(true);
            getStory(storyID).then((response) => {
                setLoadingResources(false);
                if (response.status === 200) {
                    let s = response.data;
                    setStory(s);
                    setLoadingResources(true);
                    getMapPointsByStoryID(s.contentID).then((response) => {
                        setLoadingResources(false);
                        if (response.status === 200){
                            setMapPointList(response.data);
                            currentMapPoint.current = response.data[0];
                        }
                    }).catch((error) => {
                        setLoadingResources(false);
                    });
                    setStoryTitle(story.title);
                    setAsylumSeekerName(story.asylumSeekerName);
                    setCountryOfOrigin(story.countryOfOrigin);
                    setStoryDescription(story.description);
                    getAllContentTags();

                }
            }).catch((error) => {
                setLoadingResources(false);
            });

        }else{

            let creator = props.user_id;

            //Create a new Story in the 'DRAFT' state immediately after starting Story creation
                createStory(creator).then((response) => {
                    if (response.status === 200) {
                        let newStory = response.data;
                        setStory(newStory);
                        setStoryTitle(newStory.title);
                        setAsylumSeekerName(newStory.asylumSeekerName);
                        setCountryOfOrigin(newStory.countryOfOrigin);
                        setStoryDescription(newStory.description);
                        history.push("/contentmanager/stories/" + newStory.contentID);
                    } else {
                        addToast('Error creating Story', {appearance: 'error', autoDismiss: true});
                    }
                });
            }
    }, [storyID]);

    useEffect(() => {
        if (story !== null) {
            setStoryTitle(story.title);
            setAsylumSeekerName(story.asylumSeekerName);
            setCountryOfOrigin(story.countryOfOrigin);
            setStoryDescription(story.description);
        }
    }, [story]);

    useEffect(() => {
        if (currentMapPoint.current != null) {
            updateElementsList();
        }
    }, [currentMapPoint.current]);

    useEffect(() => {
        getAllContentTags();
    }, [activeStep]);

    let setStep = (step) => {
        if (step === 1) {
            setActiveStep(1);
        } else if (step === 2) {

            if (activeStep === 1) {
                if (checkStoryForm()) {
                    saveStoryFormDetails();
                    checkMapPointsExist();
                    setActiveStep(2);
                }else{
                    addToast('All Story detail fields must be filled out!', {appearance: 'error', autoDismiss: true});
                }
            } else {
                checkMapPointsExist();
                setActiveStep(2);
            }
        } else if (step === 3) {
            if (activeStep === 1) {
                if (checkStoryForm()) {
                    setActiveStep(3);
                }
            } else {
                setActiveStep(3);
            }
        } else if (step === 4) {
            if (activeStep === 1) {
                if (checkStoryForm()) {
                    setStoryReadyForReview(true);
                    setActiveStep(4);
                }
            } else {
                setStoryReadyForReview(true);
                setActiveStep(4);
            }
        }
    };

    const stepOneTransition = useTransition(activeStep === 1, null, {
        from: {transform: 'translate3d(0, -100%, 0)'},
        enter: {transform: 'translate3d(0%, 0, 0)'},
        leave: {transform: 'translate3d(0, 100%, 0)'},
        config: {duration: 0}
    });

    const stepTwoTransition = useTransition(activeStep === 2, null, {
        from: {transform: 'translate3d(0, -100%, 0)'},
        enter: {transform: 'translate3d(0%, 0, 0)'},
        leave: {transform: 'translate3d(0, 100%, 0)'},
        config: {duration: 0}
    });
    const stepThreeTransition = useTransition(activeStep === 3, null, {
        from: {transform: 'translate3d(0, -100%, 0)'},
        enter: {transform: 'translate3d(0%, 0, 0)'},
        leave: {transform: 'translate3d(0, 100%, 0)'},
        config: {duration: 0}
    });
    const stepFourTransition = useTransition(activeStep === 4, null, {
        from: {transform: 'translate3d(0, -100%, 0)'},
        enter: {transform: 'translate3d(0%, 0, 0)'},
        leave: {transform: 'translate3d(0, 100%, 0)'},
        config: {duration: 0}
    });

    let toggleUploadModal = () => {
        setUploadModalState(!uploadModalState);
    };

    let toggleEditElementModal = () => {
        setEditElementModalState(!editElementModalState);
    };

    let setContentElementListLang = (langCode) => {
        setContentElementListLanguage(langCode);
        loadingContentElementList(true);
    };


    let loadingContentElementList = (isLoading) => {
        if (isLoading) {
            setContentElementListLoading(true);
        } else {
            setContentElementListLoading(false);
        }
    };

    let handleAddNewMapPoint = () => {
        currentMapPoint.current = null;
        setContentElementsDisplay({display: 'none'});
        setMapPointAddDeleteDisplay({display: 'none'});
        if (mapScroll.current !== null) { mapScroll.current.scrollIntoView();}
    };

    let setCoordinatesAndZoomLevel = (coordinates, zoomLevel) => {
        setCurrentMappointCoordinates(coordinates);
        setCurrentMappointZoomLevel(zoomLevel);
        addToast('Map Point location set!', {appearance: 'success', autoDismiss: true});
        setContentElementsDisplay({display: 'flex'});
        setMapPointAddDeleteDisplay({display: 'flex'});
        if (scroll.current !== null) { scroll.current.scrollIntoView();}

        if (currentMapPoint.current == null) {
            createMapPoint(zoomLevel, coordinates);
        } else {
            currentMapPoint.current.zoomLevel = zoomLevel;
            currentMapPoint.current.coordinates = coordinates;
            saveMapPointObject(currentMapPoint.current);
        }
    };

    let handleEditorSubmit = (submitted) => {
        setEditorSubmitted(submitted);
    };

    let checkStoryForm = () => {

        let valid = false;

        if (asylumSeekerName.trim() !== '' && countryOfOrigin.trim() !== '' && storyTitle.trim() !== '' && storyDescription.trim() !== '') {
            valid = true;
        }
        return valid;
    };

    let saveStoryFormDetails = () => {
        let story = null;
        setLoadingResources(true);
        getStory(storyID).then((response) => {
            setLoadingResources(false);
            if (response.status === 200) {
                story = response.data;
                story.asylumSeekerName = asylumSeekerName.trim() !== '' ? asylumSeekerName : story.asylumSeekerName;
                story.countryOfOrigin = countryOfOrigin.trim() !== '' ? countryOfOrigin : story.countryOfOrigin;
                story.title = storyTitle.trim() !== '' ? storyTitle : story.title;
                story.description = storyDescription.trim() !== '' ? storyDescription : story.description;

                saveStoryObject(story);
            } else {
                addToast('All Story detail fields must be filled out!', {appearance: 'error', autoDismiss: true});
            }
        }).catch((error) => {
            setLoadingResources(false);
        });
    };

    let updateCurrentMapPointAfterDeletion = () => {

        if (currentMapPoint.current !== null) {
            let currentMapPointID = currentMapPoint.current.id;
            //let indexOfMapPoint = story.mapPoints.findIndex(mapPoint => mapPoint.id === currentMapPointID);
            let indexOfMapPoint = mapPointList.findIndex(mapPoint => mapPoint.id === currentMapPointID);

            if (indexOfMapPoint !== -1) {
                //Set new current map point to be the next newest one, if that doesn't exist use the next oldest one
                if (mapPointList[indexOfMapPoint + 1] !== undefined) {
                    //currentMapPoint.current = story.mapPoints[indexOfMapPoint + 1];
                    currentMapPoint.current = mapPointList[indexOfMapPoint + 1];
                } else if (mapPointList[indexOfMapPoint - 1] !== undefined) {
                    //currentMapPoint.current = story.mapPoints[indexOfMapPoint - 1];
                    currentMapPoint.current = mapPointList[indexOfMapPoint - 1];
                } else {
                    //There are no map points, so create a new one
                    handleAddNewMapPoint();
                }
            }
        }
    };

    let handleContentTags = (targetName, targetValue) => {
        switch (targetName) {
            case 'contentTags':
                if (targetValue !== null && targetValue.trim() !== '') {
                    setAvailableTag(targetValue);
                }
                break;
            case 'customTag':
                if (targetValue !== null && targetValue.trim() !== '') {
                    setCustomTag(targetValue);
                }
                break;
            case 'addCustomTag':
                if (customTag !== null && customTag.trim() !== '') {
                    saveTagToDatabase(customTag);
                }
                break;
            case 'addTagToStory':
                saveTagToStoryObject((availableTag == null || availableTag == undefined) ? availableTags[0].tag : availableTag);
                break;
        }

    };

    let saveTagToStoryObject = (tag) => {

        if (tag !== null) {
            setLoadingResources(true);
            saveTagToStory(storyID, tag).then((response) => {
                setLoadingResources(false);
                if (response.status === 200) {
                    let story = response.data;
                    setStory(story);
                    addToast('Story saved successfully!', {appearance: 'success', autoDismiss: true});
                } else {
                    addToast('There was an error saving the story. Try again.', {
                        appearance: 'error',
                        autoDismiss: true
                    });
                }
            }).catch((error) => {
                setLoadingResources(false);
                addToast('There was an error saving the story. Try again.', {appearance: 'error', autoDismiss: true});
            });
        }
    };

    let saveTagToDatabase = (tag) => {
        createTag(storyID, tag).then((response) => {
            if (response.status === 200) {
                setStory(response.data);
                //update available tags list
                getAllContentTags();
            } else if (response.status === 204) {
                addToast('Cannot create Tag. Tag already exists!', {appearance: 'error', autoDismiss: true});
            }
        })
    };

    let getAllContentTags = () => {
        getAllTags().then((response) => {
            if (response.status === 200) {
                setAvailableTags(response.data);
            }
        });
    };

    let handleStoryFormChange = (targetName, targetValue) => {
        switch (targetName) {
            case 'storyTitle':
                setStoryTitle(targetValue);
                break;
            case 'asylumSeekerName':
                setAsylumSeekerName(targetValue);
                break;
            case 'countryOfOrigin':
                setCountryOfOrigin(targetValue);
                break;
            case 'description':
                setStoryDescription(targetValue);
                break;
        }
    };

    let checkStory = () => {
        story.state = 'PREPUBLISHED';
        saveStoryObject(story, true);
    };

    let deleteMapPointObject = () => {
        setLoadingResources(true);
        deleteMapPoint(storyID, currentMapPoint.current.id).then((response) => {
            setLoadingResources(false);
            if (response.status === 200) {
                setStory(response.data);

                setLoadingResources(true);
                getMapPointsByStoryID(response.data.contentID).then((response) => {
                    setLoadingResources(false);
                    if (response.status === 200){
                        setMapPointList(response.data);
                        updateCurrentMapPointAfterDeletion();
                    }

                }).catch((error) => {
                    setLoadingResources(false);
                });
                addToast('Map point successfully deleted!', {appearance: 'success', autoDismiss: true});
            } else {
                addToast('Error deleting map point.', {appearance: 'error', autoDismiss: true});
            }
        }).catch((error) => {
            setLoadingResources(false);
            addToast('Error deleting map point.', {appearance: 'error', autoDismiss: true});
        });
    };

    let updateElementsList = () => {
        getElementsByType();
    };

    let getElementsByType = () => {
        if (selectedElementsType === 'all') {
            getContentElements();
        } else if (selectedElementsType === 'image') {
            getImageElements();
        } else if (selectedElementsType === 'video') {
            getVideoElements();
        } else if (selectedElementsType === 'text') {
            getTextElements();
        } else if (selectedElementsType === 'audio') {
            getAudioElements();
        } else if (selectedElementsType === 'archived') {
            getArchivedElements();
        }
    };


    const filterLanguage = (elements) => {
        return elements.filter((element) => {
            return (contentElementListLanguage === '') || (element.language === contentElementListLanguage);
        });
    };

    let retrieveFileInformation = (res) => {
        let elements = Object.values(res)[0];
        return elements;
    };

    const filterNotArchived = (elements) => {
        return elements.filter(element => element.state !== 'ARCHIVED');
    };

    const getArchivedElements = () => {
        setSelectedElementsType('archived');
        loadingContentElementList(true);
        handleLoadingResources(true);
        axios.get("http://localhost:8080/api/content/" + "mappoints/" + currentMapPoint.current.id + "/elements/archived").then(res => {
            handleLoadingResources(false);
            if (res.status === 200) {
                let elements = filterLanguage(retrieveFileInformation(res));
                setElementsList(elements);
                setFilteredElements(elements);
                loadingContentElementList(false);
            } else {
                addToast('There was a problem retrieving the content.', {appearance: 'error', autoDismiss: true});
            }
        })
    };

    const getContentElements = () => {
        setSelectedElementsType('all');
        loadingContentElementList(true);
        handleLoadingResources(true);
        axios.get("http://localhost:8080/api/content/" + "mappoints/" + currentMapPoint.current.id + "/elements").then(res => {
            handleLoadingResources(false);
            if (res.status === 200) {
                let elements = filterLanguage(retrieveFileInformation(res));
                let notArchivedElements = filterNotArchived(elements);
                setElementsList(notArchivedElements);
                setFilteredElements(notArchivedElements);
                loadingContentElementList(false);
            } else {
                addToast('There was a problem retrieving the content.', {appearance: 'error', autoDismiss: true});
            }
        })
    };

//Retrieve all Text Content Elements
    const getTextElements = () => {
        setSelectedElementsType('text');
        loadingContentElementList(true);
        handleLoadingResources(true);
        axios.get("http://localhost:8080/api/content/" + "mappoints/" + currentMapPoint.current.id + "/elements/texts").then(res => {
            handleLoadingResources(false);
            if (res.status === 200) {
                let elements = filterLanguage(retrieveFileInformation(res));
                let notArchivedElements = filterNotArchived(elements);
                setElementsList(notArchivedElements);
                setFilteredElements(notArchivedElements);
                loadingContentElementList(false);
            } else {
                addToast('There was a problem retrieving the content.', {appearance: 'error', autoDismiss: true});
            }
        });
    };

//Retrieve all Image Content Elements
    const getImageElements = () => {
        setSelectedElementsType('image');
        loadingContentElementList(true);
        handleLoadingResources(true);
        axios.get("http://localhost:8080/api/content/" + "mappoints/" + currentMapPoint.current.id + "/elements/images").then(res => {
            handleLoadingResources(false);
            if (res.status === 200) {
                let elements = filterLanguage(retrieveFileInformation(res));
                let notArchivedElements = filterNotArchived(elements);
                setElementsList(notArchivedElements);
                setFilteredElements(notArchivedElements);
                loadingContentElementList(false);
            } else {
                addToast('There was a problem retrieving the content.', {appearance: 'error', autoDismiss: true});
            }
        })
    };

//Retrieve all Video Content Elements
    const getVideoElements = () => {
        setSelectedElementsType('video');
        loadingContentElementList(true);
        handleLoadingResources(true);
        axios.get("http://localhost:8080/api/content/" + "mappoints/" + currentMapPoint.current.id + "/elements/videos").then(res => {
            handleLoadingResources(false);
            if (res.status === 200) {
                let elements = filterLanguage(retrieveFileInformation(res));
                let notArchivedElements = filterNotArchived(elements);
                setElementsList(notArchivedElements);
                setFilteredElements(notArchivedElements);
                loadingContentElementList(false);
            } else {
                addToast('There was a problem retrieving the content.', {appearance: 'error', autoDismiss: true});
            }
        })
    };

//Retrieve all Audio Content Elements
    const getAudioElements = () => {
        setSelectedElementsType('audio');
        loadingContentElementList(true);
        handleLoadingResources(true);
        axios.get("http://localhost:8080/api/content/mappoints/" + currentMapPoint.current.id + "/elements/audios").then(res => {
            handleLoadingResources(false);
            if (res.status === 200) {
                let elements = filterLanguage(retrieveFileInformation(res));
                let notArchivedElements = filterNotArchived(elements);
                setElementsList(notArchivedElements);
                setFilteredElements(notArchivedElements);
                loadingContentElementList(false);
            } else {
                addToast('There was a problem retrieving the content.', {appearance: 'error', autoDismiss: true});
            }
        })
    };

    let createMapPoint = (zoomLevel, coordinates) => {
        //need to pass selected map zoom level and coordinates
        setLoadingResources(true);
        createMapPointRequest(storyID, zoomLevel, coordinates).then((response) => {
            setLoadingResources(false);
            if (response.status === 200) {
                let mapPoint = response.data;
                currentMapPoint.current = mapPoint;
                addToast('Story saved successfully!', {appearance: 'success', autoDismiss: true});
                let storyObj = null;
                getStory(storyID).then((response) => {
                    if (response.status === 200) {
                        storyObj = response.data;
                        setStory(storyObj);
                        addToast('Story saved successfully!', {appearance: 'success', autoDismiss: true});

                        setLoadingResources(true);
                        getMapPointsByStoryID(storyObj.contentID).then((response) => {
                            setLoadingResources(false);
                            if (response.status === 200){
                                setMapPointList(response.data);
                            }
                        }).catch((error) => {
                            setLoadingResources(false);
                        });
                    } else {
                        addToast('Error retrieving Story', {appearance: 'error', autoDismiss: true});
                    }
                });
            } else {
                addToast('Error saving Story', {appearance: 'error', autoDismiss: true});
            }
        }).catch((error) => {
            setLoadingResources(false);
        });
    };

    const confirmDeleteMapPoint = () => {
        swal({
            title: "Are you sure you want to delete this map point?",
            text: "Once deleted, you will not be able to recover this Map Point or any Content Elements it contains!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            closeOnEsc: true
        }).then((willDelete) => {
            if (willDelete) {
                deleteMapPointObject();
            }
        });
    };

    let editContentElement = (element) => {
        if (element.contentType !== 'text/html'){
            toggleEditElementModal();
            setEditableContentElement(element);
        }else{
            setEditableTextElement(element);
        }
    };

//navigation for map points that already exist
    let handlePageClick = (mapPointID) => {
        setLoadingResources(true);
        getMapPointByID(mapPointID).then((response) => {
            setLoadingResources(false);
            if (response.status === 200) {
                currentMapPoint.current = response.data;
                console.log(currentMapPoint.current);
                setMapPointPageClick(true);
            }
        }).catch((error) => {
            setLoadingResources(false);
            //currentMapPoint.current = story.mapPoints.find(mp => mp.id === mapPointID);
            currentMapPoint.current = mapPointList.find(mp => mp.id === mapPointID);
        })
    };

    let saveMapPointObject = (mapPoint) => {

        setLoadingResources(true);
        saveMapPoint(mapPoint).then((response) => {
            setLoadingResources(false);
            if (response.status === 200) {
                let mapPoint = response.data;
                currentMapPoint.current = mapPoint;
                addToast('Story saved successfully!', {appearance: 'success', autoDismiss: true});
                let story = null;
                setLoadingResources(true);
                getStory(storyID).then((response) => {
                    setLoadingResources(false);
                    if (response.status === 200) {
                        story = response.data;
                        let indexOfMapPoint = story.mapPoints.findIndex(existingMapPoint => existingMapPoint.id === mapPoint.id);
                        if (indexOfMapPoint !== -1) {
                            story.mapPoints[indexOfMapPoint] = mapPoint;
                        } else {
                            story.mapPoints.push(mapPoint);
                        }

                        saveStoryObject(story);
                    } else {
                        addToast('Error retrieving Story', {appearance: 'error', autoDismiss: true});
                    }
                }).catch((error) => {
                    setLoadingResources(false);
                });
            }
        }).catch((error) => {
            setLoadingResources(false);
        });

    };

    let saveStoryObject = (story, submitted) => {

        setLoadingResources(true);
        saveStory(story).then((response) => {
            setLoadingResources(false);
            if (response.status === 200) {
                let story = response.data;
                setStory(story);
                addToast('Story saved successfully!', {appearance: 'success', autoDismiss: true});

                setLoadingResources(true);
                getMapPointsByStoryID(story.contentID).then((response) => {
                    setLoadingResources(false);
                    if (response.status === 200){
                        setMapPointList(response.data);
                        currentMapPoint.current = response.data[0];
                    }
                }).catch((error) => {
                    setLoadingResources(false);
                });

                if (submitted){
                    history.push("/contentmanager");
                }
            } else {
                addToast('There was an error saving the story. Try again.', {
                    appearance: 'error',
                    autoDismiss: true
                });
            }
        }).catch((error) => {
            setLoadingResources(false);
            addToast('There was an error saving the story. Try again.', {appearance: 'error', autoDismiss: true});
        });
    };

    let nextStep = () => {

        if (activeStep === 1) {
            setStep(activeStep + 1)
        } else if (activeStep === 2) {
            setStep(activeStep + 1)
        } else if (activeStep === 3) {
            setStep(activeStep + 1)
        } else if (activeStep === 4) {
            checkStory();
        }
    };

    let checkMapPointsExist = () => {

        if (mapPointList !== null && mapPointList.length !== 0) {
            currentMapPoint.current = mapPointList[0];

            //Don't display map point selector if map points exist on load
            setContentElementsDisplay({display: 'flex'});
            setMappointSelectorDisplay({display: 'flex'});
            setMapPointAddDeleteDisplay({display: 'flex'});
        }
    };

    let handleLoadingResources = (isLoading) => {
        setLoadingResources(isLoading);
    };

    return (
        <div id="contentContainer" className="container is-fluid">
            <div className="tile is-ancestor">
                <div className="tile is-vertical is-parent">
                    <div className="tile is-parent">
                        <article id="editStoryHeader" className="tile is-child notification is-light box">
                            <div className="level">
                                <div className="level-left">
                                    <h1 className="title level-item">Editing
                                        Story: {story === null ? "" : story.title}</h1>
                                </div>
                                <div className="level-right">
                                    <div className="level-item">
                                        <div className="tags has-addons">
                                        <span
                                            className="tag is-danger is-light is-outlined is-medium">Story Status :</span>
                                            <span
                                                className="tag is-warning is-light is-medium">{story === null ? "" : story.state}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>

                    </div>
                    <div className="tile is-parent is-vertical">
                        <div id="storyTimeline" className="tile is-child box"  >
                            <StoryCreationProgress setStep={setStep} step={activeStep}/>
                        </div>
                        <progress className="progress is-small is-primary" max="100"
                                  value={loadingResources ? "" : 100}  >15%
                        </progress>
                    </div>
                    {stepOneTransition.map(
                        ({item, key, props}) =>
                            item && (
                                <animated.div className="tile" key={key} style={props}>
                                    <div className="tile is-parent">
                                        <div className="tile is-child box">
                                            {
                                                story !== null && <StoryForm story={story} creator={props.user_id}
                                                                             title={storyTitle}
                                                                             asylumSeekerName={asylumSeekerName}
                                                                             countryOfOrigin={countryOfOrigin}
                                                                             description={storyDescription}
                                                                             handleStoryFormChange={handleStoryFormChange}/>
                                            }
                                        </div>
                                    </div>
                                </animated.div>
                            )
                    )}
                    {stepTwoTransition.map(
                        ({item, key, props}) =>
                            item && (
                                <animated.div key={key} style={props}>
                                    <div className="tile is-parent" style={mappointSelectorDisplay} >
                                        <div id="coordinate-picker-container" className="tile is-child box" ref={mapScroll}>
                                            <h1 className="title is-size-4">Select a Location on the Map for this
                                                Map
                                                Point</h1>
                                                <CoordinatePicker
                                                    setCoordinatesAndZoomLevel={setCoordinatesAndZoomLevel}
                                                    currentMapPoint={currentMapPoint}
                                                    setCurrentMapPoint={null/*setCurrentMapPoint*/}/>
                                        </div>
                                    </div>
                                    <div id="editor-elementlist-tile" className="tile"
                                         style={contentElementsDisplay} ref={scroll}>
                                        <div className="tile is-parent is-7">
                                            <div id="editor-container" className="tile is-child box">
                                                {
                                                    currentMapPoint.current !== null && currentMapPoint.current !== undefined &&
                                                    <Editor onEditorSubmit={handleEditorSubmit}
                                                            handleLoadingResources={handleLoadingResources}
                                                            mapPoint={currentMapPoint}
                                                            storyID={storyID}
                                                            updateElementsList={updateElementsList}
                                                            setStory={setStory}
                                                            editableTextElement={editableTextElement}
                                                            setEditableTextElement={setEditableTextElement}/>
                                                }
                                            </div>
                                        </div>
                                        <div className="tile is-parent">
                                            <div className="tile is-child box">
                                                {
                                                    currentMapPoint.current !== null && currentMapPoint.current !== undefined && story !== null &&
                                                    <ContentElementList editorSubmitted={editorSubmitted}
                                                                        editContentElement={editContentElement}
                                                                        language={contentElementListLanguage}
                                                                        loadingContentElementList={loadingContentElementList}
                                                                        mapPoint={currentMapPoint}
                                                                        story={story}
                                                                        mapPoints={mapPointList}
                                                                        handleLoadingResources={handleLoadingResources}
                                                                        setStory={setStory}
                                                                        storyID={storyID}
                                                                        mapPointID={currentMapPoint.current.id}
                                                                        elementsList={elementsList}
                                                                        setFilteredElements={setFilteredElements}
                                                                        filterLanguage={filterLanguage}
                                                                        selectedElementsType={selectedElementsType}
                                                                        filteredElements={filteredElements}
                                                                        getContentElements={getContentElements}
                                                                        getImageElements={getImageElements}
                                                                        getAudioElements={getAudioElements}
                                                                        getVideoElements={getVideoElements}
                                                                        getTextElements={getTextElements}
                                                                        getArchivedElements={getArchivedElements}
                                                                        updateElementsList={updateElementsList}/>
                                                }
                                                <div className="field is-horizontal level">
                                                    <div id="content-elements-language"
                                                         className="field-label is-normal level-item">
                                                        <label className="label is-size-7 ">Content Elements Display
                                                            Language:</label>
                                                    </div>
                                                    <div className="field-body level-item">
                                                        <div className="field is-expanded ">
                                                            <div className="control has-icons-left">
                                                                <div
                                                                    className={contentElementListLoading ? "select is-small is-loading" : "select is-small"}>
                                                                    <select id="content-element-list-langs"
                                                                            defaultValue={'English'}>
                                                                        <option value='' onClick={() => {
                                                                            setContentElementListLang('')
                                                                        }}>All
                                                                        </option>
                                                                        {
                                                                            langCodes.getAllCodes().map((item, key) =>
                                                                                <option key={item}
                                                                                        value={item}
                                                                                        style={{width: '5vw'}}
                                                                                        onClick={() => {
                                                                                            setContentElementListLang(item)
                                                                                        }}>{langCodes.getName(item)}</option>
                                                                            )
                                                                        }
                                                                    </select>
                                                                </div>
                                                                <span className="icon is-small is-left">
                                                                        <i className="fas fa-globe"> </i>
                                                                    </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="has-text-centered">
                                                    <button className="button is-info is-small"
                                                            onClick={toggleUploadModal}>Upload
                                                        Content
                                                        Element
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="tile" style={mapPointAddDeleteDisplay}>
                                        <div className="tile is-parent is-7">
                                            <div className="tile box is-child">
                                                <MapPointSelector mapPoints={mapPointList}
                                                                  currentMapPoint={currentMapPoint}
                                                                  handlePageClick={handlePageClick}
                                                                  updateElementsList={updateElementsList}
                                                                  mapPointPageClick={mapPointPageClick}
                                                                  setMapPointPageClick={setMapPointPageClick}
                                                />
                                            </div>
                                        </div>
                                        <div className="tile is-parent">
                                            <div className="tile is-child box">
                                                <div className="level">
                                                    <button className="button is-primary level-left is-small"
                                                            onClick={() => handleAddNewMapPoint()}>
                                                        <span className="icon">
                                                        <i className="far fa-plus-square"> </i>
                                                        </span>
                                                        <span>Add New Map Point</span>
                                                    </button>
                                                    <button className="button is-danger level-right is-small"
                                                            onClick={() => confirmDeleteMapPoint()}>
                                                        <span className="icon">
                                                        <i className="fas fa-trash-alt"> </i>
                                                        </span>
                                                        <span>Delete Current Map Point</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </animated.div>
                            ))}
                    {stepThreeTransition.map(
                        ({item, key, props}) =>
                            item && (
                                <animated.div className="tile" key={key} style={props}>
                                    <div className="tile is-parent">
                                        <div className="tile is-child box">
                                            <h1 id="select-content-tags" className="title is-4">Select Content
                                                Tags to add to the Story</h1>
                                            <div className="tile is-parent">
                                                <div className="tile is-child">
                                                    <ContentTags story={story}
                                                                 handleContentTags={handleContentTags}
                                                                 contentTags={contentTags}
                                                                 availableTags={availableTags}
                                                                 setStory={setStory}/>
                                                    <br/>
                                                    <div className="is-divider"></div>
                                                    <h4 className="title is-4 has-text-centered">Active Content Warning
                                                        Tags</h4>
                                                    { story.tags !== null && story.tags !== undefined &&
                                                        <ContentTagDisplay tagList={story.tags} setStory={setStory}
                                                                           story={story}/>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </animated.div>
                            ))}
                    {stepFourTransition.map(
                        ({item, key, props}) =>
                            item && (
                                <div key={1}>
                                    <section className={storyReadyForReview ? "hero is-success" : "hero is-danger"}>
                                        <div className="hero-body">
                                            <div className="container">
                                                <h1 className="title">
                                                    {storyReadyForReview ? "Story ready for Submission" : "Story not ready for Submission"}
                                                </h1>
                                                <h2 className="subtitle">
                                                    {storyReadyForReview ? "Story is ready for review! Click the button below to submit." : "Please fill out the Story details in the Details section."}
                                                </h2>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            ))}
                    <div className="tile is-parent">
                        <div className="level tile box is-child is-12">
                            <div className="level">
                                <div className="level-left">
                                    <div className="level-item">
                                        <button className="button is-primary is-rounded" disabled={activeStep === 1}
                                                onClick={() => setStep(activeStep - 1)}>
                                            Previous Step: {activeStep === 2 ? "Enter Details" : (activeStep === 3 ? "Create Map Points" : (activeStep === 4 ? "Create Content Tags": ""))}
                                        </button>
                                    </div>
                                </div>
                                <div className="level-right">
                                    <div className="level-item">
                                        <button
                                            className="button is-primary is-rounded"
                                            onClick={() => nextStep()}
                                            disabled={activeStep === 4 && !storyReadyForReview}>{activeStep === 4 ? 'Submit Story for Review' : (activeStep === 3 ? 'Next Step: Finish Story Creation': (activeStep === 2 ? "Next Step: Create Content Tags": "Next Step: Create Map Points"))}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {currentMapPoint.current !== null && currentMapPoint.current !== undefined &&
                <UploadElement
                    closeModal={toggleUploadModal}
                    modalState={uploadModalState}
                    handleLoadingResources={handleLoadingResources}
                    storyID={storyID}
                    mapPointID={currentMapPoint.current.id}
                    updateElementsList={updateElementsList}
                    setStory={setStory}
                />
                }
                {
                    currentMapPoint.current !== null && editableContentElement !== null && editableContentElement !== undefined &&
                    < EditContentElement closeModal={toggleEditElementModal} modalState={editElementModalState}
                                         contentElement={editableContentElement}
                                         handleLoadingResources={handleLoadingResources}
                                         storyID={storyID}
                                         mapPointID={currentMapPoint.current.id}
                                         setStory={setStory}/>
                }
            </div>
        </div>
    );
}


const mapStateToProps = (state) => {
    return {
        user_id: state.authenticationReducer.user_id,
    };
};


ContentManager = connect(mapStateToProps)(ContentManager);
