import React, {forwardRef, useEffect} from 'react';
import {useToasts} from "react-toast-notifications";
import {useHistory} from "react-router-dom";
import {useState} from "react";
import swal from "sweetalert";
import {deleteStory} from "../../libraries/deleteStory";
import {getStory} from "../../libraries/getStory";
import {saveStory} from "../../libraries/saveStory";
import {createStory} from "../../libraries/createStory";
import {connect} from "react-redux";
import MaterialTable from "material-table";

import PublicIcon from '@material-ui/icons/Public';
import AddBox from '@material-ui/icons/AddBox';
import Archive from '@material-ui/icons/Archive';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import UnarchiveIcon from '@material-ui/icons/Unarchive';
import LockIcon from '@material-ui/icons/Lock';
import DeleteIcon from '@material-ui/icons/Delete';
import PersonIcon from '@material-ui/icons/Person';
import AddCircleIcon from '@material-ui/icons/AddCircle';

import ViewColumn from '@material-ui/icons/ViewColumn';
import {getArchivedStoryList} from "../../libraries/getArchivedStoryList";
import green from "@material-ui/core/colors/green";
import './sass/manageStories.scss';

/**
 * Displays every Story saved in the database. Every Story that is not archived can be edited, archived, published,
 * or deleted. Archived Stories can be view by clicking a separate button. Stories can be sorted, filtered, and searched for by different criteria.
 * @param props props received from ManageStories.jsx
 * @returns {*}
 * @constructor
 */
export function FilterTable(props) {

    const {addToast} = useToasts();
    const history = useHistory();
    const [rows, setRows] = useState(props.storyList);
    const [archivedStories, setArchivedStories] = useState([]);
    const [displayArchivedStories, setDisplayArchivedStories] = useState(false);
    const [displayOwnedStories, setDisplayOwnedStories] = useState(false);
    const [loadingResources, setLoadingResources] = useState(false);
    const pageLoaderStyles = {display: 'flex', width: '100%', height: '100vh', top: '30%', position: 'static'};

    useEffect(() => {
        if (displayArchivedStories) {
            let archivedStories = props.storyList.filter(story => story.state === 'ARCHIVED');
            setRows(archivedStories);
        } else if (displayOwnedStories){
            let ownedStories = props.storyList.filter(story => story.creator === props.user_id);
            setRows(ownedStories);
        } else {
            let nonArchivedStories = props.storyList.filter(story => story.state !== 'ARCHIVED');
            setRows(nonArchivedStories);
        }
    }, [displayArchivedStories, displayOwnedStories, props.storyList]);

    const tableIcons = {
        Publish: forwardRef((props, ref) => <PublicIcon {...props} ref={ref}/>),
        Edit: forwardRef((props, ref) => <Edit {...props} ref={ref}/>),
        Search: forwardRef((props, ref) => <Search {...props} ref={ref}/>),
        Add: forwardRef((props, ref) => <AddBox {...props} ref={ref}/>),
        Check: forwardRef((props, ref) => <Check {...props} ref={ref}/>),
        Clear: forwardRef((props, ref) => <Clear {...props} ref={ref}/>),
        Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref}/>),
        DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref}/>),
        Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref}/>),
        Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref}/>),
        FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref}/>),
        LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref}/>),
        NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref}/>),
        PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref}/>),
        ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref}/>),
        SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref}/>),
        ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref}/>),
        ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref}/>),
        UnarchiveIcon: forwardRef((props, ref) => <UnarchiveIcon {...props} ref={ref}/>),
        LockIcon: forwardRef((props, ref) => <LockIcon {...props} ref={ref}/>),
        DeleteIcon: forwardRef((props, ref) => <DeleteIcon {...props} ref={ref}/>),
        PersonIcon: forwardRef((props, ref) => <PersonIcon {...props} ref={ref}/>),
        AddCircleIcon: forwardRef((props, ref) => <AddCircleIcon {...props} ref={ref}/>)
    };

    const confirmDeleteStory = (story) => {
        swal({
            title: "Are you sure you want to delete this Story?",
            text: "Once deleted, you will not be able to recover this Story or any Map Points it contains!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            closeOnEsc: true
        }).then((willDelete) => {
            if (willDelete) {
                deleteStoryObject(story);
            }
        });
    };

    const confirmPublishStory = (story) => {
        swal({
            title: "Are you sure you want to Publish this Story?",
            text: "This will make the Story publicly viewable!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            closeOnEsc: true
        }).then((willPublish) => {
            if (willPublish) {
                publishStory(story);
            }
        });
    };

    const confirmUnpublishStory = (story) => {
        swal({
            title: "Are you sure you want to Unpublish this Story?",
            text: "This will make the Story only privately viewable.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            closeOnEsc: true
        }).then((willUnPublish) => {
            if (willUnPublish) {
                unPublishStory(story);
            }
        });
    };

    let deleteStoryObject = (story) => {
        setLoadingResources(true);
        deleteStory(story.contentID).then((response) => {
            setLoadingResources(false);
            if (response.status === 200) {
                addToast('Story successfully deleted!', {appearance: 'success', autoDismiss: true});

                //refresh table
                props.refreshStoryTable();
            } else {
                addToast('Error deleting Story.', {appearance: 'error', autoDismiss: true});
            }
        }).catch((error) => {
            addToast('Error deleting Story.', {appearance: 'error', autoDismiss: true});
        })
    };

    let editStoryObject = (story) => {
        setLoadingResources(true);
        getStory(story.contentID).then((response) => {
            setLoadingResources(false);
            if (response.status === 200) {
                history.push("/contentmanager/stories/" + response.data.contentID);
            } else {
                addToast('Could not open Story for editing.', {appearance: 'error', autoDismiss: true});
            }
        }).catch((error) => {
            addToast('Could not open Story for editing.', {appearance: 'error', autoDismiss: true});
        });
    };

    let publishStory = (story) => {
        story.state = "PUBLISHED";
        setLoadingResources(true);
        saveStory(story).then((response) => {
            setLoadingResources(false);
            addToast('Story successfully Published!', {appearance: 'success', autoDismiss: true});
            props.refreshStoryTable();
        }).catch((error) => {
            addToast('An error occured. Story has not been Published.', {appearance: 'error', autoDismiss: true});
        });
    };

    let unPublishStory = (story) => {
        story.state = "PREPUBLISHED";
        setLoadingResources(true);
        saveStory(story).then((response) => {
            setLoadingResources(false);
            addToast('Story successfully Unpublished!', {appearance: 'success', autoDismiss: true});
            props.refreshStoryTable();
        }).catch((error) => {
            addToast('An error occured. Story has not been unpublished.', {appearance: 'error', autoDismiss: true});
        });
    };

    /** Handles archiving and un-archiving Stories. Stories that are un-archived go into DRAFT state.*/
    let archiveStoryObject = (rowData, action) => {
        let contentID = rowData.contentID;
        setLoadingResources(true);
        getStory(contentID).then((response) => {
            setLoadingResources(false);
            if (response.status === 200) {
                let story = response.data;
                story.state = action;

                setLoadingResources(true);
                saveStory(story).then((response) => {
                    setLoadingResources(false);
                    if (response.status === 200) {
                        addToast('Story successfully put into ' + action + ' state!', {
                            appearance: 'success',
                            autoDismiss: true
                        });

                        //update table
                        props.refreshStoryTable();
                    } else {
                        addToast('Error putting Story into ' + action + ' state!', {
                            appearance: 'error',
                            autoDismiss: true
                        });
                    }
                }).catch((error) => {
                    addToast('Error putting Story into ' + action + ' state!', {
                        appearance: 'error',
                        autoDismiss: true
                    });
                });
            } else {
                addToast('Error putting Story into ' + action + ' state!', {
                    appearance: 'error',
                    autoDismiss: true
                });
            }
        }).catch((error) => {
            addToast('Error putting Story into ' + action + ' state!', {
                appearance: 'error',
                autoDismiss: true
            });
        });
    };

    let getArchivedStories = () => {
        setLoadingResources(true);
        getArchivedStoryList().then((res) => {
            setLoadingResources(false);
            if (res.status === 200) {
                setArchivedStories(res.data);
                setDisplayArchivedStories(true);
            }
        })
    };

    let setStoryDisplay = () => {
        if (displayArchivedStories) {
            setDisplayArchivedStories(false);
            props.refreshStoryTable();
        } else {
            getArchivedStories()
        }
    };

    let createNewStory = () => {
        createStory(props.user_id).then((response) => {
            if (response.status === 200){
                let newStory = response.data;
                history.push("/contentmanager/stories/" + newStory.contentID);
            }else{
                //display message and update other UI elements
                addToast('Error creating Story', {appearance: 'error', autoDismiss: true});
            }
        }).catch((error) => {
            addToast('Error creating Story', {appearance: 'error', autoDismiss: true});
        });
    };

    return (
        <>
                <progress className="progress is-small is-primary" max="100"
                          value={loadingResources ? "" : 100}>15%
                </progress>
                    <MaterialTable
                        title="Manage Stories"
                        icons={tableIcons}
                        columns={
                            [
                                {title: 'Title', field: 'title'},
                                {field: 'asylumSeekerName', title: 'Asylum Seeker Name'},
                                {field: 'countryFull', title: 'Country of Origin'},
                                {field: 'creatorUser.creatorUserName', title: 'Creator'},
                                {field: 'state', title: 'Status'}
                            ]
                        }
                        data={rows}
                        actions={[
                            rowData => ({
                                icon: () => <Edit color='primary'/>,
                                tooltip: 'Edit Story',
                                onClick: (event, rowData) => editStoryObject(rowData),
                                hidden: rowData.state === 'ARCHIVED'
                            }),
                            rowData => ({
                                icon: () => <DeleteIcon color='secondary'/>,
                                tooltip: 'Delete Story',
                                onClick: (event, rowData) => confirmDeleteStory(rowData),
                            }),
                            rowData => ({
                                icon: () => <Archive color="error"/>,
                                tooltip: 'Archive Story',
                                onClick: (event, rowData) => archiveStoryObject(rowData, 'ARCHIVED'),
                                hidden: rowData.state === 'ARCHIVED',
                            }),
                            rowData => ({
                                icon: () => <UnarchiveIcon color='error'/>,
                                tooltip: 'Unarchive Story',
                                onClick: (event, rowData) => archiveStoryObject(rowData, 'DRAFT'),
                                hidden: rowData.state !== 'ARCHIVED',
                            }),
                            rowData => ({
                                icon: () => <PublicIcon style={{color: green[500]}}/>,
                                tooltip: 'Publish Story',
                                onClick: (event, rowData) => confirmPublishStory(rowData),
                                hidden: rowData.state !== 'PREPUBLISHED',
                            }),
                            rowData => ({
                                icon: () => <LockIcon/>,
                                tooltip: 'Unpublish Story',
                                onClick: (event, rowData) => confirmUnpublishStory(rowData),
                                hidden: rowData.state !== 'PUBLISHED'
                            }),
                            {
                                icon: () => displayArchivedStories ? "View All Stories" : "View Archived Stories",
                                tooltip: "View Archived Stories",
                                isFreeAction: true,
                                onClick: (event) => setStoryDisplay(),
                            },
                            {
                                icon: () => <AddCircleIcon color='primary'/>,
                                tooltip: "Create a New Story",
                                isFreeAction: true,
                                onClick: (event) => createNewStory(),
                            }
                        ]}
                    />
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        user_id: state.authenticationReducer.user_id,
    };
};


FilterTable = connect(mapStateToProps)(FilterTable);

/*
            <progress className="progress is-small is-primary" max="100"
                      value={loadingResources ? "" : 100}>15%
            </progress>
 */
