import React, {useState} from 'react';
import {useToasts} from 'react-toast-notifications';
import axios from "axios";
import "./sass/uploadelement.scss";
import no_file_selected from './nofileselected.jpg';
import invalid_file from './invalid_file.jpg';
import {UploadPreview} from "./UploadPreview";
import langCodes from "iso-639-1";

/**
 * A modal for uploading a new ContentElement file. Acceptable file types are images, videos, and audio files. File size limit is 100 MB.
 * @param props props received from ContentManager.jsx
 * @returns {null|*}
 * @constructor
 */
export function UploadElement(props) {

    const {addToast} = useToasts();
    const [fileName, setFileName] = useState("No file selected");
    const [file, setFile] = useState(null);
    const [uploadSrc, setUploadSrc] = useState(no_file_selected);
    const [uploadedImageStyle, setUploadedImageStyle] = useState({display: 'block'});
    const [uploadedAudioStyle, setUploadedAudioStyle] = useState({display: 'none'});
    const [uploadedVideoStyle, setUploadedVideoStyle] = useState({display: 'none'});
    const [captionDivStyle, setCaptionDivStyle] = useState({display: 'none'});
    const [descriptionDivStyle, setDescriptionDivStyle] = useState({display: 'none'});
    const [audioType, setAudioType] = useState(null);
    const [imgWidth, setImgWidth] = useState(null);
    const [imgHeight, setImgHeight] = useState(null);
    const [videoDuration, setVideoDuration] = useState(null);
    const [audioDuration, setAudioDuration] = useState(null);
    const [fileIsValid, setFileIsValid] = useState(false);
    const [uploadType, setUploadType] = useState("image");
    const [uploadDisabled, setUploadDisabled] = useState(false);
    const [uploadInfoStyle, setUploadInfoStyle] = useState({display: 'none'});
    const [uploadLanguage, setUploadLanguage] = useState('en');
    const [uploadInProgress, setUploadInProgress] = useState(false);

    //upload limit in bytes
    const FILE_UPLOAD_LIMIT = 100000000;

    let fileRef = React.createRef();
    let uploadElementModal = React.createRef();
    let caption = React.createRef();
    let description = React.createRef();
    let filePath = "";

    if (!props.modalState) {
        if (uploadInfoStyle.display === 'block') {
            setUploadInfoStyle({display: 'none'});
        }
        if (file !== null) {
            setFile(null);
        }
        if (fileName !== 'No file selected') {
            setFileName("No file selected");
        }
        if (fileIsValid !== false) {
            setFileIsValid(false);
        }
        if (uploadDisabled !== true) {
            setUploadDisabled(true);
        }

        if (uploadSrc !== no_file_selected) {
            setUploadSrc(no_file_selected);
        }

        if (uploadedImageStyle.display !== 'block') {
            setUploadedImageStyle({display: 'block'});
        }

        if (uploadedAudioStyle.display === 'block') {
            setUploadedAudioStyle({display: 'none'});
        }

        if (uploadedVideoStyle.display === 'block') {
            setUploadedVideoStyle({display: 'none'});
        }


        return null;
    }


    let handleLanguageChange = (event) => {
        setUploadLanguage(event.target.value)
    };

    const onUpload = (e) => {

        if (caption !== null && caption !== undefined && caption.current.value.length < 151) {

            if (fileIsValid) {
                const formData = new FormData();

                if (file.type.substring(0, 5) === 'image') {
                    formData.append('imgWidth', imgWidth);
                    formData.append('imgHeight', imgHeight);
                    formData.append('length', 0);
                    formData.append('imgCaption', caption.current.value);
                } else if (file.type.substring(0, 5) === 'video') {
                    formData.append('length', videoDuration);
                    formData.append('imgCaption', null);
                    formData.append('imgWidth', 0);
                    formData.append('imgHeight', 0);
                } else if (file.type.substring(0, 5) === 'audio') {
                    formData.append('length', audioDuration);
                    formData.append('imgCaption', null);
                    formData.append('imgWidth', 0);
                    formData.append('imgHeight', 0);
                }

                formData.append('file', file);
                formData.append('mapPointID', props.mapPointID);
                formData.append('language', uploadLanguage);
                formData.append('description', description.current.value);
                formData.append('contentID', props.storyID);

                let config = {
                    headers: {
                        'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
                    }
                };


                caption.current.value = '';
                description.current.value = '';
                setUploadInProgress(true);
                props.handleLoadingResources(false);
                props.handleLoadingResources(true);
                axios.post("http://localhost:8080/api/content/upload", formData, config).then(res => {
                    setUploadInProgress(false);
                    props.handleLoadingResources(false);
                    if (res.status === 200) {
                        addToast('File uploaded successfully!', {appearance: 'success', autoDismiss: true});
                        setUploadSrc(no_file_selected);
                        setUploadType("image");
                        setFileName("No file selected");
                        setFile(null);
                        setFileIsValid(false);
                        setUploadDisabled(true);
                        setUploadedAudioStyle({display: 'none'});
                        setUploadedVideoStyle({display: 'none'});
                        setUploadedImageStyle({display: 'block'});
                        props.updateElementsList();
                        props.setStory(res.data);
                    } else if (res.status === 304) {
                        addToast('A file with that name already exists!.', {appearance: 'error', autoDismiss: true});
                    } else {
                        addToast('A file with that name already exists!.', {appearance: 'error', autoDismiss: true});
                    }
                }).catch((error) => {
                    addToast('A file with that name already exists!.', {appearance: 'error', autoDismiss: true});
                    setUploadInProgress(false);
                });
            }
        }else{
            addToast('Maximum length for image caption is 150 characters.', {appearance: 'error', autoDismiss: true});
        }
    };

    const handleDescriptionChange = () => {
       if (description.current.value.trim() !== '' && fileIsValid){
           setUploadDisabled(false);
       }else{
           setUploadDisabled(true);
       }
    };

    const fileChangeHandler = (e) => {
        e.preventDefault();
        let file = fileRef.current.files[0];
        filePath = fileRef.current.value;
        let validFile = false;
        //100MB upload limit
        if (file.size < FILE_UPLOAD_LIMIT) {
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onloadend = function (e) {
                let contentType = file.type.substring(0, 5);
                if (contentType === 'image') {
                    let img = new Image();
                    img.src = e.target.result;
                    setImgWidth(img.width);
                    setImgHeight(img.height);
                    setUploadSrc(reader.result);
                    setUploadInfoStyle({display: 'block'});
                    setUploadedAudioStyle({display: 'none'});
                    setUploadedVideoStyle({display: 'none'});
                    setUploadType("image");
                    setCaptionDivStyle({display: 'block'});
                    setDescriptionDivStyle({display: 'block'});
                    validFile = true;
                } else if (contentType === 'video') {
                    setUploadInfoStyle({display: 'block'});
                    setUploadedVideoStyle({display: 'block'});
                    setUploadedAudioStyle({display: 'none'});
                    setUploadedImageStyle({display: 'none'});
                    setCaptionDivStyle({display: 'none'});
                    setDescriptionDivStyle({display: 'block'});
                    setUploadSrc(reader.result);
                    setUploadType("video");
                    let media = new Audio(reader.result);
                    media.onloadedmetadata = () => {
                        setVideoDuration(media.duration);
                    };
                    validFile = true;
                } else if (contentType === 'audio') {
                    setUploadInfoStyle({display: 'block'});
                    setUploadedVideoStyle({display: 'none'});
                    setUploadedAudioStyle({display: 'block'});
                    setUploadedImageStyle({display: 'none'});
                    setCaptionDivStyle({display: 'none'});
                    setDescriptionDivStyle({display: 'block'});
                    setUploadType("audio");
                    setUploadSrc(reader.result);
                    setAudioType(file.type);
                    let media = new Audio(reader.result);
                    media.onloadedmetadata = () => {
                        if (media.duration === Infinity){
                           setAudioDuration(36000);
                        }else{
                            setAudioDuration(media.duration);
                        }
                    };
                    validFile = true;
                } else {
                    setUploadInfoStyle({display: 'none'});
                    setUploadType("image");
                    setUploadSrc(invalid_file);
                    setCaptionDivStyle({display: 'none'});
                    setDescriptionDivStyle({display: 'none'});
                    validFile = false;
                    setUploadDisabled(true);
                }

                if (validFile) {
                    setFile(file);
                    setFileName(filePath.substring(12));
                    setFileIsValid(true);
                }
            };

        } else {
            addToast('File size is too large! Maximum file size is 100 MB', {appearance: 'success', autoDismiss: true});
        }
    };

    return (
        <div className="modal is-active" id="uploadElementModal" ref={uploadElementModal}>
            <div className="modal-background" onClick={props.closeModal}> </div>
            <div id="uploadModal" className="modal-card">
                <header className="modal-card-head">
                    <p className="modal-card-title">Upload File</p>
                    <button id="closeModal" onClick={props.closeModal} className="delete" aria-label="close"> </button>
                </header>
                <section className="modal-card-body">
                    <div className="columns is-centered">
                        <div className="column is-full">
                            <h1 id="file-upload-header" className="is-size-5">Upload an Audio, Video, or Image file (Maximum file size is 100 MB)</h1>
                            <br/>
                            <div className="file has-name">
                                <label id="upload-file-label" className="file-label">
                                    <input type="file" id="fileInput" ref={fileRef} className="file-input"
                                           name="upload-btn"
                                           onChange={fileChangeHandler} accept="image/*,audio/*,video/*"/>
                                    <span className="file-cta">
                                            <span className="file-icon">
                                                <i className="fas fa-upload"> </i>
                                            </span>
                                            <span className="file-label">
                                                Choose a file...
                                            </span>
                                        </span>
                                    <button className="file-name button">
                                        {fileName}
                                    </button>
                                </label>
                            </div>
                            <br/>
                            <UploadPreview src={uploadSrc} imageStyle={uploadedImageStyle}
                                           audioStyle={uploadedAudioStyle} videoStyle={uploadedVideoStyle}
                                           uploadType={uploadType}/>
                            <br/>
                            <div className="is-divider"> </div>
                            <div style={uploadInfoStyle}>
                                <div className="field">
                                    <div className="control" style={captionDivStyle}>
                                        <label className="label">Enter a caption for the image (maximum 150 characters):</label>
                                        <input className="input" ref={caption} type="text"
                                               placeholder="(Optional) Enter a caption for the image" required/>
                                    </div>
                                </div>
                                <div className="field">
                                    <div className="control" style={descriptionDivStyle}>
                                        <label className="label">Enter a description for the file:
                                        </label>
                                        <textarea className="textarea" ref={description} style={{resize: 'none'}}
                                                  placeholder="Enter description here." required onChange={handleDescriptionChange}/>
                                    </div>
                                </div>
                                <div id="upload-language" className="field is-horizontal level">
                                    <div className="level-left">
                                        <label className="label level-item">Language of uploaded file:</label>
                                    </div>
                                    <div className="level-right">
                                        <div className="field-body level-item">
                                            <div className="field is-expanded">
                                                <div className="control has-icons-left">
                                                    <div className="select is-fullwidth">
                                                        <select name="language"
                                                                defaultValue={'English'}
                                                                onChange={handleLanguageChange}>
                                                            <option value='English'>English</option>
                                                            {
                                                                langCodes.getAllCodes().map((item, key) =>
                                                                    <option key={item}
                                                                            value={item}>{langCodes.getName(item)}</option>
                                                                )
                                                            }
                                                        </select>
                                                    </div>
                                                    <span className="icon is-small is-left">
                                            <i className="fas fa-globe"> </i>
                                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <footer id="modalFooter" className="modal-card-foot">
                    <button className={uploadInProgress ? "button is-primary is-loading" : "button is-primary"} onClick={e => onUpload(e)}
                            disabled={uploadDisabled}>Upload
                    </button>
                    <button id="cancelModal" onClick={props.closeModal} className="button">Cancel</button>
                </footer>
            </div>
        </div>
    );
}
