import React, {useEffect} from "react";
import {ContentListElement} from './ContentListElement';
import "./sass/contentelementlist.scss";

/**
 * Component that holds a list of individual ContentListElement components.
 * @param props props from parent component
 * @returns {null|*}
 * @constructor
 */
export function ContentElementList(props) {
    let searchInput = React.createRef();

    useEffect(() => {
        if (props.mapPoint.current !== undefined) {
            props.updateElementsList();
        }
    }, [props.language]);

    let handleTextInput = () => {
        let query = searchInput.current.value;
        if (query.trim() !== '') {
            const result = props.elementsList.filter(el => el.filePath.toLowerCase().startsWith(query.toLowerCase()));
            props.setFilteredElements(props.filterLanguage(result));
        } else {
            props.setFilteredElements(props.filterLanguage(props.elementsList));
        }
    };

    let editContentElement = (element) => {
        props.editContentElement(element);
    };

    if (props.mapPoint.current !== null) {

        return (
            <article className="panel list-panel">
                <p className="panel-heading">
                    Map Point {props.mapPoints.findIndex(mp => mp.id === props.mapPoint.current.id) + 1} Content Elements
                </p>
                <p className="panel-tabs">
                    <a className={props.selectedElementsType === 'all' ? 'is-active' : ''}
                       onClick={props.getContentElements}>All</a>
                    <a className={props.selectedElementsType === 'text' ? 'is-active' : ''}
                       onClick={props.getTextElements}>Text</a>
                    <a className={props.selectedElementsType === 'image' ? 'is-active' : ''}
                       onClick={props.getImageElements}>Image</a>
                    <a className={props.selectedElementsType === 'video' ? 'is-active' : ''}
                       onClick={props.getVideoElements}>Video</a>
                    <a className={props.selectedElementsType === 'audio' ? 'is-active' : ''}
                       onClick={props.getAudioElements}>Audio</a>
                    <a className={props.selectedElementsType === 'archived' ? 'is-active' : ''}
                       onClick={props.getArchivedElements}>Archived</a>
                </p>
                <div className="panel-block">
                    <p className="control has-icons-left">
                        <input className="input is-primary" type="text" placeholder="Search" onChange={handleTextInput}
                               ref={searchInput}/>
                        <span className="icon is-left">
                        <i className="fas fa-search" aria-hidden="true"> </i>
                    </span>
                    </p>
                </div>
                {
                    props.filteredElements !== null && props.filteredElements !== undefined && props.filteredElements.length > 0 &&
                    props.filteredElements.map((item) => (
                        <ContentListElement key={item.id}
                                            item={item}
                                            updateElementList={props.updateElementsList}
                                            editContentElement={editContentElement}
                                            handleLoadingResources={props.handleLoadingResources}
                                            setStory={props.setStory}
                                            storyID={props.storyID}
                                            mapPointID={props.mapPointID}/>
                    ))
                }

                {
                    props.filteredElements === null || props.filteredElements === undefined || props.filteredElements.length === 0 &&
                    <a className="panel-block">
                            <span className="panel-icon">
                            <i className="fas fa-warning" aria-hidden="true"> </i>
                            </span>
                        <b>No files to display!</b>
                    </a>
                }

            </article>
        );
    } else {
        return null;
    }
}

