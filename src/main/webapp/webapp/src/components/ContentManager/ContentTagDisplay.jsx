import React from 'react';
import {useToasts} from "react-toast-notifications";
import {removeTagFromStory} from "../../libraries/removeTagFromStory";
import './sass/contentTagsStyles.scss';

/**
 * Component that displays content warning tags that have been applied to the Story currently being edited.
 * @param props props received from ContentManager.jsx
 * @returns {*}
 * @constructor
 */
export function ContentTagDisplay(props) {

    const {addToast} = useToasts();

    let removeTag = (event) => {
        const targetName = event.target.name;
        removeTagFromStory(props.story.contentID, targetName).then((response) => {
            if (response.status === 200) {
                addToast('Story successfully saved!', {appearance: 'success', autoDismiss: true});

                //refresh tags through props
                props.setStory(response.data);
            }
        }).catch((error) => {
            addToast('Error removing Tag from Story.', {appearance: 'error', autoDismiss: true});
        })
    };

    return (

        <div className="tags">
            {
                props.tagList.map((item) =>
                    <span key={item.tag} name={item.tag} className="tag content-tag is-warning">
                        {item.tag}
                      <button name={item.tag} className="delete is-small" onClick={removeTag}> </button>
                    </span>
                )
            }
        </div>
    );
}

