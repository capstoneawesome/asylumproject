import React, {useEffect, useState} from 'react';
import {useToasts} from 'react-toast-notifications';
import "./sass/editelement.scss";
import no_file_selected from './nofileselected.jpg';
import invalid_file from './invalid_file.jpg';
import {UploadPreview} from "./UploadPreview";
import {saveContentElement} from "../../libraries/saveContentElement";
import langCodes from 'iso-639-1';
import {saveImageContentElement} from "../../libraries/saveImageContentElement";

/**
 * Component that displays a modal for editing the attributes of a single ContentElement.
 * @param props props received from ContentManager.jsx
 * @returns {null|*}
 * @constructor
 */
export function EditContentElement(props) {
    const {addToast} = useToasts();

    const [fileName, setFileName] = useState("No file selected");
    const [file, setFile] = useState(null);
    const [uploadSrc, setUploadSrc] = useState(no_file_selected);
    const [uploadedImageStyle, setUploadedImageStyle] = useState({display: 'block'});
    const [uploadedAudioStyle, setUploadedAudioStyle] = useState({display: 'none'});
    const [uploadedVideoStyle, setUploadedVideoStyle] = useState({display: 'none'});
    const [captionDivStyle, setCaptionDivStyle] = useState({display: 'none'});
    const [descriptionDivStyle, setDescriptionDivStyle] = useState({display: 'none'});
    const [fileIsValid, setFileIsValid] = useState(false);
    const [uploadType, setUploadType] = useState("image");
    const [uploadDisabled, setUploadDisabled] = useState(false);
    const [uploadInfoStyle, setUploadInfoStyle] = useState({display: 'none'});
    const [elementLanguage, setElementLanguage] = useState(props.contentElement !== null ? props.contentElement.language : 'en');
    const [caption, setCaption] = useState(props.contentElement !== null ? props.contentElement.caption : '');
    const [description, setDescription] = useState(props.contentElement !== null ? props.contentElement.description : '');

    const setDisplayStyles = (file, contentType) => {
        if (contentType === 'image') {
            let img = new Image();
            //set image source based on file upload or downloaded file
            setUploadInfoStyle({display: 'block'});
            setUploadedAudioStyle({display: 'none'});
            setUploadedVideoStyle({display: 'none'});
            setUploadType("image");
            setCaptionDivStyle({display: 'block'});
            setDescriptionDivStyle({display: 'block'});
        } else if (contentType === 'video') {
            setUploadInfoStyle({display: 'block'});
            setUploadedVideoStyle({display: 'block'});
            setUploadedAudioStyle({display: 'none'});
            setUploadedImageStyle({display: 'none'});
            setCaptionDivStyle({display: 'none'});
            setDescriptionDivStyle({display: 'block'});
            setUploadType("video");
        } else if (contentType === 'audio') {
            setUploadInfoStyle({display: 'block'});
            setUploadedVideoStyle({display: 'none'});
            setUploadedAudioStyle({display: 'block'});
            setUploadedImageStyle({display: 'none'});
            setCaptionDivStyle({display: 'none'});
            setDescriptionDivStyle({display: 'block'});
            setUploadType("audio");
        } else {
            setUploadInfoStyle({display: 'none'});
            setUploadType("image");
            setUploadSrc(invalid_file);
            setCaptionDivStyle({display: 'none'});
            setDescriptionDivStyle({display: 'none'});
            setUploadDisabled(true);
        }
    };

    useEffect(() => {
        if (props.contentElement !== undefined) {

            setCaption(props.contentElement.caption);
            setDescription(props.contentElement.description);
            setElementLanguage(props.contentElement.language);

            let contentType = props.contentElement.contentType.substring(0, 5);
            setDisplayStyles(null, contentType);

            let contentElement = props.contentElement;

            switch (contentType) {
                case 'image':
                    setUploadSrc("http://localhost:8080/api/content/" + props.storyID + "/mapPoints/" + props.mapPointID + "/images/" + contentElement.filePath);
                    break;
                case 'video':
                    setUploadSrc("http://localhost:8080/api/content/" + props.storyID + "/mapPoints/" + props.mapPointID + "/videos/" + contentElement.filePath);
                    break;
                case 'audio':
                    setUploadSrc("http://localhost:8080/api/content/" + props.storyID + "/mapPoints/" + props.mapPointID + "/audio/" + contentElement.filePath);
                    break;
            }
        }
    }, [props.contentElement]);

    if (!props.modalState) {
        if (uploadInfoStyle.display === 'block') {
            setUploadInfoStyle({display: 'none'});
        }
        if (file !== null) {
            setFile(null);
        }
        if (fileName !== 'No file selected') {
            setFileName("No file selected");
        }
        if (fileIsValid !== false) {
            setFileIsValid(false);
        }
        if (uploadDisabled !== true) {
            setUploadDisabled(true);
        }

        if (uploadSrc !== no_file_selected) {
            setUploadSrc(no_file_selected);
        }

        if (uploadedImageStyle.display !== 'block') {
            setUploadedImageStyle({display: 'block'});
        }

        if (uploadedAudioStyle.display === 'block') {
            setUploadedAudioStyle({display: 'none'});
        }

        if (uploadedVideoStyle.display === 'block') {
            setUploadedVideoStyle({display: 'none'});
        }

        return null;
    }


    let handleLanguageChange = (event) => {
        setElementLanguage(event.target.value)
    };

//content element file is unchanged, just save the changed metadata
    let saveContentElementObject = () => {

        let element = props.contentElement;
        element.description = description;
        element.language = elementLanguage;
        element.caption = caption;
        props.handleLoadingResources(true);

        if (element.contentType.substring(0, 5) === 'image') {
            saveImageContentElement(element, props.storyID, props.mapPointID).then(res => {
                props.handleLoadingResources(false);
                if (res.status === 200) {
                    //set updated Story
                    props.setStory(res.data);
                    addToast('Content Element saved successfully!', {appearance: 'success', autoDismiss: true});
                } else {
                    addToast('There was an error saving the changes.', {appearance: 'error', autoDismiss: true});
                }
            }).catch((error) => {
                addToast('There was an error saving the changes.', {appearance: 'error', autoDismiss: true});
            });
        } else {
            saveContentElement(element, props.storyID, props.mapPointID).then(res => {
                props.handleLoadingResources(false);
                if (res.status === 200) {
                    //set updated Story
                    props.setStory(res.data);
                    addToast('Content Element saved successfully!', {appearance: 'success', autoDismiss: true});
                } else {
                    addToast('There was an error saving the changes.', {appearance: 'error', autoDismiss: true});
                }
            }).catch((error) => {
                addToast('There was an error saving the changes.', {appearance: 'error', autoDismiss: true});
            });
        }
    };

    let handleFormChange = (event) => {
        const target = event.target;
        const value = target.value;
        switch (target.name) {
            case 'caption':
                setCaption(value);
                break;
            case 'description':
                setDescription(value);
                break;
        }
    };

    return (
        <div className="modal is-active" id="editElementModal">
            <div className="modal-background" onClick={props.closeModal}></div>
            <div id="editModal" className="modal-card">
                <header className="modal-card-head">
                    <p className="modal-card-title">Edit Content Element</p>
                    <button id="closeModal" onClick={props.closeModal} className="delete" aria-label="close"></button>
                </header>
                <section className="modal-card-body">
                    <div className="columns is-centered">
                        <div className="column is-full">
                            <h1 id="file-upload-header"
                                className="is-size-5">{props.contentElement === undefined ? "No file selected" : props.contentElement.filePath}</h1>
                            <br/>
                            <UploadPreview src={uploadSrc} imageStyle={uploadedImageStyle}
                                           audioStyle={uploadedAudioStyle} videoStyle={uploadedVideoStyle}
                                           uploadType={uploadType}/>
                            <br/>
                            <div className="is-divider"></div>
                            <div style={uploadInfoStyle}>
                                <div className="field">
                                    <div className="control" style={captionDivStyle}>
                                        <label className="label">Enter a caption for the image (optional):</label>
                                        <input name="caption" className="input" type="text"
                                               placeholder="(Optional) Enter a caption for the image"
                                               value={caption} onChange={handleFormChange}/>
                                    </div>
                                </div>
                                <div className="field">
                                    <div className="control" style={descriptionDivStyle}>
                                        <label className="label">Enter a description for the file
                                            (optional):</label>
                                        <textarea name="description" className="textarea" style={{resize: 'none'}}
                                                  value={description}
                                                  onChange={handleFormChange}/>
                                    </div>
                                </div>
                                <div id="upload-language" className="field is-horizontal level">
                                    <div className="level-left">
                                        <label className="label level-item">Language of uploaded file:</label>
                                    </div>
                                    <div className="level-right">
                                        <div className="field-body level-item">
                                            <div className="field is-expanded">
                                                <div className="control has-icons-left">
                                                    <div className="select is-fullwidth">
                                                        <select name="language"
                                                                value={elementLanguage}
                                                                onChange={handleLanguageChange}>
                                                            <option value='en'>English</option>
                                                            {
                                                                langCodes.getAllCodes().map((item, key) =>
                                                                    <option key={item}
                                                                            value={item}>{langCodes.getName(item)}</option>
                                                                )
                                                            }
                                                        </select>
                                                    </div>
                                                    <span className="icon is-small is-left">
                                            <i className="fas fa-globe"> </i>
                                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <footer id="modalFooter" className="modal-card-foot">
                    <button className="button is-primary" onClick={e => saveContentElementObject(e)}
                    >Save Changes
                    </button>
                    <button id="cancelModal" onClick={props.closeModal} className="button">Cancel</button>
                </footer>
            </div>
        </div>
    );
}
