import React from "react";
import swal from 'sweetalert';
import {useToasts} from 'react-toast-notifications';
import {archiveContentElement} from '../../libraries/archiveContentElement';
import './sass/contentListElement.scss';
import {getContentElementByID} from "../../libraries/getContentElementByID";
import {deleteContentElement} from "../../libraries/deleteContentElement";

/**
 * Component that hold a list of individual Content Elements available on a Map Point.
 * @param props props from the ContentElementList container
 * @returns {*}
 * @constructor
 */
export function ContentListElement(props) {

    const {addToast} = useToasts();

    let panelBlock = React.createRef();
    let icon = 'fas fa-book';

    const selectElement = (e) => {
        e.preventDefault();
        if (panelBlock.current.classList.contains('is-active')) {
            panelBlock.current.classList.remove('is-active');
        } else {
            panelBlock.current.classList.add('is-active');
        }
    };

    const archiveElement = (element, id) => {
        props.handleLoadingResources(true);
        archiveContentElement(element, props.storyID, props.mapPointID, id, "archive").then((response) => {
            props.handleLoadingResources(false);
            if (response.status === 200) {
                props.updateElementList();
                addToast('Content Element successfully archived', {appearance: 'success', autoDismiss: true});
                props.setStory(response.data);
            } else {
                addToast('Content Element was not archived', {appearance: 'error', autoDismiss: true});
            }
        });
    };


    const unarchiveElement = (element, id) => {
        props.handleLoadingResources(true);
        archiveContentElement(element, props.storyID, props.mapPointID, id, "unarchive").then((response) => {
            props.handleLoadingResources(false);
            if (response.status === 200) {
                props.updateElementList();
                addToast('Content Element successfully unarchived', {appearance: 'success', autoDismiss: true});
                props.setStory(response.data);
            } else {
                addToast('Content Element was not unarchived', {appearance: 'error', autoDismiss: true});
            }
        });
    };
    const confirmDeleteElement = (id) => {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Content Element!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            closeOnEsc: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    deleteElement(id);
                }
            });
    };

    const deleteElement = (id) => {
        props.handleLoadingResources(true);
        deleteContentElement(id, props.mapPointID, props.storyID).then((response) => {
            props.handleLoadingResources(false);
            if (response.status === 200) {
                props.updateElementList();
                swal("Content Element was successfully deleted!", {
                    icon: "success",
                });
                props.setStory(response.data);
            } else {
                swal("There was an error. Content Element was not deleted!");
            }
        });
    };

    const editContentElement = (id) => {
        //get element by id
        getContentElementByID(id).then((response) => {
            if (response.status === 200) {
                let retrievedContentElement = response.data;
                if (retrievedContentElement !== null) {
                    props.editContentElement(retrievedContentElement);
                }
            }
        }).catch((error) => {
        });
    };

    if (props.item.fileType === 'text') {
        icon = 'fas fa-book';
    } else if (props.item.fileType === 'audio') {
        icon = 'fas fa-music';
    } else if (props.item.fileType === 'video') {
        icon = 'fas fa-film';
    } else if (props.item.fileType === 'image') {
        icon = 'fas fa-image';
    }

    return (
        <a className="panel-block" ref={panelBlock} onClick={e => selectElement(e)}>
                <span className="panel-icon">
                    <i className={icon} aria-hidden="true"> </i>
                </span>
            <span id="contentListElement" className="is-size-7">{props.item.filePath}</span>
            <div className="container">
            </div>
            {
                props.item.state !== 'ARCHIVED' &&
            <span className="icon has-text-info" onClick={() => editContentElement(props.item.id)}>
                <i className="fas fa-edit is-large" title="Edit Element"> </i>
            </span>
            }
            {
                props.item.state === 'ARCHIVED' &&
                <span className="icon has-text-primary" onClick={() => unarchiveElement(props.item, props.item.id)}>
                <i className="fas fa-box-open is-large" title="Unarchive Element"> </i>
                </span>
            }
            <span className="icon has-text-danger" onClick={(e) => {
                props.item.state === 'ARCHIVED' ? confirmDeleteElement(props.item.id) : archiveElement(props.item, props.item.id)
            }}>
            <i className={props.item.state === 'ARCHIVED' ? "fas fa-trash is-large" : "fas fa-archive is-large"}
               title={props.item.state === 'ARCHIVED' ? 'Delete Element' : "Archive Element"}> </i>
            </span>
        </a>
    )
}

