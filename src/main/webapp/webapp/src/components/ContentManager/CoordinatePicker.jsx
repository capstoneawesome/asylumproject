import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import Feature from "ol/Feature";
import Point from "ol/geom/Point";
import VectorLayer from 'ol/layer/Vector';
import VectorSource from "ol/source/Vector";
import {defaults} from 'ol/control';
import XYZ from "ol/source/XYZ";
import {fromLonLat, toLonLat, transform} from "ol/proj";
import * as Geocoder from 'ol-geocoder';
import 'ol-geocoder/dist/ol-geocoder.css';
import 'ol/ol.css';
import './sass/coordinatepicker.scss';

import React, {useState, useEffect, useRef} from 'react';
import ZoomControl from "../Map/controls/ZoomControl";
import LocationDisplay from "./LocationDisplay";
import Style from "ol/style/Style";
import Stroke from "ol/style/Stroke";
import Fill from "ol/style/Fill";

/**
 * Component that handles picking a location for a single map point in a Story.
 * @param props props recieved from ContentManager.jsx
 * @returns {*}
 * @constructor
 */
function CoordinatePicker(props) {
    const [center, setCenter] = useState([0,0]);
    const [zoom, setZoom] = useState(3);

    let map = new Map({
            layers: [
                new TileLayer({
                    source: new XYZ({
                        url: 'http://tile.stamen.com/terrain/{z}/{x}/{y}.jpg'
                    })
                })],
            target: null,
            view: new View({
                center: fromLonLat(center),
                zoom: zoom
            }),
            controls: defaults({
                zoom: false,
                attribution: false,
                rotate: false
            })

        });

    var markerVectorLayer;

    map.on('click', function(evt){
        let currentZoom = map.getView().getZoom();
        console.log("zoom", currentZoom);
        if (markerVectorLayer !== null && markerVectorLayer !== undefined){
            map.removeLayer(markerVectorLayer);
        }

        let coordinates = transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');
        props.setCoordinatesAndZoomLevel(coordinates, currentZoom);

        var marker = new Feature({
            geometry: new Point(
                fromLonLat(coordinates)
            ),
        });

        var vectorSource = new VectorSource({
            features: [marker]
        });

        markerVectorLayer= new VectorLayer({
            source: vectorSource,
        });

        var pixel = map.getEventPixel(evt.originalEvent);
        map.forEachFeatureAtPixel(pixel, function (feature) {
            feature.setStyle(new Style({
                stroke: new Stroke({color: 'rgba(255,255,255,0.5)'}),
                fill: new Fill({color: 'rgba(255,255,255,0.5)'})
            }));
        });
        map.addLayer(markerVectorLayer);
    });


    const geocoder = new Geocoder('nominatim', {
        provider: 'osm',
        autoComplete: true,

        lang: 'en',
        placeholder: 'Search for ...',
        limit: 5,
        keepOpen: true,
        preventDefault: true
    });

    geocoder.on('addresschosen', function(e){
        const coord = e.coordinate;

        map.getView().animate({center: coord, duration: 4000});
        map.getView().animate({zoom: 3, duration: 2000}, {zoom: 9, duration: 2000});

    });

    useEffect(() => {
        map.addControl(geocoder);
        map.setTarget('coord-picker-window');
        map.on("moveend", function(e){
            setCenter(toLonLat(map.getView().getCenter()));
            setZoom(map.getView().getZoom());
            console.log("move end zoom", map.getView().getZoom());
        });
    }, []);


        return (
            <div id='coord-picker-window' className="coord-picker-box">
                <ZoomControl id="coord-picker-zoom" mapObject={map}> </ZoomControl>
                <LocationDisplay mapObject={map} center={center} zoom={zoom}> </LocationDisplay>
            </div>
        )

}

export default CoordinatePicker;

