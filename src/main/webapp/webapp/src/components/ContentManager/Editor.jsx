import ReactQuill from "react-quill";
import {useToasts} from 'react-toast-notifications';
import React, {useEffect, useState} from "react";
import axios from "axios";
import PropTypes from "prop-types";
import "./sass/editor.scss";
import "react-quill/dist/quill.snow.css";
import languageCodes from "../../libraries/language-codes_json";
import langCodes from 'iso-639-1';
import {saveTextContentElement} from "../../libraries/saveTextContentElement";

/**
 * Displays a WYSIWYG text editor for creation of Text Content Elements.
 * @param props props received from ContentManager.jsx
 * @returns {*}
 * @constructor
 */
export function Editor(props) {

    const {addToast} = useToasts();
    let quillRef = null;
    let mainEditor = React.createRef();
    let textTitle = React.createRef();
    const [theme, setTheme] = useState('snow');
    const [editorHtml, setEditorHtml] = useState('');
    const [editorTitle, setTitle] = useState('');
    const [textLanguage, setTextLanguage] = useState('en');

    useEffect(() => {
        if (typeof mainEditor.current.getEditor !== 'function') return;
        quillRef = mainEditor.current.getEditor();
    });

    useEffect(() => {
       if (props.editableTextElement !== null){
           setEditorHtml(props.editableTextElement.description);
           setTitle(props.editableTextElement.filePath);
       }
    }, [props.editableTextElement]);

    let handleEditorChange = (html) => {
        setEditorHtml(html);
    };

    let handleTitleChange = () => {
        setTitle(textTitle.current.value);
    };

    let handleThemeChange = (newTheme) => {
        if (newTheme === "core") newTheme = null;
        setTheme(newTheme);
    };

    let handleLanguageChange = (event) => {
       setTextLanguage(event.target.value)
    };

    let handleSubmit = () => {
        //Check if title or contents are empty
        if (quillRef.getText().trim() === "") {
            addToast('Text contents cannot be blank.', {appearance: 'error', autoDismiss: true});
        } else if (editorTitle.trim() === "") {
            addToast('Title contents cannot be blank.', {appearance: 'error', autoDismiss: true});
        } else {

            let config = {
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
                }
            };
            let textSize = new Blob([editorHtml]).size;
            let length = editorHtml.length;
            let mapPointID = props.mapPoint.current.id;
            props.handleLoadingResources(true);

            //update existing element
            if (props.editableTextElement !== null){
                props.editableTextElement.language = textLanguage;
                props.editableTextElement.fileSize = textSize;
                props.editableTextElement.filePath = editorTitle;
                props.editableTextElement.description = editorHtml;
                props.editableTextElement.length = length;

                saveTextContentElement(props.editableTextElement, props.storyID, mapPointID)
                    .then((response) => {
                        props.setEditableTextElement(null);
                        props.handleLoadingResources(false);
                        if (response.status === 200) {
                            addToast('Text element successfully saved!', {appearance: 'success', autoDismiss: true});
                            //Send editor data to ContentManager and then update elements list
                            props.onEditorSubmit(true);
                            props.updateElementsList();
                            props.setStory(response.data);
                        } else {
                            //display error
                            addToast('There was an error saving the file.', {appearance: 'error', autoDismiss: true});
                        }
                    }).catch((error) => {
                    props.handleLoadingResources(false);
                    addToast('There was an error saving the file.', {appearance: 'error', autoDismiss: true});
                });
            //save new element
            }else {
                let text =
                    {
                        "language": textLanguage,
                        "filePath": editorTitle,
                        "fileSize": textSize,
                        "description": editorHtml,
                        "fileType": "text",
                        "contentType": 'text/html',
                        "length": length,
                        "state": "DRAFT"
                    };

                axios.post(
                    "http://localhost:8080/api/content/" + props.storyID + "/mappoints/" + mapPointID + "/elements/text", text, config)
                    .then((response) => {
                        props.setEditableTextElement(null);
                        props.handleLoadingResources(false);
                        if (response.status === 200) {
                            addToast('Text element successfully saved!', {appearance: 'success', autoDismiss: true});
                            //Send editor data to ContentManager and then update elements list
                            props.onEditorSubmit(true);
                            props.updateElementsList();
                            props.setStory(response.data);
                        } else {
                            //display error
                            addToast('A file with that name already exists!', {appearance: 'error', autoDismiss: true});
                        }
                    }).catch((error) => {
                        addToast('A file with that name already exists!.', {appearance: 'error', autoDismiss: true});
                        props.handleLoadingResources(false);
                });
            }
        }
    };

    return (
        <>
            <div>
                <h1 className="title is-4 has-text-centered">{props.editableTextElement !== null ? "Editing Text Element: " + props.editableTextElement.filePath : 'Create a Text Element'}</h1>
                <input className="input" type="text" ref={textTitle}
                       placeholder="Enter title text here" onChange={handleTitleChange} value={editorTitle}/>
                <ReactQuill
                    theme={theme}
                    onChange={handleEditorChange}
                    value={editorHtml}
                    modules={Editor.modules}
                    formats={Editor.formats}
                    bounds={'.app'}
                    placeholder={"Enter text here."}
                    ref={mainEditor}/>
            </div>
            <div className="has-text-centered">
                <div className="level">
                    <div className="level-left">
                        <div id="text-language" className="field is-horizontal level-item">
                            <label className="label is-size-7">Text Language:</label>
                            <div className="field-body">
                                <div className="field is-expanded">
                                    <div className="control has-icons-left">
                                        <div id="text-language-select" className="select is-small">
                                            <select name="language"
                                                    defaultValue={props.story === undefined ? 'English' : props.story.language} onChange={handleLanguageChange}>
                                                <option value='English'>English</option>
                                                {
                                                    languageCodes.map((item, key) =>
                                                        <option key={item.alpha2}
                                                                value={item.alpha2}>{item.English}</option>
                                                    )
                                                }
                                            </select>
                                        </div>
                                        <span className="icon is-small is-left">
                                            <i className="fas fa-globe"> </i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="level-right">
                        <button id="saveTextButton" className="button is-link is-small level-item"
                                data-tooltip="Add the text document to the list of content for the Map Point"
                                onClick={e => handleSubmit(e)}>
                            Save Text Element to List
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
}

/*
* Quill modules to attach to editor
* See https://quilljs.com/docs/modules/ for complete options
*/
Editor.modules = {
    toolbar: [
        [{'header': '1'}, {'header': '2'}, {'font': []}],
        [{size: []}],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [{'list': 'ordered'}, {'list': 'bullet'},
            {'indent': '-1'}, {'indent': '+1'}],
        ['link'],
        ['clean']
    ],
    clipboard: {
        // toggle to add extra line breaks when pasting HTML:
        matchVisual: false,
    }
};

/*
* Quill editor formats
* See https://quilljs.com/docs/formats/
*/
Editor.formats = [
    'header', 'font', 'size',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'link',
];

/*
* PropType validation
*/
Editor.propTypes = {
    placeholder: PropTypes.string,
};

