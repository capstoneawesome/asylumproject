import React from "react";
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {Pagination} from 'react-bulma-components';

/**
 * Displays all of the created map points for the current Story in the Content Manager.
 * @param props props received from ContentManager.jsx
 * @returns {null|*}
 * @constructor
 */
export function MapPointSelector(props) {

    props.setMapPointPageClick(false);
    if (props.currentMapPoint.current !== null && props.currentMapPoint.current !== undefined) {
        let currentMapPointIndex = null;
        let currentMapPointID = props.currentMapPoint.current.id;
        let indexOfMapPoint = props.mapPoints.findIndex(mapPoint => mapPoint.id === currentMapPointID);
        if (indexOfMapPoint !== -1) {
            currentMapPointIndex = indexOfMapPoint;
        }

        let handlePageChange = (pageNumber) => {
            console.log(props.mapPoints[pageNumber - 1].id);
            props.handlePageClick(props.mapPoints[pageNumber - 1].id);
        };

        return (
            <div>
                <Pagination
                    current={currentMapPointIndex + 1}
                    total={props.mapPoints.length}
                    delta={5}
                    onChange={handlePageChange}
                    next={'Next Map Point'}
                    previous={'Previous Map Point'}
                    className={'pagination is-small'}
                    autoHide={false}
                    />
            </div>
        )
    }else{
        return null;
    }
}
