import React from 'react';

/**
 * Component that displays available content warning tags that be applied to a Story. Also handles creation of new Content tags.
 * @param props props received from ContentManager.jsx
 * @returns {*}
 * @constructor
 */
export function ContentTags(props) {

    let handleContentTags = (event) => {
        const target = event.target;
        const value = target.value;
        props.handleContentTags(target.name, value);
    };

    return (
        <div>
            <div className="field">
                <label className="label">Select a Content Tag to add to this Story:</label>
                <div className="field has-addons is-fullwidth">
                    <div className="control has-icons-left is-expanded">
                        <div className="select is-fullwidth">
                            <select name="contentTags" defaultValue={props.availableTags[0]} onChange={handleContentTags}>
                                {
                                    props.availableTags.map((item) =>
                                        <option key={item.tag} value={item.tag}>{item.tag}</option>
                                    )
                                }
                            </select>
                        </div>
                        <span className="icon is-small is-left">
                                <i className="fas fa-globe"> </i>
                    </span>
                    </div>
                    <div className="control">
                        <a name="addTagToStory" className="button is-success" onClick={handleContentTags}>Add Tag to
                            Story
                        </a>
                    </div>
                </div>
            </div>

            <div className="field">
                <label className="label">Add a new Content Tag to the Story and to the list of available Tags:</label>
                <div className="field has-addons is-fullwidth">
                    <div className="control is-expanded">
                        <input name="customTag" className="input" type="text"
                               placeholder="Enter Tag description here" onChange={handleContentTags}/>
                    </div>
                    <div className="control">
                        <a name="addCustomTag" className="button is-primary" onClick={handleContentTags}>Add Tag to Story and List
                        </a>
                    </div>
                </div>
            </div>
        </div>
    );
}