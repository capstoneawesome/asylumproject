import React, {useEffect, useRef} from 'react';
import Control from "ol/control/Control";
import './sass/locationdisplay.scss';

/**
 * Displays the values of the map center point and and current zoom level on the map point picker in the Content Manager.
 * @param props props received from CoordinatePicker.jsx
 * @returns {*}
 * @constructor
 */
export default function LocationDisplay(props){

    const mapObject = props.mapObject;
    const displayRef = useRef(null);

    useEffect(() =>{
            const display = new Control({element: displayRef.current});
            mapObject.addControl(display);
        }, []
    );


    return(
        <div ref={displayRef} className="location-display-box">
            <div className="tags has-addons is-paddingless is-marginless">
                <span className="tag">Center Point</span>
                <span className="tag is-primary">{props.center}</span>
            </div>
            <div className="tags has-addons is-paddingless is-marginless">
                <span className="tag">Zoom Level</span>
                <span className="tag is-primary">{props.zoom}</span>
            </div>
        </div>
    );

}