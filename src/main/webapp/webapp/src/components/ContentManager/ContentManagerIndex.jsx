import React, {useEffect, useRef, useState} from 'react';
import {Link, Route, Switch, useHistory} from "react-router-dom";
import {useToasts} from 'react-toast-notifications';
import ContentManager from "./ContentManager";
import {ManageStories} from "./ManageStories";
import './sass/contentmanagerindexstyles.scss';
import {createStory} from "../../libraries/createStory";
import {connect} from "react-redux";
import Side from "../Dashboard/Side/Side";
import Top from "../Dashboard/Top/Top";
import Avatar from "react-avatar";
import * as THREE from "three";
import RINGS from "vanta/dist/vanta.rings.min";
import * as authenticationActions from "../../actions/authentication";

/**
 * Entry point into the content management sub-system. Handles navigation between the Manage Stories list and the Create a Story interface.
 * @param props props from App.jsx
 * @returns {*}
 * @constructor
 */
export function ContentManagerIndex(props) {

    const history = useHistory();
    const {addToast} = useToasts();

    let creator = props.user_id;
    //Create a new Story in the 'DRAFT' state immediately after starting Story creation
    let createNewStory = () => {
        createStory(creator).then((response) => {
            if (response.status === 200) {
                let newStory = response.data;
                history.push("/contentmanager/stories/" + newStory.contentID);
            } else {
                //display message and update other UI elements
                addToast('Error creating Story', {appearance: 'error', autoDismiss: true});
            }
        });
    };

    const [vantaEffect, setVantaEffect] = useState(0);
    const background = useRef(null);

    useEffect(() => {
        if (!vantaEffect) {
            setVantaEffect(RINGS({
                THREE: THREE,
                el: background.current,
                mouseControls: true,
                touchControls: true,
                minHeight: 120.00,
                minWidth: 200.00,
                scale: 1.00,
                scaleMobile: 1.00,
                backgroundColor: 0xffffff,
                color: 0xb6651d
            }))
        }
        return () => {
            if (vantaEffect) vantaEffect.destroy()
        }
    }, [vantaEffect]);

    const handleCheckingPermission = () => {
        if(props.user_authorities.includes("ROLE_SYSTEM_ADMIN")) {
            return "ADMINISTRATOR";
        } else if(props.user_authorities.includes("ROLE_CONTENT_CURATOR")) {
            return "CONTENT CURATOR";
        } else if(props.user_authorities.includes("ROLE_TEACHER")) {
            return "TEACHER";
        }
    };

    return (
        <>
                <Side/>
                <Top/>
                <section className="hero asp-dashboard-body asp-full-height asp-default-color-background welcome is-small">
                    <div className="asp-scrollable-body">

                        <section className="hero asp-default-color-background welcome is-small">
                            <div ref={background} className="hero-body">
                                <div className="container">
                                    <div className="columns">
                                        <div className="asp-round-avatar column is-2">
                                            <Avatar name={props.user_firstName + " " + props.user_lastName} />
                                        </div>
                                        <div className="column is-8">
                                            <h1 className="title">
                                                Hello, {props.user_username}.
                                            </h1>
                                            <h2 className="subtitle">
                                                {handleCheckingPermission()}
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <Switch>
                            <Route exact path="/contentmanager/stories"
                                   component={ContentManager}/>
                    <Route exact path="/contentmanager/stories/:storyID"
                           component={ContentManager}/>
                    <Route exact path="/contentmanager" component={ManageStories}/>
                </Switch>
                    </div>
                </section>
    </>
    )
        ;
}

const mapStateToProps = (state) => {
    return {
        user_id: state.authenticationReducer.user_id,
        isSignIn: state.authenticationReducer.isSignIn,
        user_defaultLanguage: state.authenticationReducer.user_defaultLanguage,
        user_firstName: state.authenticationReducer.user_firstName,
        user_lastName: state.authenticationReducer.user_lastName,
        user_authorities: state.authenticationReducer.user_authorities,
        user_username: state.authenticationReducer.user_username,
        user_email: state.authenticationReducer.user_email,
        user_photoPath: state.authenticationReducer.user_photoPath,
    };
};

const mapDispatchProps = (dispatch) => {
    // return bindActionCreator(actions, dispatch);
    return {
        handleAuthentication: (isSignIn) => {dispatch(authenticationActions.authenticationAction(isSignIn))},
        handleDefaultLanguage: (user_defaultLanguage) => {dispatch(authenticationActions.defaultLanguageAction(user_defaultLanguage))},
        handleFirstName: (user_firstName) => {dispatch(authenticationActions.firstNameAction(user_firstName))},
        handleLastName: (user_lastName) => {dispatch(authenticationActions.lastNameAction(user_lastName))},
        handlePhotoPath: (user_photoPath) => {dispatch(authenticationActions.photoPathAction(user_photoPath))},
        handleAuthorities: (user_authorities) => {dispatch(authenticationActions.authoritiesAction(user_authorities))},
        handleUsername: (user_username) => {dispatch(authenticationActions.usernameAction(user_username))},
        handleEmail: (user_email) => {dispatch(authenticationActions.emailAction(user_email))},
    };
};

ContentManagerIndex = connect(mapStateToProps, mapDispatchProps)(ContentManagerIndex);

/*
                <div id="headerDiv" className="container">
                    <h1 className="title is-1">Content Manager</h1>
                </div>
 */
/*

            <div id='menu-column' className="column is-2">
                <aside>
                    <p className="menu-label">
                        Management Options
                    </p>
                    <ul className="menu-list">
                        <li><Link to='/contentmanager'>Manage Stories</Link></li>
                        <li><a onClick={createNewStory}>Create New Story</a></li>
                        <li><a>Manage Resource Pages</a></li>
                    </ul>
                </aside>
            </div>
 */
