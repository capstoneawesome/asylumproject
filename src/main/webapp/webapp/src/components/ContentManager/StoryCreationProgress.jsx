import React from "react";
import './sass/storycreationprogress.scss';

/**
 * Displays headings for the different steps in the Story creation process. Also handles navigation between the different steps.
 * @param props props received from ContentManager.jsx
 * @returns {*}
 * @constructor
 */
export function StoryCreationProgress(props) {

    let setStep = (step) => {
        props.setStep(step);
    };

    return (
        <>
            <ul className="steps has-content-centered">
                <li className={props.step === 1 ? 'steps-segment is-active' : 'steps-segment'} onClick={() => setStep(1)}>
                    <a href="#" className="steps-marker">
                    <span className="steps-marker">
                        <span className="icon">
                            <i className="fa fa-book"> </i>
                        </span>
                    </span>
                    </a>
                    <div className="steps-content">
                        <a>
                            <p className="is-size-4">Details</p>
                        </a>
                    </div>
                </li>
                <li className={props.step === 2 ? 'steps-segment is-active' : 'steps-segment'} onClick={() => setStep(2)}>
                    <a href="#" className="steps-marker">
                    <span className="steps-marker">
                        <span className="icon">
                            <i className="fa fa-map"> </i>
                        </span>
                    </span>
                    </a>
                    <div className="steps-content">
                        <a>
                            <p className="is-size-4">Map Points
                            </p>
                        </a>
                    </div>
                </li>
                <li className={props.step === 3 ? 'steps-segment is-active' : 'steps-segment'} onClick={() => setStep(3)}>
                    <a href="#" className="steps-marker">
                    <span className="steps-marker">
                        <span className="icon">
                            <i className="fa fa-tag"> </i>
                        </span>
                    </span>
                    </a>
                    <div className="steps-content">
                        <a>
                        <p className="is-size-4">Content Tags</p>
                        </a>
                    </div>
                </li>
                <li className={props.step === 4 ? 'steps-segment is-active' : 'steps-segment'} onClick={() => setStep(4)}>
                    <a className="steps-marker">
                    <span className="steps-marker">
                        <span className="icon">
                            <i className="fa fa-check"> </i>
                        </span>
                    </span>
                    </a>
                    <div className="steps-content">
                        <a>
                        <p className="is-size-4">Finished</p>
                        </a>
                    </div>
                </li>
            </ul>
        </>
    );
}

