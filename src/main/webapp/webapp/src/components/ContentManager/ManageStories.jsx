import React, {useEffect, useState} from 'react'
import {useHistory} from "react-router-dom";
import {useToasts} from 'react-toast-notifications';
import './sass/manageStories.scss';
import {FilterTable} from "./FilterTable";
import {getStoryList} from "../../libraries/getStoryList";

/**
 * Handles retrieval of stories to fill the FilterTable component.
 * @param props props received from ContentManagerIndex.jsx
 * @returns {*}
 * @constructor
 */
export function ManageStories(props) {

    const [storyList, setStoryList] = useState(null);
    const [ isLoading, setIsLoading] = useState(false);
    const pageLoaderStyles = {display: 'flex', width: '100%', height: '100vh', top: '30%', position: 'static'};


    let getStoryListArray = () => {
        setIsLoading(true);
        getStoryList().then(res => {
            setIsLoading(false);
            if (res.status === 200) {
                setStoryList(res.data);
            }
        });
    };

    useEffect(() => {
        getStoryListArray();
    }, []);

    return (
        <div>
            <div className="tile is-ancestor is-12">
                <div className="tile is-vertical is-parent">
                    <div className="tile is-parent">
                        <div className="tile is-child">
                            {
                                isLoading ?
                                    <div id="manage-stories-pageloader" className="pageloader is-active" style={pageLoaderStyles}> <span className="title">Loading Stories</span> </div>
                                   :
                                storyList !== null &&
                                <FilterTable storyList={storyList} refreshStoryTable={getStoryListArray}/>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}



