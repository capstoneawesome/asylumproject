import React from "react";
import "./sass/uploadpreview.scss";

/**
 * Container for holding the file object from a file uploaded from a user's computer.
 * @param props props received from EditContentElement.jsx
 * @returns {*}
 * @constructor
 */
export function UploadPreview(props){
    return(
        <div id="upload-preview">
            <img src={props.src} alt='upload image preview' style={props.imageStyle}/>
            <video controls src={props.src} style={props.videoStyle}> </video>
            <audio controls src={props.src} style={props.audioStyle}> </audio>
        </div>
    );
}