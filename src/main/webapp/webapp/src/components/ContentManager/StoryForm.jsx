import React from 'react';
import countryCodes from '../../libraries/country_codes';

/**
 * Displays a form to enter the initial Story details when creating or editing a Story. The fields are: Story Title, Asylum Seeker Name, Country of Origin, and Description.
 * @param props props received from ContentManager.jsx
 * @returns {*}
 * @constructor
 */
export function StoryForm(props) {

    let handleFormChange = (event) => {
       const target = event.target;
       const value = target.value;
       props.handleStoryFormChange(target.name, value);
    };

    return (
        <>
            <div className="field is-horizontal">
                <div className="field-label is-normal">
                    <label className="label">Story Title</label>
                </div>
                <div className="field-body">
                    <div className="field">
                        <input name="storyTitle" className="input" type="text" placeholder="Story Title" defaultValue={props.story.title} onChange={handleFormChange} required/>
                    </div>
                </div>
            </div>
            <div className="field is-horizontal">
                <div className="field-label is-normal">
                    <label className="label">Asylum-Seeker Name</label>
                </div>
                <div className="field-body">
                    <div className="field is-expanded">
                        <div className="control is-expanded has-icons-left">
                            <input name="asylumSeekerName" className="input" type="text" placeholder="Asylum-Seeker Name"
                                   defaultValue={props.story.asylumSeekerName} onChange={handleFormChange}
                                   required/>
                            <span className="icon is-small is-left">
                            <i className="fas fa-user"> </i>
                        </span>
                        </div>
                    </div>
                </div>
            </div>

            <div className="field is-horizontal">
                <div className="field-label is-normal">
                    <label className="label">Country of Origin</label>
                </div>
                <div className="field-body">
                    <div className="field is-expanded">
                        <div className="control has-icons-left">
                            <div className="select is-fullwidth">
                                <select name="countryOfOrigin" value={props.countryOfOrigin} onChange={handleFormChange}>
                                    {
                                        countryCodes.map( (item) =>
                                            <option key={item.Code} value={item.Code}>{item.Name}</option>
                                        )
                                    }
                                </select>
                            </div>
                            <span className="icon is-small is-left">
                                <i className="fas fa-globe"> </i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>


            <div className="field is-horizontal">
                <div className="field-label is-normal">
                    <label className="label">Description</label>
                </div>
                <div className="field-body">
                    <div className="field">
                        <div className="control has-icons-left">
                            <textarea name="description" className="textarea is-fullwidth" placeholder="Enter a short description here" defaultValue={props.story.description} onChange={handleFormChange}/>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}