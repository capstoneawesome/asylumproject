import React from 'react';
import './sass/aboutModalStyles.scss';

/**
 * A modal window that displays information about the Asylum Seekers website.
 * @param props received from App.jsx
 * @returns {null|*}
 * @constructor
 */
export function About(props){


    if (!props.aboutModalState) {
        return null;
    }

    return(
        <div id="about-modal" className="modal">
            <div className="modal-background" onClick={props.closeAboutModal}> </div>
            <div className="modal-card">
                <header className="modal-card-head">
                    <p className="modal-card-title">About Asylum Seekers in New York City</p>
                    <button className="delete" aria-label="close" onClick={props.closeAboutModal}> </button>
                </header>
                <section className="modal-card-body">
                    <p>The purpose of the Asylum Seeker’s Website is to store, manage, and present the stories of asylum-seekers arriving in the United States, through an interactive and professional web application.</p>
                    <p>The website is meant to be educational and engaging by using a map-based interface to convey the journeys that asylum seekers had to go through. The website is made engaging through interacting with the map points that make up the journey and also through various media elements including text, audio, and video, and images.</p><p> The website also filters the presented media elements through a content rating system, so that it is usable by students in 3rd to 12th grade. The stories are managed through a custom content management system which allows every aspect of each story to be edited.</p>
                </section>
                <footer className="modal-card-foot">
                    <button className="button is-info" onClick={props.closeAboutModal}>Close Window</button>
                </footer>
            </div>
        </div>
    )
}