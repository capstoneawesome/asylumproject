import React, {Component} from "react";
import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import { MapControls } from "three/examples/jsm/controls/OrbitControls";
import { Interaction } from "three.interaction";
import Gsap from "gsap";

import groundImg from "./ground.jpeg";
import worldImgBK from "../3ds/skybox/miramar_bk.jpg";
import worldImgFT from "../3ds/skybox/miramar_ft.jpg";
import worldImgUP from "../3ds/skybox/miramar_up.jpg";
import worldImgDN from "../3ds/skybox/miramar_dn.jpg";
import worldImgLF from "../3ds/skybox/miramar_lf.jpg";
import worldImgRT from "../3ds/skybox/miramar_rt.jpg";
import jongilImg from "./IMG_1192.JPG";
import bogusImg from "./bogus.jpg";

class RenderView extends Component {

    //These are not states
    //3d essentials
    camera;
    scene;
    light;
    renderer;
    controls;
    interaction;

    //etc
    grid;
    axesHelper;

    //Custom objects
    boxExample;
    world;
    heart;
    jongil;

    init = () => {

        //camera
        this.camera = new THREE.PerspectiveCamera( 85, window.innerWidth / window.innerHeight, 0.25, 1000 );
        this.camera.position.set( -60, 30, 0 );
        // this.camera.lookAt( new THREE.Vector3( 0, 2, 0 ) );
        this.camera.lookAt( new THREE.Vector3( 0, 100, 0 ) );

        //scene
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color( 0xe0e0e0 );
        // this.scene.fog = new THREE.Fog( 0xe0e0e0, 20, 100 );

        //renderer
        this.renderer = new THREE.WebGLRenderer({antialias: true});
        this.renderer.setSize(window.innerWidth, window.innerHeight);

        //controls
        //orbit
        // this.controls = new OrbitControls( this.camera, this.renderer.domElement );
        // this.controls.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
        // this.controls.dampingFactor = 0.05;
        // this.controls.screenSpacePanning = false;
        // this.controls.minDistance = 5;
        // this.controls.maxDistance = 100;
        // this.controls.maxPolarAngle = Math.PI / 2;
        // this.controls.update();

        //map
        this.controls = new MapControls( this.camera, this.renderer.domElement );
        //controls.addEventListener( 'change', render ); // call this only in static scenes (i.e., if there is no animation loop)
        this.controls.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
        this.controls.dampingFactor = 0.05;
        this.controls.screenSpacePanning = false;
        this.controls.minDistance = 5;
        this.controls.maxDistance = 100;
        this.controls.maxPolarAngle = Math.PI / 2;

        //interaction
        this.interaction = new Interaction(this.renderer, this.scene, this.camera);

        //lights
        this.light = new THREE.AmbientLight( 0xffffff, 0.5 ); // soft white light
        this.scene.add( this.light );

        //grid
        // this.grid = new THREE.GridHelper( 200, 50, 0x000000, 0x000000 );
        // this.grid.material.opacity = 0.2;
        // this.grid.material.transparent = true;
        // this.scene.add( this.grid );

        //axesHelper
        this.axesHelper = new THREE.AxesHelper( 5 );
        this.scene.add( this.axesHelper );

        //box object
        let geometry = new THREE.BoxGeometry(0.2, 0.2, 0.2);
        let material = new THREE.MeshNormalMaterial();
        this.boxExample = new THREE.Mesh(geometry, material);
        this.boxExample.position.y = 1.5;
        this.scene.add(this.boxExample);

        //heart object
        // let heartX = 0, heartY = 0;
        // let heartShape = new THREE.Shape();
        //
        // heartShape.moveTo( heartX + 5, heartY + 5 );
        // heartShape.bezierCurveTo( heartX + 5, heartY + 5, heartX + 4, heartY, heartX, heartY );
        // heartShape.bezierCurveTo( heartX - 6, heartY, heartX - 6, heartY + 7,heartX - 6, heartY + 7 );
        // heartShape.bezierCurveTo( heartX - 6, heartY + 11, heartX - 3, heartY + 15.4, heartX + 5, heartY + 19 );
        // heartShape.bezierCurveTo( heartX + 12, heartY + 15.4, heartX + 16, heartY + 11, heartX + 16, heartY + 7 );
        // heartShape.bezierCurveTo( heartX + 16, heartY + 7, heartX + 16, heartY, heartX + 10, heartY );
        // heartShape.bezierCurveTo( heartX + 7, heartY, heartX + 5, heartY + 5, heartX + 5, heartY + 5 );
        //
        // let heartGeometry = new THREE.ShapeGeometry( heartShape );
        // let heartMaterial = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
        // this.heart = new THREE.Mesh( heartGeometry, heartMaterial ) ;
        // this.heart.position.set(0, 0, 4);
        // this.scene.add( this.heart );

        //jongil object
        let jongilGeometry = new THREE.CylinderGeometry( 1, 1, 0.1, 36 );
        let jongilTexture = new THREE.TextureLoader().load(jongilImg);
        // let jongilMaterial = new THREE.MeshBasicMaterial( {color: 0xffff00} );
        let jongilMaterial = new THREE.MeshBasicMaterial( {map: jongilTexture, side: THREE.DoubleSide});
        this.jongil = new THREE.Mesh( jongilGeometry, jongilMaterial );
        this.jongil.rotation.z = - Math.PI / 2;
        this.jongil.position.y = 1;
        this.jongil.position.z = 3;
        this.scene.add( this.jongil );
        this.jongil.on("click", () => {
            console.log("Jongil is clicked");
            let newTexture = new THREE.TextureLoader().load(bogusImg);
            jongilMaterial.map = newTexture;
        });

        //text
        // var loader = new THREE.FontLoader();
        // loader.load( 'three/exmaples/fonts/helvetiker_bold.typeface.json', function ( font ) {
        //
        //     var textGeo = new THREE.TextBufferGeometry( "THREE.JS", {
        //
        //         font: font,
        //
        //         size: 200,
        //         height: 50,
        //         curveSegments: 12,
        //
        //         bevelThickness: 2,
        //         bevelSize: 5,
        //         bevelEnabled: true
        //
        //     } );
        //
        //     textGeo.computeBoundingBox();
        //     var centerOffset = - 0.5 * ( textGeo.boundingBox.max.x - textGeo.boundingBox.min.x );
        //
        //     var textMaterial = new THREE.MeshPhongMaterial( { color: 0xff0000, specular: 0xffffff } );
        //
        //     var mesh = new THREE.Mesh( textGeo, textMaterial );
        //     mesh.position.x = centerOffset;
        //     mesh.position.y = FLOOR + 67;
        //
        //     mesh.castShadow = true;
        //     mesh.receiveShadow = true;
        //
        //     this.scene.add( mesh );
        //
        // } );

        //world
        let worldMaterials = [];
        let texture_ft = new THREE.TextureLoader().load(worldImgFT);
        let texture_bk = new THREE.TextureLoader().load(worldImgBK);
        let texture_up = new THREE.TextureLoader().load(worldImgUP);
        let texture_dn = new THREE.TextureLoader().load(worldImgDN);
        let texture_rt = new THREE.TextureLoader().load(worldImgRT);
        let texture_lf = new THREE.TextureLoader().load(worldImgLF);

        worldMaterials.push(new THREE.MeshBasicMaterial( { map: texture_ft, side: THREE.DoubleSide }));
        worldMaterials.push(new THREE.MeshBasicMaterial( { map: texture_bk, side: THREE.DoubleSide }));
        worldMaterials.push(new THREE.MeshBasicMaterial( { map: texture_up, side: THREE.DoubleSide }));
        worldMaterials.push(new THREE.MeshBasicMaterial( { map: texture_dn, side: THREE.DoubleSide }));
        worldMaterials.push(new THREE.MeshBasicMaterial( { map: texture_rt, side: THREE.DoubleSide }));
        worldMaterials.push(new THREE.MeshBasicMaterial( { map: texture_lf, side: THREE.DoubleSide }));

        let worldGeometry = new THREE.BoxGeometry( 400, 400, 400 );

        this.world = new THREE.Mesh( worldGeometry, worldMaterials );
        this.scene.add( this.world );

        //ground
        let groundTexture = new THREE.TextureLoader().load(groundImg);
        // let groundMaterial = new THREE.MeshLambertMaterial( { map: groundTexture } );
        let groundMaterial = new THREE.MeshBasicMaterial( { map: groundTexture } );
        let ground = new THREE.Mesh(
            new THREE.PlaneBufferGeometry( 400, 400 ),
            groundMaterial
        );
        ground.rotation.x = - Math.PI / 2;
        this.scene.add( ground );

        document.querySelector("#viewport").appendChild(this.renderer.domElement);
    }

    animate = () => {

        requestAnimationFrame(this.animate);

        this.boxExample.rotation.x += 0.01;
        this.boxExample.rotation.y += 0.02;

        this.jongil.rotation.y += 0.01;

        this.controls.update();
        this.renderer.render(this.scene, this.camera);

    }

    componentDidMount() {
        this.init();
        this.animate();
    }

    render() {
        return (
            <>
                <section id="viewport"></section>
            </>
        );
    }

}

export default RenderView;