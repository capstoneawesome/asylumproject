import React, {Component} from "react";
import RenderView from "./RenderView/RenderView";
import YouTube from "react-youtube";

class Intro extends Component {

    render() {
        const opts = {
            height: "225",
            width: "360",
            playerVars: {
                autoplay: 1
            }
        };

        return (
            <>
                {/*<YouTube*/}
                {/*    videoId="7rK6qzwwOi0"*/}
                {/*    opts={opts}*/}
                {/*    className="is-overlay asp-youtube"*/}
                {/*/>*/}
                <RenderView />
            </>
        );
    }

}

export default Intro;
