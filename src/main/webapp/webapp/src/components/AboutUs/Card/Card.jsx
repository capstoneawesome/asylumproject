import React, {Component} from "react";
import Jongil from "../IMG_1192.JPG";
import JohnDoe from "../download.jpg";

class Card extends Component {

    handleProfile = (name) => {

        if(name === "JONGIL YOON") {
            return (
                <img src={Jongil} alt="Placeholder image" />
            );
        } else {
            return (
                <img src={JohnDoe} alt="Placeholder image" />
            );
        }
    }

    render() {
        return (
            <>
                <div className="container asp-common-body">
                    <div className="card">
                        {/*<div className="card-image">*/}
                        {/*    <figure className="image is-4by3">*/}
                        {/*        <img src={Jongil} alt="Placeholder image" />*/}
                        {/*    </figure>*/}
                        {/*</div>*/}
                        <div className="card-content">
                            <div className="media">
                                <div className="media-left">
                                    <figure className="image is-48x48">
                                        {this.handleProfile(this.props.name)}
                                    </figure>
                                </div>
                                <div className="media-content">
                                    <p className="title is-4">{this.props.name}</p>
                                    <p className="subtitle is-6">@{this.props.name}</p>
                                </div>
                            </div>

                            <div className="content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                                <a href="#">#css</a> <a href="#">#responsive</a>
                                <br />
                                <time dateTime="2016-1-1">11:09 AM - 31 Jan 2020</time>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }

}

export default Card;