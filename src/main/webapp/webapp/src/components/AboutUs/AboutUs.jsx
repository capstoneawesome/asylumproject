import React, {Component} from "react";
import Card from "./Card/Card";
import CoordinatePicker from "../ContentManager/CoordinatePicker";
import NewMenu from "../MainMenu/NewMenu";

class AboutUs extends Component {

    members = [
        "JEFFREY BORCHERT",
        "AMIR BOZORGYAMCHI",
        "CALDER TROMBLEY",
        "JORGE BLANCO",
        "JONGIL YOON",
    ]

    render() {
        return (
            <>
                <section className="hero-body">
                    {this.members.map((v, i) => {
                        return (
                            <Card key={i} name={v} />
                        );
                    })}
                </section>
                <CoordinatePicker/>
                <NewMenu/>

            </>
        );
    }

}

export default AboutUs;