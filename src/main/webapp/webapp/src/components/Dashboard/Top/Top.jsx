import React from "react";
import {NavLink} from "react-router-dom";

/**
 * Component that display the top navigation bar in the dashboard.
 * @returns {*}
 */

export default function Top() {

    return (
        <>
            <nav className="navbar asp-navbar-top is-fixed-top">

                <div className="asp-navbar-offset" />

                <div className="navbar-menu fadeIn animated faster">
                    <div className="navbar-end">
                        <NavLink to="/map" title="Map" className="navbar-item has-divider is-desktop-icon-only">
                            <span className="icon has-update-mark"><i className="fas fa-map-marked-alt"/></span>
                            <span>Map</span>
                        </NavLink>
                        <NavLink to="#" title="Updates" className="navbar-item has-divider is-desktop-icon-only">
                            <span className="icon has-update-mark"><i className="fas fa-bell"/></span>
                            <span>Updates</span>
                        </NavLink>
                        <NavLink to="/signout" title="Log out" className="navbar-item is-desktop-icon-only">
                            <span className="icon"><i className="fas fa-sign-out-alt"/></span>
                            <span>Log out</span>
                        </NavLink>
                    </div>
                </div>

            </nav>
        </>
    );

};
