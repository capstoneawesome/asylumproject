import React, {useEffect, useRef, useState} from "react";

import * as THREE from "three";
import RINGS from "vanta/dist/vanta.rings.min";
import {Redirect, Route, Switch} from "react-router-dom";
import Accountboard from "../../Account/Accountboard/Accountboard";
import AccountView from "../../Account/Accountboard/View";
import Adminboard from "../../Admin/Adminboard/Adminboard";
import Users from "../../Admin/Adminboard/Users/Users";
import Reports from "../../Admin/Adminboard/Report/Reports";
import DBManager from "../../Admin/Adminboard/Database/DBManager";
import Avatar from "react-avatar";
import * as authenticationActions from "../../../actions/authentication";
import {connect} from "react-redux";

// Redux settings below
// Before you edit this section, Please refer to connect() method of react-redux
const mapStateToProps = (state) => {
    return {
        isSignIn: state.authenticationReducer.isSignIn,
        user_defaultLanguage: state.authenticationReducer.user_defaultLanguage,
        user_firstName: state.authenticationReducer.user_firstName,
        user_lastName: state.authenticationReducer.user_lastName,
        user_authorities: state.authenticationReducer.user_authorities,
        user_username: state.authenticationReducer.user_username,
        user_email: state.authenticationReducer.user_email,
        user_photoPath: state.authenticationReducer.user_photoPath,
    };
};

const mapDispatchProps = (dispatch) => {
    // return bindActionCreator(actions, dispatch);
    return {
        handleAuthentication: (isSignIn) => {dispatch(authenticationActions.authenticationAction(isSignIn))},
        handleDefaultLanguage: (user_defaultLanguage) => {dispatch(authenticationActions.defaultLanguageAction(user_defaultLanguage))},
        handleFirstName: (user_firstName) => {dispatch(authenticationActions.firstNameAction(user_firstName))},
        handleLastName: (user_lastName) => {dispatch(authenticationActions.lastNameAction(user_lastName))},
        handlePhotoPath: (user_photoPath) => {dispatch(authenticationActions.photoPathAction(user_photoPath))},
        handleAuthorities: (user_authorities) => {dispatch(authenticationActions.authoritiesAction(user_authorities))},
        handleUsername: (user_username) => {dispatch(authenticationActions.usernameAction(user_username))},
        handleEmail: (user_email) => {dispatch(authenticationActions.emailAction(user_email))},
    };
};

Body = connect(mapStateToProps, mapDispatchProps)(Body);
// Redux settings end

/**
 * Component that display a body of dashboard.
 * It provides a placeholder for each sub menu content can be mounted on.
 * @param props props from parent component
 * @returns {*}
 */

export default function Body(props) {

    const [vantaEffect, setVantaEffect] = useState(0);
    const background = useRef(null);

    const handleCheckingPermission = () => {
        if(props.user_authorities.includes("ROLE_SYSTEM_ADMIN")) {
            return "ADMINISTRATOR";
        } else if(props.user_authorities.includes("ROLE_CONTENT_CURATOR")) {
            return "CONTENT CURATOR";
        } else if(props.user_authorities.includes("ROLE_TEACHER")) {
            return "TEACHER";
        }
    };

    useEffect(() => {
        if (!vantaEffect) {
            setVantaEffect(RINGS({
                THREE: THREE,
                el: background.current,
                mouseControls: true,
                touchControls: true,
                minHeight: 120.00,
                minWidth: 200.00,
                scale: 1.00,
                scaleMobile: 1.00,
                backgroundColor: 0xf8f8f8,
                color: 0xb6651d
            }))
        }
        return () => {
            if (vantaEffect) vantaEffect.destroy()
        }
    }, [vantaEffect]);

    return (
        <>
            <section className="hero asp-dashboard-body asp-full-height asp-default-color-background welcome is-small">
                <div className="asp-scrollable-body">

                    <section className="hero asp-default-color-background welcome is-small">
                        <div ref={background} className="hero-body">
                            <div className="container">
                                <div className="columns">
                                    <div className="asp-round-avatar column is-2">
                                        <Avatar name={props.user_firstName + " " + props.user_lastName} />
                                    </div>
                                    <div className="column is-8">
                                        <h1 className="title">
                                            Hello, {props.user_username}.
                                        </h1>
                                        <h2 className="subtitle">
                                            {handleCheckingPermission()}
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <Switch>
                        <Redirect exact from="/account" to="/account/accountboard" />
                        <Redirect exact from="/admin" to="/admin/adminboard" />
                        <Route path="/account/accountboard" component={Accountboard} />
                        <Route path="/account/view" component={AccountView} />
                        <Route path="/admin/adminboard" component={Adminboard} />
                        <Route path="/admin/users" component={Users} />
                        <Route path="/admin/reports" component={Reports} />
                        <Route path="/admin/database" component={DBManager} />
                    </Switch>

                </div>
            </section>
        </>
    );
};

// Array.from(document.getElementsByClassName("menu is-menu-main")).forEach(function (e) {
//     Array.from(e.getElementsByClassName("has-dropdown-icon")).forEach(function (e) {
//         e.addEventListener("click", function (e) {
//             var t = e.currentTarget.getElementsByClassName("dropdown-icon")[0].getElementsByClassName("mdi")[0];
//             e.currentTarget.parentNode.classList.toggle("is-active"), t.classList.toggle("mdi-plus"), t.classList.toggle("mdi-minus")
//         })
//     })
// }), Array.from(document.getElementsByClassName("jb-aside-mobile-toggle")).forEach(function (e) {
//     e.addEventListener("click", function (e) {
//         var t = e.currentTarget.getElementsByClassName("icon")[0].getElementsByClassName("mdi")[0];
//         document.documentElement.classList.toggle("has-aside-mobile-expanded"), t.classList.toggle("mdi-forwardburger"), t.classList.toggle("mdi-backburger")
//     })
// }), Array.from(document.getElementsByClassName("jb-navbar-menu-toggle")).forEach(function (e) {
//     e.addEventListener("click", function (e) {
//         var t = e.currentTarget.getElementsByClassName("icon")[0].getElementsByClassName("mdi")[0];
//         document.getElementById(e.currentTarget.getAttribute("data-target")).classList.toggle("is-active"), t.classList.toggle("mdi-dots-vertical"), t.classList.toggle("mdi-close")
//     })
// }), Array.from(document.getElementsByClassName("jb-modal")).forEach(function (e) {
//     e.addEventListener("click", function (e) {
//         var t = e.currentTarget.getAttribute("data-target");
//         document.getElementById(t).classList.add("is-active"), document.documentElement.classList.add("is-clipped")
//     })
// }), Array.from(document.getElementsByClassName("jb-modal-close")).forEach(function (e) {
//     e.addEventListener("click", function (e) {
//         e.currentTarget.closest(".modal").classList.remove("is-active"), document.documentElement.classList.remove("is-clipped")
//     })
// }), Array.from(document.getElementsByClassName("jb-notification-dismiss")).forEach(function (e) {
//     e.addEventListener("click", function (e) {
//         e.currentTarget.closest(".notification").classList.add("is-hidden")
//     })
// });
