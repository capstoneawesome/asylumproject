import React from "react";
import {useHistory} from 'react-router-dom';
import {useToasts} from 'react-toast-notifications';
import MenuItem from "../../MenuItem";
import {createStory} from "../../../../../libraries/createStory";
import {connect} from "react-redux";

/**
 * Component that display as a sub menu set in a side menu bar and pass URI address into MenuItem.
 * @param props props from parent component
 * @returns {*}
 */

export default function CuratorSets(props) {

    const history = useHistory();
    const {addToast} = useToasts();

    let creator = props.user_id;

    //Create a new Story in the 'DRAFT' state immediately after starting Story creation
    let createNewStory = () => {
        createStory(creator).then((response) => {
            if (response.status === 200) {
                let newStory = response.data;
                history.push("/contentmanager/stories/" + newStory.contentID);
            } else {
                addToast('Error creating Story', {appearance: 'error', autoDismiss: true});
            }
        });
    };

    return (
        <>
            <p className="menu-label">Content Management</p>
            <MenuItem register={props.handleRegister} menuName="Manage Stories" iconName="fa-tools" uri="/contentmanager"/>
            <MenuItem register={props.handleRegister} menuName="Create a New Story" iconName="fa-user-edit" uri="/contentmanager/stories"/>
        </>
    );

};

const mapStateToProps = (state) => {
    return {
        user_id: state.authenticationReducer.user_id,
    };
};

CuratorSets = connect(mapStateToProps)(CuratorSets);
