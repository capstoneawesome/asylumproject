import React from "react";
import * as authenticationActions from "../../../../actions/authentication";
import {connect} from "react-redux";
import AdminSets from "./AdminSets/AdminSets";
import TeacherSets from "./TeacherSets/TeacherSets";
import CuratorSets from "./CuratorSets/CuratorSets";

// Redux settings below.
// This is why you can use 'store' in a grand child component
// without passing 'props' through every components in an inheritance hierarchy.

// This function related to binding states declared in between Redux store and your component
const mapStateToProps = (state) => {
    return {
        isSignIn: state.authenticationReducer.isSignIn,
        user_authorities: state.authenticationReducer.user_authorities,
    };
};

// if you only refer to states and you don't change states in Redux store, you probably don't need to have below function
// This function is related to modifying states in Redux store
const mapDispatchProps = (dispatch) => {
    return {
        handleAuthentication: (isSignIn) => {dispatch(authenticationActions.authenticationAction(isSignIn))},
    };
};

MenuAddOn = connect(mapStateToProps, mapDispatchProps)(MenuAddOn);

/**
 * Component that check user permission in order to display sub-menu sets.
 * @param props props from parent component
 * @returns {null|*}
 */

export default function MenuAddOn(props) {

    return (
        <>
            {
                props.user_authorities !== null && props.user_authorities.includes("ROLE_TEACHER")
                    ? <TeacherSets {...props}/>
                    : <></>
            }
            {
                props.user_authorities !== null && props.user_authorities.includes("ROLE_CONTENT_CURATOR")
                    ? <CuratorSets {...props}/>
                    : <></>
            }
            {
                props.user_authorities !== null && props.user_authorities.includes("ROLE_SYSTEM_ADMIN")
                    ? <AdminSets {...props}/>
                    : <></>
            }
        </>
    );

}