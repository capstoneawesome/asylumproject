import React from "react";
import MenuItem from "../../MenuItem";

/**
 * Component that display as a sub menu set in a side menu bar and pass URI address into MenuItem.
 * @param props props from parent component
 * @returns {*}
 */

export default function AdminSets(props) {
    return (
        <>
            <p className="menu-label">Administration</p>
            <MenuItem register={props.handleRegister} menuName="Users" iconName="fa-users-cog" uri="/admin/users"/>
            <MenuItem register={props.handleRegister} menuName="Report" iconName="fa-chart-pie" uri="/admin/reports"/>
            <MenuItem register={props.handleRegister} menuName="Database" iconName="fa-database" uri="/admin/database"/>
        </>
    );
};