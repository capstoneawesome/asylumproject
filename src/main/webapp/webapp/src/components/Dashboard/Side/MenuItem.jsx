import React, {useEffect, useRef, useState} from "react";
import {Link, NavLink} from "react-router-dom";

/**
 * Component that display each menu item as a link in a side menu bar.
 * @param props props from parent component
 * @returns {*}
 * @constructor
 */

export default function MenuItem(props) {

    return (
        <>
            <li className="asp-menu-activation">
                <Link onClick={props.register} to={props.uri} className="has-icon" title={props.menuName}>
                    <span className="icon"><i className={"fas" + " " + props.iconName}/></span>
                    <span className="asp-dashboard-item-label">{props.menuName}</span>
                </Link>
            </li>
        </>
    );

};

