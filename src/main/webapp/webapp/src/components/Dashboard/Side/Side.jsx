import React, {useState} from "react";
import MenuItem from "./MenuItem";
import MenuAddOn from "./MenuAddOn/MenuAddOn";

/**
 * Component that display side menu bar which contains sub-menu sets depending on user permission.
 * @returns {*}
 */

export default function Side() {

    const [previouslyActivatedMenu, setPreviouslyActivatedMenu] = useState(null);

    const handleRegister = e => {
        console.log(e.target);
        let newlyActivatedMenu = e.target;

        if(previouslyActivatedMenu !== null) {
            previouslyActivatedMenu.classList.remove("is-active");
        }

        newlyActivatedMenu.classList.add("is-active");
        setPreviouslyActivatedMenu(newlyActivatedMenu);
    };

    return (
        <>
            <aside className="asp-side is-placed-left">

                <div className="aside-branding">
                    <div className="aside-branding-label" />
                </div>

                <div className="menu-container ps">
                    <div className="menu is-menu-main">

                        <ul className="menu-list">
                            <p className="menu-label">General</p>
                            <MenuItem register={handleRegister} menuName="Account" iconName="fa-tachometer-alt" uri="/account"/>
                            {/*<MenuItem ref={activatedLink} fetchNode={handleDeactivatedLink} menuName="Account" iconName="fa-tachometer-alt" uri="/account"/>*/}
                        </ul>

                        <ul className="menu-list">
                            <MenuAddOn handleRegister={handleRegister} />
                        </ul>

                    </div>
                </div>

                <div className="menu is-menu-bottom">
                    <ul className="menu-list">
                        <p className="menu-label">Map Access</p>
                        <MenuItem register={handleRegister} menuName="Map" iconName="fa-map-marked-alt" uri="/map"/>
                    </ul>
                </div>

            </aside>
        </>
    );

};