import React from "react";
import Top from "./Top/Top";
import Side from "./Side/Side";
import ContentBody from "./Body/Body";
import "./css/dashboard.scss";

/**
 * Component that compose a basic layout for a user dashboard.
 * @returns {*}
 */

export default function Dashboard() {

    return (
        <>
            <Top />
            <Side />
            <ContentBody />
        </>
    );

};