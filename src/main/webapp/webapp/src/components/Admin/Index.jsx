import React, {Component} from "react";
import Dashboard from "../Dashboard";
// import {Redirect, Route, Link, Switch, BrowserRouter} from "react-router-dom";
// import Users from "./Adminboard/Users/Users";
// import Reports from "./Adminboard/Report/Reports";
// import DBManager from "./Adminboard/Database/DBManager";
// import Adminboard from "./Adminboard/Adminboard";
import "./css/admin.scss";

/**
 * Component that give a placeholder for dashboard for administrator.
 * @returns {*}
 */

class Index extends Component {

    render() {
        return (
            <Dashboard/>
            // <>
            //     {/*<div className="hero is-fullheight-with-navbar">*/}
            //     <div className="hero is-fullheight">
            //         <div className="hero-body asp-hero-body">
            //
            //             <div className="columns">
            //                 <div className="column is-3 ">
            //                     <aside className="menu is-hidden-mobile">
            //                         <p className="menu-label">
            //                             General
            //                         </p>
            //                         <ul className="menu-list">
            //                             <Link to="/account">Dashboard</Link>
            //                             <Link to="/map">Map</Link>
            //                         </ul>
            //                         <br/>
            //                         <br/>
            //                         <p className="menu-label">
            //                             Administration
            //                         </p>
            //                         <ul className="menu-list">
            //                             <li>
            //                                 <Link to="/admin/adminboard">Adminboard</Link>
            //                             </li>
            //                             <li>
            //                                 <Link to="/admin/users">Users</Link>
            //                             </li>
            //                             {/*<li><a>Archive</a></li>*/}
            //                             {/*<li><a>Backup</a></li>*/}
            //                             <li>
            //                                 <Link to="/admin/reports">Reports</Link>
            //                             </li>
            //                             <li>
            //                                 <Link to="/admin/database">Database</Link>
            //                             </li>
            //                         </ul>
            //                     </aside>
            //                 </div>
            //
            //                 <div className="column is-9">
            //
            //                     {/*Adminboard*/}
            //                     <Switch>
            //                         <Redirect exact from="/admin" to="/admin/adminboard" />
            //
            //                         <Route path="/admin/adminboard" component={Adminboard} />
            //
            //                         {/*users*/}
            //                         <Route path="/admin/users" component={Users} />
            //
            //                         {/*permission*/}
            //                         {/*backup*/}
            //                         {/*archive*/}
            //
            //                         {/*Reports*/}
            //                         <Route path="/admin/reports" component={Reports} />
            //                         <Route path="/admin/database" component={DBManager} />
            //                     </Switch>
            //
            //                 </div>
            //
            //             </div>
            //         </div>
            //     </div>
            // </>
        );
    }

}

export default Index;
