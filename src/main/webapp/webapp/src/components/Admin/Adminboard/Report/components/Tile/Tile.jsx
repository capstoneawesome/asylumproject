import React from "react";
import "./tile_style.scss"

/**
 * Component responsible to display data in a tile format.
 * @param props props from parent component.
 * @returns {null|*}
 * @constructor
 */
export default function Tile (props) {

    return (
        <section className="info-tiles" >
            {/*<div className="tile is-ancestor has-text-centered">*/}
            {/*    <div className="tile is-parent">*/}
                    <article className="tile is-child box">
                        <p className="title">{props.displayData.value}</p>
                        <p className="subtitle">{props.displayData.name} {props.plural}</p>
                    </article>
                {/*</div>*/}
            {/*</div>*/}
        </section>

    );

}

