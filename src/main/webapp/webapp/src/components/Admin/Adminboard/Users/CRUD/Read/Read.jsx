// <div className="card has-table has-mobile-sort-spaced">
//     <header className="card-header"><p className="card-header-title"><span className="icon"><i
//         className="mdi mdi-account-multiple default"></i></span><span>Clients</span></p>
//         <button type="button" className="button is-small"><span className="icon"><i
//             className="mdi mdi-refresh default"></i></span><span>Refresh</span></button>
//     </header>
//     <div className="notification is-card-toolbar">
//         <div className="level">
//             <div className="level-left">
//                 <div className="level-item">
//                     <div className="buttons has-addons">
//                         <button className="button is-active">Active</button>
//                         <button disabled="disabled" className="button">Recent</button>
//                         <button disabled="disabled" className="button">Archived</button>
//                     </div>
//                 </div>
//             </div>
//             <div className="level-right">
//                 <div className="level-item">
//                     <form>
//                         <div className="field has-addons">
//                             <div className="control"><input type="text" placeholder="Sample field..." className="input">
//                             </div>
//                             <div className="control">
//                                 <button type="submit" className="button is-primary"><span className="icon"><i
//                                     className="mdi mdi-dots-horizontal default"></i></span></button>
//                             </div>
//                         </div>
//                     </form>
//                 </div>
//             </div>
//         </div>
//     </div>
//     <div className="card-content">
//         <div><!---->
//             <div className="b-table">
//                 <div className="field table-mobile-sort">
//                     <div className="field has-addons">
//                         <div className="control is-expanded"><span
//                             className="select is-fullwidth"><select><!----> <!----> <!---->
//                             <option value="[object Object]">
//                     Name
//                 </option><option value="[object Object]">
//                     Company
//                 </option><option value="[object Object]">
//                     City
//                 </option><option value="[object Object]">
//                     Progress
//                 </option>
//                             <!----><!----></select></span> <!----></div>
//                         <div className="control">
//                             <button className="button is-primary"><span className="icon is-small"><i
//                                 className="mdi mdi-arrow-up"></i></span></button>
//                         </div>
//                     </div>
//                 </div>
//                 <!---->
//                 <div className="table-wrapper has-mobile-cards">
//                     <table className="table is-striped is-hoverable">
//                         <thead>
//                         <tr><!----> <!---->
//                             <th className="">
//                                 <div className="th-wrap"> <!----></div>
//                             </th>
//                             <th className="is-current-sort is-sortable">
//                                 <div className="th-wrap">Name <span className="icon is-small"><i
//                                     className="mdi mdi-arrow-up"></i></span></div>
//                             </th>
//                             <th className="is-sortable">
//                                 <div className="th-wrap">Company <span className="icon is-small is-invisible"><i
//                                     className="mdi mdi-arrow-up"></i></span></div>
//                             </th>
//                             <th className="is-sortable">
//                                 <div className="th-wrap">City <span className="icon is-small is-invisible"><i
//                                     className="mdi mdi-arrow-up"></i></span></div>
//                             </th>
//                             <th className="is-sortable">
//                                 <div className="th-wrap">Progress <span className="icon is-small is-invisible"><i
//                                     className="mdi mdi-arrow-up"></i></span></div>
//                             </th>
//                             <th className="">
//                                 <div className="th-wrap">Created <!----></div>
//                             </th>
//                             <th className="">
//                                 <div className="th-wrap"> <!----></div>
//                             </th>
//                             <!----></tr>
//                         <!----> <!----></thead>
//                         <tbody>
//                         <tr draggable="false" className=""><!----> <!---->
//                             <td className="has-no-head-mobile is-image-cell">
//                                 <div className="image"><img src="/data-sources/avatars/andy-lee-642320-unsplash.jpg"
//                                                             className="is-rounded"></div>
//                             </td>
//                             <td data-label="Name" className=""> Alvis Anderson MD</td>
//                             <td data-label="Company" className=""> Waters Ltd</td>
//                             <td data-label="City" className=""> South Larissatown</td>
//                             <td data-label="Progress" className="is-progress-col">
//                                 <progress max="100" className="progress is-small is-primary" value="41">41</progress>
//                             </td>
//                             <td data-label="Created" className=""><small title="Sep 19, 2018"
//                                                                          className="has-text-grey is-abbr-like">Sep 19,
//                                 2018</small></td>
//                             <td className="is-actions-cell">
//                                 <div className="buttons is-right"><a href="#/client/15"
//                                                                      className="button is-small is-primary"><span
//                                     className="icon is-small"><i className="mdi mdi-account-edit"></i></span></a>
//                                     <button type="button" className="button is-small is-danger"><span
//                                         className="icon is-small"><i className="mdi mdi-trash-can"></i></span></button>
//                                 </div>
//                             </td>
//                             <!----></tr>
//                         <!----> <!---->
//                         <tr draggable="false" className=""><!----> <!---->
//                             <td className="has-no-head-mobile is-image-cell">
//                                 <div className="image"><img src="/data-sources/avatars/andy-lee-642320-unsplash.jpg"
//                                                             className="is-rounded"></div>
//                             </td>
//                             <td data-label="Name" className=""> America Mertz</td>
//                             <td data-label="Company" className=""> Dickens Inc</td>
//                             <td data-label="City" className=""> Angusstad</td>
//                             <td data-label="Progress" className="is-progress-col">
//                                 <progress max="100" className="progress is-small is-primary" value="63">63</progress>
//                             </td>
//                             <td data-label="Created" className=""><small title="Mar 14, 2019"
//                                                                          className="has-text-grey is-abbr-like">Mar 14,
//                                 2019</small></td>
//                             <td className="is-actions-cell">
//                                 <div className="buttons is-right"><a href="#/client/36"
//                                                                      className="button is-small is-primary"><span
//                                     className="icon is-small"><i className="mdi mdi-account-edit"></i></span></a>
//                                     <button type="button" className="button is-small is-danger"><span
//                                         className="icon is-small"><i className="mdi mdi-trash-can"></i></span></button>
//                                 </div>
//                             </td>
//                             <!----></tr>
//                         <!----> <!---->
//                         <tr draggable="false" className=""><!----> <!---->
//                             <td className="has-no-head-mobile is-image-cell">
//                                 <div className="image"><img src="/data-sources/avatars/nicolas-horn-689011-unsplash.jpg"
//                                                             className="is-rounded"></div>
//                             </td>
//                             <td data-label="Name" className=""> Augustine Greenholt</td>
//                             <td data-label="Company" className=""> Streich, Purdy and Friesen</td>
//                             <td data-label="City" className=""> Port Nikohaven</td>
//                             <td data-label="Progress" className="is-progress-col">
//                                 <progress max="100" className="progress is-small is-primary" value="28">28</progress>
//                             </td>
//                             <td data-label="Created" className=""><small title="Dec 4, 2018"
//                                                                          className="has-text-grey is-abbr-like">Dec 4,
//                                 2018</small></td>
//                             <td className="is-actions-cell">
//                                 <div className="buttons is-right"><a href="#/client/16"
//                                                                      className="button is-small is-primary"><span
//                                     className="icon is-small"><i className="mdi mdi-account-edit"></i></span></a>
//                                     <button type="button" className="button is-small is-danger"><span
//                                         className="icon is-small"><i className="mdi mdi-trash-can"></i></span></button>
//                                 </div>
//                             </td>
//                             <!----></tr>
//                         <!----> <!---->
//                         <tr draggable="false" className=""><!----> <!---->
//                             <td className="has-no-head-mobile is-image-cell">
//                                 <div className="image"><img src="/data-sources/avatars/nicolas-horn-689011-unsplash.jpg"
//                                                             className="is-rounded"></div>
//                             </td>
//                             <td data-label="Name" className=""> Barry Weber</td>
//                             <td data-label="Company" className=""> Schulist, Mosciski and Heidenreich</td>
//                             <td data-label="City" className=""> East Violettestad</td>
//                             <td data-label="Progress" className="is-progress-col">
//                                 <progress max="100" className="progress is-small is-primary" value="42">42</progress>
//                             </td>
//                             <td data-label="Created" className=""><small title="Jul 24, 2019"
//                                                                          className="has-text-grey is-abbr-like">Jul 24,
//                                 2019</small></td>
//                             <td className="is-actions-cell">
//                                 <div className="buttons is-right"><a href="#/client/19"
//                                                                      className="button is-small is-primary"><span
//                                     className="icon is-small"><i className="mdi mdi-account-edit"></i></span></a>
//                                     <button type="button" className="button is-small is-danger"><span
//                                         className="icon is-small"><i className="mdi mdi-trash-can"></i></span></button>
//                                 </div>
//                             </td>
//                             <!----></tr>
//                         <!----> <!---->
//                         <tr draggable="false" className=""><!----> <!---->
//                             <td className="has-no-head-mobile is-image-cell">
//                                 <div className="image"><img
//                                     src="/data-sources/avatars/benjamin-parker-699522-unsplash.jpg"
//                                     className="is-rounded"></div>
//                             </td>
//                             <td data-label="Name" className=""> Cathrine Erdman</td>
//                             <td data-label="Company" className=""> Nolan-Kozey</td>
//                             <td data-label="City" className=""> West Biankafurt</td>
//                             <td data-label="Progress" className="is-progress-col">
//                                 <progress max="100" className="progress is-small is-primary" value="79">79</progress>
//                             </td>
//                             <td data-label="Created" className=""><small title="Jan 30, 2019"
//                                                                          className="has-text-grey is-abbr-like">Jan 30,
//                                 2019</small></td>
//                             <td className="is-actions-cell">
//                                 <div className="buttons is-right"><a href="#/client/14"
//                                                                      className="button is-small is-primary"><span
//                                     className="icon is-small"><i className="mdi mdi-account-edit"></i></span></a>
//                                     <button type="button" className="button is-small is-danger"><span
//                                         className="icon is-small"><i className="mdi mdi-trash-can"></i></span></button>
//                                 </div>
//                             </td>
//                             <!----></tr>
//                         <!----> <!---->
//                         <tr draggable="false" className=""><!----> <!---->
//                             <td className="has-no-head-mobile is-image-cell">
//                                 <div className="image"><img
//                                     src="/data-sources/avatars/freestocks-org-190062-unsplash.jpg"
//                                     className="is-rounded"></div>
//                             </td>
//                             <td data-label="Name" className=""> Cleora Vandervort DDS</td>
//                             <td data-label="Company" className=""> Shields and Sons</td>
//                             <td data-label="City" className=""> Port Shannaport</td>
//                             <td data-label="Progress" className="is-progress-col">
//                                 <progress max="100" className="progress is-small is-primary" value="15">15</progress>
//                             </td>
//                             <td data-label="Created" className=""><small title="Jan 14, 2019"
//                                                                          className="has-text-grey is-abbr-like">Jan 14,
//                                 2019</small></td>
//                             <td className="is-actions-cell">
//                                 <div className="buttons is-right"><a href="#/client/17"
//                                                                      className="button is-small is-primary"><span
//                                     className="icon is-small"><i className="mdi mdi-account-edit"></i></span></a>
//                                     <button type="button" className="button is-small is-danger"><span
//                                         className="icon is-small"><i className="mdi mdi-trash-can"></i></span></button>
//                                 </div>
//                             </td>
//                             <!----></tr>
//                         <!----> <!---->
//                         <tr draggable="false" className=""><!----> <!---->
//                             <td className="has-no-head-mobile is-image-cell">
//                                 <div className="image"><img src="/data-sources/avatars/andy-lee-642320-unsplash.jpg"
//                                                             className="is-rounded"></div>
//                             </td>
//                             <td data-label="Name" className=""> Devante Jast</td>
//                             <td data-label="Company" className=""> Goyette and Sons</td>
//                             <td data-label="City" className=""> Williamsonfurt</td>
//                             <td data-label="Progress" className="is-progress-col">
//                                 <progress max="100" className="progress is-small is-primary" value="38">38</progress>
//                             </td>
//                             <td data-label="Created" className=""><small title="Aug 4, 2019"
//                                                                          className="has-text-grey is-abbr-like">Aug 4,
//                                 2019</small></td>
//                             <td className="is-actions-cell">
//                                 <div className="buttons is-right"><a href="#/client/10"
//                                                                      className="button is-small is-primary"><span
//                                     className="icon is-small"><i className="mdi mdi-account-edit"></i></span></a>
//                                     <button type="button" className="button is-small is-danger"><span
//                                         className="icon is-small"><i className="mdi mdi-trash-can"></i></span></button>
//                                 </div>
//                             </td>
//                             <!----></tr>
//                         <!----> <!---->
//                         <tr draggable="false" className=""><!----> <!---->
//                             <td className="has-no-head-mobile is-image-cell">
//                                 <div className="image"><img
//                                     src="/data-sources/avatars/benjamin-parker-699522-unsplash.jpg"
//                                     className="is-rounded"></div>
//                             </td>
//                             <td data-label="Name" className=""> Domenica Harber</td>
//                             <td data-label="Company" className=""> Ankunding Inc</td>
//                             <td data-label="City" className=""> New Gailbury</td>
//                             <td data-label="Progress" className="is-progress-col">
//                                 <progress max="100" className="progress is-small is-primary" value="31">31</progress>
//                             </td>
//                             <td data-label="Created" className=""><small title="Feb 9, 2019"
//                                                                          className="has-text-grey is-abbr-like">Feb 9,
//                                 2019</small></td>
//                             <td className="is-actions-cell">
//                                 <div className="buttons is-right"><a href="#/client/27"
//                                                                      className="button is-small is-primary"><span
//                                     className="icon is-small"><i className="mdi mdi-account-edit"></i></span></a>
//                                     <button type="button" className="button is-small is-danger"><span
//                                         className="icon is-small"><i className="mdi mdi-trash-can"></i></span></button>
//                                 </div>
//                             </td>
//                             <!----></tr>
//                         <!----> <!---->
//                         <tr draggable="false" className=""><!----> <!---->
//                             <td className="has-no-head-mobile is-image-cell">
//                                 <div className="image"><img
//                                     src="/data-sources/avatars/benjamin-parker-699522-unsplash.jpg"
//                                     className="is-rounded"></div>
//                             </td>
//                             <td data-label="Name" className=""> Jailyn Reichel</td>
//                             <td data-label="Company" className=""> O'Reilly and Sons</td>
//                             <td data-label="City" className=""> West Lydia</td>
//                             <td data-label="Progress" className="is-progress-col">
//                                 <progress max="100" className="progress is-small is-primary" value="81">81</progress>
//                             </td>
//                             <td data-label="Created" className=""><small title="Aug 4, 2019"
//                                                                          className="has-text-grey is-abbr-like">Aug 4,
//                                 2019</small></td>
//                             <td className="is-actions-cell">
//                                 <div className="buttons is-right"><a href="#/client/6"
//                                                                      className="button is-small is-primary"><span
//                                     className="icon is-small"><i className="mdi mdi-account-edit"></i></span></a>
//                                     <button type="button" className="button is-small is-danger"><span
//                                         className="icon is-small"><i className="mdi mdi-trash-can"></i></span></button>
//                                 </div>
//                             </td>
//                             <!----></tr>
//                         <!----> <!---->
//                         <tr draggable="false" className=""><!----> <!---->
//                             <td className="has-no-head-mobile is-image-cell">
//                                 <div className="image"><img src="/data-sources/avatars/nicolas-horn-689011-unsplash.jpg"
//                                                             className="is-rounded"></div>
//                             </td>
//                             <td data-label="Name" className=""> Jarret Lemke</td>
//                             <td data-label="Company" className=""> Roberts, Gerlach and Runolfsson</td>
//                             <td data-label="City" className=""> Brettchester</td>
//                             <td data-label="Progress" className="is-progress-col">
//                                 <progress max="100" className="progress is-small is-primary" value="33">33</progress>
//                             </td>
//                             <td data-label="Created" className=""><small title="Jan 20, 2019"
//                                                                          className="has-text-grey is-abbr-like">Jan 20,
//                                 2019</small></td>
//                             <td className="is-actions-cell">
//                                 <div className="buttons is-right"><a href="#/client/44"
//                                                                      className="button is-small is-primary"><span
//                                     className="icon is-small"><i className="mdi mdi-account-edit"></i></span></a>
//                                     <button type="button" className="button is-small is-danger"><span
//                                         className="icon is-small"><i className="mdi mdi-trash-can"></i></span></button>
//                                 </div>
//                             </td>
//                             <!----></tr>
//                         <!----> <!----></tbody>
//                         <!----></table>
//                 </div>
//                 <div className="level">
//                     <div className="level-left"></div>
//                     <div className="level-right">
//                         <div className="level-item">
//                             <nav className="pagination"><a role="button" href="#" disabled="disabled"
//                                                            className="pagination-link pagination-previous"><span
//                                 className="icon" aria-hidden="true"><i
//                                 className="mdi mdi-chevron-left mdi-24px"></i></span></a> <a role="button" href="#"
//                                                                                              className="pagination-link pagination-next"><span
//                                 className="icon" aria-hidden="true"><i
//                                 className="mdi mdi-chevron-right mdi-24px"></i></span></a>
//                                 <ul className="pagination-list"><!----> <!---->
//                                     <li><a role="button" href="#" aria-current="true"
//                                            className="pagination-link is-current ">1</a></li>
//                                     <li><a role="button" href="#" className="pagination-link">2</a></li>
//                                     <!----> <!----></ul>
//                             </nav>
//                         </div>
//                     </div>
//                 </div>
//             </div>
//         </div>
//     </div>
//     <!----></div>