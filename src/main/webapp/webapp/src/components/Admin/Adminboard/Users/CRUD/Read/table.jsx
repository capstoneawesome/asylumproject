import React, {useState, useEffect} from "react";
import {Link, Route} from "react-router-dom";
import UserDetails from "./UserDetails";
import ISO6391 from "iso-639-1";
import ISO6393 from "iso-639-3";
import Avatar from "react-avatar";
import Moment from "moment";
import Delete from "../Delete/Delete";

/**
 * Component that holds a table for user list.
 * It contains user create, delete and modify functions.
 * @param props props from parent component
 * @returns {*}
 */
function Table(props) {

    let tableRows = [];
    let isSelected = "";

    const [viewUser, setViewUser] = useState(false);
    const [deleteUser, setDeleteUser] = useState(false);

    const handleView = () => {
        setViewUser(true);
    };

    const handleCloseView = () => {
        setViewUser(false);
        setDeleteUser(false);
        props.handleRefresh();
    };

    const handleDelete = () => {
        setDeleteUser(true);
    };

    const handlePermission = (permissions) => {
        let icons = [];
        if(permissions.includes("ROLE_SITE_USER")) {
            icons.push(<i key="1" title="user" className="fas fa-user"/>);
        }
        if(permissions.includes("ROLE_TEACHER")) {
            icons.push(<i key="2" title="Teacher" className="fas fa-chalkboard-teacher"/>);
        }
        if(permissions.includes("ROLE_CONTENT_CURATOR")) {
            icons.push(<i key="3" title="Curator" className="fas fa-user-edit"/>);
        }
        if(permissions.includes("ROLE_SYSTEM_ADMIN")) {
            icons.push(<i key="4" title="Administrator" className="fas fa-user-cog"/>);
        }
        return icons;
    };

    return (
        tableRows = props.userList.sort((left, right) => {
            return new Date(left.createDateTime) - new Date(right.createDateTime);
        }).reverse().map((user, key) => {

            const permissions = user["permissions"].map(permission => {
                return permission["name"]
            });

            if (props.selectedUser.id === user.id) {
                isSelected = "is-selected";
            } else {
                isSelected = "";
            }
            return (
                <tr className={isSelected} key={user.id} value={user.id} onClick={() => props.setUser(user.id)}>
                    <th>
                        <label className="b-checkbox checkbox">
                            <input type="checkbox" true-value="true" value="false" />
                            <span className="check"/>
                            <span className="control-label"/>
                        </label>
                    </th>
                    <td className="asp-profile-avatar asp-profile-avatar-listview">
                        <Avatar name={user.firstName + " " + user.lastName} />
                    </td>
                    {/*<td>{user.id}</td>*/}
                    <td value="cell">{user.username}</td>
                    <td>{handlePermission(permissions)}</td>
                    <td>{user.email}</td>
                    <td>{ISO6391.getName(user.defaultLanguage.toLowerCase())}</td>
                    <td>{Moment(user.createDateTime, "YYYY-MM-DD").fromNow()}</td>
                    {/*2020-02-11T15:19:38.000+0000*/}
                    <td>
                        <Link to="/admin/users/readlist/viewuser">
                            <button title="Update" className="button is-small" onClick={handleView}>
                                <i className="fas fa-cog"/>
                            </button>
                        </Link>
                    </td>
                    <td>
                        <button title="Delete" className="button is-small" onClick={handleDelete}>
                            <i className="fas fa-trash-alt"/>
                        </button>
                    </td>
                    {/*<td>{user.firstName}</td>*/}
                    {/*<td>{user.lastName}</td>*/}
                    {/*<td>{user.phoneNumber}</td>*/}
                    {/*<td>{user.createDateTime}</td>*/}
                    {/*<td>{user.updateDateTime}</td>*/}
                    {/*<td>{user.deleted + ""}</td>*/}
                    {/*<td>{user.enabled + ""}</td>*/}
                    {/*<td>{user.credentialsNonExpired + ""}</td>*/}
                    {/*<td>{user.accountNonExpired + ""}</td>*/}
                    {/*<td>{user.accountNonLocked + ""}</td>*/}
                    {/*<td>{user.creatorUserName + ""}</td>*/}
                </tr>
            )
        }),
            <>
                <div className="table-wrapper has-mobile-cards">
                    <table defaultValue="table" className="table is-striped is-hoverable">
                        <thead>
                            <tr>
                                {/*<th><abbr title="id">Id</abbr></th>*/}
                                <th>
                                    <label className="b-checkbox checkbox">
                                        <input type="checkbox" true-value="true" value="false" />
                                        <span className="check"/>
                                        <span className="control-label"/>
                                    </label>
                                </th>
                                <th><abbr title="photoPath"/></th>
                                <th className="is-sortable">
                                    <div className="th-wrap">
                                        Username
                                        <span className="icon is-small">
                                            <i className="fa fa-sort-alpha-up"/>
                                        </span>
                                    </div>
                                </th>
                                <th>
                                    <div className="th-wrap">
                                        Permissions
                                    </div>
                                </th>
                                <th className="is-sortable">
                                    <div className="th-wrap">
                                        Email
                                    </div>
                                </th>
                                <th>
                                    <div className="th-wrap">
                                        Language
                                    </div>
                                </th>
                                {/*<th><abbr title="firstName">First Name</abbr></th>*/}
                                {/*<th><abbr title="lastName">Last Name</abbr></th>*/}
                                {/*<th><abbr title="phoneNumber">phoneNumber</abbr></th>*/}
                                <th className="is-current-sort is-sortable">
                                    <div className="th-wrap">
                                        Created
                                        <span className="icon is-small is-desc">
                                            <i className="fas fa-sort-numeric-up-alt"/>
                                        </span>
                                    </div>
                                </th>
                                {/*<th><abbr title="updateDateTime">UpdateDateTime</abbr></th>*/}
                                {/*<th><abbr title="deleted">Deleted</abbr></th>*/}
                                {/*<th><abbr title="enabled">Enabled</abbr></th>*/}
                                {/*<th><abbr title="credentialsNonExpired">credentialsNonExpired</abbr></th>*/}
                                {/*<th><abbr title="accountNonExpired">accountNonExpired</abbr></th>*/}
                                {/*<th><abbr title="accountNonLocked">accountNonLocked</abbr></th>*/}
                                {/*<th><abbr title="creatorUserName">creatorUserName</abbr></th>*/}
                                <th><abbr title="view"/></th>
                                <th><abbr title="delete"/></th>
                            </tr>
                        </thead>
                        <tbody>
                            {tableRows}
                        </tbody>
                    </table>
                </div>


                <div id="asp-crud-overlay" className={viewUser ? "modal is-active" : "modal is-hidden"}>
                    <div className="modal-background"/>
                    <section className="hero">
                        <div className="modal-card">
                            <header className="modal-card-head">
                                <p className="modal-card-title">User Details</p>
                                <button className="delete" aria-label="close" onClick={handleCloseView}/>
                            </header>
                            <Route path="/admin/users/readlist/viewuser" render={(routeProps) => (<UserDetails {...routeProps} user={props.selectedUser} clearScreen={handleCloseView}/>)}/>
                        </div>
                    </section>
                </div>

                <div className={deleteUser ? "modal is-active" : "modal is-hidden"}>
                    <div className="modal-background"/>
                        <div className="modal-card">
                            <header className="modal-card-head">
                                <p className="modal-card-title">Confirm action</p>
                                <button className="delete" aria-label="close" onClick={handleCloseView}/>
                            </header>
                            <section className="modal-card-body">
                                <p>This will permanently delete <b>{props.selectedUser.username}</b></p>
                                <p>Action can not be undone.</p>
                            </section>
                            {/*<Route path="/admin/users/readlist/deleteuser" render={(routeProps) => (<Delete {...routeProps} user={props.selectedUser}/>)}/>*/}
                            <Delete user={props.selectedUser} clearScreen={handleCloseView}/>
                        </div>
                        {/*<button type="button" className="modal-close is-large" style=""/>*/}
                </div>
            </>
    );
};

export default Table;