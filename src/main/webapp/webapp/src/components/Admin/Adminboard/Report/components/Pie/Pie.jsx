import React from "react";
import { ResponsivePie } from '@nivo/pie';

// import data from '../../pie_dummy_data'
import config from './config';
import './chart.css';

/**
 * Component that displays pie charts based on provided data from backend.
 * @param props props from parent component.
 * @returns {null|*}
 * @constructor
 */
export default function Pie (props) {

    let color = ['#e8a838', '#61cdbb', '#97e3d5', '#e8c1a0', '#f47560', '#f1e15b'];
    if(props.config === "stories")
        color = ['#F2C0B8', '#e8a838', '#61CDBB', '#97e3d5'];


    return (
        // <div className="pie" style={{"width": "95%", "height": props.height}}>
        <div className="pie" style={{"height": props.height}}>
            <ResponsivePie
                data={props.data}
                margin={config.margin}
                innerRadius={0.1}
                padAngle={3}
                cornerRadius={0}
                colors={color}
                borderWidth={1}
                startAngle = {-65}
                borderColor={{from: 'color', modifiers: [['darker', 0.5]]}}
                radialLabelsSkipAngle={5}
                radialLabelsTextXOffset={2}
                radialLabelsTextColor="#333333"
                radialLabelsLinkOffset={0}
                radialLabelsLinkDiagonalLength={2}
                radialLabelsLinkHorizontalLength={12}
                radialLabelsLinkStrokeWidth={1}
                radialLabelsLinkColor={{from: 'color'}}
                slicesLabelsSkipAngle={10}
                slicesLabelsTextColor="#333333"
                isInteractive={false}
                animate={true}
                motionStiffness={70}
                motionDamping={15}
                defs={config.defs}
                fill={config.fill}
                // legends={config.legends}
            />
        </div>
    );
}
