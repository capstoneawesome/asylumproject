import React, {Component} from "react";
import axios from "axios";
import ISO6391 from "iso-639-1";
import Permission from "./Permission/Permission";
import {connect} from "react-redux";
import Feedback from "./Feedback";
import * as EmailValidator from "email-validator";
import Sanitizer from "sanitize-html";
import ReactPasswordStrength from 'react-password-strength';
import Phone from "phone";

/**
 * Component that display user creation box.
 * It includes multiple input forms for user information.
 * Html tags will be sanitized when validation process is triggered for user input.
 * Phone number will be automatically formatted when common phone number is given.
 * Password strength meter checks password string from a user.
 * Email format will be checked by email-validator module.
 *
 * @param props props from parent component
 * @returns {*}
 * @constructor
 */
// This component includes extra configuration objects below class definition
class Create extends Component {

    constructor(props) {
        super(props);

        this.state = {
            // For event handling these states need to be used as 'Controlled Component'
            // userName: "",
            // email: "",
            feedbackUsername: false,
            feedbackEmail: false,
            feedbackConfirmPassword: false,
            password: "",
            confirmPassword: "",
            defaultLanguage: "en",
            permissions: []
            // active: undefined,
            // creationDate: undefined,
            // creator: undefined,
        };

        // UnControlled Components
        this.userName = React.createRef();
        this.email = React.createRef();
        // this.password = React.createRef();
        // this.password = "";
        this.confirmPassword = React.createRef();

        this.firstName = React.createRef();
        this.lastName = React.createRef();
        this.phoneNumber = React.createRef();
        // this.photoPath = React.createRef();

        // this.feedbackUsername = false;
    }

    handleSubmit = (e) => {
        e.preventDefault();

        if(this.isAdmin()) {
            this.sendRequest();
        }
    };

    handleUserName = () => {
        let currentUsername = this.userName.current.value;

        const url = "http://localhost:8080/api/users/username/" + currentUsername;

        const config = {
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
            }
        };

        if(this.validateUsername(currentUsername)) {
            axios.get(
                url,
                config
            ).then((result) => {
                if (result.status === 200) {
                    if (result.data) {
                        // Occupied username
                        this.setState({
                            feedbackUsername: false,
                        })
                    } else {
                        this.setState({
                            feedbackUsername: true,
                        })
                    }
                }
            }).catch(error => {
                console.log("error occurred " + error);
            });
        } else {
            this.setState({
                feedbackUsername: false,
            })
        }
    };

    handleEmail = () => {
        let currentEmail = this.email.current.value;

        const url = "http://localhost:8080/api/users/email/" + currentEmail;

        const config = {
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
            }
        };

        if(this.validateEmail(currentEmail)) {
            axios.get(
                url,
                config
            ).then((result) => {
                if (result.status === 200) {
                    if (result.data) {
                        // Occupied username
                        this.setState({
                            feedbackEmail: false,
                        })
                    } else {
                        console.log(this.validateEmail(currentEmail));
                        this.setState({
                            feedbackEmail: true,
                        })
                    }
                }
            }).catch(error => {
                console.log("error occurred " + error);
            });
        } else {
            this.setState({
                feedbackEmail: false,
            })
        }
    };

    handlePasswordMeter = o => {
        this.setState({
            password: o["password"],
        })
    };

    handleConfirmPassword = e => {
        let currentConfirmPassword = e.target.value;

        if(currentConfirmPassword === this.state.password) {
            this.setState({
                feedbackConfirmPassword: true,
            })
        } else {
            this.setState({
                feedbackConfirmPassword: false,
            })
        }

    };

    handlePermissions = checkboxes => {
        // Please refer to extra configuration below class definition.
        // Parameter, 'checkboxes' is in following format. e.g. {1: true, 2: true, 3: false, 4: false}
        let selectedItems = Object.keys(checkboxes).map(key => checkboxes[key] && key).filter(v => v !== false);

        let selectedPermissions = PERMISSIONS.map(permission => {
            let matched = selectedItems.find(id => parseInt(id) === parseInt(permission.id));
            if(matched !== undefined) {
                return permission;
            }
        }).filter(v => v !== undefined);

        this.setState({
            permissions: selectedPermissions,
        })
    };

    handleName = () => {
    };

    handlePhoneNumber = e => {
        let currentPhone = e.target.value;
        if(Phone(currentPhone).length > 0) {
            console.log(Phone(currentPhone));
        }
    };

    handleDefaultLanguage = (e) => {
        this.setState({
            defaultLanguage: e.target.value,
        });
    };

    handleLanguageSelectionMount = () => {
        // let defaultLanguageCode = this.state.defaultLanguage;
        return (
            ISO6391.getAllCodes().map((languageCode, index) => {
                return <option key={index} value={languageCode}>{ISO6391.getName(languageCode)}</option>;
                // let languageName = ISO6391.getName(languageCode);
                // return (
                //     languageCode === defaultLanguageCode
                //         ? <option key={index} value={languageCode}>{languageName}</option>
                //         : <option key={index} value={languageCode}>{languageName}</option>
                // );
            })
        );
    };

    isAdmin = () => {
        return this.props.user_authorities.includes("ROLE_SYSTEM_ADMIN");
    };

    sendRequest = () => {
        console.log(this.password);

        const url = "http://localhost:8080/api/users/register";

        const config = {
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
            }
        };

        const data = {
            "firstName": this.firstName.current.value,
            "lastName": this.lastName.current.value,
            "userName": this.userName.current.value,
            "email": this.email.current.value,
            "password": this.state.password,
            "phoneNumber": this.phoneNumber.current.value,
            // "photoPath": this.photoPath.current.value,
            "photoPath": "c:",

            // Please be noticed that 2 lines below use 'states' in order to handle mouse events
            // "userName": this.state.userName,
            "defaultLanguage": this.state.defaultLanguage,
            "permissions": this.state.permissions,

            // Data Fields below need to be handled by server-side application
            // "createDateTime"
            // "updateDateTime"
            // "creatorUserName"
            // "delete"
            // "enabled"
            // "resetToken"
            // "credentialNonExpired"
            // "accountNonExpired"
            // "accountNonLocked"
        };

        axios.post(
            url,
            data,
            config
        ).then((result) => {
            // console.log(result.data);
            if (result.status === 201) {
                this.props.handleCloseModal();
                this.props.handleRefresh();
            }
        }).catch(error => {
            console.log("error occurred");
        });
    };

    validateUsername = username => {
        let isValid = true;
        // maximum length 30
        // any letter (include special characters)
        // capital lower cases
        // number
        // username string can not have HTML tags. it will be sanitized
        let sanitizedUsername = Sanitizer(username);
        // console.log("sanitized: " + sanitizedUsername + " raw: " + username + " is injected? " + (username !== sanitizedUsername));
        if(username !== sanitizedUsername) {
            // there was a HTML injection attempt
            isValid = false;
        }

        if(username.length <= 0 || username.length > 30) {
            isValid = false;
        }

        return username !== null && username !== "" && isValid;
    };

    validateEmail = email => {
        let isValid = true;

        let sanitizedEmail = Sanitizer(email);
        if(email !== sanitizedEmail) {
            // there was a HTML injection attempt
            isValid = false;
        }

        return email !== null && email !== "" && isValid && EmailValidator.validate(email);
    };

    validateName = name => {
        let isValid = true;

    };


    render() {
        return (
            <>
                <section className="modal-card-body">
                    <div className="columns is-centered">
                        <div className="column">
                            <form className="box" onSubmit={this.handleSubmit}>

                                {/*username required*/}
                                <div className="field">
                                    <label className="label">Username</label>
                                    <div className="control has-icons-left has-icons-right">
                                        <input
                                            className="input"
                                            type="text"
                                            // placeholder="Username"
                                            // value={this.state.userName}
                                            onChange={this.handleUserName}
                                            ref={this.userName}
                                            required
                                        />
                                        <span className="icon is-small is-left">
                                          <i className="fas fa-user"/>
                                        </span>
                                        <span className="icon is-small is-right">
                                          <i className="fas fa-check"/>
                                        </span>
                                    </div>
                                    <Feedback isAvailable={this.state.feedbackUsername} succeedMsg="This username is available" failedMsg="This username is NOT available" />
                                </div>

                                {/*email required*/}
                                <div className="field">
                                    <label className="label">Email</label>
                                    <div className="control has-icons-left has-icons-right">
                                        <input
                                            className="input"
                                            type="email"
                                            // placeholder="Email"
                                            // value={this.state.email}
                                            onChange={this.handleEmail}
                                            ref={this.email}
                                            required
                                        />
                                        <span className="icon is-small is-left">
                                          <i className="fas fa-envelope"/>
                                        </span>
                                        <span className="icon is-small is-right">
                                          <i className="fas fa-exclamation-triangle"/>
                                        </span>
                                    </div>
                                    <Feedback isAvailable={this.state.feedbackEmail} succeedMsg="This email is available" failedMsg="This email is NOT available" />
                                </div>

                                {/*password required*/}
                                <div className="field">
                                    <label className="label">Password</label>
                                    {/*<div className="control has-icons-left has-icons-right">*/}
                                    <div className="control has-icons-left">
                                        <ReactPasswordStrength
                                            className="asp-strength-meter"
                                            // style={{ display: 'block' }}
                                            minLength={5}
                                            minScore={2}
                                            scoreWords={['weak', 'okay', 'good', 'strong', 'stronger']}
                                            changeCallback={this.handlePasswordMeter}
                                            inputProps={{ name: "password_input", autoComplete: "off", className: "input" }}
                                            required
                                        />
                                        {/*<input*/}
                                        {/*    className="input"*/}
                                        {/*    type="password"*/}
                                        {/*    placeholder="Password"*/}
                                        {/*    // value={this.state.password}*/}
                                        {/*    // onChange={this.handlePassword}*/}
                                        {/*    ref={this.password}*/}
                                        {/*    required*/}
                                        {/*/>*/}
                                        <span className="icon is-small is-left">
                                          <i className="fas fa-key"/>
                                        </span>
                                        {/*<span className="icon is-small is-right">*/}
                                        {/*  <i className="fas fa-exclamation-triangle"/>*/}
                                        {/*</span>*/}
                                    </div>
                                </div>

                                {/*confirm password required*/}
                                <div className="field">
                                    <label className="label">Confirm Password</label>
                                    <div className="control has-icons-left has-icons-right">
                                        <input
                                            className="input"
                                            type="password"
                                            // placeholder="Confirm password"
                                            // value={this.state.confirmPassword}
                                            onChange={this.handleConfirmPassword}
                                            ref={this.confirmPassword}
                                        />
                                        <span className="icon is-small is-left">
                                          <i className="fas fa-key"/>
                                        </span>
                                        <span className="icon is-small is-right">
                                          <i className="fas fa-exclamation-triangle"/>
                                        </span>
                                    </div>
                                    <Feedback isAvailable={this.state.feedbackConfirmPassword} succeedMsg="match" failedMsg="does not match" />
                                </div>


                                {/*<div className="file has-name field">*/}
                                {/*    <label className="label">Profile Picture</label>*/}
                                {/*    <div className="control has-icons-left has-icons-right">*/}
                                {/*        <label className="file-label">*/}
                                {/*            <input className="file-input" type="file" name="resume" />*/}
                                {/*                <span className="file-cta">*/}
                                {/*                  <span className="file-icon">*/}
                                {/*                    <i className="fas fa-upload"/>*/}
                                {/*                  </span>*/}
                                {/*                  <span className="file-label">*/}
                                {/*                    Choose a file…*/}
                                {/*                  </span>*/}
                                {/*                </span>*/}
                                {/*                <span className="file-name">*/}
                                {/*                    file path*/}
                                {/*                </span>*/}
                                {/*        </label>*/}
                                {/*    </div>*/}
                                {/*</div>*/}

                                {/*firstname required*/}
                                <div className="field">
                                    <label className="label">First Name</label>
                                    <div className="control">
                                        <input
                                            className="input"
                                            type="text"
                                            // placeholder="Text input"
                                            // value={this.state.firstName}
                                            onChange={this.handleName}
                                            ref={this.firstName}
                                            required
                                        />
                                    </div>
                                </div>

                                {/*lastname required*/}
                                <div className="field">
                                    <label className="label">Last Name</label>
                                    <div className="control">
                                        <input
                                            className="input"
                                            type="text"
                                            // placeholder="Text input"
                                            // value={this.state.lastName}
                                            onChange={this.handleName}
                                            ref={this.lastName}
                                            required
                                        />
                                    </div>
                                </div>

                                {/*language required*/}
                                <div className="field">
                                    <label className="label">Language</label>
                                    <div className="control">
                                        <div className="select">
                                            <select name="language" value={this.state.defaultLanguage} onChange={this.handleDefaultLanguage}>
                                                {this.handleLanguageSelectionMount()}
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                {/*phone number*/}
                                <div className="field">
                                    <label className="label">Phone Number</label>
                                    <div className="control has-icons-left has-icons-right">
                                        <div className="field has-addons">
                                            {/*<p className="control">*/}
                                            {/*    <a className="button is-static">*/}
                                            {/*        +1*/}
                                            {/*    </a>*/}
                                            {/*</p>*/}
                                            <p className="control is-expanded">
                                                <input
                                                    className="input"
                                                    type="tel"
                                                    // placeholder="Your phone number"
                                                    // value={this.state.phoneNumber}
                                                    onChange={this.handlePhoneNumber}
                                                    ref={this.phoneNumber}
                                                />
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                {/*permission*/}
                                <div className="field">
                                    <label className="label checkbox">Permission</label>
                                    <div className="control has-icons-left has-icons-right">
                                        <Permission permissionPolicies={PERMISSIONS} onPermissionsChange={this.handlePermissions} />
                                    </div>
                                </div>

                                <div className="field is-grouped">
                                    <div className="control">
                                        <button className="button is-link">Submit</button>
                                    </div>
                                    {/*<div className="control">*/}
                                    {/*    <button className="button is-link is-light">Cancel</button>*/}
                                    {/*</div>*/}
                                </div>

                            </form>
                        </div>
                    </div>
                </section>
            </>
        );
    };

}

// permissions variable in array format
// this is NOT belonging to Permission component below
const PERMISSIONS = [
    {id: 1, name: "ROLE_SITE_USER"},
    {id: 2, name: "ROLE_TEACHER"},
    {id: 3, name: "ROLE_CONTENT_CURATOR"},
    {id: 4, name: "ROLE_SYSTEM_ADMIN"}
];

// Redux settings below.
// This is why you can use 'store' in a grand child component
// without passing 'props' through every components in an inheritance hierarchy.

// This function related to binding states declared in between Redux store and your component
const mapStateToProps = (state) => {
    return {
        isSignIn: state.authenticationReducer.isSignIn,
        user_authorities: state.authenticationReducer.user_authorities,
    };
};

// if you only refer to states and you don't change states in Redux store, you probably don't need to have below function
// This function is related to modifying states in Redux store
const mapDispatchProps = (dispatch) => {
    return {
        // handleAuthentication: (isSignIn) => {dispatch(authenticationActions.authenticationAction(isSignIn))},
    };
};

Create = connect(mapStateToProps, mapDispatchProps)(Create);

export default Create;