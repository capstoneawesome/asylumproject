import React from "react";
import { ResponsiveLine } from '@nivo/line'

import config from './config';
import './chart.css';

/**
 * Component that displays line charts based on provided data from backend.
 * @param props props from parent component.
 * @returns {null|*}
 * @constructor
 */
export default function Lines (props) {

    const color = ['#f47560', '#f1e15b', '#e8a838', '#61cdbb', '#97e3d5', '#e8c1a0'];

    return(
        <div className="lines" style={{"width": "95%"}}>
            <ResponsiveLine
                data={props.data}
                margin={config.margin}
                xScale={{ type: 'point' }}
                yScale={{ type: 'linear', min: 3600, max: 'auto', stacked: true, reverse: false }}
                axisTop={null}
                axisRight={null}
                axisBottom={config.axisBottom}
                axisLeft={config.axisLeft}
                // colors={{ scheme: 'nivo' }}
                colors={ color }
                pointSize={10}
                pointColor={{ theme: 'background' }}
                pointBorderWidth={2}
                pointBorderColor={{ from: 'serieColor' }}
                pointLabel="y"
                pointLabelYOffset={-12}
                isInteractive={false}
                enableArea={true}
                areaBaselineValue={3600}
                areaOpacity={0.2}
                useMesh={true}
                legends={config.legends}
                motionStiffness={50}
            />
        </div>
    );
}
