import React, {useEffect, useState} from "react";
import Checkbox from "./Checkbox";

/**
 * Component that create check boxes regarding to permission policy.
 * @param permissionPolicies it contains permission name and id code for each permission.
 * @param onPermissionsChange it is a listener method to keep track of clicking on a checkbox.
 * @returns {*}
 * @constructor
 */

export default function Permission({permissionPolicies, onPermissionsChange}) {

    // Destructed state
    const [checkboxes, setCheckboxes] = useState(
        permissionPolicies.reduce(
            (permissions, permission) => {

                // I would've been better to use an Javascript object as a key,
                // but we can not use the object as a key in a wrapper object according to Javascript specification.
                let key = permission.id;
                let value = null;

                if(key === 1) {
                    value = true;
                } else {
                    value = false;
                }

                permissions[key] = value;
                return permissions;

            },
            {}
        )
    );

    // const selectAllCheckBoxes = isSelected => {
    //     Object.keys(checkboxes).forEach(checkbox => {
    //         setCheckboxes(prevState => ({
    //             checkboxes: {
    //                 ...prevState.checkboxes,
    //                 [checkbox]: isSelected
    //             }
    //         }));
    //     });
    // };

    // const selectAll = () => selectAllCheckBoxes(true);
    //
    // const deselectAll = () => selectAllCheckBoxes(false);

    const handleCheckboxChange = changeEvent => {
        const {value} = changeEvent.target;

        setCheckboxes({
            ...checkboxes,
            [parseInt(value)]: !checkboxes[parseInt(value)]
        });
    };

    // const testForPassingMethod = () => {
    //     console.log("test");
    // }

    // const handleFormSubmit = formSubmitEvent => {
    //     formSubmitEvent.preventDefault();
    //
    //     Object.keys(checkboxes)
    //         .filter(checkbox => checkboxes[checkbox])
    //         .forEach(checkbox => {
    //             console.log(checkbox, "is selected.");
    //         });
    // };

    const createCheckbox = option => {
        let labelValue;
        switch (option.id) {
            case 1:
                labelValue = "User";
                break;
            case 2:
                labelValue = "Teacher";
                break;
            case 3:
                labelValue = "Curator";
                break;
            case 4:
                labelValue = "Admin";
                break;
            default:
                labelValue = "User";
                break;
        }

        // return <Checkbox
        //     id={option.id}
        //     label={labelValue}
        //     isSelected={checkboxes[option.id]}
        //     onCheckboxChange={handleCheckboxChange}
        //     key={option.id}
        // />

        return option.id !== 1
            ?
                (
                    <Checkbox
                        id={option.id}
                        label={labelValue}
                        isSelected={checkboxes[option.id]}
                        onCheckboxChange={handleCheckboxChange}
                        key={option.id}
                        // isDisabled=""
                    />
                )
            :
                (
                    // <Checkbox
                    //     id={option.id}
                    //     label={labelValue}
                    //     isSelected={checkboxes[option.id]}
                    //     onCheckboxChange={handleCheckboxChange}
                    //     key={option.id}
                    //     isDisabled="disabled"
                    // />
                    <React.Fragment key={option.id}/>
                );
    };

    const createCheckboxes = () => permissionPolicies.map(createCheckbox);

    useEffect(() => {
        onPermissionsChange(checkboxes);
    }, [onPermissionsChange, checkboxes]);

    return (
        <>
            <div className="container">
                <div className="row mt-5">
                    <div className="col-sm-12">
                        {/*<form onSubmit={handleFormSubmit}>*/}
                            {createCheckboxes()}

                            {/*<div className="form-group mt-2">*/}
                            {/*    <button*/}
                            {/*        type="button"*/}
                            {/*        className="btn btn-outline-primary mr-2"*/}
                            {/*        onClick={selectAll}*/}
                            {/*    >*/}
                            {/*        Select All*/}
                            {/*    </button>*/}
                            {/*    <button*/}
                            {/*        type="button"*/}
                            {/*        className="btn btn-outline-primary mr-2"*/}
                            {/*        onClick={deselectAll}*/}
                            {/*    >*/}
                            {/*        Deselect All*/}
                            {/*    </button>*/}
                            {/*    <button type="submit" className="btn btn-primary">*/}
                            {/*        Save*/}
                            {/*    </button>*/}
                            {/*</div>*/}
                        {/*</form>*/}
                    </div>
                </div>
            </div>
        </>
    );

};

