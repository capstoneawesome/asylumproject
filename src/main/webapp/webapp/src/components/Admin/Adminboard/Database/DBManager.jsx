import React, {useState} from "react";
import Generate from "./Components/Generate";
import Restore from "./Components/Restore";

/**
 * Component that holds child components: Generate, Restore and Upload.
 * @returns {null/*}
 * @constructor
 */
export default function DBManager () {

    const [dbToken] = useState("cQrUe8Gs5uAAAAAAAAAADYDT9az6zsa4mmp7Zi_XM5o83v-A6IIuXXJUd4JduTnA");
    const [url] = useState("http://localhost:8080/api");
    // const [url] = useState("http://10.0.0.141:8080/api");
    // const [url] = useState("/api");

    return (
        <div className="hero-body">
            {/*<div style={{maxWidth: "750px"}} >*/}
            <div >
                <section className="hero is-info is-small">
                    <div className="hero-body">
                        <div className="container">
                            <h5 className="title">
                                Manage Database Backups
                            </h5>
                        </div>
                    </div>
                </section>
                <br/>

                <Generate dbToken={dbToken} url={url} />

                <Restore dbToken={dbToken} url={url} />

                {/*<Upload dbToken={dbToken} url={url} />*/}
            </div>
        </div>
        );

}
