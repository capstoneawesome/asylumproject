import React, {useState, useEffect} from "react";
import axios from "axios";
import Table from "./table";
import {Link, Route} from "react-router-dom";
import Create from "../Create/Create";

/**
 * Component that display outer HTML container for user list table.
 * @returns {*}
 */

function ReadList() {

    const [users, setUsers] = useState([]);
    const [selectedUser, setSelectedUser] = useState({});
    const [addUser, setAddUser] = useState(false);
    const [viewUser, setViewUser] = useState(false);
    const [needRefresh, setNeedRefresh] = useState(1);
    const [processingRequest, setProcessingRequest] = useState(false);

    const setUser = (id) => {
        const user = users.find(u => u.id === id);

        setSelectedUser(user);

    };

    const handleView = () => {
        setViewUser(true);
    };

    const handleCloseView = () => {
        setViewUser(false);
    };

    const handleCreate = () => {
        setAddUser(true);
    };

    const handleCloseModal = () => {
        setAddUser(false);
    };

    const handleRefresh = () => {
        setNeedRefresh(needRefresh * -1);
    };

    const fetchUsers = () => {
        setProcessingRequest(true);
        let list = [];

        axios.defaults.headers.get['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
        axios.get("http://localhost:8080/api/users")
            .then((result) => {
                if (result.status === 200) {

                    result.data.map((user) => {
                        list = [...list, user]
                    });
                    setUsers(list);
                }
            });
        setProcessingRequest(false);
    };

    useEffect(() => {
        fetchUsers();
    }, [needRefresh]);

    return (
        <>
            <div className="hero-body">

                <div className="asp-user-list card has-table has-mobile-sort-spaced">
                    <header className="card-header">
                        <p className="card-header-title">
                            <span className="icon">
                                <i className="fa fa-users"/>
                            </span>
                            <span>Users</span>
                        </p>
                    </header>
                    <div className="notification is-card-toolbar">
                        <div className="level">
                            <div className="level-left">
                                <div className="level-item">
                                    <div className="buttons has-addons">
                                        <button className={processingRequest ? "button is-loading is-small" : "button is-small"} onClick={handleRefresh}>
                                            <i className="fa fa-sync-alt"/>
                                        </button>
                                        <Link to="/admin/users/readlist/create">
                                            <button className="button is-small" onClick={handleCreate}>
                                                <i className="fas fa-user-plus"/>
                                            </button>
                                        </Link>
                                        <button disabled="disabled" className="button is-small">
                                            <i className="fas fa-user-minus"/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="level-right">
                                <div className="level-item">
                                    <form>
                                        <div className="field has-addons">
                                            <div className="control">
                                                <input type="text" placeholder="Search" className="input" />
                                            </div>
                                            <div className="control">
                                                <button type="submit" className="button">
                                                    <span className="icon">
                                                        <i className="fa fa-search"/>
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/*User list*/}
                    <section id="asp-user-list">
                        <Table userList={users} setUser={setUser} selectedUser={selectedUser} handleRefresh={handleRefresh}/>
                    </section>

                </div>

                {/*Add user modal*/}
                <div id="asp-crud-overlay" className={addUser ? "modal is-active" : "modal is-hidden"}>
                    <div className="modal-background"/>
                    <section className="hero">
                        <div className="modal-card">
                            <header className="modal-card-head">
                                <p className="modal-card-title">Add User</p>
                                <button className="delete" aria-label="close" onClick={handleCloseModal}/>
                            </header>
                            <Route path="/admin/users/readlist/create" component={() => <Create handleCloseModal={handleCloseModal} handleRefresh={handleRefresh} />}/>
                        </div>
                    </section>
                </div>
            </div>
            {/*<div id="asp-crud-overlay" className={viewUser ? "modal is-active" : "modal is-hidden"}>
                <div className="modal-background"></div>
                <section className="hero">
                    <div className="modal-card">
                        <header className="modal-card-head">
                            <p className="modal-card-title">User Details</p>
                            <button className="delete" aria-label="close" onClick={handleCloseView}></button>
                        </header>
                        <Route path="/admin/users/readlist/viewuser" render={(routeProps) => (<UserDetails {...routeProps} user={selectedUser} />)} />


                    </div>
                </section>
            </div>*/}
        </>
    );
}

export default ReadList;