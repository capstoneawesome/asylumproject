export default {
    margin: {
        "top": 20,
        "right": 20,
        "bottom": 20,
        "left": 20
    },
    defs: [
        {
            "id": "dots",
            "type": "patternDots",
            "background": "inherit",
            "color": 'rgba(255, 255, 255, 0.3)',
            "size": 4,
            "padding": 1,
            "stagger": true
        },
        {
            "id": "lines1",
            "type": "patternLines",
            "background": "inherit",
            "color": "#eed312",
            "rotation": -45,
            "lineWidth": 6,
            "spacing": 10
        },
        {
            "id": "lines2",
            "type": "patternLines",
            "background": "inherit",
            "color": "#F99684",
            "rotation": -45,
            "lineWidth": 6,
            "spacing": 10
        }
    ],
    fill: [
        {
            "match": {
                "id": "DRAFT"
            },
            "id": "dots"
        },
        {
            "match": {
                "id": "ARCHIVED"
            },
            "id": "lines2"
        }
    ],
    legends: [
        {
            anchor: 'bottom',
            direction: 'row',
            translateY: 56,
            itemWidth: 50,
            itemHeight: 18,
            itemTextColor: '#999',
            symbolSize: 18,
            symbolShape: 'circle',
            effects: [
                {
                    on: 'hover',
                    style: {
                        itemTextColor: '#000'
                    }
                }
            ]
        }
    ]
}
