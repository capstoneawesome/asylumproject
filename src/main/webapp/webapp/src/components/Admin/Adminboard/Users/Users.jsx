import React, {Component} from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import ReadList from "./CRUD/Read/ReadList";

/**
 * Component that give a placeholder for an user list.
 * @returns {*}
 */

class Users extends Component {

    render() {

        return(
            <>
                <Switch>
                    <Redirect exact from="/admin/users" to="/admin/users/readlist" />
                    <Route path="/admin/users/readlist" component={ReadList} />
                </Switch>
            </>
        );

    }

}

export default Users;