export default {
    keys: [
        "draft",
        "prepublished",
        "published",
        "archived"
    ],
    margin: {
        "top": 20,
        "right": 30,
        "bottom": 50,
        "left": 140
    },
    defs: [
        {
            "id": "dots",
            "type": "patternDots",
            "background": "inherit",
            "color": 'rgba(255, 255, 255, 0.3)',
            "size": 4,
            "padding": 1,
            "stagger": true
        },
        {
            "id": "lines1",
            "type": "patternLines",
            "background": "inherit",
            "color": "#eed312",
            "rotation": -45,
            "lineWidth": 6,
            "spacing": 10
        },
        {
            "id": "lines2",
            "type": "patternLines",
            "background": "inherit",
            "color": "#F99684",
            "rotation": -45,
            "lineWidth": 6,
            "spacing": 10
        }
    ],
    fill: [
        {
            "match": {
                "id": "draft"
            },
            "id": "dots"
        },
        {
            "match": {
                "id": "archived"
            },
            "id": "lines2"
        }
    ],
    axisBottom: {
        "tickSize": 5,
        "tickPadding": 5,
        "tickRotation": 0,
        "tickValues": 2,
        "legend": "# of Stories",
        "legendPosition": "middle",
        "legendOffset": 32
    },
    axisLeft: {
        "tickSize": 5,
        "tickPadding": 5,
        "tickRotation": 0,
        "legend": "",
        "legendPosition": "middle",
        "legendOffset": -40
    },
    layout: "horizontal",
    legends: [
        {
            "dataFrom": "keys",
            "anchor": "bottom-left",
            "direction": "row",
            "justify": false,
            "translateX": 0,
            "translateY": 70,
            "itemsSpacing": 1,
            "itemWidth": 35,
            "itemHeight": 10,
            "itemDirection": "left-to-right",
            "itemOpacity": 0.85,
            "symbolSize": 20,
            "effects": [
                {
                    "on": "hover",
                    "style": {
                        "itemOpacity": 1
                    }
                }
            ]
        }
    ]
}
