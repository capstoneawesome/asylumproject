import React from "react";

/**
 * Component that display a feedback message on user input.
 * @param props props from parent component
 * @returns {*}
 */

export default function Feedback(props) {
    return (
        <>
            <p className={props.isAvailable ? "help is-success" : "help is-danger"}>{props.isAvailable ? props.succeedMsg : props.failedMsg}</p>
        </>
    );
};