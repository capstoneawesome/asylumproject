import React, {useEffect, useState} from "react";
import axios from "axios";
import {useToasts} from "react-toast-notifications";
import * as moment from "moment";

/**
 * Component that displays Stories Logged Events.
 * @param props props from parent component.
 * @returns {null|*}
 * @constructor
 */
export default function StoryEvents(props) {

    const {addToast} = useToasts();

    const [dropDown, setDropDown] = useState(false);
    const [storiesEvents, setStoriesEvents] = useState( []);
    const [listStories, setListStories] = useState([]);
    const [selectedElements, setSelectedElements] = useState([]);
    const [tableData, setTableData] = useState([]);

    useEffect(() => {
        fetchStories();
    }, []);

    function fetchStories() {
        axios.defaults.headers.get['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
        axios.get(props.url + "/admin/events/stories")
            .then((result) => {
                if(result.status === 200) {
                    setStoriesEvents(result.data.events);
                    setListStories(result.data.stories);
                } else {
                    console.log("Error while retrieving stories information.");
                }
            }).then(() => {
        });
    }

    function setRowEffect(element) {
        let rowEffect;
        if(element.operation === 'Deleted')
            rowEffect = "is-size-7 has-background-danger has-text-white";
        else if(element.operation === 'Created')
            rowEffect = "is-size-7 has-text-info has-text-weight-semibold ";
        else if(element.operation === 'Password Change')
            rowEffect = "is-size-7 has-background-warning";
        else
            rowEffect = "is-size-7";
        return rowEffect;
    }

    function selectedStory(id) {
        setDropDown(!dropDown);
        tableData.splice(0);
        for(let i = 0; i < storiesEvents.length; i++) {
            if(storiesEvents[i].storyId === id)
                tableData.push(storiesEvents[i]);
        }
        // if(index < 0) {
        //     selectedElements.splice(0);
        //     for(let i = 0; i < listElements.length; i++) {
        //         selectedElements.push(listElements[i].username);
        //     }
        // }
        // else if(selectedElements.indexOf(listElements[index].username) < 0) {
        //     selectedElements.push(listElements[index].username);
        // }
        // fetchResults(); TODO
    }

    function removeElement(index) {
        // fetchResults(); TODO
        addToast(selectedElements.splice(index, 1) + " was removed.", {appearance: 'warning', autoDismiss: true});
    }

    return (
        <div className="rows">
            <div className="row">
                <div className="columns">
                    <div className="column is-one-fifth">
                        <div className={dropDown?"dropdown is-active":""}>
                            <div className="dropdown-trigger">
                                <button onClick={() => setDropDown(!dropDown)} className="button" aria-haspopup="true" aria-controls="dropdown-menu2">
                                    <span>Select Story</span>
                                    <span className="icon is-small">
                                        <i className="fas fa-angle-down" aria-hidden="true"/>
                                    </span>
                                </button>
                            </div>
                            <div className="dropdown-menu" id="dropdown-menu2" role="menu">
                                <div className="dropdown-content is-small">
                                    {/*<div onClick={() => elementSelected(-1)} className="dropdown-item is-small">*/}
                                    {/*    <p>All</p>*/}
                                    {/*</div>*/}
                                    {/*<hr className="dropdown-divider" />*/}
                                    {listStories.map((element, index) => {
                                        return (
                                            <div key={index}>
                                                <div onClick={() => selectedStory(element.storyId)} className="dropdown-item is-small">
                                                    <p>{element.storyTitle}</p>
                                                </div>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="column is-four-fifths">
                        {/*<div className="tags">*/}
                        {/*    {selectedElements.map((element, index) => {*/}
                        {/*        return (*/}
                        {/*            // <div className="columns">*/}
                        {/*            //      <div className="column">*/}
                        {/*            <span className="tag is-rounded is-info" key={index}>*/}
                        {/*                                            {element}*/}
                        {/*                <button className="delete is-small" onClick={() => removeElement(index)} />*/}
                        {/*                                        </span>*/}
                        {/*            // </div>*/}
                        {/*            // </div>*/}
                        {/*        )*/}
                        {/*    })}*/}
                        {/*</div>*/}
                    </div>
                </div>
            </div><br/>
            {tableData.length > 0 ?
                <div>
                    <div className="row">
                        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                            <thead>
                            <tr className="is-size-7">
                                <th>Story Title</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Operation</th>
                                <th>State</th>
                                <th>Acting User</th>
                                {/*<th>Username</th>*/}
                                {/*<th>Name</th>*/}
                            </tr>
                            </thead>
                            <tbody>
                            {tableData.map((dataTable, index) => {
                                return(
                                    // <tr key={index} className={dataTable.operation === 'Deleted'? "has-background-danger has-text-white has-text-weight-semibold": "" }>
                                    <tr key={index} className={setRowEffect(dataTable)} >
                                        <td className="has-background-white">{dataTable.operation === 'Created' ? dataTable.storyTitle : null}</td>
                                        {/*<td className="has-background-white">{dataTable.user}</td>*/}
                                        <td><time dateTime="2016-1-1">{moment.utc(dataTable.dateTime).local().format('MMM DD, YYYY')}</time></td>
                                        <td><time dateTime="2016-1-1">{moment.utc(dataTable.dateTime).local().format('hh:mm:ss a')}</time></td>
                                        <td>{dataTable.operation} </td>
                                        <td>{dataTable.storyState} </td>
                                        <td>{dataTable.actingUser}</td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>
                    </div><br/>
                    <div className="row">
                        <div className="columns is-1">
                            <div className="column is-2 is-offset-8 ">
                                <button className="button is-info is-light is-small">Download CSV</button>
                            </div>
                            <div className="column is-2 ">
                                <button className="button is-danger is-light is-small">Download PDF</button>
                            </div>
                        </div>
                    </div>
                </div> : null}
        </div>
    )
}
