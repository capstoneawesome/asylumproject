import React, {useState, useEffect, useRef} from 'react';
import './css/style.scss';
import Permission from "../Create/Permission/Permission";
import {connect} from "react-redux";
import Feedback from "../Create/Feedback";
import ReactPasswordStrength from "react-password-strength";
import axios from "axios";
import Sanitizer from "sanitize-html";

// Redux settings below.
// This is why you can use 'store' in a grand child component
// without passing 'props' through every components in an inheritance hierarchy.

// This function related to binding states declared in between Redux store and your component
const mapStateToProps = (state) => {
    return {
        isSignIn: state.authenticationReducer.isSignIn,
        user_authorities: state.authenticationReducer.user_authorities,
    };
};

// if you only refer to states and you don't change states in Redux store, you probably don't need to have below function
// This function is related to modifying states in Redux store
const mapDispatchProps = (dispatch) => {
    return {
        // handleAuthentication: (isSignIn) => {dispatch(authenticationActions.authenticationAction(isSignIn))},
    };
};

UserDetails = connect(mapStateToProps, mapDispatchProps)(UserDetails);

// permissions variable in array format
// this is NOT belonging to Permission component below
const PERMISSIONS = [
    {id: 1, name: "ROLE_SITE_USER"},
    {id: 2, name: "ROLE_TEACHER"},
    {id: 3, name: "ROLE_CONTENT_CURATOR"},
    {id: 4, name: "ROLE_SYSTEM_ADMIN"}
];

/**
 * Component that display user modification box so that administrator can apply updates on a user.
 * @param props props from parent component
 * @returns {*}
 */

export default function UserDetails(props){

    const [edit, setEdit] = useState(false);
    const [username, setUsername] = useState(props.user.username);
    const [firstName, setFirstName] = useState(props.user.firstName);
    const [lastName, setLastName] = useState(props.user.lastName);
    const [email, setEmail] = useState(props.user.email);
    const [phone, setPhone] = useState(props.user.phoneNumber);
    const [photo, setPhoto] = useState(props.user.photoPath);
    const [defaultLanguage, setDefaultLanguage] = useState(props.user.defaultLanguage);

    const [feedbackUsername, setFeedbackUsername] = useState(false);

    const userNameRef = useRef();

    useEffect(() => {
        setUsername(props.user.username);
        // setFirstName(props.user.firstName);
        // setLastName(props.user.lastName);
        // setEmail(props.user.email);
        // setPhone(props.user.phoneNumber);
        // setDefaultLanguage(props.user.defaultLanguage);
    });

    const handleClick = () => {
        setEdit(prev => !prev);
    };

    const handleSubmit = () => {

    };

    const handleUserName = (e) => {
        // setUsername(e.target.value);
        let currentUsername = userNameRef.current;

        const url = "http://localhost:8080/api/users/username/" + currentUsername;

        const config = {
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
            }
        };

        if(validateUsername(currentUsername)) {
            axios.get(
                url,
                config
            ).then((result) => {
                if (result.status === 200) {
                    if (result.data) {
                        // Occupied username
                        setFeedbackUsername(false);
                    } else {
                        setFeedbackUsername(true);
                    }
                }
            }).catch(error => {
                console.log("error occurred " + error);
            });
        } else {
            setFeedbackUsername(false);
        }
    };

    const validateUsername = username => {
        let isValid = true;
        // maximum length 30
        // any letter (include special characters)
        // capital lower cases
        // number
        // username string can not have HTML tags. it will be sanitized
        let sanitizedUsername = Sanitizer(username);
        // console.log("sanitized: " + sanitizedUsername + " raw: " + username + " is injected? " + (username !== sanitizedUsername));
        if(username !== sanitizedUsername) {
            // there was a HTML injection attempt
            isValid = false;
        }

        if(username.length <= 0 || username.length > 30) {
            isValid = false;
        }

        return username !== null && username !== "" && isValid;
    };

    const handleEmail = (e) => {
        setEmail(e.target.value);
    };

    const handleFirstName = (e) => {
        setFirstName(e.target.value);
    };

    const handleLastName = (e) => {
        setLastName(e.target.value);
    };

    const handlePhone = (e) => {
        setPhone(e.target.value);
    };

    const handleLanguage = (e) => {
        setDefaultLanguage(e.target.value);
    };

    return (
        <section className="modal-card-body">
            <div className="columns is-centered">
                <div className="column">
                    <form className="box" onSubmit={handleSubmit}>

                        {/*username required*/}
                        <div className="field">
                            <label className="label">Username</label>
                            <div className="control has-icons-left has-icons-right">
                                <input
                                    className="input"
                                    type="text"
                                    onChange={handleUserName}
                                    ref={userNameRef}
                                    placeholder={username}
                                    required
                                />
                                <span className="icon is-small is-left">
                                  <i className="fas fa-user"/>
                                </span>
                                <span className="icon is-small is-right">
                                  <i className="fas fa-check"/>
                                </span>
                            </div>
                            <Feedback isAvailable={feedbackUsername} succeedMsg="This username is available" failedMsg="This username is NOT available" />
                        </div>

                        {/*email required*/}
                        {/*<div className="field">*/}
                        {/*    <label className="label">Email</label>*/}
                        {/*    <div className="control has-icons-left has-icons-right">*/}
                        {/*        <input*/}
                        {/*            className="input"*/}
                        {/*            type="email"*/}
                        {/*            onChange={this.handleEmail}*/}
                        {/*            ref={this.email}*/}
                        {/*            required*/}
                        {/*        />*/}
                        {/*        <span className="icon is-small is-left">*/}
                        {/*          <i className="fas fa-envelope"/>*/}
                        {/*        </span>*/}
                        {/*        <span className="icon is-small is-right">*/}
                        {/*          <i className="fas fa-exclamation-triangle"/>*/}
                        {/*        </span>*/}
                        {/*    </div>*/}
                        {/*    <Feedback isAvailable={this.state.feedbackEmail} succeedMsg="This email is available" failedMsg="This email is NOT available" />*/}
                        {/*</div>*/}

                        {/*/!*password required*!/*/}
                        {/*<div className="field">*/}
                        {/*    <label className="label">Password</label>*/}
                        {/*    <div className="control has-icons-left">*/}
                        {/*        <ReactPasswordStrength*/}
                        {/*            className="asp-strength-meter"*/}
                        {/*            // style={{ display: 'block' }}*/}
                        {/*            minLength={5}*/}
                        {/*            minScore={2}*/}
                        {/*            scoreWords={['weak', 'okay', 'good', 'strong', 'stronger']}*/}
                        {/*            changeCallback={this.handlePasswordMeter}*/}
                        {/*            inputProps={{ name: "password_input", autoComplete: "off", className: "input" }}*/}
                        {/*            required*/}
                        {/*        />*/}
                        {/*        <span className="icon is-small is-left">*/}
                        {/*          <i className="fas fa-key"/>*/}
                        {/*        </span>*/}
                        {/*        /!*<span className="icon is-small is-right">*!/*/}
                        {/*        /!*  <i className="fas fa-exclamation-triangle"/>*!/*/}
                        {/*        /!*</span>*!/*/}
                        {/*    </div>*/}
                        {/*</div>*/}

                        {/*/!*confirm password required*!/*/}
                        {/*<div className="field">*/}
                        {/*    <label className="label">Confirm Password</label>*/}
                        {/*    <div className="control has-icons-left has-icons-right">*/}
                        {/*        <input*/}
                        {/*            className="input"*/}
                        {/*            type="password"*/}
                        {/*            onChange={this.handleConfirmPassword}*/}
                        {/*            ref={this.confirmPassword}*/}
                        {/*        />*/}
                        {/*        <span className="icon is-small is-left">*/}
                        {/*          <i className="fas fa-key"/>*/}
                        {/*        </span>*/}
                        {/*        <span className="icon is-small is-right">*/}
                        {/*          <i className="fas fa-exclamation-triangle"/>*/}
                        {/*        </span>*/}
                        {/*    </div>*/}
                        {/*    <Feedback isAvailable={this.state.feedbackConfirmPassword} succeedMsg="match" failedMsg="does not match" />*/}
                        {/*</div>*/}

                        {/*/!*firstname required*!/*/}
                        {/*<div className="field">*/}
                        {/*    <label className="label">First Name</label>*/}
                        {/*    <div className="control">*/}
                        {/*        <input*/}
                        {/*            className="input"*/}
                        {/*            type="text"*/}
                        {/*            onChange={this.handleName}*/}
                        {/*            ref={this.firstName}*/}
                        {/*            required*/}
                        {/*        />*/}
                        {/*    </div>*/}
                        {/*</div>*/}

                        {/*/!*lastname required*!/*/}
                        {/*<div className="field">*/}
                        {/*    <label className="label">Last Name</label>*/}
                        {/*    <div className="control">*/}
                        {/*        <input*/}
                        {/*            className="input"*/}
                        {/*            type="text"*/}
                        {/*            onChange={this.handleName}*/}
                        {/*            ref={this.lastName}*/}
                        {/*            required*/}
                        {/*        />*/}
                        {/*    </div>*/}
                        {/*</div>*/}

                        {/*/!*language required*!/*/}
                        {/*<div className="field">*/}
                        {/*    <label className="label">Language</label>*/}
                        {/*    <div className="control">*/}
                        {/*        <div className="select">*/}
                        {/*            <select name="language" value={this.state.defaultLanguage} onChange={this.handleDefaultLanguage}>*/}
                        {/*                {this.handleLanguageSelectionMount()}*/}
                        {/*            </select>*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*</div>*/}

                        {/*/!*phone number*!/*/}
                        {/*<div className="field">*/}
                        {/*    <label className="label">Phone Number</label>*/}
                        {/*    <div className="control has-icons-left has-icons-right">*/}
                        {/*        <div className="field has-addons">*/}
                        {/*            <p className="control is-expanded">*/}
                        {/*                <input*/}
                        {/*                    className="input"*/}
                        {/*                    type="tel"*/}
                        {/*                    onChange={this.handlePhoneNumber}*/}
                        {/*                    ref={this.phoneNumber}*/}
                        {/*                />*/}
                        {/*            </p>*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*</div>*/}

                        {/*/!*permission*!/*/}
                        {/*<div className="field">*/}
                        {/*    <label className="label checkbox">Permission</label>*/}
                        {/*    <div className="control has-icons-left has-icons-right">*/}
                        {/*        <Permission permissionPolicies={PERMISSIONS} onPermissionsChange={this.handlePermissions} />*/}
                        {/*    </div>*/}
                        {/*</div>*/}

                        {/*<div className="field is-grouped">*/}
                        {/*    <div className="control">*/}
                        {/*        <button className="button is-link">Submit</button>*/}
                        {/*    </div>*/}
                        {/*</div>*/}

                    </form>
                </div>
            </div>
        </section>

     //    <section className="modal-card-body">
     //        <div className="columns is-centered">
     //            <div className="column">
     //                <form className="box" >
     //
     //              {/*username required*/}
     //              <div className="field">
     //                  <label className="label">Username</label>
     //                  <div className="control has-icons-left has-icons-right">
     //                      <input
     //                          className="input"
     //                          type="text"
     //                          placeholder="Username"
     //                          value={username}
     //                          onChange={handleUsername}
     //                          readOnly={!edit}
     //                          required
     //                      />
     //                      <span className="icon is-small is-left">
     //                                              <i className="fas fa-user"/>
     //                                            </span>
     //                      <span className="icon is-small is-right">
     //                                              <i className="fas fa-check"/>
     //                                            </span>
     //                  </div>
     //              </div>
     //
     //              {/*email required*/}
     //              <div className="field">
     //                  <label className="label">Email</label>
     //                  <div className="control has-icons-left has-icons-right">
     //                      <input
     //                          className="input is-danger"
     //                          type="email"
     //                          placeholder="Email"
     //                          value={email}
     //                          onChange={handleEmail}
     //                          readOnly={!edit}
     //                          required
     //                      />
     //                      <span className="icon is-small is-left">
     //                                              <i className="fas fa-envelope"/>
     //                                            </span>
     //                      <span className="icon is-small is-right">
     //                                              <i className="fas fa-exclamation-triangle"/>
     //                                            </span>
     //                  </div>
     //              </div>
     //
     //              {/*firstname required*/}
     //              <div className="field">
     //                  <label className="label">First Name</label>
     //                  <div className="control">
     //                      <input
     //                          className="input"
     //                          type="text"
     //                          placeholder="Text input"
     //                          value={firstName}
     //                          onChange={handleFirstName}
     //                          readOnly={!edit}
     //                          required
     //                      />
     //                  </div>
     //              </div>
     //
     //              {/*lastname required*/}
     //              <div className="field">
     //                  <label className="label">Last Name</label>
     //                  <div className="control">
     //                      <input
     //                          className="input"
     //                          type="text"
     //                          placeholder="Text input"
     //                          value={lastName}
     //                          onChange={handleLastName}
     //                          readOnly={!edit}
     //                          required
     //                      />
     //                  </div>
     //              </div>
     //
     //                    <div className="field">
     //                        <label className="label">Language</label>
     //                        <div className="control">
     //                            <input
     //                                className="input"
     //                                type="text"
     //                                placeholder="Text input"
     //                                value={defaultLanguage}
     //                                onChange={handleLanguage}
     //                                readOnly={!edit}
     //                                required
     //                            />
     //                        </div>
     //                    </div>
     //
     //              {/*language required*/}
     //                    {/*<div className="field">
     //                  <label className="label">Language</label>
     //                  <div className="control">
     //                      <div className="select">
     //                          <select name="language" value={defaultLanguage} onChange={handleLanguage}>
     //                              {/*this.handleLanguageSelectionMount()*/}
     //                    {/*} </select>
     //                      </div>
     //                  </div>
     //              </div>*/}
     //
     //              {/*phone number*/}
     //              <div className="field">
     //                  <label className="label">Phone Number</label>
     //                  <div className="control has-icons-left has-icons-right">
     //                      <div className="field has-addons">
     //                          <p className="control">
     //                              <a className="button is-static">
     //                                  +1
     //                              </a>
     //                          </p>
     //                          <p className="control is-expanded">
     //                              <input
     //                                  className="input"
     //                                  type="tel"
     //                                  placeholder="Your phone number"
     //                                  value={phone}
     //                                  onChange={handlePhone}
     //                                  readOnly={!edit}
     //                              />
     //                          </p>
     //                      </div>
     //                  </div>
     //              </div>
     //
     //              {/*permission*/}
     //              <div className="field">
     //                  <label className="label checkbox">Permission</label>
     //                  <div className="control has-icons-left has-icons-right">
     //
     //                      {/*<Permission />*/}
     //
     //                  </div>
     //              </div>
     //
     //              <div className="field is-grouped">
     //                  <div className="control">
     //                      <button className="button is-link" onClick={handleClick}>{edit ? "Save Changes" : "Edit"}</button>
     //                  </div>
     //
     //              </div>
     //
     //          </form>
     //      </div>
     //    </div>
     // </section>

    );
}