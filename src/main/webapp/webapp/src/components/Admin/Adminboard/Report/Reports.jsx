import React, {Component, useRef, useState} from 'react';
import './adminreport.css';

import UserReports from "./components/Sections/UserReports";
import ContentReports from "./components/Sections/ContentReports";
import TrafficReports from "./components/Sections/TrafficReports";
import ViewEvents from "./components/Sections/ViewEvents";

/**
 * Component that holds all report child components.
 * @returns {null/*}
 * @constructor
 */
export default function Reports () {

    const url = "http://localhost:8080/api";
    // const url = "http://10.0.0.141:8080/api";
    // const url = "/api";

    return (
        <div className="hero-body">
            {/*<div className="rows is-center" style={{maxWidth: "750px"}} >*/}
            <div className="rows is-center" >
                <section className="hero is-info is-small">
                    <div className="hero-body">
                        <div className="container">
                            <h5 className="title">
                                Reports
                            </h5>
                        </div>
                    </div>
                </section>
                <br/>
                <div className="row">
                    <ViewEvents url={url} />
                </div><br/>
                <div className="row">
                    <TrafficReports url={url} />
                </div><br/>
                <div className="row">
                    <UserReports url={url} />
                </div><br/>
                <div className="row">
                    <ContentReports url={url} />
                </div>
            </div>
        </div>
    );
}
