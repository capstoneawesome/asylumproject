import React, { useState } from "react";
import UserEvents from "../Tabs/UserEvents";
import StoryEvents from "../Tabs/StoryEvents";
import DBEvents from "../Tabs/DBEvents";

/**
 * Component that displays Logged event for Users, Stories and Database.
 * @param props props from parent component.
 * @returns {null|*}
 * @constructor
 */
function ViewEvents(props)  {

    const [showBody, setViewContent] = useState(false);
    const [showTab, setShowTab] = useState( 1);

    function changeTab(show) {
        setShowTab(show);
    }

    return (
        <div>
            <div className="card">
                {/*<header className="card-header has-background-grey-lighter">*/}
                <header className="card-header">
                    <p className="card-header-title">Events Report</p>
                    <a href="#" className="card-header-icon" aria-label="more options">
                        <span className="icon">
                            <i className={showBody?"fas fa-angle-up":"fa fa-angle-down"} aria-hidden="true" onClick={() => setViewContent(!showBody)} />
                        </span>
                    </a>
                </header>
                {showBody?
                    <div style={{padding: "20px"}}>
                        <div className="event-report-container">
                            {/*<div className="tabs is-boxed">*/}
                            <div className="tabs is-toggle is-toggle-rounded">
                                <ul>
                                    <li className={showTab === 1? "is-active": ""} >
                                        <a href="#" onClick={() => changeTab(1)}>
                                            <span className="icon is-small">
                                                <i className="fas fa-user" aria-hidden="true"/></span>
                                            <span >Users</span>
                                        </a>
                                    </li>
                                    <li className={showTab === 2? "is-active": ""} >
                                        <a href="#" onClick={() => changeTab(2)}>
                                            <span className="icon is-small">
                                                <i className="fas fa-book-open" aria-hidden="true"/></span>
                                            <span >Stories</span>
                                        </a>
                                    </li>
                                    <li className={showTab === 3? "is-active": ""} >
                                        <a href="#" onClick={() => changeTab(3)}>
                                            <span className="icon is-small">
                                                <i className="fas fa-database" aria-hidden="true"/></span>
                                            <span >Data Base</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            {showTab === 1? <UserEvents url={props.url}/>: null}
                            {showTab === 2? <StoryEvents url={props.url}/> : null}
                            {showTab === 3? <DBEvents url={props.url}/> : null}
                        </div>
                    </div> : null }
            </div>
        </div>
    );



                    }
export default ViewEvents;



