import React, {useEffect, useState} from "react";
import axios from "axios";
import Tile from "./Report/components/Tile/Tile";


export default function Adminboard() {

    const [totalUsers, setTotalUsers] = useState({"name": "Total Users", "value": 0});
    const [numContents, setNumContents] = useState({"name": "Total Stories", "value": 0});

    const fetchNumUsers = () => {

        const url = "http://localhost:8080/api/admin/reports/users";
        const config = {
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
            }
        };

        axios.get(
            url,
            config
        ).then((result) => {
            if (result.status === 200) {
                setTotalUsers({"name": "Total Users", "value": result.data.totalUsers});
            } else {
                console.log("Error while retrieving number of users.");
            }
        }).catch((error) => {
            console.log('error');
            console.log(error);
        }).then(() => {
            // tryTokenRefresh()
        });
    };

    const fetchNumContents = () => {

        const url = "http://localhost:8080/api/admin/reports/contents";
        const config = {
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
            }
        };

        axios.get(
            url,
            config
        ).then((result) => {
            if(result.status == 200) {
                setNumContents({"name": result.data[0].name, "value": result.data[0].value});
            } else {
                console.log("Error while retrieving number of contents");
            }
        }).then(() => {
        });
    };

    useEffect(() => {
        fetchNumUsers();
        fetchNumContents();
    }, []);

    return (
        <>
            <div className="hero-body">
                <section className="info-tiles">
                    <div className="tile is-ancestor has-text-centered">

                        <div className="tile is-parent">
                            <article className="tile is-child box">
                                <p className="title">439k</p>
                                <p className="subtitle">Traffics</p>
                            </article>
                        </div>


                        {/*total story counts*/}
                        <div className="tile is-parent">
                            <div className="card is-card-widget tile is-child">
                                <header className="card-header">
                                    <p className="card-header-title">
                                        <span className="icon">
                                            <b>{numContents.name}</b>
                                        </span>
                                    </p>
                                </header>
                                <div className="card-content">
                                    <div className="level is-mobile">
                                        <div className="level-item has-widget-icon">
                                            <div className="is-widget-icon">
                                                <span className="icon is-large">
                                                    <i className="fas fa-book fa-2x"/>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="level-item">
                                            <div className="is-widget-label">
                                                <h1 className="title">
                                                    <div> {numContents.value}</div>
                                                </h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/*total user counts*/}
                        <div className="tile is-parent">
                            <div className="card is-card-widget tile is-child">
                                <header className="card-header">
                                    <p className="card-header-title">
                                        <span className="icon">
                                            <b>{totalUsers.name}</b>
                                        </span>
                                    </p>
                                </header>
                                <div className="card-content">
                                    <div className="level is-mobile">
                                        <div className="level-item has-widget-icon">
                                            <div className="is-widget-icon">
                                                <span className="icon is-large">
                                                    <i className="fas fa-users fa-2x"/>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="level-item">
                                            <div className="is-widget-label">
                                                <h1 className="title">
                                                    <div> {totalUsers.value}</div>
                                                </h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="tile is-parent">
                            <article className="tile is-child box">
                                <p className="title"></p>
                                <p className="subtitle">Backup Counts</p>
                            </article>
                        </div>
                    </div>
                </section>
                {/*<div className="columns">*/}
                {/*    <div className="column is-6">*/}
                {/*        <div className="card events-card">*/}
                {/*            <header className="card-header">*/}
                {/*                <p className="card-header-title">*/}
                {/*                    New Stories*/}
                {/*                </p>*/}
                {/*                <a href="#" className="card-header-icon" aria-label="more options">*/}
                {/*                  <span className="icon">*/}
                {/*                    <i className="fa fa-angle-down" aria-hidden="true"></i>*/}
                {/*                  </span>*/}
                {/*                </a>*/}
                {/*            </header>*/}
                {/*            <div className="card-table">*/}
                {/*                <div className="content">*/}
                {/*                    <table className="table is-fullwidth is-striped">*/}
                {/*                        <tbody>*/}
                {/*                        <tr>*/}
                {/*                            <td width="5%"><i className="fa fa-bell-o"></i></td>*/}
                {/*                            <td>Lorum ipsum dolem aire</td>*/}
                {/*                            <td className="level-right"><a*/}
                {/*                                className="button is-small is-primary" href="#">Action</a>*/}
                {/*                            </td>*/}
                {/*                        </tr>*/}
                {/*                        <tr>*/}
                {/*                            <td width="5%"><i className="fa fa-bell-o"></i></td>*/}
                {/*                            <td>Lorum ipsum dolem aire</td>*/}
                {/*                            <td className="level-right"><a*/}
                {/*                                className="button is-small is-primary" href="#">Action</a>*/}
                {/*                            </td>*/}
                {/*                        </tr>*/}
                {/*                        <tr>*/}
                {/*                            <td width="5%"><i className="fa fa-bell-o"></i></td>*/}
                {/*                            <td>Lorum ipsum dolem aire</td>*/}
                {/*                            <td className="level-right"><a*/}
                {/*                                className="button is-small is-primary" href="#">Action</a>*/}
                {/*                            </td>*/}
                {/*                        </tr>*/}
                {/*                        <tr>*/}
                {/*                            <td width="5%"><i className="fa fa-bell-o"></i></td>*/}
                {/*                            <td>Lorum ipsum dolem aire</td>*/}
                {/*                            <td className="level-right"><a*/}
                {/*                                className="button is-small is-primary" href="#">Action</a>*/}
                {/*                            </td>*/}
                {/*                        </tr>*/}
                {/*                        <tr>*/}
                {/*                            <td width="5%"><i className="fa fa-bell-o"></i></td>*/}
                {/*                            <td>Lorum ipsum dolem aire</td>*/}
                {/*                            <td className="level-right"><a*/}
                {/*                                className="button is-small is-primary" href="#">Action</a>*/}
                {/*                            </td>*/}
                {/*                        </tr>*/}
                {/*                        <tr>*/}
                {/*                            <td width="5%"><i className="fa fa-bell-o"></i></td>*/}
                {/*                            <td>Lorum ipsum dolem aire</td>*/}
                {/*                            <td className="level-right"><a*/}
                {/*                                className="button is-small is-primary" href="#">Action</a>*/}
                {/*                            </td>*/}
                {/*                        </tr>*/}
                {/*                        <tr>*/}
                {/*                            <td width="5%"><i className="fa fa-bell-o"></i></td>*/}
                {/*                            <td>Lorum ipsum dolem aire</td>*/}
                {/*                            <td className="level-right"><a*/}
                {/*                                className="button is-small is-primary" href="#">Action</a>*/}
                {/*                            </td>*/}
                {/*                        </tr>*/}
                {/*                        <tr>*/}
                {/*                            <td width="5%"><i className="fa fa-bell-o"></i></td>*/}
                {/*                            <td>Lorum ipsum dolem aire</td>*/}
                {/*                            <td className="level-right"><a*/}
                {/*                                className="button is-small is-primary" href="#">Action</a>*/}
                {/*                            </td>*/}
                {/*                        </tr>*/}
                {/*                        <tr>*/}
                {/*                            <td width="5%"><i className="fa fa-bell-o"></i></td>*/}
                {/*                            <td>Lorum ipsum dolem aire</td>*/}
                {/*                            <td className="level-right"><a*/}
                {/*                                className="button is-small is-primary" href="#">Action</a>*/}
                {/*                            </td>*/}
                {/*                        </tr>*/}
                {/*                        </tbody>*/}
                {/*                    </table>*/}
                {/*                </div>*/}
                {/*            </div>*/}
                {/*            <footer className="card-footer">*/}
                {/*                <a href="#" className="card-footer-item">View All</a>*/}
                {/*            </footer>*/}
                {/*        </div>*/}
                {/*    </div>*/}
                {/*    <div className="column is-6">*/}
                {/*        <div className="card">*/}
                {/*            <header className="card-header">*/}
                {/*                <p className="card-header-title">*/}
                {/*                    Story Search*/}
                {/*                </p>*/}
                {/*                <a href="#" className="card-header-icon" aria-label="more options">*/}
                {/*                  <span className="icon">*/}
                {/*                    <i className="fa fa-angle-down" aria-hidden="true"></i>*/}
                {/*                  </span>*/}
                {/*                </a>*/}
                {/*            </header>*/}
                {/*            <div className="card-content">*/}
                {/*                <div className="content">*/}
                {/*                    <div className="control has-icons-left has-icons-right">*/}
                {/*                        <input className="input is-large" type="text" placeholder="" />*/}
                {/*                        <span className="icon is-medium is-left">*/}
                {/*                          <i className="fa fa-search"></i>*/}
                {/*                        </span>*/}
                {/*                        <span className="icon is-medium is-right">*/}
                {/*                          <i className="fa fa-check"></i>*/}
                {/*                        </span>*/}
                {/*                    </div>*/}
                {/*                </div>*/}
                {/*            </div>*/}
                {/*        </div>*/}
                {/*        <div className="card">*/}
                {/*            <header className="card-header">*/}
                {/*                <p className="card-header-title">*/}
                {/*                    User Search*/}
                {/*                </p>*/}
                {/*                <a href="#" className="card-header-icon" aria-label="more options">*/}
                {/*                  <span className="icon">*/}
                {/*                    <i className="fa fa-angle-down" aria-hidden="true"></i>*/}
                {/*                  </span>*/}
                {/*                </a>*/}
                {/*            </header>*/}
                {/*            <div className="card-content">*/}
                {/*                <div className="content">*/}
                {/*                    <div className="control has-icons-left has-icons-right">*/}
                {/*                        <input className="input is-large" type="text" placeholder="" />*/}
                {/*                        <span className="icon is-medium is-left">*/}
                {/*                          <i className="fa fa-search"></i>*/}
                {/*                        </span>*/}
                {/*                        <span className="icon is-medium is-right">*/}
                {/*                          <i className="fa fa-check"></i>*/}
                {/*                        </span>*/}
                {/*                    </div>*/}
                {/*                </div>*/}
                {/*            </div>*/}
                {/*        </div>*/}
                {/*    </div>*/}
                {/*</div>*/}
            </div>
        </>
    );

};

