import React, {useEffect, useState} from "react";
import Pdf from 'react-to-pdf';

import '../../sass/sections.scss';
import Lines from "../Lines/Lines";
import axios from "axios";

/**
 * Component that displays Network Traffic Charts.
 * @param props props from parent component
 * @returns {null|*}
 * @constructor
 */
function TrafficReports(props) {

    const [showBody, setViewContent] = useState(true);
    const [trafficData, setTrafficData] = useState([]);

    const ref = React.createRef();
    const pdfOptions = {
        orientation: 'landscape',
        unit: 'mm',
        format: [580,500]
    };

    function fetchTrafficData() {
        axios.defaults.headers.get['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
        axios.get(props.url + "/admin/reports/traffic")
            .then((result) => {
                if(result.status == 200) {
                    setTrafficData(result.data);
                } else {
                    console.log("Error while retrieving traffic information.");
                }
            }).then(() => {
        });
    }

    useEffect(() => {
        fetchTrafficData();
    }, []);


    return(

        <div className="is-centered">
            <div className="card">
                {/*<header className="card-header has-background-grey-lighter">*/}
                <header className="card-header">
                    <p className="card-header-title">Traffic Chart</p>
                    <a href="#" className="card-header-icon" aria-label="more options">
                        <span className="icon">
                            <i className={showBody?"fas fa-angle-up":"fa fa-angle-down"} aria-hidden="true" onClick={() => setViewContent(!showBody)} />
                        </span>
                    </a>
                </header>
                {showBody?
                    <div>
                        <div className="card-content">
                            <div className="content">

                                <strong>Stories Visited per Day</strong><br/>
                                <Lines data={trafficData}/>

                            </div>
                        </div>
                    </div> : null }
            </div>
        </div>
    );
}
export default TrafficReports;
