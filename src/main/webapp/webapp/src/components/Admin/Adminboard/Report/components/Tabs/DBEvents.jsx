import React, {useEffect, useState} from "react";
import axios from "axios";
import {useToasts} from "react-toast-notifications";
import * as moment from "moment";

/**
 * Component that displays Database Logged Events.
 * @returns {null|*}
 * @constructor
 */
export default function DBEvents() {

    const {addToast} = useToasts();
    const [backups, setBackups] = useState([]);

    useEffect(() => {
        fetchBackups();
    }, []);

    function fetchBackups() {
        // setProcessingRequest(true);
        axios.defaults.headers.post['Authorization'] = localStorage.getItem("tokenType") + " " + "cQrUe8Gs5uAAAAAAAAAADYDT9az6zsa4mmp7Zi_XM5o83v-A6IIuXXJUd4JduTnA";
        axios.defaults.headers.post['Content-Type'] = "application/json";
        // axios.defaults.headers.post['User-Agent'] = "api-explorer-client";
        axios.post("https://api.dropboxapi.com/2/files/list_folder", {
            "path": "/sysadmin/backups"
        })
            .then((result) => {
                if(result.status === 200) {
                    setBackups(result.data.entries);
                }
                else {
                    addToast('Backup files information unavailable.', {appearance: 'warning', autoDismiss: true});
                    if(result.status === 409) addToast('Path not found', {appearance: 'warning', autoDismiss: true});
                    // setProcessingRequest(false);
                }
            })
            .catch((error) => {
                console.log(error);
                addToast('Backup files information unavailable.', {appearance: 'warning', autoDismiss: true});
                addToast(error.toString(), {appearance: 'warning', autoDismiss: true});
            })
    }

    return (
        <div>
            <table className="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                <thead>
                <tr>
                    {/*<th>Select</th>*/}
                    <th>Date</th>
                    <th>Time</th>
                    <th>Size (KB)</th>
                    <th>Operation</th>
                    <th>Acting User</th>
                </tr>
                </thead>

                <tbody>
                {backups.map((file, index) => {
                    return(
                        <tr key={index}>
                            <td><time dateTime="2016-1-1">{moment.utc(file.server_modified).local().format('MMM DD, YYYY')}</time></td>
                            <td><time dateTime="2016-1-1">{moment.utc(file.server_modified).local().format('hh:mm:ss a')}</time></td>
                            <td>{(file.size/1024).toFixed(2)}</td>
                            <td>Created</td>
                            <td>Jonas John</td>
                        </tr>
                    )
                })}
                </tbody>
            </table>

        </div>
    )
}
