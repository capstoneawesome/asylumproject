import React, { useEffect, useState } from "react";
import axios from "axios";
import Pdf from 'react-to-pdf';

import Bars from "../Bars/Bars";
import Tile from "../Tile/Tile";

import '../../sass/sections.scss';

import configLang from '../Bars/config_lang';
import configCurator from '../Bars/config_curator';

/**
 * Component that displays User Information Charts.
 * @param props props from parent component.
 * @returns {null|*}
 * @constructor
 */
function UserReports(props) {

    const ref = React.createRef();
    const pdfOptions = {
        orientation: 'landscape',
        unit: 'mm',
        format: [550,580]
    };

    const [showBody, setViewContent] = useState(true);
    const [totalUsers, setTotalUsers] = useState({"name": "Total Users", "value": 0});
    const [totalSysAdmins, setTotalSysAdmins] = useState({"name": "SysAdmin.", "value": 0});
    const [totalContentCurators, setTotalContentCurators] = useState({"name": "Content Curators", "value": 0});
    const [totalTeachers, setTotalTeachers] = useState({"name": "Teachers", "value": 0});
    const [langData, setLangData] = useState([{"name": "", "value": 0}]);
    const [dataCurator, setDataCurator] = useState([]);

    function fetchNumUsers() {
        let usersData = {"totalUsers": 0, "numSystemAdmin": 0, "numCurators": 0, "numTeachers": 0};

        axios.defaults.headers.get['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
        axios.get(props.url + "/admin/reports/users")
            .then((result) => {
                if (result.status === 200) {
                    usersData = result.data;
                    setTotalUsers({"name": "Total Users", "value": usersData.totalUsers});
                    setTotalSysAdmins({"name": "SysAdmin.", "value": usersData.numSystemAdmin});
                    setTotalContentCurators({"name": "Content Curators", "value": usersData.numCurators});
                    setTotalTeachers({"name": "Teachers", "value": usersData.numTeachers});
                } else {
                    console.log("Error while retrieving number of users.");
                }
            }).catch((error) => {
            console.log('error');
            console.log(error);
        }).then(() => {
            // tryTokenRefresh()
        });
    }

    function fetchLanguages() {
        axios.defaults.headers.get['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
        axios.get(props.url + "/admin/reports/user_language")
            .then((result) => {
                if (result.status === 200) {
                    setLangData(result.data);
                } else {
                    console.log("Error while retrieving language information.");
                }
            }).catch((error) => {
            console.log('error');
            console.log(error);
        }).then(() => {
            // tryTokenRefresh()
        });
    }

    function fetchCuratorData() {
        axios.defaults.headers.get['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
        axios.get(props.url + "/admin/reports/curator_stories")
            .then((result) => {
                if (result.status === 200) {
                    setDataCurator(result.data.curators);
                } else {
                    console.log("Error while retrieving Curators Data.");
                }
            }).catch((error) => {
            console.log('error');
            console.log(error);
        }).then(() => {
            // tryTokenRefresh()
        });
    }

    useEffect(() => {
        fetchNumUsers();
        fetchLanguages();
        fetchCuratorData();
    }, []);

    return (

        <div>

            <div className="card">
                {/*<header className="card-header has-background-grey-lighter">*/}
                <header className="card-header">
                    <p className="card-header-title">Users Charts</p>
                    <a href="#" className="card-header-icon" aria-label="more options">
                        <span className="icon">
                            <i className={showBody?"fas fa-angle-up":"fa fa-angle-down"} aria-hidden="true" onClick={() => setViewContent(!showBody)} />
                        </span>
                    </a>
                </header>
                {showBody?
                    <div>
                        <div className="card-content">
                            <div className="content">
                                <div className="rows">
                                    <div className="row">
                                        <div className="columns">
                                            <div className="column is-one-third is-offset-one-third">
                                                <Tile displayData={totalUsers} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="column is-three-fifths is-offset-one-fifth">
                                        <div className="columns">
                                            <div className="column">
                                                <Tile displayData={totalSysAdmins} />
                                            </div>
                                            <div className="column">
                                                <Tile displayData={totalTeachers} />
                                            </div>
                                            <div className="column">
                                                <Tile displayData={totalContentCurators} />
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="columns">
                                            <div className="column">
                                                <strong>Users per Language</strong><br/>
                                                <Bars data={langData} index={"name"} config={configLang} colorBy={"solid"} />
                                                {/*<Bars data={langData} index={"name"} config={configLang} />*/}
                                            </div>
                                            <div className="column">
                                                <strong>Stories per Content Curator</strong><br/>
                                                <Bars data={dataCurator} index={"curator"} config={configCurator} colorBy={"id"} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> : null }
            </div>
        </div>
    );
}

export default UserReports;
