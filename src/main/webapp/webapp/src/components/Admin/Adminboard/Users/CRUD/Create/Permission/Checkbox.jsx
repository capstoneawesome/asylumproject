import React from "react";

/**
 * Component that holds a list of individual ContentListElement components.
 * @param id an id number for each permission
 * @param label a string value for description of each permission
 * @param isSelected a boolean value to check whether a checkbox is selected or not.
 * @param onCheckboxChange a listener method to handle with DOM event.
 * @returns {*}
 */

const Checkbox = ({id, label, isSelected, onCheckboxChange}) => (

    <div className="form-check">
        <input
            type="checkbox"
            name="permissions"
            value={id}
            checked={isSelected}
            onChange={onCheckboxChange}
            // className="switch is-rtl"
        />
        <label>
            {label}
        </label>
    </div>

);

export default Checkbox;

