import React, {useEffect, useState} from "react";
import * as moment from "moment";
import axios from "axios";
import {useToasts} from "react-toast-notifications";

/**
 * Component that displays User Logged Events.
 * @param props props from parent component.
 * @returns {null|*}
 * @constructor
 */
export default function UserEvent(props) {

    const {addToast} = useToasts();

    const [dropDown, setDropDown] = useState(false);
    const [usersEvents, setUsersEvents] = useState([]);
    const [listUsers, setListUsers] = useState( []);
    const [selectedElements, setSelectedElements] = useState([]);
    // const [tableData, setTableData] = useState([]);
    const [selectedUsers, setSelectedUsers] = useState([]);
    const [pdfLink, setPdfLink] = useState("");
    const [csvLink, setCsvLink] = useState("");

    useEffect(() => {
        fetchUsers();
        fetchLinks();
    }, []);

    function fetchLinks() {
        axios.defaults.headers.post['Authorization'] = localStorage.getItem("tokenType") + " " + "cQrUe8Gs5uAAAAAAAAAADYDT9az6zsa4mmp7Zi_XM5o83v-A6IIuXXJUd4JduTnA";
        axios.defaults.headers.post['Content-Type'] = "application/json";
        // axios.defaults.headers.post['User-Agent'] = "api-explorer-client";
        axios.post("https://api.dropboxapi.com/2/files/get_temporary_link", {
            "path": "/sysadmin/reports/user_events_report.csv"
        })
            .then((result) => {
                if(result.status === 200) setCsvLink(result.data.link);
                else addToast('CSV Download link unable at this moment. Please press "Refresh" to update.', {appearance: 'error', autoDismiss: true});
            })
            .catch((error) => {
                addToast('CSV Download link unable at this moment. Please press "Refresh" to update.', {appearance: 'error', autoDismiss: true});
                addToast(error.toString(), {appearance: 'error', autoDismiss: true});
            })

        axios.post("https://api.dropboxapi.com/2/files/get_temporary_link", {
            "path": "/sysadmin/reports/user_events_report.pdf"
        })
            .then((result) => {
                if(result.status === 200) setPdfLink(result.data.link);
                else addToast('PDF Download link unable at this moment. Please press "Refresh" to update.', {appearance: 'error', autoDismiss: true});
            })
            .catch((error) => {
                addToast('PDF Download link unable at this moment. Please press "Refresh" to update.', {appearance: 'error', autoDismiss: true});
                addToast(error.toString(), {appearance: 'error', autoDismiss: true});
            })
    }

    function downloadCSV(link) {
        window.open(link, "_self");
    }

    function downloadPDF(link) {
        window.open(link, "_self");
    }

    function fetchUsers() {
        // console.log(props.url + "/admin/events/users");
        axios.defaults.headers.get['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
        axios.get(props.url + "/admin/events/users")
            .then((result) => {
                if(result.status === 200) {
                    setUsersEvents(result.data.events);
                    setListUsers(result.data.users);
                } else {
                    console.log("Error while retrieving users information.");
                }
            }).then(() => {
        });

    }

    // function fetchUsers_old() {
    //     axios.defaults.headers.get['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
    //     axios.get(props.url + "/admin/reports/all_users")
    //         .then((result) => {
    //             if(result.status === 200) {
    //                 setListElements(result.data);
    //             } else {
    //                 console.log("Error while retrieving users information.");
    //             }
    //         }).then(() => {
    //     });
    // }

    // function fetchResults() {
    //
    //     axios.defaults.headers.post['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
    //     axios.post(props.url + "/admin/reports/filter_users", selectedElements)
    //         .then((result) => {
    //             if(result.status === 200) {
    //                 // setTableData(result.data);
    //                 setUsersEvents(result.data.events);
    //                 setListUsers(result.data.users);
    //             } else {
    //                 console.log("Error while retrieving table data.");
    //             }
    //         }).then(() => {
    //     });
    // }

    function setRowEffect(element) {
        let rowEffect;
        if(element.operation === 'Deleted')
            rowEffect = "is-size-7 has-background-danger has-text-white";
        else if(element.operation === 'Created')
            rowEffect = "is-size-7 has-text-info has-text-weight-semibold ";
        else if(element.operation === 'Password Change')
            rowEffect = "is-size-7 has-background-warning";
        else
            rowEffect = "is-size-7";
        return rowEffect;
    }

    function elementSelected(index) {
        setDropDown(!dropDown);
        if(index < 0) {
            selectedElements.splice(0);
            selectedUsers.splice(0);
            for(let i = 0; i < usersEvents.length; i++) {
                selectedElements.push(usersEvents[i]);
            }
            for(let i = 0; i < listUsers.length; i++) {
                selectedUsers.push(listUsers[i]);
            }
        }
        let found = false;
        for(let i = 0; i < selectedElements.length; i++) {
            if(selectedElements[i].userId === index)
                found = true;
        }
        if(!found) {
            for(let i = 0; i < listUsers.length; i++) {
                if(listUsers[i].userId === index)
                    selectedUsers.push(listUsers[i]);
            }
            for(let i = 0; i < usersEvents.length; i++) {
                if(usersEvents[i].userId === index)
                    selectedElements.push(usersEvents[i]);
            }
        }
    }

    function removeElement(index) {
        // fetchResults();
        let tempList = selectedElements.slice();
        selectedElements.splice(0);
        for(let i = 0; i < tempList.length; i++) {
            if(tempList[i].userId !== index)
                selectedElements.push(tempList[i]);
        }
        for(let i = 0; i < selectedUsers.length; i++) {
            if(selectedUsers[i].userId === index)
                selectedUsers.splice(i, 1);
        }
        addToast("User was removed.", {appearance: 'warning', autoDismiss: true});

    }

    return(
        <div className="rows">
            <div className="row">
                <div className="columns">
                    <div className="column is-one-fifth">
                        <div className={dropDown?"dropdown is-active":""}>
                            <div className="dropdown-trigger">
                                <button onClick={() => setDropDown(!dropDown)} className="button" aria-haspopup="true" aria-controls="dropdown-menu2">
                                    <span>Select User</span>
                                    <span className="icon is-small">
                                        <i className="fas fa-angle-down" aria-hidden="true"/>
                                    </span>
                                </button>
                            </div>
                            <div className="dropdown-menu" id="dropdown-menu2" role="menu">
                                <div className="dropdown-content is-small">
                                    <div onClick={() => elementSelected(-1)} className="dropdown-item is-small">
                                        <p>All</p>
                                    </div>
                                    <hr className="dropdown-divider" />
                                    {listUsers.map((element, index) => {
                                        return (
                                            <div key={index}>
                                                {/*<hr className="dropdown-divider" />*/}
                                                <div onClick={() => elementSelected(element.userId)} className="dropdown-item is-small">
                                                    <p>{element.username}</p>
                                                </div>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="column is-four-fifths">
                        <div className="tags">
                            {selectedUsers.map((element, index) => {
                                return (
                                    <span className="tag is-rounded is-info" key={index}>
                                                                    {element.username}
                                        <button className="delete is-small" onClick={() => removeElement(element.userId)} />
                                                                </span>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div><br/>
            {selectedElements.length > 0 ?
                <div>
                    <div className="row">
                        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                            <thead>
                            <tr className="is-size-7">
                                <th>Username</th>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Operation</th>
                                <th>Acting User</th>
                            </tr>
                            </thead>
                            <tbody>
                            {selectedElements.map((dataTable, index) => {
                                return(
                                    // <tr key={index} className={dataTable.operation === 'Deleted'? "has-background-danger has-text-white has-text-weight-semibold": "" }>
                                    <tr key={index} className={setRowEffect(dataTable)} >
                                        <td className="has-background-white">{dataTable.operation === 'Created' ? dataTable.userName : null}</td>
                                        <td className="has-background-white">{dataTable.operation === 'Created' ? dataTable.user : null}</td>
                                        <td><time dateTime="2016-1-1">{moment.utc(dataTable.dateTime).local().format('MMM DD, YYYY')}</time></td>
                                        <td><time dateTime="2016-1-1">{moment.utc(dataTable.dateTime).local().format('hh:mm:ss a')}</time></td>
                                        <td>{dataTable.operation} </td>
                                        <td>{dataTable.actingUser}</td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>
                    </div><br/>
                    <div className="row">
                        <div className="columns is-1">
                            <div className="column is-2 is-offset-8 ">
                                <button className="button is-info is-light is-small" onClick={() => downloadCSV(csvLink)}>Download CSV</button>
                            </div>
                            <div className="column is-2 ">
                                <button className="button is-danger is-light is-small" onClick={() => downloadPDF(pdfLink)}>Download PDF</button>
                            </div>
                        </div>
                    </div>
                </div> : null}

        </div>
    )

}
