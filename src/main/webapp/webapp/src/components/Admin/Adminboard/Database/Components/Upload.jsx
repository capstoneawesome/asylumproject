import React, {useState} from "react";

/**
 * Component that holds input fields to upload a local database backup file to the cloud repository.
 * @param props props from parent component
 * @returns {null|*}
 * @constructor
 */
export default function Upload (props) {

    const [showBody, setViewContent] = useState(false);

    function toogleView() {
        setViewContent(!showBody);
    }

    return (
        <div >
            <div className="card">
                <header className="card-header has-background-grey-lighter">
                    <p className="card-header-title">Upload Backup File</p>
                    <a href="#" className="card-header-icon" aria-label="more options">
                        <span className="icon">
                            <i className={showBody ? "fas fa-angle-up" : "fas fa-angle-down"} aria-hidden="true" onClick={toogleView} />
                        </span>
                    </a>
                </header>
                {showBody?
                    <div className="card-content">
                        <div className="content">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris.
                            <a href="#">@bulmaio</a>. <a href="#">#css</a> <a href="#">#responsive</a>
                            <br/>
                            <time dateTime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                        </div>
                    </div> : null }
            </div>
            {/*<footer className="card-footer">*/}
            {/*    <a href="#" className="card-footer-item">Save</a>*/}
            {/*    <a href="#" className="card-footer-item">Edit</a>*/}
            {/*    <a href="#" className="card-footer-item">Delete</a>*/}
            {/*</footer>*/}
            <br />
        </div>
    );

}
