import React from 'react';
import { ResponsiveBar } from '@nivo/bar';

import './chart.css';

/**
 * Component that displays bar charts based on provided information from backend.
 * @param props props from parent component
 * @returns {null|*}
 * @constructor
 */
export default function Bars (props) {

    const color2 = ['#f47560', '#61cdbb', '#97e3d5', '#e8c1a0', '#f1e15b', '#e8a838'];
    const color1 = ['#f1e15b'];
    const color = ['#97e3d5'];
    const mycolor = ['#e8a838', '#61CDBB', '#97e3d5', '#F2C0B8'];

    return (
        // <div className="bars" style={{"width": "95%"}}>
        <div className="bars" style={{"height": props.height}}>
            <ResponsiveBar
                data={props.data}
                keys={props.config.keys}
                // indexBy="Language"
                indexBy={props.index}
                margin={props.config.margin}
                padding={0.3}
                layout = {props.config.layout}
                // colors="nivo"
                // colors={props.colorBy === "index"? color: "nivo"}
                colors={props.colorBy === "solid"? color: mycolor}
                // colors={props.colorBy === "solid"? color: mycolor}
                // colorBy="index"
                // colorBy="id"
                colorBy={props.colorBy}
                defs={props.config.defs}
                fill={props.config.fill}
                borderColor="inherit:darker(1.6)"
                axisTop={null}
                axisRight={null}
                axisBottom={props.config.axisBottom}
                axisLeft={props.config.axisLeft}
                labelSkipWidth={12}
                labelSkipHeight={12}
                labelTextColor="inherit:darker(1.6)"
                isInteractive={props.config.isInteractive}
                animate={true}
                motionStiffness={45}
                motionDamping={15}
                // legends={config.legends}
            />
        </div>
    );
}
