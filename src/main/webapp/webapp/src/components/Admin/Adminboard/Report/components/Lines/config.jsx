export default {
    // keys: [
    //     "hot dog",
    //     "burger",
    //     "sandwich",
    //     "kebab",
    //     "fries",
    //     "donut"
    // ],
    margin: {
        "top": 20,
        "right": 20,
        "bottom": 70,
        "left": 60
    },
    // defs: [
    //     {
    //         id: 'dots',
    //         type: 'patternDots',
    //         background: 'inherit',
    //         color: 'rgba(255, 255, 255, 0.3)',
    //         size: 4,
    //         padding: 1,
    //         stagger: true
    //     },
    //     {
    //         id: 'lines',
    //         type: 'patternLines',
    //         background: 'inherit',
    //         color: 'rgba(255, 255, 255, 0.3)',
    //         rotation: -45,
    //         lineWidth: 6,
    //         spacing: 10
    //     }
    // ],
    // fill: [
    //     {
    //         match: {
    //             id: 'ruby'
    //         },
    //         id: 'dots'
    //     },
    //     {
    //         match: {
    //             id: 'c'
    //         },
    //         id: 'dots'
    //     },
    //     {
    //         match: {
    //             id: 'go'
    //         },
    //         id: 'dots'
    //     },
    //     {
    //         match: {
    //             id: 'python'
    //         },
    //         id: 'dots'
    //     },
    //     {
    //         match: {
    //             id: 'scala'
    //         },
    //         id: 'lines'
    //     },
    //     {
    //         match: {
    //             id: 'lisp'
    //         },
    //         id: 'lines'
    //     },
    //     {
    //         match: {
    //             id: 'elixir'
    //         },
    //         id: 'lines'
    //     },
    //     {
    //         match: {
    //             id: 'javascript'
    //         },
    //         id: 'lines'
    //     }
    // ],
    axisBottom: {
        orient: 'bottom',
        tickSize: 5,
        tickPadding: 5,
        tickRotation: -45,
        legend: '',
        legendOffset: 43,
        legendPosition: 'middle'
    },
    axisLeft: {
        orient: 'left',
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: '# of Stories',
        legendOffset: -50,
        legendPosition: 'middle'
    },
    legends: [
        {
            anchor: 'bottom-left',
            direction: 'row',
            justify: false,
            translateX: -35,
            translateY: 70,
            itemsSpacing: 0,
            itemDirection: 'left-to-right',
            itemWidth: 63,
            itemHeight: 20,
            itemOpacity: 0.75,
            symbolSize: 8,
            symbolShape: 'circle',
            symbolBorderColor: 'rgba(0, 0, 0, .5)',
            effects: [
                {
                    on: 'hover',
                    style: {
                        itemBackground: 'rgba(0, 0, 0, .03)',
                        itemOpacity: 1
                    }
                }
            ]
        }
    ]
}
