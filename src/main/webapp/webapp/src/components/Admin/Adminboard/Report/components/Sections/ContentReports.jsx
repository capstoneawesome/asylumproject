import React, { useState, useEffect } from "react";
import axios from "axios";
import Pdf from 'react-to-pdf'

import '../../sass/sections.scss'
import Bars from "../Bars/Bars";
import Pie from "../Pie/Pie";
import Tile from "../Tile/Tile"

import config from '../Bars/config';

/**
 * Component that displays Content Information Charts.
 * @param props props from parent component.
 * @returns {null|*}
 * @constructor
 */
function ContentReports(props) {

    const ref = React.createRef();
    const pdfOptions = {
        orientation: 'portrait',
        unit: 'mm',
        format: [1500,580]
    };

    const defaultPieHeight = "330px";

    const [showBody, setViewContent] = useState(true);
    const [numContents, setNumContents] = useState([]);
    const [numElements, setNumElements] = useState([]);
    const [storageData, setStorageData] = useState({"elements": []});
    const [storiesStatus, setStoriesStatus] = useState({"elements": []});
    const [storiesLanguage, setStoriesLanguage] = useState({"elements": []});
    const [storiesCountry, setStoriesCountry] = useState({"countries": []});

    function fetchNumContents() {
        axios.defaults.headers.get['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
        axios.get(props.url + "/admin/reports/contents")
            .then((result) => {
                if(result.status === 200) {
                    setNumContents(result.data);
                } else {
                    console.log("Error while retrieving number of contents");
                }
            }).then(() => {
        });
    }

    function fetchStorageData() {
        axios.defaults.headers.get['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
        axios.get(props.url + "/admin/reports/storage")
            .then((result) => {
                if(result.status === 200) {
                    setStorageData(result.data);
                } else {
                    console.log("Error while retrieving number of contents");
                }
            }).then(() => {
        });
    }

    function fetchNumElements() {
        axios.defaults.headers.get['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
        axios.get(props.url + "/admin/reports/content_elements")
            .then((result) => {
                if(result.status === 200) {
                    setNumElements(result.data);
                } else {
                    console.log("Error while retrieving number of elements");
                }
            }).then(() => {
        });
    }

    function fetchStoriesStatus() {
        axios.defaults.headers.get['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
        axios.get(props.url + "/admin/reports/story_status")
            .then((result) => {
                if(result.status === 200) {
                    setStoriesStatus(result.data);
                } else {
                    console.log("Error while retrieving stories per status");
                }
            }).then(() => {
        });
    }

    function fetchStoriesLanguage() {
        axios.defaults.headers.get['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
        axios.get(props.url + "/admin/reports/story_language")
            .then((result) => {
                if(result.status === 200) {
                    setStoriesLanguage(result.data);
                } else {
                    console.log("Error while retrieving stories per language");
                }
            }).then(() => {
        });
    }

    function fetchStoriesCountry() {
        axios.defaults.headers.get['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
        axios.get(props.url + "/admin/reports/story_country")
            .then((result) => {
                if (result.status === 200) {
                    setStoriesCountry(result.data);
                } else {
                    console.log("Error while retrieving country information.");
                }
            }).catch((error) => {
            console.log('error');
            console.log(error);
        }).then(() => {
            // tryTokenRefresh()
        });

    }

    useEffect(() => {
        fetchNumContents();
        fetchStoriesStatus();
        fetchStoriesLanguage();
        fetchStoriesCountry();
        fetchNumElements();
        fetchStorageData();
    }, []);

    return (
        <div>
            <div className="card">
                {/*<header className="card-header has-background-grey-lighter">*/}
                <header className="card-header">
                    <p className="card-header-title">Content Information Charts</p>
                    <a href="#" className="card-header-icon" aria-label="more options">
                        <span className="icon">
                            <i className={showBody?"fas fa-angle-up":"fa fa-angle-down"} aria-hidden="true" onClick={() => setViewContent(!showBody)} />
                        </span>
                    </a>
                </header>
                {showBody?
                    <div>
                        <div className="card-content">
                            <div className="content">
                                <div className="rows">
                                    <div className="row">

                                        <div className="column is-8 is-offset-2">
                                        <div className="columns">
                                            <div className="column is-4">
                                                <div className="rows"><br/>
                                                {numContents.map((element, index) => {
                                                    return (
                                                        <div className="row" key={index}>
                                                            <Tile displayData={element} /><br/>
                                                        </div>
                                                    )
                                                })}
                                                </div>
                                            </div>
                                            <div className="column is-8">
                                                <strong>Stories per Status</strong><br/>
                                                <Pie data={storiesStatus.elements} height={defaultPieHeight} config={"stories"}/>
                                            </div>
                                        </div>
                                        </div>

                                    </div><hr/>
                                    <div className="columns">
                                        <div className="column">
                                            <div className="rows">
                                                <div className="row">
                                                    <strong>Stories per Country of Origin</strong><br/>
                                                    <Bars data={storiesCountry.countries} index={"country"} config={config} colorBy={"id"} height={defaultPieHeight} />
                                                </div><br/>
                                                <div className="row">
                                                    <Tile displayData={{"name": "Countries", "value": storiesCountry.countries.length}} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="column">
                                            <div className="rows">
                                                <div className="row">
                                                    <Tile displayData={{"name": "Languages", "value": storiesLanguage.elements.length}} />
                                                </div><br/>
                                                <div className="row">
                                                    <strong>Stories per Language</strong><br/>
                                                    <Pie data={storiesLanguage.elements}  height={defaultPieHeight}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div><hr/>
                                    <div className="row">
                                        <div className="column is-8 is-offset-2">
                                        <div className="columns">
                                            <div className="column is-8">
                                                <strong>Storage Space (MB)</strong><br/>
                                                <Pie data={storageData.elements} height={defaultPieHeight}/>
                                            </div>
                                            <div className="column">
                                            {numElements.map((element, index) => {
                                                return (
                                                    // <div className="row" key={index}>
                                                    <div key={index}>
                                                        <Tile displayData={element} plural="File(s)" height={"30px"}/><br/>
                                                    </div>
                                                )
                                            })}
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> : null }
            </div>
        </div>
    );

}
export default ContentReports;
