import React from "react";
import axios from "axios";

/**
 * Component that display confirm action message in order to double check on user deletion.
 * @param props props from parent component
 * @returns {*}
 */

export default function Delete(props) {

    const handleConfirmDelete = () => {
        const url = "http://localhost:8080/api/users/" + props.user.id;

        const config = {
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
            }
        };

        axios.delete(
            url,
            config
        ).then((result) => {
            if(result.status === 200) {
                props.clearScreen();
            } else {
                console.log(result.data);
            }
        });

    };

    return (
        <>
            <footer className="modal-card-foot">
                <button type="button" className="button">Cancel</button>
                <button className="button is-danger" onClick={handleConfirmDelete}>Delete</button>
            </footer>
        </>
    );

};