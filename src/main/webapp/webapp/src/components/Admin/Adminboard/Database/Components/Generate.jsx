import React, { useState } from "react";
import axios from "axios";
import {useToasts} from 'react-toast-notifications';
import * as moment from "moment";

/**
 * Component that provides functionality for database backup file generation.
 * @param props props from parent component
 * @returns {null|*}
 * @constructor
 */
export default function Generate (props) {

    const {addToast} = useToasts();
    const [showBody, setViewContent] = useState(true);
    const [processingRequest, setProcessingRequest] = useState(false);
    const [message, setMessage] = useState(null);
    const [fileLink, setFileLink] = useState({"available": false, "link": "", "size": "", "date": ""});
    const [sendMail, setSendMail] = useState(false);

    function fetchDownloadLink(path) {
        axios.defaults.headers.post['Authorization'] = localStorage.getItem("tokenType") + " " + props.dbToken;
        axios.defaults.headers.post['Content-Type'] = "application/json";
        // axios.defaults.headers.post['User-Agent'] = "api-explorer-client";
        axios.post("https://api.dropboxapi.com/2/files/get_temporary_link", {
            "path": path
        })
            .then((result) => {
                if(result.status === 200) {
                    let size = result.data.metadata.size/1024;
                    setFileLink({"available": true, "link": result.data.link, "size": size.toFixed(2), "date": result.data.metadata.server_modified});
                }
                else {
                    addToast('Downloadable link not available at this moment.', {appearance: 'warning', autoDismiss: true});
                    if(result.status === 409) addToast('Path not found', {appearance: 'warning', autoDismiss: true});
                }
            })
            .catch((error) => {
                console.log(error);
                setMessage("An error has occurred.");
                addToast('Downloadable link not available at this moment.', {appearance: 'warning', autoDismiss: true});
                addToast(error.toString(), {appearance: 'warning', autoDismiss: true});
            })
    }

    function generateBackup() {

        addToast('Generating Backup File. Please wait until backup process is finished', {appearance: 'info', autoDismiss: true});
        setProcessingRequest(true);

        axios.defaults.headers.post['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
        axios.post(props.url + "/admin/backup", {
            "send_mail": sendMail,
            "mail_to": ""
        })
            .then((result) => {
                if (result.status === 200) {
                    setMessage(result.data.mailSent? "A copy was sent to your email address": "");
                    addToast('Backup file has been generated and saved!. You may download a copy', {appearance: 'success', autoDismiss: true});
                    fetchDownloadLink(result.data.path);
                }
                else {
                    setMessage("An error has happened please try again");
                    addToast('Unable to generate Backup, please try again', {appearance: 'error', autoDismiss: true});
                }
                setProcessingRequest(false);
            })
            .catch((error) => {
                console.log(error);
                setMessage("An error has happened, please try again");
                addToast('Unable to generate Backup, please try again', {appearance: 'error', autoDismiss: true});
                addToast(error.toString(), {appearance: 'error', autoDismiss: true});
                setProcessingRequest(false);
            })
    }

    function getMailCopy() {
        setSendMail(!sendMail);
    }

    return (
        <div >
            <div className="card">
                <header className="card-header has-background-grey-lighter">
                    <p className="card-header-title">Generate Backup File</p>
                    <a href="#" className="card-header-icon" aria-label="more options">
                        <span className="icon">
                            <i className={showBody?"fas fa-angle-up":"fas fa-angle-down"} aria-hidden="true" onClick={() => setViewContent(!showBody)} />
                        </span>
                    </a>
                </header>
                {showBody?
                    <div>

                <div className="card-content">
                    <div className="content">

                        <div className="columns">
                            <div className="column">
                                <div className="field">
                                    <div className="control">
                                        <label className="checkbox">
                                            <input type="checkbox" onClick={getMailCopy} />
                                            Send a copy to email.
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div className="column">
                                <div className="control">
                                    <button className={processingRequest? "button is-success is-loading is-small": "button is-success is-small"} onClick={generateBackup}>{processingRequest? "": "Generate Backup"}</button>
                                </div>
                            </div>
                            <div className="column">
                                <p>{processingRequest? "" : message}</p>
                            </div>
                        </div>
                    </div>
                </div>
                { (fileLink.available && !processingRequest) ?
                <footer className="card-footer">
                    <p className="card-footer-item">
                        <a href={fileLink.link} >Download Backup File</a>
                    </p>
                    <p className="card-footer-item">
                        File size: {fileLink.size} KB
                    </p>
                    <p className="card-footer-item">
                        <time dateTime="2016-1-1">{moment.utc(fileLink.date).local().format('MMM DD, YYYY hh:mm a')}</time>
                    </p>
                </footer>
                : ""}
                </div> : null }
            </div>
            <br />
        </div>
    );

}
