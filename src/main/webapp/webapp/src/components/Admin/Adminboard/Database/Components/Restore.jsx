import React, {useEffect, useState} from "react";
import axios from "axios";
import {useToasts} from "react-toast-notifications";
import * as moment from "moment";
import swal from 'sweetalert';

/**
 * Component that holds a list of existing database backup files.
 * @param props props from parent component
 * @returns {null|*}
 * @constructor
 */
export default function Restore (props) {

    const {addToast} = useToasts();
    const [showBody, setViewContent] = useState(true);
    const [backups, setBackups] = useState([]);
    const [processingRequest, setProcessingRequest] = useState(false);

    useEffect(() => {
        fetchBackups();
    }, []);

    useEffect(() => {
        for(let i = 0; i < backups.length; i++) {
            getDownloadLink(backups[i].path_lower, backups[i]);
        }
    }, [backups]);

    function getDownloadLink(path, elem) {

        axios.defaults.headers.post['Authorization'] = localStorage.getItem("tokenType") + " " + props.dbToken;
        axios.defaults.headers.post['Content-Type'] = "application/json";
        // axios.defaults.headers.post['User-Agent'] = "api-explorer-client";
        axios.post("https://api.dropboxapi.com/2/files/get_temporary_link", {
            "path": path
        })
            .then((result) => {
                if(result.status === 200) elem.downLink = result.data.link;
                else addToast('Download link unable at this moment. Please press "Refresh" to update.', {appearance: 'error', autoDismiss: true});
            })
            .catch((error) => {
                addToast('Download link unable at this moment. Please press "Refresh" to update.', {appearance: 'error', autoDismiss: true});
                addToast(error.toString(), {appearance: 'error', autoDismiss: true});
            })
    }

    function toogleView() {
        setViewContent(!showBody);
        if(!showBody) fetchBackups();
    }

    function confirmDeleteElement(index) {
        swal( {
            title: "Are you sure you want to delete this Backup File?",
            text: "Once deteled, you will not be able to recover this Backup File!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            closeOnEsc: true
        }).then((willDelete) => {
            if(willDelete) {
                dElem(index);
            }
        });
    }

    function dElem(index) {

        const path = backups[index].path_lower;

        axios.defaults.headers.post['Authorization'] = localStorage.getItem("tokenType") + " " + props.dbToken;
        axios.defaults.headers.post['Content-Type'] = "application/json";
        // axios.defaults.headers.post['User-Agent'] = "api-explorer-client";
        axios.post("https://api.dropboxapi.com/2/files/delete_v2", {
            "path": path
        })
            .then((result) => {
                if(result.status === 200) {
                    addToast('Please wait while backup file is deleted', {appearance: 'info', autoDismiss: true});
                    // fetchBackups();
                    let temp = backups;
                    temp.splice(index, 1);
                    setBackups(temp);
                }
                else {
                    addToast('Unable to delete file.', {appearance: 'warning', autoDismiss: true});
                    if(result.status === 409) addToast('Path not found', {appearance: 'warning', autoDismiss: true});
                }
            })
            .catch((error) => {
                console.log(error);
                addToast('Unable to delete file.', {appearance: 'warning', autoDismiss: true});
                addToast(error.toString(), {appearance: 'warning', autoDismiss: true});
            })
    }

    function fetchBackups() {
        setProcessingRequest(true);
        axios.defaults.headers.post['Authorization'] = localStorage.getItem("tokenType") + " " + props.dbToken;
        axios.defaults.headers.post['Content-Type'] = "application/json";
        // axios.defaults.headers.post['User-Agent'] = "api-explorer-client";
        axios.post("https://api.dropboxapi.com/2/files/list_folder", {
            "path": "/sysadmin/backups"
        })
            .then((result) => {
                if(result.status === 200) {
                    setBackups(result.data.entries);
                    setProcessingRequest(false);
                    // addToast('Data retrieved', {appearance: 'info', autoDismiss: true});
                }
                else {
                    addToast('Backup files information unavailable.', {appearance: 'warning', autoDismiss: true});
                    if(result.status === 409) addToast('Path not found', {appearance: 'warning', autoDismiss: true});
                    setProcessingRequest(false);
                }
            })
            .catch((error) => {
                console.log(error);
                addToast('Backup files information unavailable.', {appearance: 'warning', autoDismiss: true});
                addToast(error.toString(), {appearance: 'warning', autoDismiss: true});
                setProcessingRequest(false);
            })
    }

    function download(link) {
        window.open(link, "_self");
    }

    function toServer(path) {
        axios.defaults.headers.get['Authorization'] = localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken");
        axios.get(props.url + "/admin/retrieve?path=" + path)
            .then((result) => {
                if (result.status === 200)
                    addToast("Backup file " + path + " is now ready in the server's \"resource\" directory", {appearance: 'warning', autoDismiss: true});
                else
                    addToast('Unable to upload file to server. Please try again.', {appearance: 'error', autoDismiss: true});
            })
            .catch((error) => {
                addToast('Unable to upload file to server. Please try again.', {appearance: 'error', autoDismiss: true});
                addToast(error.toString(), {appearance: 'error', autoDismiss: true});
            })
    }

    return (
        <div >
            <div className="card">
                <header className="card-header has-background-grey-lighter">
                    <p className="card-header-title">
                        Database Backup Files &nbsp;
                        <button className={processingRequest? "button is-success is-loading is-small": "button is-success is-small"} onClick={fetchBackups}>Refresh</button>
                    </p>
                    <a href="#" className="card-header-icon" aria-label="more options">
                        <span className="icon">
                            <i className={showBody?"fas fa-angle-up":"fas fa-angle-down"} aria-hidden="true" onClick={toogleView} />
                        </span>
                    </a>
                </header>
                {showBody?
                    <div className="card-content">
                        <div className="content">

                            <table className="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                                <thead>
                                <tr>
                                    {/*<th>Select</th>*/}
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Size (KB)</th>
                                    <th>Upload</th>
                                    <th>Download</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>

                                <tbody>
                                {backups.map((file, index) => {
                                    return(
                                        <tr key={index}>
                                            <td><time dateTime="2016-1-1">{moment.utc(file.server_modified).local().format('MMM DD, YYYY')}</time></td>
                                            <td><time dateTime="2016-1-1">{moment.utc(file.server_modified).local().format('hh:mm:ss a')}</time></td>
                                            <td>{(file.size/1024).toFixed(2)}</td>
                                            <td>
                                                <div className="control">
                                                    <button className="button is-success is-small" onClick={() => toServer(file.path_lower)} >Upload</button>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="control">
                                                    <button className="button is-info is-small" onClick={() => download(file.downLink)} >Download</button>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="control">
                                                    <button className="button is-warning is-small" onClick={() => confirmDeleteElement(index)} >Delete</button>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                })}
                                </tbody>
                            </table>

                        </div>
                    </div> : null }
            </div>

            <br />
        </div>
    );

}
