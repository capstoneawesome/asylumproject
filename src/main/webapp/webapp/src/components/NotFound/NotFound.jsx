import React from "react";
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";

/**
 * Component that redirect to the main page when URI is not recognized.
 * @returns {*}
 */

export default function NotFound() {

    return (
        <Redirect to="/map" />
    );

};