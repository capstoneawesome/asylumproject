import React, {useState} from "react";
import * as authenticationActions from "../../actions/authentication";
import {connect} from "react-redux";
import "./css/stargate.scss"
import {Redirect} from "react-router";

// Redux settings below
// Before you edit this section, Please refer to connect() method of react-redux
const mapStateToProps = (state) => {
    return {
        isSignIn: state.authenticationReducer.isSignIn,
    };
};

const mapDispatchProps = (dispatch) => {
    return {
        handleAuthentication: (isSignIn) => {dispatch(authenticationActions.authenticationAction(isSignIn))},
    };
};

Stargate = connect(mapStateToProps, mapDispatchProps)(Stargate);

/**
 * Component that display a sign-in button on the map.
 * @param props props from parent component
 * @returns {*}
 */

export default function Stargate(props) {

    const [route, setRoute] = useState(null);

    const handleConnection = () => {
        if(props.isSignIn) {
            setRoute("/account");
        } else {
            setRoute("/signin");
        }
    };

    const handleMountingNode = () => {
        if(props.isSignIn) {
            return (
                <div>
                    <button id="avatar-icon" onClick={handleConnection}>Dashboard</button>
                </div>
            );
        } else {
            return (
                <div>
                    <button id="avatar-icon" onClick={handleConnection}>Signin</button>
                </div>
            );
        }
    };

    return (
        route === null
            ? handleMountingNode()
            : <Redirect to={route} />
    );

};
