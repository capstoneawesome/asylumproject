import React, {Component} from "react";
import axios from "axios";
import {Redirect} from "react-router-dom";

// Visual effects
import * as THREE from 'three';
import CLOUDS from "vanta/dist/vanta.clouds.min";

// Redux settings
import {connect} from "react-redux";
// Every files under actions directory are working as libraries. (They are not components)
import * as authenticationActions from "../../actions/authentication"

/**
 * Component that display log-in form in order to take username and password.
 * @param props props from parent component
 * @returns {*}
 * @constructor
 */

class SignIn extends Component {

    constructor(props) {
        super(props);
        this.state = {
            usernameOrEmail: "",
            password: "",
            redirect: null,
        };

        this.background = React.createRef();
    };

    handleSubmit = (e) => {
        e.preventDefault();
        if(this.validateForm() === true)
            this.sendRequest();
    };

    handleUsernameFormOnChange = (e) => {
        // e.preventDefault();
        this.setState({
            usernameOrEmail: e.target.value,
        });
    };

    handlePasswordFormOnChange = (e) => {
        // e.preventDefault();
        this.setState({
            password: e.target.value,
        });
    };

    validateForm = () => {
        return true;
    };

    sendRequest = () => {

        const url = "http://localhost:8080/api/auth/signin";

        const config = {
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
            }
        };

        const data = {
            "usernameOrEmail": this.state.usernameOrEmail,
            "password": this.state.password
        };

        axios.post(
            url,
            data,
            config
        ).then((result) => {

            if(result !== null) {

                if (result.status === 200) {
                    //SignIn Succeed
                    //For security issue, never save the token to variables in react component
                    Object.keys(result.data).map((key) => {
                        if (key === "user") {
                            let user = result.data[key];
                            // this.props.handleUser(user);
                            Object.keys(user).map(key => {
                                switch (key) {
                                    case "id":
                                        this.props.handleUserId(user[key]);
                                        break;
                                    case "defaultLanguage":
                                        this.props.handleDefaultLanguage(user[key]);
                                        break;
                                    case "firstName":
                                        this.props.handleFirstName(user[key]);
                                        break;
                                    case "lastName":
                                        this.props.handleLastName(user[key]);
                                        break;
                                    case "permissions":
                                        this.props.handleAuthorities(user[key]);
                                        break;
                                    case "username":
                                        this.props.handleUsername(user[key]);
                                        break;
                                    case "email":
                                        this.props.handleEmail(user[key]);
                                        break;
                                    case "photoPath":
                                        this.props.handlePhotoPath(user[key]);
                                        break;
                                    // case "phoneNumber":
                                    //     this.props.handlePhoneNumber(user[key]);
                                    //     break;
                                    // case "delete":
                                    //     this.props.handleUserDelete(user[key]);
                                    //     break;
                                    // case "enabled":
                                    //     this.props.handleEnabled(user[key]);
                                    //     break;
                                    // case "accountNonExpired":
                                    //     this.props.handleAccountNonExpired(user[key]);
                                    //     break;
                                    // case "credentialsNonExpired":
                                    //     this.props.handleCredentialsNonExpired(user[key]);
                                    //     break;
                                    // case "accountNonLocked":
                                    //     this.props.handleAccountNonLocked(user[key]);
                                    //     break;
                                    // case "creatorUserName":
                                    //     this.props.handleCreatorUserName(user[key]);
                                    //     break;
                                    // case "createDateTime":
                                    //     this.props.handleCreateDateTime(user[key]);
                                    //     break;
                                    // case "updateDateTime":
                                    //     this.props.handleUpdateDateTime(user[key]);
                                    //     break;
                                    // case "resetToken":
                                    //     this.props.handleResetToken(user[key]);
                                    //     break;
                                    default:
                                        break;
                                }
                            });
                        } else {
                            // token
                            localStorage.setItem(key, result.data[key]);
                        }
                    });
                    // SignIn successful
                    // set isSignIn to true
                    this.props.handleAuthentication(true);
                    this.handleRedirection();
                } else {
                    //SignIn Failed
                    // set isSignIn to false
                    this.props.handleAuthentication(false);
                    // console.log("another http code");
                }
            } else {
                // just in case error is not catched
                this.props.handleAuthentication(false);
                // console.log("result null catched");
            }

        }).catch(error => {
            // result is null and an error is catched
            this.props.handleAuthentication(false);
            console.log(this.props.isSignIn);
        });
    };

    handleRedirection = () => {
        if (this.props.user_authorities.length === 1) {
            // user
            this.setState({
                redirect: "/map",
            });
        } else {
            // if (this.props.user_authorities.includes("ROLE_SYSTEM_ADMIN")) {
            //     // system administrator
            //     this.setState({
            //         redirect: "/admin",
            //     });
            // } else {
            //     if (this.props.user_authorities.includes("ROLE_CONTENT_CURATOR")) {
            //         // content manager
            //         this.setState({
            //             redirect: "/contentmanager",
            //         });
            //     } else {
            //         // teacher (Temporary redirection)
            //         this.setState({
            //             redirect: "/map",
            //         });
            //     }
            // }
            this.setState({
                redirect: "/account",
            })
        }
    };

    componentDidMount() {
        this.background = CLOUDS({
            THREE: THREE,
            el: this.background.current,
        })
    };

    componentWillUnmount() {
        // if(this.background) this.background.destroy()
        if (this.background) {
            this.background.destroy()
        }
    }

    render() {
        return this.props.isSignIn && this.state.redirect !== null
            ?
                (
                    <Redirect to={this.state.redirect} />
                )
            :
                (
                    <>
                        <section className="hero is-fullheight asp-default-color-background">
                            <div ref={this.background} className="hero-body">
                                <div className="container">
                                    <div className="columns is-centered">
                                        <div className="column is-5-tablet is-4-desktop is-3-widescreen">
                                            <form onSubmit={this.handleSubmit} className="box">
                                                <div className="field">
                                                    <label htmlFor="" className="label">Username (or Email)</label>
                                                    <div className="control has-icons-left">
                                                        <input
                                                            type="text"
                                                            placeholder="Jedi@starwars.univ"
                                                            className="input"
                                                            required
                                                            value={this.state.usernameOrEmail}
                                                            onChange={this.handleUsernameFormOnChange}
                                                        />
                                                        <span className="icon is-small is-left">
                                                          <i className="fa fa-envelope"/>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="field">
                                                    <label htmlFor="" className="label">Password</label>
                                                    <div className="control has-icons-left">
                                                        <input
                                                            type="password"
                                                            placeholder="*******"
                                                            className="input"
                                                            required
                                                            value={this.state.password}
                                                            onChange={this.handlePasswordFormOnChange}
                                                        />
                                                        <span className="icon is-small is-left">
                                                          <i className="fa fa-lock"/>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="field">
                                                    <label htmlFor="" className="checkbox">
                                                        <input type="checkbox" />
                                                            Remember me
                                                    </label>
                                                </div>
                                                <div className="field">
                                                    <button className="button is-success" type="submit">
                                                        Login
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </>
                );
    };

}

// Redux settings below
// Before you edit this section, Please refer to connect() method of react-redux
const mapStateToProps = (state) => {
    return {
        // user: state.authenticationReducer.user,
        isSignIn: state.authenticationReducer.isSignIn,
        user_id: state.authenticationReducer.user_id,
        user_defaultLanguage: state.authenticationReducer.user_defaultLanguage,
        user_firstName: state.authenticationReducer.user_firstName,
        user_lastName: state.authenticationReducer.user_lastName,
        user_authorities: state.authenticationReducer.user_authorities,
        user_username: state.authenticationReducer.user_username,
        user_email: state.authenticationReducer.user_email,
        user_photoPath: state.authenticationReducer.user_photoPath,
        // user_phoneNumber: state.authenticationReducer.user_phoneNumber,
        // user_userDelete: state.authenticationReducer.user_userDelete,
        // user_enabled: state.authenticationReducer.user_enabled,
        // user_accountNonExpired: state.authenticationReducer.user_accountNonExpired,
        // user_credentialNonExpired: state.authenticationReducer.user_credentialNonExpired,
        // user_accountNonLocked: state.authenticationReducer.user_accountNonLocked,
        // user_creatorUserName: state.authenticationReducer.user_creatorUserName,
        // user_createDateTime: state.authenticationReducer.user_createDateTime,
        // user_updateDateTime: state.authenticationReducer.user_updateDateTime,
        // user_resetToken: state.authenticationReducer.user_resetToken,
    };
};

const mapDispatchProps = (dispatch) => {
    // return bindActionCreator(actions, dispatch);
    return {
        // handleUser: (user) => {dispatch(authenticationActions.userAction(user))},
        handleAuthentication: (isSignIn) => {dispatch(authenticationActions.authenticationAction(isSignIn))},
        handleUserId: (user_id) => {dispatch(authenticationActions.userIdAction(user_id))},
        handleDefaultLanguage: (user_defaultLanguage) => {dispatch(authenticationActions.defaultLanguageAction(user_defaultLanguage))},
        handleFirstName: (user_firstName) => {dispatch(authenticationActions.firstNameAction(user_firstName))},
        handleLastName: (user_lastName) => {dispatch(authenticationActions.lastNameAction(user_lastName))},
        handlePhotoPath: (user_photoPath) => {dispatch(authenticationActions.photoPathAction(user_photoPath))},
        handleAuthorities: (user_authorities) => {dispatch(authenticationActions.authoritiesAction(user_authorities))},
        handleUsername: (user_username) => {dispatch(authenticationActions.usernameAction(user_username))},
        handleEmail: (user_email) => {dispatch(authenticationActions.emailAction(user_email))},
        // handlePhoneNumber: (user_phoneNumber) => {dispatch(authenticationActions.phoneNumberAction(user_phoneNumber))},
        // handleUserDelete: (user_userDelete) => {dispatch(authenticationActions.userDeleteAction(user_userDelete))},
        // handleEnabled: (user_enabled) => {dispatch(authenticationActions.enabledAction(user_enabled))},
        // handleAccountNonExpired: (user_accountNonExpired) => {dispatch(authenticationActions.accountNonExpiredAction(user_accountNonExpired))},
        // handleCredentialsNonExpired: (user_credentialNonExpired) => {dispatch(authenticationActions.credentialsNonExpiredAction(user_credentialNonExpired))},
        // handleAccountNonLocked: (user_accountNonLocked) => {dispatch(authenticationActions.accountNonLockedAction(user_accountNonLocked))},
        // handleCreatorUserName: (user_creatorUserName) => {dispatch(authenticationActions.creatorUserNameAction(user_creatorUserName))},
        // handleCreateDateTime: (user_createDateTime) => {dispatch(authenticationActions.createDateTimeAction(user_createDateTime))},
        // handleUpdateDateTime: (user_updateDateTime) => {dispatch(authenticationActions.updateDateTimeAction(user_updateDateTime))},
        // handleResetToken: (user_resetToken) => {dispatch(authenticationActions.resetTokenAction(user_resetToken))},
    };
};

SignIn = connect(mapStateToProps, mapDispatchProps)(SignIn);

export default SignIn;
