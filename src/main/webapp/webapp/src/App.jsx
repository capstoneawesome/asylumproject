import React, {useState} from 'react';
import {BrowserRouter, Redirect, Route, Switch, useHistory} from "react-router-dom";

import Intro from "./components/Intro/Intro";
import MapWindow from "./components/Map/MapWindow";
import ContentManager from "./components/ContentManager/ContentManager";
import {ContentManagerIndex} from "./components/ContentManager/ContentManagerIndex";
import AboutUs from "./components/AboutUs/AboutUs";
import Account from "./components/Account/index";
import SignIn from "./components/SignIn/SignIn";
import Admin from "./components/Admin/Index";
import SignOut from "./components/SignOut/SignOut";
import {animated, useTransition} from "react-spring";
import useRouter from "./customHooks/useRouter";
import {ManageStories} from "./components/ContentManager/ManageStories";
import * as authenticationActions from "./actions/authentication";
import {connect} from "react-redux";
import {ToastProvider} from 'react-toast-notifications';
import ProtectedRoute from "./components/ProtectedRoute/ProtectedRoute";
import MainMenu from "./components/MainMenu/MainMenu";
import NewMenu from "./components/MainMenu/NewMenu";
// import UserDisplay from "./components/MainMenu/UserDisplay";
import axios from "axios";

//dev
import Dashboard from "./components/Dashboard";
import ErrorHandler from "./ErrorHandler";
import NotFound from "./components/NotFound/NotFound";
import {About} from "./components/Intro/About";


/**
 * Top-level Component in the React application. Handles routing and transitions to the major pages of the website.
 * @param props props from React.js
 * @returns the application section based on the URL.
 * @constructor
 */
function App(props) {

    // Please refer to 'animated.dev' component below
    const globalProps = props;

    const locationData = useRouter();

    const pageTransition = useTransition(locationData, locationData => locationData.key, {
        from: {position: 'absolute', width: '100%', transform: 'translate3d(100%, 0, 0)'},
        enter: {transform: 'translate3d(0, 0, 0)', position: 'absolute'},
        leave: {transform: 'translate3d(100%, 0, 0)', position: 'absolute'}
    });

    let transElements = [];

    const [aboutModalOpen, setAboutModalOpen] = useState(false);
    let toggleAboutModal = () => {
        setAboutModalOpen(!aboutModalOpen);
    };

    transElements = pageTransition.map(({item, props, key}) => (

        <animated.div key={key} style={props} className="transition-div">




            {/*Please be noticed that there is a 'globalProps' standing for actual 'props' in an inheritance hierarchy */}
            {/*This structure was caused by 'props' passed into this function from 'animated' component*/}
            {/*Please let me know if you have better solution for this issue - Jongil*/}




            <Switch currentLocation={item}>


                {/*The first page*/}
                <Redirect exact from="/" to={window.location.search === '' ? "/map" : "/map" + window.location.search}/>

                <Route exact path="/signin" component={SignIn}/>
                <Route exact path="/signout" component={SignOut}/>
                <Route exact path="/map" render={(props) => <MapWindow {...props} filterString={window.location.search}/>}/>
                    {/*Protected Routing below*/}
                {
                    // account
                    globalProps.user_authorities !== null
                        // ? <ProtectedRoute isAllowed={globalProps.user_authorities.includes("ROLE_SYSTEM_ADMIN")} alternative="/map" exact path="/admin" component={Admin} />
                        ? <ProtectedRoute isAllowed={globalProps.user_authorities.includes("ROLE_SITE_USER")} alternative="/map" path="/account" component={Account} />
                        : <Redirect from="/account" to="/map" />
                }
                {
                    // admin
                    globalProps.user_authorities !== null
                        // ? <ProtectedRoute isAllowed={globalProps.user_authorities.includes("ROLE_SYSTEM_ADMIN")} alternative="/map" exact path="/admin" component={Admin} />
                        ? <ProtectedRoute isAllowed={globalProps.user_authorities.includes("ROLE_SYSTEM_ADMIN")} alternative="/account" path="/admin" component={Admin} />
                        : <Redirect from="/admin" to="/map" />
                }
                {
                    // contentmanager
                    globalProps.user_authorities !== null
                        ? <ProtectedRoute isAllowed={globalProps.user_authorities.includes("ROLE_CONTENT_CURATOR")} alternative="/account" path="/contentmanager" component={ContentManagerIndex} />
                        : <Redirect from="/contentmanager" to="/map" />
                }

                <Route component={NotFound} />

            </Switch>

        </animated.div>

    ));

    return (
        <ErrorHandler axios={axios}>
            <ToastProvider>
                <BrowserRouter>
                    <NewMenu setAboutModalOpen={setAboutModalOpen} aboutModalOpen={aboutModalOpen}>
                    </NewMenu>
                    <About aboutModalState={aboutModalOpen} closeAboutModal={toggleAboutModal}/>
                    {transElements[0]}
                    {transElements[1]}
                    {transElements[2]}
                    {transElements[3]}
                    {transElements[4]}
                    {transElements[5]}
                </BrowserRouter>
        </ToastProvider>
        </ErrorHandler>
    );

}

// Redux settings below.
// This is why you can use 'store' in a grand child component
// without passing 'props' through every components in an inheritance hierarchy.

// This function related to binding states declared in between Redux store and your component
const mapStateToProps = (state) => {
    return {
        isSignIn: state.authenticationReducer.isSignIn,
        user_authorities: state.authenticationReducer.user_authorities,
    };
};

// if you only refer to states and you don't change states in Redux store, you probably don't need to have below function
// This function is related to modifying states in Redux store
const mapDispatchProps = (dispatch) => {
    return {
        handleAuthentication: (isSignIn) => {dispatch(authenticationActions.authenticationAction(isSignIn))},
    };
};

App = connect(mapStateToProps, mapDispatchProps)(App);

export default App;
