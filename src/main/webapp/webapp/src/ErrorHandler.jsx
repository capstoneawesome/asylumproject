import React, {Component} from 'react';
import {Redirect} from "react-router-dom";
import {tryTokenRefresh} from "./libraries/tryTokenRefresh";

/**
 * Handles JWT refresh and redirect to sign out if authorization error occurs.
 */
export default class ErrorHandler extends Component {


    constructor(props) {
        super(props);
        this.state = {
            error: null,
        };
    }

    componentDidMount() {
        // Set axios interceptors
        this.requestInterceptor = this.props.axios.interceptors.request.use(req => {
            console.log(req.headers);
            return req;
        }, error => {
            this.setState({error});

        });

        this.responseInterceptor = this.props.axios.interceptors.response.use(
            res => {
                console.log(res.headers);
                if (localStorage.getItem("accessToken") !== null) {
                    if (res.headers.url !== "/api/auth/refresh") {
                        tryTokenRefresh();
                    }
                }
                return res;
            },
            error => {
                console.log(error.config);
                if ((error.response.status === 401 || error.response.status === 500) && error.config.url !== "http://localhost:8080/api/auth/signin") {
                    this.setState({error});
                }else{
                    return <Redirect to="/signin"/>
                }
            }
        );
    }

    componentWillUnmount() {
        // Remove handlers, so Garbage Collector will get rid of if WrappedComponent will be removed
        this.props.axios.interceptors.request.eject(this.requestInterceptor);
        this.props.axios.interceptors.response.eject(this.responseInterceptor);
    }

    render() {
        if (this.state.error !== null) {
            return <Redirect to="/signin"/>
        }
        return this.props.children;
    };
}

