import axios from "axios";

/**
 * Sends a request to the server to create a new Content Tag.
 * @param contentID the contentID of the containing Content object
 * @param tagDesc A custom description for the new Tag
 * @returns {Promise<AxiosResponse<T>>}
 */
export function createTag(contentID, tagDesc) {

    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    let tag = {
       tag: tagDesc
    };


    return axios.post("http://localhost:8080/api/content/" + contentID + "/tags", tag, config);
}
