import axios from "axios";

/**
 * Sends a request to the server to get every available Tag.
 * @returns {Promise<AxiosResponse<T>>}
 */
export function getAllTags(){

    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    return axios.get("http://localhost:8080/api/content/tags", config);
}
