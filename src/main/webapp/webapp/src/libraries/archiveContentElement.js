import axios from "axios";


/**
 * Archives or unarchives a single ContentElement on the server.
 * @param element the element to be archived
 * @param contentID the containing Content contentID
 * @param mapPointID the containing MapPoint mapPointID
 * @param id the id of the ContentElement to be archived
 * @param action either 'archive' or 'unarchive'
 * @returns {Promise<AxiosResponse<T>>}
 */
export function archiveContentElement(element, contentID, mapPointID, id, action) {

    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    return axios.put("http://localhost:8080/api/content/" + contentID + "/mappoints/" + mapPointID + "/elements/" + id + "/" + action , element, config);
}
