import axios from "axios";

/**
 * Sends a request to the server to save an Image object.
 * @param contentElement the Image object to save
 * @param contentID the contentID of the Content containing this Image
 * @param mapPointID the mapPointID of the MapPoint containing this Image
 * @returns {Promise<AxiosResponse<T>>}
 */
export function saveImageContentElement(contentElement, contentID, mapPointID) {

    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };


    return axios.put("http://localhost:8080/api/content/" + contentID + "/mappoints/" + mapPointID + "/elements/images/" + contentElement.id + "/edit", contentElement, config);
}
