import axios from 'axios';

/**
 * Sends a request to the server to get a single MapPoint by its ID.
 * @param id the id of the MapPoint to retrieve
 * @returns {Promise<AxiosResponse<T>>}
 */
export function getMapPointByID(id) {

    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    return axios.get("http://localhost:8080/api/content/mappoints/" + id, config);
}