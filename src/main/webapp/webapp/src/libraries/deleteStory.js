import axios from 'axios';

/**
 * Sends a request to the server to soft-delete a single Story object
 * @param id the id of the Story to be deleted
 * @returns {Promise<AxiosResponse<T>>}
 */
export function deleteStory(id) {

    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    return axios.delete("http://localhost:8080/api/content/" + id, config);
}
