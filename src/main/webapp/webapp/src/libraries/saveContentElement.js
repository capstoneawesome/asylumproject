import axios from "axios";

/**
 * Saves a single ContentElement to the server.
 * @param contentElement the JSON object of the ContentElement to save
 * @param contentID the containing contentID
 * @param mapPointID the containing mapPointID
 * @returns {Promise<AxiosResponse<T>>}
 */
export function saveContentElement(contentElement, contentID, mapPointID) {

    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };


    return axios.put("http://localhost:8080/api/content/" + contentID + "/mappoints/" + mapPointID + "/elements/" + contentElement.id + "/edit", contentElement, config);
}
