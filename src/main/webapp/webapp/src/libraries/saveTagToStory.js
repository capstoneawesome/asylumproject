import axios from "axios";

/**
 * Sends a request to the server to save a Tag object to the current Story being edited.
 * @param contentID the contentID of the Story to save to
 * @param tag the Tag object to save
 * @returns {Promise<AxiosResponse<T>>}
 */
export function saveTagToStory(contentID, tag) {
    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    return axios.put("http://localhost:8080/api/content/" + contentID + "/tags", tag, config);
}
