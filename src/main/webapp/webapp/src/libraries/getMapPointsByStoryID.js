import axios from 'axios';

/**
 * Sends a request to the server to get every MapPoint for a Story.
 * @param id the id of the Story to retrieve for
 * @returns {Promise<AxiosResponse<T>>}
 */
export function getMapPointsByStoryID(id) {

    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    return axios.get("http://localhost:8080/api/content/" + id + "/mappoints", config);
}
