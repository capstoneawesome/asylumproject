import axios from "axios";

/**
 * Sends a request to the server to save a MapPoint object.
 * @param mapPoint the MapPoint object to save
 * @returns {Promise<AxiosResponse<T>>}
 */
export function saveMapPoint(mapPoint) {
    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    return axios.put("http://localhost:8080/api/content/mappoints/", mapPoint, config);
}
