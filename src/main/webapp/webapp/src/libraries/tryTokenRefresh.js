import axios from 'axios';

/**
 * Sends a request to the server to refresh the JWT of the current authenticated user.
 */
export function tryTokenRefresh() {
    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    axios.get("http://localhost:8080/api/auth/refresh",
        config).then((response) =>{
            if (response.status === 200){
                console.log("refreshed token");
                //set new refreshed token
                localStorage.setItem("accessToken", response.data["accessToken"]);
            }
            //do not display status code from server on server error
    }).catch((error) => {
        //do not display 304 message when token cannot be refreshed yet
    });
}