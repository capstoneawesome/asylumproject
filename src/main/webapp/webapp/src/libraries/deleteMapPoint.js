import React from 'react';
import axios from "axios";

/**
 * Sends a request to the server to soft-delete a single MapPoint.
 * @param contentID the contentID of the containing Story.
 * @param mapPointID the mapPointID of the MapPoint to be deleted.
 * @returns {Promise<AxiosResponse<T>>}
 */
export function deleteMapPoint(contentID, mapPointID){

    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    return axios.delete(
        "http://localhost:8080/api/content/" + contentID + "/mappoints/" + mapPointID,
        config
    );


}