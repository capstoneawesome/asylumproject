import React from 'react';
import axios from "axios";

/**
 * Sends a server request to create a new MapPoint.
 * @param storyID the storyID of the containing Story.
 * @param zoomLevel the current zoom level of the MapPoint to be created.
 * @param coordinates the latitude and longitude of the MapPoint
 * @returns {Promise<AxiosResponse<T>>}
 */
export function createMapPointRequest(storyID, zoomLevel, coordinates) {

    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    let newMapPoint = {
        "contentElement": null,
        "coordinates": coordinates,
        "zoomLevel": zoomLevel,
    };

    return axios.post(
        "http://localhost:8080/api/content/stories/" + storyID + "/mapPoints",
        newMapPoint,
        config
    );
}
