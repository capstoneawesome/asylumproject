import React from 'react';
import axios from "axios";

/**
 * Sends a request to the server to soft-delete a single ContentElement
 * @param elementID the id of the ContentElement to be deleted
 * @param mapPointID the id of the MapPoint containing the ContentElement
 * @param contentID the id of the Story containing this ContentElement
 * @returns {Promise<AxiosResponse<T>>}
 */
export function deleteContentElement(elementID, mapPointID, contentID){

    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    return axios.delete(
        "http://localhost:8080/api/content/" + contentID + "/mappoints/" + mapPointID + "/elements/" + elementID,
        config
    );

}