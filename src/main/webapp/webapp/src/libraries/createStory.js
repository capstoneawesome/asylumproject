import React from "react";
import axios from "axios";

/**
 * Sends a request to the server to create a new Story with the minimum number of attributes.
 * @param creator the user id of the user that is creating this Story
 * @returns {Promise<AxiosResponse<T>>}
 */
export function createStory(creator) {

    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    let newStory = {
        "language": "en",
        "mapPoints": null,
        "asylumSeekerName": "",
        "contentRating": "no rating defined",
        "countryOfOrigin": "US",
        "languages": null,
        "contentElements": null,
        "tags": null,
        "title": "",
        "creator": creator,
        "state": "DRAFT",
        "description": ""
    };

    return axios.post(
        "http://localhost:8080/api/content/stories",
        newStory,
        config
    );
}
