import axios from "axios";

/**
 * Sends as request to the server to save a single edited Story object.
 * @param story the Story object to save
 * @returns {Promise<AxiosResponse<T>>}
 */
export function saveStory(story) {
    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    return axios.put("http://localhost:8080/api/content/stories", story, config);
}
