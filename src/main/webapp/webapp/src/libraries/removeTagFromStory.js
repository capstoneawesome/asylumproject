import axios from "axios";

/**
 * Sends a request to the server to remove a single Tag object from a Story.
 * @param contentID the contentID of the Story that the Tag will be removed from
 * @param tag the Tag to be removed from the containing Story
 * @returns {Promise<AxiosResponse<T>>}
 */
export function removeTagFromStory(contentID, tag) {
    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    return axios.put("http://localhost:8080/api/content/" + contentID + "/tags/remove", tag, config);
}
