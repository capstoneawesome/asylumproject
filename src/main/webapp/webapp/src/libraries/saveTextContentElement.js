import axios from "axios";

/**
 * Sends a request to the server to save a Text ContentElement.
 * @param contentElement the Text element to save
 * @param contentID the contentID of the containing Content object
 * @param mapPointID the mapPointID of the containing MapPoint
 * @returns {Promise<AxiosResponse<T>>}
 */
export function saveTextContentElement(contentElement, contentID, mapPointID) {

    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };


    return axios.put("http://localhost:8080/api/content/" + contentID + "/mappoints/" + mapPointID + "/elements/text/" + contentElement.id + "/edit", contentElement, config);
}
