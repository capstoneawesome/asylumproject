import axios from "axios";

/**
 * Sends a request to the server to get a list of every Story that has a state of ARCHIVED.
 * @returns {Promise<AxiosResponse<T>>}
 */
export function getArchivedStoryList(){

    let config = {
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    };

    return axios.get("http://localhost:8080/api/content/stories/archived", config);
}
