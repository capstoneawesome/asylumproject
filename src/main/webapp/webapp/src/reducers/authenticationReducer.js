import {
    USER_ACCOUNTNONEXPIRED, USER_ACCOUNTNONLOCKED,
    AUTHENTICATION, DEAUTHENTICATION, USER_AUTHORITIES, USER_CREATEDATETIME,
    USER_UPDATEDATETIME, USER_CREATORUSERNAME, USER_CREDENTIALSNONEXPIRED,
    USER_DEFAULTLANGUAGE, USER_EMAIL, USER_ENABLED, USER_RESETTOKEN,
    USER_FIRSTNAME,
    USER_LASTNAME,
    USER_PHONENUMBER, USER_PHOTOPATH,
    USER_ID, USER_USERDELETE, USER_USERNAME
} from "../actions/authentication";
// The next line has been commented out because of following issue
// import {AUTHENTICATION, USER} from "../actions/authentication";

// To be able to decrease workload but increase efficiency,
// get rid of wrapper object.
// which means that we need to store plain data into Redux store.
//
// Good practice
// # every single line stands for a plain data
// user_id
// user_name
// user_email
//
// Bad practice
// # 'user' is considered as a wrapper object
// user {
//  id
//  name
//  email
// }
//
// Suggestion from Jongil
// Let's use prefix instead of the wrapper object!!
// As you can see in the above example, 'user_' will be a prefix for user class(object).
// Furthermore, using plain object in Redux store is one of the recommendation from Redux developers.
// Thank you for cooperation!!
const initialState = {
    // isSignIn value is really not belonging to user object
    isSignIn: false,

    // user
    user_id: null,
    user_defaultLanguage: null,
    user_firstName: null,
    user_lastName: null,
    user_phoneNumber: null,
    user_photoPath: null,
    user_userDelete: false,
    user_enabled: true,
    user_authorities: null,
    user_username: null,
    user_email: null,
    user_accountNonExpired: false,
    user_credentialsNonExpired: false,
    user_accountNonLocked: false,
    user_creatorUserName: null,
    user_createDateTime: null,
    user_updateDateTime: null,
    user_resetToken: null
    // user: null
};

const authenticationReducer = (state = initialState, action) => {
    switch(action.type) {
        // case USER:
        //     return Object.assign(
        //         {},
        //         state,
        //         {user: action.user}
        //     );
        case AUTHENTICATION:
            return Object.assign(
                {},
                state,
                {isSignIn: action.isSignIn}
            );
        case DEAUTHENTICATION:
            return Object.assign(
                {},
                state,
                initialState
            );
        case USER_ID:
            return Object.assign(
                {},
                state,
                {user_id: action.user_id}
            );
        case USER_DEFAULTLANGUAGE:
            return Object.assign(
                {},
                state,
                {user_defaultLanguage: action.user_defaultLanguage}
            );
        case USER_FIRSTNAME:
            return Object.assign(
                {},
                state,
                {user_firstName: action.user_firstName}
            );
        case USER_LASTNAME:
            return Object.assign(
                {},
                state,
                {user_lastName: action.user_lastName}
            );
        case USER_PHONENUMBER:
            return Object.assign(
                {},
                state,
                {user_phoneNumber: action.user_phoneNumber}
            );
        case USER_PHOTOPATH:
            return Object.assign(
                {},
                state,
                {user_photoPath: action.user_photoPath}
            );
        case USER_USERDELETE:
            return Object.assign(
                {},
                state,
                {user_userDelete: action.user_userDelete}
            );
        case USER_ENABLED:
            return Object.assign(
                {},
                state,
                {user_enabled: action.user_enabled}
            );
        case USER_AUTHORITIES:
            // const permissions = [];
            // Object.values(action.user_authorities).map(value => {
            //     Object.values(value).map(value => {
            //         permissions.push(value);
            //     })
            // });
            const permissions = [];
            Object.values(action.user_authorities).forEach(o => {
                Object.values(o).forEach(value => {
                    permissions.push(value);
                })
            });
            return Object.assign(
                [],
                state,
                {user_authorities: permissions}
            );
        case USER_USERNAME:
            return Object.assign(
                {},
                state,
                {user_username: action.user_username}
            );
        case USER_EMAIL:
            return Object.assign(
                {},
                state,
                {user_email: action.user_email}
            );
        case USER_ACCOUNTNONEXPIRED:
            return Object.assign(
                {},
                state,
                {user_accountNonExpired: action.user_accountNonExpired}
            );
        case USER_CREDENTIALSNONEXPIRED:
            return Object.assign(
                {},
                state,
                {user_credentialsNonExpired: action.user_credentialsNonExpired}
            );
        case USER_ACCOUNTNONLOCKED:
            return Object.assign(
                {},
                state,
                {user_accountNonLocked: action.user_accountNonLocked}
            );
        case USER_CREATORUSERNAME:
            return Object.assign(
                {},
                state,
                {user_creatorUserName: action.user_creatorUserName}
            );
        case USER_CREATEDATETIME:
            return Object.assign(
                {},
                state,
                {user_createDateTime: action.user_createDateTime}
            );
        case USER_UPDATEDATETIME:
            return Object.assign(
                {},
                state,
                {user_updateDateTime: action.user_updateDateTime}
            );
        case USER_RESETTOKEN:
            return Object.assign(
                {},
                state,
                {user_resetToken: action.user_resetToken}
            );
        default:
            return state;
    }
};

export default authenticationReducer;
