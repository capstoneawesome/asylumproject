import { __RouterContext} from 'react-router-dom';
import React, {useContext} from 'react';

/**
 * Custom hook which grabs and returns router context using the useContext React hook and the provider set up by the
 * browser router. Should be deprecated in favor of the new useHistory and useLocation hooks added to react-Router.
 * @returns React Router context including history stack.
 */

export default function useRouter() {
    return useContext(__RouterContext);
}