export const AUTHENTICATION = "AUTHENTICATION";
export const DEAUTHENTICATION = "DEAUTHENTICATION";
export const USER_ID = "USER_ID";
export const USER_DEFAULTLANGUAGE = "USER_DEFAULTLANGUGE";
export const USER_FIRSTNAME = "USER_FRISTNAME";
export const USER_LASTNAME = "USER_LASTNAME";
export const USER_PHONENUMBER = "USER_PHONENUMBER";
export const USER_PHOTOPATH = "USER_PHOTOPATH";
export const USER_USERDELETE = "USER_USERDELETE";
export const USER_ENABLED = "USER_ENABLED";
export const USER_AUTHORITIES = "USER_AUTHORITIES";
export const USER_USERNAME = "USER_USERNAME";
export const USER_EMAIL = "USER_EMAIL";
export const USER_ACCOUNTNONEXPIRED = "USER_ACCOUNTNONEXPIRED";
export const USER_CREDENTIALSNONEXPIRED = "USER_CREDENTIALSNONEXPIRED";
export const USER_ACCOUNTNONLOCKED = "USER_ACCOUNTNONLOCKED";
export const USER_CREATORUSERNAME = "USER_CREATORUSERNAME";
export const USER_CREATEDATETIME = "USER_CREATEDATETIME";
export const USER_UPDATEDATETIME = "USER_UPDATEDATETIME";
export const USER_RESETTOKEN = "USER_RESETTOKEN";
// export const USER = "USER";

// export function userAction(user) {
//     return {
//         type: USER,
//         user
//     };
// }
export function authenticationAction(isSignIn) {
    return {
        type: AUTHENTICATION,
        isSignIn
    };
}
export function deauthenticationAction() {
    return {
        type: DEAUTHENTICATION
    };
}
export function userIdAction(user_id) {
    return {
        type: USER_ID,
        user_id
    };
}
export function defaultLanguageAction(user_defaultLanguage) {
    return {
        type: USER_DEFAULTLANGUAGE,
        user_defaultLanguage
    };
}
export function firstNameAction(user_firstName) {
    return {
        type: USER_FIRSTNAME,
        user_firstName
    };
}
export function lastNameAction(user_lastName) {
    return {
        type: USER_LASTNAME,
        user_lastName
    };
}
export function phoneNumberAction(user_phoneNumber) {
    return {
        type: USER_PHONENUMBER,
        user_phoneNumber
    }
}
export function photoPathAction(user_photoPath) {
    return {
        type: USER_PHOTOPATH,
        user_photoPath
    };
}
export function userDeleteAction(user_userDelete) {
    return {
        type: USER_USERDELETE,
        user_userDelete
    };
}
export function enabledAction(user_enabled) {
    return {
        type: USER_ENABLED,
        user_enabled
    };
}
export function authoritiesAction(user_authorities) {
    return {
        type: USER_AUTHORITIES,
        user_authorities
    };
}
export function usernameAction(user_username) {
    return {
        type: USER_USERNAME,
        user_username
    };
}
export function emailAction(user_email) {
    return {
        type: USER_EMAIL,
        user_email
    };
}
export function accountNonExpiredAction(user_accountNonExpired) {
    return {
        type: USER_ACCOUNTNONEXPIRED,
        user_accountNonExpired
    };
}
export function credentialsNonExpiredAction(user_credentialsNonExpired) {
    return {
        type: USER_CREDENTIALSNONEXPIRED,
        user_credentialsNonExpired
    };
}
export function accountNonLockedAction(user_accountNonLocked) {
    return {
        type: USER_ACCOUNTNONLOCKED,
        user_accountNonLocked
    };
}
export function creatorUserNameAction(user_creatorUserName) {
    return {
        type: USER_CREATORUSERNAME,
        user_creatorUserName
    };
}
export function createDateTimeAction(user_createDateTime) {
    return {
        type: USER_CREATEDATETIME,
        user_createDateTime
    };
}
export function updateDateTimeAction(user_updateDateTime) {
    return {
        type: USER_UPDATEDATETIME,
        user_updateDateTime
    };
}
export function resetTokenAction(user_resetToken) {
    return {
        type: USER_RESETTOKEN,
        user_resetToken
    };
}
