// React
import React from "react";
import ReactDOM from "react-dom";
import MapWindow from "./components/Map/MapWindow.jsx";
// React Routing Import
import {BrowserRouter} from "react-router-dom";

// Redux
import {Provider} from "react-redux";
import {createStore} from "redux";
import {persistStore, persistReducer} from "redux-persist";
import storage from "redux-persist/lib/storage";
import rootReducer from "./reducers/rootReducer";
import {PersistGate} from "redux-persist/integration/react";

// React Service Worker
import * as serviceWorker from "./serviceWorker";

// Project Launcher
import App from "./App";
import "./app.scss";

const persistConfig = {
    key: "root",
    storage,
};
const persistedReducer = persistReducer(persistConfig, rootReducer);


// Redux Store Setting
const store = createStore(
    //  .../webapp/webapp/src/lib/reducer/authenticationReducer.js
    // rootReducer,
    persistedReducer,
    // To be able to use redux devtool extension for browser
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
const persistor = persistStore(store);

// React DOM Setting
ReactDOM.render(

    <BrowserRouter>
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <App><MapWindow /></App>
            </PersistGate>
        </Provider>
    </BrowserRouter>
    ,
    document.getElementById('root')

);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
