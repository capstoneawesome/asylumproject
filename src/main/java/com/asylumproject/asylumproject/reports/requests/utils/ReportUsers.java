package com.asylumproject.asylumproject.reports.requests.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReportUsers {
    private boolean include;
    private ReportUsersFilters filters;

    @JsonProperty("include")
    public boolean getInclude() { return include; }
    @JsonProperty("include")
    public void setInclude(boolean value) { this.include = value; }

    @JsonProperty("filters")
    public ReportUsersFilters getFilters() { return filters; }
    @JsonProperty("filters")
    public void setFilters(ReportUsersFilters value) { this.filters = value; }
}
