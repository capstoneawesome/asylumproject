package com.asylumproject.asylumproject.reports.requests.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Report {
    private boolean include;
    private ReportFaqsFilters filters;

    /**
     * Validate if report should be included.
     * @return true if included, otherwise false.
     */
    @JsonProperty("include")
    public boolean getInclude() { return include; }

    /**
     * Sets if report should be included.
     * @param value true to include, otherwise false.
     */
    @JsonProperty("include")
    public void setInclude(boolean value) { this.include = value; }

    /**
     * Retrieve the report filters.
     * @return the report filters.
     */
    @JsonProperty("filters")
    public ReportFaqsFilters getFilters() { return filters; }

    /**
     * Set report filters.
     * @param value the report filters to set.
     */
    @JsonProperty("filters")
    public void setFilters(ReportFaqsFilters value) { this.filters = value; }
}
