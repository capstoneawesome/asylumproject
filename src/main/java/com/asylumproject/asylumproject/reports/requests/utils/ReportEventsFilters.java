package com.asylumproject.asylumproject.reports.requests.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReportEventsFilters {

    private String[] types;
    private String[] operations;
    private long[] actingUsers;

    /**
     * Retrieve the array of types.
     * @return the array of types.
     */
    @JsonProperty("types")
    public String[] getTypes() { return types; }

    /**
     * Set the types.
     * @param value the types.
     */
    @JsonProperty("types")
    public void setTypes(String[] value) { this.types = value; }

    /**
     * Retrieve the operations.
     * @return the operations.
     */
    @JsonProperty("operations")
    public String[] getOperations() { return operations; }

    /**
     * Set the operations.
     * @param value the operations to set.
     */
    @JsonProperty("operations")
    public void setOperations(String[] value) { this.operations = value; }

    /**
     * Retrieve a the acting users.
     * @return the acting users.
     */
    @JsonProperty("acting_users")
    public long[] getActingUsers() { return actingUsers; }

    /**
     * Set the acting users.
     * @param value the acting user to set.
     */
    @JsonProperty("acting_users")
    public void setActingUsers(long[] value) { this.actingUsers = value; }
}
