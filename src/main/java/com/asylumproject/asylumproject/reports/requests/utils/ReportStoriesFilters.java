package com.asylumproject.asylumproject.reports.requests.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReportStoriesFilters {
    private String[] languages;
    private String[] status;
    private long[] curators;
    private String[] countries;

    @JsonProperty("languages")
    public String[] getLanguages() { return languages; }
    @JsonProperty("languages")
    public void setLanguages(String[] value) { this.languages = value; }

    @JsonProperty("status")
    public String[] getStatus() { return status; }
    @JsonProperty("status")
    public void setStatus(String[] value) { this.status = value; }

    @JsonProperty("curators")
    public long[] getCurators() { return curators; }
    @JsonProperty("curators")
    public void setCurators(long[] value) { this.curators = value; }

    @JsonProperty("countries")
    public String[] getCountries() { return countries; }
    @JsonProperty("countries")
    public void setCountries(String[] value) { this.countries = value; }
}
