package com.asylumproject.asylumproject.reports.requests.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReportStories {
    private boolean include;
    private ReportStoriesFilters filters;

    @JsonProperty("include")
    public boolean getInclude() { return include; }
    @JsonProperty("include")
    public void setInclude(boolean value) { this.include = value; }

    @JsonProperty("filters")
    public ReportStoriesFilters getFilters() { return filters; }
    @JsonProperty("filters")
    public void setFilters(ReportStoriesFilters value) { this.filters = value; }
}
