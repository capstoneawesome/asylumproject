package com.asylumproject.asylumproject.reports.requests;

import java.util.List;

public class ListUsernames {

    private List<String> listUsernames;

    public ListUsernames (List<String> listUsernames) {
        this.listUsernames = listUsernames;
    }

    public List<String> getListUsernames() {
        return listUsernames;
    }
}
