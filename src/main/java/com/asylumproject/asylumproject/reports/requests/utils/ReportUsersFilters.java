package com.asylumproject.asylumproject.reports.requests.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
public class ReportUsersFilters {
    private long[] users;
    private String[] permissions;
    private String[] languages;
    private boolean deleted;

    @JsonProperty("users")
    public long[] getUsers() { return users; }
    @JsonProperty("users")
    public void setUsers(long[] value) { this.users = value; }

    @JsonProperty("permissions")
    public String[] getPermissions() { return permissions; }
    @JsonProperty("permissions")
    public void setPermissions(String[] value) { this.permissions = value; }

    @JsonProperty("languages")
    public String[] getLanguages() { return languages; }
    @JsonProperty("languages")
    public void setLanguages(String[] value) { this.languages = value; }

    @JsonProperty("deleted")
    public boolean getDeleted() { return deleted; }
    @JsonProperty("deleted")
    public void setDeleted(boolean value) { this.deleted = value; }
}
