package com.asylumproject.asylumproject.reports.requests.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReportEvents {

    private boolean include;
    private ReportEventsFilters filters;

    /**
     * Validates if filter is included.
     * @return if filter is included.
     */
    @JsonProperty("include")
    public boolean getInclude() { return include; }

    /**
     * Set if filter is included.
     * @param value filter included.
     */
    @JsonProperty("include")
    public void setInclude(boolean value) { this.include = value; }

    /**
     * Retrieve report filters.
     * @return the report filters.
     */
    @JsonProperty("filters")
    public ReportEventsFilters getFilters() { return filters; }

    /**
     * Set the report filters.
     * @param value the report filters.
     */
    @JsonProperty("filters")
    public void setFilters(ReportEventsFilters value) { this.filters = value; }
}
