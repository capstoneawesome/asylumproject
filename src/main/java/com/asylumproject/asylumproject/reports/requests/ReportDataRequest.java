package com.asylumproject.asylumproject.reports.requests;

import com.asylumproject.asylumproject.reports.requests.utils.Report;
import com.asylumproject.asylumproject.reports.requests.utils.ReportEvents;
import com.asylumproject.asylumproject.reports.requests.utils.ReportStories;
import com.asylumproject.asylumproject.reports.requests.utils.ReportUsers;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ReportDataRequest {
    private Report reportResources;
    private Report reportFaqs;
    private ReportUsers reportUsers;
    private ReportEvents reportEvents;
    private ReportStories reportStories;
    private String timeZone;

    @JsonProperty("report_resources")
    public Report getReportResources() { return reportResources; }
    @JsonProperty("report_resources")
    public void setReportResources(Report value) { this.reportResources = value; }

    @JsonProperty("report_faqs")
    public Report getReportFaqs() { return reportFaqs; }
    @JsonProperty("report_faqs")
    public void setReportFaqs(Report value) { this.reportFaqs = value; }

    @JsonProperty("report_users")
    public ReportUsers getReportUsers() { return reportUsers; }
    @JsonProperty("report_users")
    public void setReportUsers(ReportUsers value) { this.reportUsers = value; }

    @JsonProperty("report_events")
    public ReportEvents getReportEvents() { return reportEvents; }
    @JsonProperty("report_events")
    public void setReportEvents(ReportEvents value) { this.reportEvents = value; }

    @JsonProperty("report_stories")
    public ReportStories getReportStories() { return reportStories; }
    @JsonProperty("report_stories")
    public void setReportStories(ReportStories value) { this.reportStories = value; }

    @JsonProperty("time_zone")
    public String getTimeZone() { return timeZone; }
    @JsonProperty("time_zone")
    public void setTimeZone(String value) { this.timeZone = value; }
}
