package com.asylumproject.asylumproject.reports.requests.utils;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.NONE)
public class ReportFaqsFilters {
}
