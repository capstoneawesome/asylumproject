package com.asylumproject.asylumproject.reports.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class UserAttribute {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotNull
    private String displayName;

    @NotNull
    private String name;

    @NotNull
    private String type;

    @NotNull
    private String description;

    @NotNull
    private boolean enabled;

    /**
     * Class default constructor.
     */
    public UserAttribute() {}

    /**
     * Class constructor based on set parameters.
     * @param displayName the display name.
     * @param name the name.
     * @param type the type.
     * @param description the description
     * @param enabled is enabled.
     */
    public UserAttribute(String displayName, String name, String type, String description, boolean enabled) {
        this.displayName = displayName;
        this.name = name;
        this.type = type;
        this.description = description;
        this.enabled = enabled;
    }

    /**
     * Retrieve the id.
     * @return the id.
     */
    public int getId() {
        return id;
    }

    /**
     * Set the name.
     * @param name the name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Retrieve the name.
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the type.
     * @param type the type to set.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Retrieve the type.
     * @return the type.
     */
    public String getType() {
        return type;
    }

    /**
     * Set enabled.
     * @param enabled enabled to set.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Validate if enabled.
     * @return if enabled.
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Set the description.
     * @param description the description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Retrieve the description.
     * @return the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the display name.
     * @param displayName the display name to set.
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Retrieve the display name.
     * @return the display name.
     */
    public String getDisplayName() {
        return displayName;
    }

}
