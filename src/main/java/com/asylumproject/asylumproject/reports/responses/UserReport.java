package com.asylumproject.asylumproject.reports.responses;

import java.util.ArrayList;
import java.util.List;

public class UserReport {

    private List<UserEvent> events;
    private List<UserList> users;

    public UserReport (List<UserEvent> event) {
        this.events = event;
        users = new ArrayList();
        buildUsers(event);
    }

    public List<UserList> getUsers() {
        return users;
    }

    public List<UserEvent> getEvents() {
        return events;
    }

    private void buildUsers(List<UserEvent> event) {
        for(UserEvent uEvent: event) {
            boolean found = false;
            for(UserList list: users) {
                if(!found && list.getUserId() == uEvent.getUserId()) {
                    found = true;
                }
            }
            if(!found)
                users.add(new UserList(uEvent.getUserId(), uEvent.getUserName()));
        }
    }
}

class UserList {

    private int userId;
    private String username;

    public UserList(int userId, String username) {
        this.userId = userId;
        this.username = username;
    }

    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }
}
