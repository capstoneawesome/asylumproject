package com.asylumproject.asylumproject.reports.responses;

import java.util.ArrayList;
import java.util.List;

public class BarStoriesPerCurator {

    private List<CuratorData> curators = new ArrayList<>();

    public BarStoriesPerCurator(List<ReportStoriesPerCurator> list) {
        for(ReportStoriesPerCurator spc: list) {
            boolean found = false;
            for(CuratorData cd: curators) {
                if(cd.getCurator().equals(spc.getCurator())) {
                    found = true;
                    switch (spc.getState()) {
                        case "DRAFT":
                            cd.setDRAFT(spc.getNumber());
                            break;
                        case "PREPUBLISHED":
                            cd.setPREPUBLISHED(spc.getNumber());
                            break;
                        case "PUBLISHED":
                            cd.setPUBLISHED(spc.getNumber());
                            break;
                        case "ARCHIVED":
                            cd.setARCHIVED(spc.getNumber());
                            break;
                    }
                }
            }
            if(!found)
                curators.add(new CuratorData(spc.getCurator(), spc.getState(), spc.getNumber()));
        }
    }

    public List<CuratorData> getCurators() {
        return curators;
    }
}

