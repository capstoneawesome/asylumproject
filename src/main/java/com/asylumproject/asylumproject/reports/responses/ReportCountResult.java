package com.asylumproject.asylumproject.reports.responses;

import java.io.Serializable;

public class ReportCountResult implements Serializable {

    private String rName;
    private long rValue;

    public ReportCountResult() {}

    public ReportCountResult(String rName, long rValue) {
        this.rName = rName;
        this.rValue = rValue;
    }

    public String getName() {
        return rName;
    }

    public long getValue() {
        return rValue;
    }

    public void setRname(String rName) {
        this.rName = rName;
    }

    public void setRvalue(long rValue) {
        this.rValue = rValue;
    }
}
