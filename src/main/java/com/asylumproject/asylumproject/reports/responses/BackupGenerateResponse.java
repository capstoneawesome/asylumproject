package com.asylumproject.asylumproject.reports.responses;

public class BackupGenerateResponse {

    private boolean mailSent;
    private String path;

    public BackupGenerateResponse() {}

    public BackupGenerateResponse(boolean mailSent, String path) {
        this.mailSent = mailSent;
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public boolean isMailSent() {
        return mailSent;
    }
}
