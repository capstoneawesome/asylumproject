package com.asylumproject.asylumproject.reports.responses;

import com.asylumproject.asylumproject.problemdomain.Story;

public class ReportStoriesPerCountry {

    private String dtype;
    private String country;
    private Story.State state;
    private long number;

    public ReportStoriesPerCountry() {}

    public ReportStoriesPerCountry(String dtype, String country, Story.State state, long number) {
        this.dtype = dtype;
        this.country = country;
        this.state = state;
        this.number = number;
    }

    public String getDtype() {
        return dtype;
    }

    public String getCountry() {
        return country;
    }

    public long getNumber() {
        return number;
    }

    public String getState() {
        return state.name();
    }
}
