package com.asylumproject.asylumproject.reports.responses;

import java.util.ArrayList;
import java.util.List;

public class StoryReport {

    private List<StoryEvent> events;
    private List<StoryList> stories;

    public StoryReport (List<StoryEvent> event) {
        this.events = event;
        stories = new ArrayList<>();
        buildStories(event);
    }

    public List<StoryList> getStories() {
        return stories;
    }

    public List<StoryEvent> getEvents() {
        return events;
    }

    private void buildStories(List<StoryEvent> event) {
        for(StoryEvent sEvent: event) {
            boolean found = false;
            for(StoryList list: stories) {
                if(!found && list.getStoryId() == sEvent.getStoryId()) {
                    found = true;
                }
            }
            if(!found)
                stories.add(new StoryList(sEvent.getStoryId(), sEvent.getStoryTitle()));
        }
    }
}

class StoryList {

    private int storyId;
    private String storyTitle;

    public StoryList(int storyId, String storyTitle) {
        this.storyId = storyId;
        this.storyTitle = storyTitle;
    }

    public int getStoryId() {
        return storyId;
    }

    public String getStoryTitle() {
        return storyTitle;
    }
}
