package com.asylumproject.asylumproject.reports.responses;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public class PieElement {

    private String id;
    private String label;
    private BigInteger value;

    public PieElement() {}

    public PieElement(String id, BigInteger value) {
        this.id = id;
        this.label = id;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public BigInteger getValue() {
        return value;
    }
}
