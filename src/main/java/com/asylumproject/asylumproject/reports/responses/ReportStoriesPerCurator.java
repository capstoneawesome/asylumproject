package com.asylumproject.asylumproject.reports.responses;

import com.asylumproject.asylumproject.problemdomain.Story;
import com.asylumproject.asylumproject.problemdomain.User;

public class ReportStoriesPerCurator {

    private String dtype;
    private User curator;
    private Story.State state;
    private long number;

    public ReportStoriesPerCurator() {}

    public ReportStoriesPerCurator(String dtype, User curator, Story.State state, long number) {
        this.dtype = dtype;
        this.curator = curator;
        this.state = state;
        this.number = number;
    }

    public String getDtype() {
        return dtype;
    }

    public String getCurator() {
        return curator.getUsername();
    }

    public long getNumber() {
        return number;
    }

    public String getState() {
        return state.name();
    }
}
