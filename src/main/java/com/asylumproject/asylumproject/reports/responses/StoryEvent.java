package com.asylumproject.asylumproject.reports.responses;

import com.asylumproject.asylumproject.problemdomain.Event;
import com.asylumproject.asylumproject.problemdomain.Story;

import java.util.Date;

public class StoryEvent {

    private int storyId;
    private Date dateTime;
    private Event.Operation operation;
    private String actingUser;
    private String storyDescription;
    private String storyTitle;
    private String asylumSeekerName;
    private Story.State storyState;

    public StoryEvent() {}

    public StoryEvent(int storyId, Date dateTime,
                      Event.Operation operation,
                      String actingFName,
                      String actingLName,
                      String storyDescription,
                      String storyTitle,
                      String asylumSeekerName,
                      Story.State storyState) {
        this.storyId = storyId;
        this.dateTime = dateTime;
        this.operation = operation;
        this.actingUser = actingFName + " " + actingLName;
        this.storyDescription = storyDescription;
        this.storyTitle = storyTitle;
        this.asylumSeekerName = asylumSeekerName;
        this.storyState = storyState;
    }

    public String getActingUser() {
        return actingUser;
    }

    public String getOperation() {
        return operation.getDisplayName();
    }

    public Date getDateTime() {
        return dateTime;
    }

    public String getAsylumSeekerName() {
        return asylumSeekerName;
    }

    public String getStoryDescription() {
        return storyDescription;
    }

    public String getStoryTitle() {
        return storyTitle;
    }

    public int getStoryId() {
        return storyId;
    }

    public String getStoryState() {
        return storyState.name();
    }
}
