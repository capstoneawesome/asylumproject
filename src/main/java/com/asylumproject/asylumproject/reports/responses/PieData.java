package com.asylumproject.asylumproject.reports.responses;

import javax.persistence.Tuple;
import javax.persistence.TupleElement;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class PieData {

    private List<PieElement> pieElementList = new ArrayList<>();

    public PieData(List<Tuple> listTuples) {
        for(Tuple tuple: listTuples) {

            List<TupleElement<?>> elements = tuple.getElements();

            pieElementList.add(new PieElement((String) tuple.get(0), (BigInteger) tuple.get(1)));
        }
    }

    public List<PieElement> getElements() {
        return this.pieElementList;
    }
}
