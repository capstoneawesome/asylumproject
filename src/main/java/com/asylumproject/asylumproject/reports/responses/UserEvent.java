package com.asylumproject.asylumproject.reports.responses;

import com.asylumproject.asylumproject.problemdomain.Event;

import java.util.Date;

public class UserEvent {

    private int userId;
    private Date dateTime;
    private Event.Operation operation;
    private String aUserFName;
    private String aUserLName;
    private String userName;
    private String userFName;
    private String userLName;

    public UserEvent() {}

    public UserEvent(int userId,
                     Date dateTime,
                     Event.Operation operation,
                     String aUserFName,
                     String aUserLName,
                     String userName,
                     String userFName,
                     String userLName) {
        this.userId = userId;
        this.dateTime = dateTime;
        this.operation = operation;
        this.aUserFName = aUserFName;
        this.aUserLName = aUserLName;
        this.userName = userName;
        this.userFName = userFName;
        this.userLName = userLName;
    }

    public int getUserId() {
        return userId;
    }

    public String getOperation() {
        return operation.getDisplayName();
    }

    public String getUserName() {
        return userName;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public String getActingUser() {
        return aUserFName + " " + aUserLName;
    }

    public String getUser() {
        return userFName + " " + userLName;
    }
}
