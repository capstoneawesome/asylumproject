package com.asylumproject.asylumproject.reports.responses;

import java.util.ArrayList;
import java.util.List;

public class BarStoriesPerCountry {

    private List<CountryData> countries = new ArrayList<>();

    public BarStoriesPerCountry(List<ReportStoriesPerCountry> list) {
        for(ReportStoriesPerCountry spc: list) {
            boolean found = false;
            for(CountryData cd: countries) {
                if(cd.getCountry().equals(spc.getCountry())) {
                    found = true;
                    switch (spc.getState()) {
                        case "DRAFT":
                            cd.setDRAFT(spc.getNumber());
                            break;
                        case "PREPUBLISHED":
                            cd.setPREPUBLISHED(spc.getNumber());
                            break;
                        case "PUBLISHED":
                            cd.setPUBLISHED(spc.getNumber());
                            break;
                        case "ARCHIVED":
                            cd.setARCHIVED(spc.getNumber());
                            break;
                    }
                }
            }
            if(!found)
                countries.add(new CountryData(spc.getCountry(), spc.getState(), spc.getNumber()));
        }
    }

    public List<CountryData> getCountries() {
        return countries;
    }

}
