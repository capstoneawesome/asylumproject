package com.asylumproject.asylumproject.reports.responses;

import java.util.ArrayList;
import java.util.List;

public class TrafficData {

    private String id;
    private String color;
    private List<DataInfo> data;

    public TrafficData () {
        this.id = "days";
        this.color = "hsl(182, 70%, 50%)";
        this.data = new ArrayList<>();
        data.add(new DataInfo("Feb-26",3771));
        data.add(new DataInfo("Feb-27",3764));
        data.add(new DataInfo("Feb-28",3854));
        data.add(new DataInfo("Feb-29",3731));
        data.add(new DataInfo("Mar-01",3841));
        data.add(new DataInfo("Mar-02",3895));
        data.add(new DataInfo("Mar-03",3698));
        data.add(new DataInfo("Mar-04",3787));
        data.add(new DataInfo("Mar-05",3813));
        data.add(new DataInfo("Mar-06",3695));
        data.add(new DataInfo("Mar-07",3755));
        data.add(new DataInfo("Mar-08",3828));
        data.add(new DataInfo("Mar-09",3841));
        data.add(new DataInfo("Mar-10",3936));
        data.add(new DataInfo("Mar-11",3775));
        data.add(new DataInfo("Mar-12",3769));
        data.add(new DataInfo("Mar-13",3867));
        data.add(new DataInfo("Mar-14",3895));
        data.add(new DataInfo("Mar-15",3831));
        data.add(new DataInfo("Mar-16",3783));
        data.add(new DataInfo("Mar-17",3751));
        data.add(new DataInfo("Mar-18",3720));
        data.add(new DataInfo("Mar-19",3828));
        data.add(new DataInfo("Mar-20",3831));
        data.add(new DataInfo("Mar-21",3868));
        data.add(new DataInfo("Mar-22",3693));
        data.add(new DataInfo("Mar-23",3701));
        data.add(new DataInfo("Mar-24",3909));
        data.add(new DataInfo("Mar-25",3800));
        data.add(new DataInfo("Mar-26",3730));
    }

    public String getId() {
        return id;
    }

    public String getColor() {
        return color;
    }

    public List<DataInfo> getData() {
        return data;
    }
}

class DataInfo {
    private String x;
    private int y;

    public DataInfo (String x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getY() {
        return y;
    }

    public String getX() {
        return x;
    }
}
