package com.asylumproject.asylumproject.reports.responses;

public class CountryData {

    private String country;
    private long DRAFT;
    private long PREPUBLISHED;
    private long PUBLISHED;
    private long ARCHIVED;

    public CountryData(String country,
                       String state,
                       long number) {
        this.country = country;
        switch (state) {
            case "DRAFT":
                this.DRAFT = number;
                break;
            case "PREPUBLISHED":
                this.PREPUBLISHED = number;
                break;
            case "PUBLISHED":
                this.PUBLISHED = number;
                break;
            case "ARCHIVED":
                this.ARCHIVED = number;
                break;
        }
    }

    public long getDRAFT() {
        return DRAFT;
    }

    public long getPREPUBLISHED() {
        return PREPUBLISHED;
    }

    public long getPUBLISHED() {
        return PUBLISHED;
    }

    public long getARCHIVED() {
        return ARCHIVED;
    }

    public String getCountry() {
        return country;
    }

    public void setDRAFT(long DRAFT) {
        this.DRAFT = DRAFT;
    }

    public void setPREPUBLISHED(long PREPUBLISHED) {
        this.PREPUBLISHED = PREPUBLISHED;
    }

    public void setPUBLISHED(long PUBLISHED) {
        this.PUBLISHED = PUBLISHED;
    }

    public void setARCHIVED(long ARCHIVED) {
        this.ARCHIVED = ARCHIVED;
    }

}
