package com.asylumproject.asylumproject.reports;

public class PieReportData {

    private String id;
    private String label;
    private double value;
    private String color;

    public PieReportData(String label, double value) {
        this.id = label;
        this.label = label;
        this.value = value;
        this.color = "hst(118, 70%, 50%)";
    }

    public double getValue() {
        return value;
    }

    public String getColor() {
        return color;
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }
}
