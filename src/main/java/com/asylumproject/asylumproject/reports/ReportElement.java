package com.asylumproject.asylumproject.reports;

public class ReportElement {

    private String name;
    private long value;

    public ReportElement(String name, long value) {
        this.name = name;
        this.value = value;
    }

    public long getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
