package com.asylumproject.asylumproject.reports;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BackupRequest {
    private boolean sendMail;
    private String mailTo;

    @JsonProperty("send_mail")
    public boolean getSendMail() { return sendMail; }
    @JsonProperty("send_mail")
    public void setSendMail(boolean value) { this.sendMail = value; }

    @JsonProperty("mail_to")
    public String getMailTo() { return mailTo; }
    @JsonProperty("mail_to")
    public void setMailTo(String value) { this.mailTo = value; }
}
