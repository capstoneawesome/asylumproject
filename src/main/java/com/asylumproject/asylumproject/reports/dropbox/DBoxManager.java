package com.asylumproject.asylumproject.reports.dropbox;

import com.asylumproject.asylumproject.reports.ZipManager;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;

import java.io.*;
import java.util.ArrayList;

public class DBoxManager {

    private static final String ACCESS_TOKEN = "cQrUe8Gs5uAAAAAAAAAAIr7mF8TtRCy_A7_fLcNRtPHenxFW602WjRQgQ_7ZTuVU";

    public enum FileType{
        REPORT("/sysadmin/reports/"),
        BACKUP("/sysadmin/backups/");

        private final String path;

        FileType(String path) {
            this.path = path;
        }

        public String getPath() {
            return path;
        }
    }

    /**
     * Upload a file to the DropBox storage service.
     * @param fileType file type.
     * @param file the file.
     * @param fileName the file name.
     * @return the path to the uploaded file.
     */
    public static String uploadFile(FileType fileType, File file, String fileName) {

        DbxRequestConfig config = new DbxRequestConfig("AsylumProject");
        DbxClientV2 client = new DbxClientV2(config, ACCESS_TOKEN);

        try (InputStream in = new FileInputStream(file)) {
            FileMetadata metadata = client.files().uploadBuilder(fileType.getPath() + fileName).uploadAndFinish(in);
            return metadata.getPathLower();
        } catch (IOException | DbxException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Download a file from the DropBox storage service.
     * @param path the path to the file to download.
     * @return the downloaded file.
     */
    public static File downloadFile(String path) {

        DbxRequestConfig config = new DbxRequestConfig("AsylumProject");
        DbxClientV2 client = new DbxClientV2(config, ACCESS_TOKEN);

        String[] values = path.split("/");
        String fileName = values[values.length - 1];
        String localPath = "src/main/resources/databaseTemps/" + fileName;

        OutputStream outputStream;
        try {
            outputStream = new FileOutputStream(localPath);
            client.files().downloadBuilder(path).download(outputStream);
            outputStream.close();

            String extension = localPath.substring(localPath.length() - 3);

            ArrayList<File> files;
            if(extension.equals("zip")) {
                files = ZipManager.decompress(localPath);
                File zipFile = new File(localPath);
                if(zipFile.exists()) zipFile.delete();
                return files.size() == 1? files.get(0) : null;
            }
            else if(extension.equals("sql")) {
                File localFile = new File(localPath);
                return localFile.exists()? localFile: null;
            }
            else
                return null;
        } catch (IOException | DbxException e) {
            e.printStackTrace();
            return null;
        }

    }
}
