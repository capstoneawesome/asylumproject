package com.asylumproject.asylumproject.reports;

public class ExistingElements {

    private int numImages;
    private int numVideos;
    private int numAudios;
    private int others;

    public ExistingElements() {

    }

    public ExistingElements(int numImages, int numVideos, int numAudios, int others) {
        this.numImages = numImages;
        this.numVideos = numVideos;
        this.numAudios = numAudios;
        this.others = others;
    }

    public int getNumAudios() {
        return numAudios;
    }

    public int getNumImages() {
        return numImages;
    }

    public int getNumVideos() {
        return numVideos;
    }

    public int getOthers() {
        return others;
    }

    public void setNumAudios(int numAudios) {
        this.numAudios = numAudios;
    }

    public void setNumImages(int numImages) {
        this.numImages = numImages;
    }

    public void setNumVideos(int numVideos) {
        this.numVideos = numVideos;
    }

    public void setOthers(int others) {
        this.others = others;
    }
}
