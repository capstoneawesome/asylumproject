package com.asylumproject.asylumproject.reports;

public class ExistingUsers {

    private int totalUsers;
    private int numSystemAdmin;
    private int numCurators;
    private int numTeachers;

    public ExistingUsers() {

    }

    public ExistingUsers(int totalUsers, int numSystemAdmin, int numCurators, int numTeachers) {
        this.totalUsers = totalUsers;
        this.numSystemAdmin = numSystemAdmin;
        this.numCurators = numCurators;
        this.numTeachers = numTeachers;
    }

    public int getNumCurators() {
        return numCurators;
    }

    public int getNumTeachers() {
        return numTeachers;
    }

    public int getNumSystemAdmin() {
        return numSystemAdmin;
    }

    public int getTotalUsers() {
        return totalUsers;
    }

    public void setNumCurators(int numCurators) {
        this.numCurators = numCurators;
    }

    public void setNumTeachers(int numTeachers) {
        this.numTeachers = numTeachers;
    }

    public void setNumSystemAdmin(int numSystemAdmin) {
        this.numSystemAdmin = numSystemAdmin;
    }

    public void setTotalUsers(int totalUsers) {
        this.totalUsers = totalUsers;
    }
}
