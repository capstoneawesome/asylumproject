package com.asylumproject.asylumproject.reports;

import com.asylumproject.asylumproject.problemdomain.Content;
import com.asylumproject.asylumproject.problemdomain.Event;
import com.asylumproject.asylumproject.problemdomain.User;

import java.util.List;

public class UserData {

    private User user;
    private List<Event> events;
    private List<Content> contents;

    public UserData(User user, List<Event> events, List<Content> contents) {
        this.user = user;
        this.events = events;
        this.contents = contents;
    }

    public User getUser() {
        return user;
    }
}
