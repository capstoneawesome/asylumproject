package com.asylumproject.asylumproject.reports;

public class ExistingContents {

    private int numStories;
    private int numResources;
    private int numFaqs;
    private int numOthers;

    public ExistingContents() {}

    public ExistingContents(int numStories, int numResources, int numFaqs, int numOthers) {
        this.numStories = numStories;
        this.numResources = numResources;
        this.numFaqs = numFaqs;
        this.numOthers = numOthers;
    }

    public void setNumFaqs(int numFaqs) {
        this.numFaqs = numFaqs;
    }

    public void setNumOthers(int numOthers) {
        this.numOthers = numOthers;
    }

    public void setNumResources(int numResources) {
        this.numResources = numResources;
    }

    public void setNumStories(int numStories) {
        this.numStories = numStories;
    }

    public int getNumFaqs() {
        return numFaqs;
    }

    public int getNumOthers() {
        return numOthers;
    }

    public int getNumResources() {
        return numResources;
    }

    public int getNumStories() {
        return numStories;
    }
}
