package com.asylumproject.asylumproject.payload;


/**
 * Represents an object response sent to client when a JWT refresh is requested.
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 */
public class JwtRefreshResponse {

    private String accessToken;
    private String tokenType = "Bearer";

    public JwtRefreshResponse(String accessToken){
        this.accessToken = accessToken;
    }

    public String getAccessToken(){
        return this.accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

}
