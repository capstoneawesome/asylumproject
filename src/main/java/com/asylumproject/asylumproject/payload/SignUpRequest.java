package com.asylumproject.asylumproject.payload;

import com.asylumproject.asylumproject.permission.Permission;
import com.asylumproject.asylumproject.problemdomain.User;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

public class SignUpRequest {

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotBlank
    private String userName;

    @NotBlank
    @Email
    private String email;

    @NotBlank
    private String password;

    @NotBlank
    private String defaultLanguage;

    private User creator;

    private String phoneNumber;

    private String photoPath;


    /**
     * Holds all of the permissions for a User. Each permission entry contains the name of the permission and
     * whether it is enabled (TRUE) or disabled (FALSE).
     * Cannot be null;
     */
    @org.hibernate.annotations.Type(
            type = "org.hibernate.type.SerializableToBlobType",
            parameters = { @org.hibernate.annotations.Parameter( name = "classname", value = "java.util.HashMap" ) }
    )
    private Set<Permission> permissions;

    /**
     * Retrieve the set of permissions.
     * @return the set of permissions.
     */
    public Set<Permission> getPermissions() {
        return permissions;
    }

    /**
     * A set of permissions to be set.
     * @param permissions set of permissions.
     */
    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    /**
     * Retrieve the photo path.
     * @return the photo path.
     */
    public String getPhotoPath() {
        return photoPath;
    }

    /**
     * Sets the photo path.
     * @param photoPath the photo path to set.
     */
    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    /**
     * Set phone number.
     * @param phoneNumber the phone number to set.
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Retrieve the username.
     * @return the username.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Set the username.
     * @param username the username to set.
     */
    public void setUserName(String username) {
        this.userName = username;
    }

    /**
     * Retrieve the email address.
     * @return the email address.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set an email address.
     * @param email the email address to set.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Retrieve the password.
     * @return the password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the password.
     * @param password the password to set.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Retrieve the first name.
     * @return the first name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the first name.
     * @param firstName the first name to set.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Retrieve the last name.
     * @return the last name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the last name.
     * @param lastName the last name to set.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Retrieve the default language.
     * @return the default language.
     */
    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    /**
     * Set the default language.
     * @param defaultLanguage the default language to set.
     */
    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    /**
     * Retrieve the phone number.
     * @return the phone number.
     */
    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    /**
     * The User ID of the User that created this User.
     * @return The User ID of the User that created this User.
     */

    /**
     * Retrieve the creator user.
     * @return the creator user.
     */
    public User getCreator() {
        return creator;
    }

    /**
     * Set the creator user.
     * @param creator the creator user to set.
     */
    public void setCreator(User creator) {
        this.creator = creator;
    }
}
