package com.asylumproject.asylumproject.payload;

import javax.validation.constraints.NotBlank;

public class LoginRequest {

    @NotBlank
    private String userName;

    @NotBlank
    private String password;

    /**
     * Retrieve the username or email address value.
     * @return the username of email address value.
     */
    public String getUsernameOrEmail(){
        return userName;
    }

    /**
     * Set the username or email address value.
     * @param usernameOrEmail the username or email address value.
     */
    public void setUsernameOrEmail(String usernameOrEmail) {
        this.userName = usernameOrEmail;
    }

    /**
     * Retrieve the password.
     * @return the password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the password.
     * @param password the password to set.
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
