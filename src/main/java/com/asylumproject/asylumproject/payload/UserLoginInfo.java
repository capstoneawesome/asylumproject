package com.asylumproject.asylumproject.payload;

import com.asylumproject.asylumproject.permission.Permission;
import com.asylumproject.asylumproject.problemdomain.User;

import java.util.Set;

public class UserLoginInfo {

    private User user;

    /**
     * Retrieve the user log in information.
     * @param user the user log in information.
     */
    public UserLoginInfo(User user) {
        this.user = user;
    }

    /**
     * Retrieve the id.
     * @return the id.
     */
    public int getId() {
        return user.getID();
    }

    /**
     * Retrieve the user name.
     * @return the user name.
     */
    public String getUsername() {
        return user.getUsername();
    }

    /**
     * Retrieve the email address.
     * @return the email address.
     */
    public String getEmail() {
        return user.getEmail();
    }

    /**
     * Retrieve the set of permissions.
     * @return the set of permissions.
     */
    public Set<Permission> getPermissions() {
        return user.getPermissions();
    }

    /**
     * Retrieve the photo path.
     * @return the photo path.
     */
    public String getPhotoPath() {
        return user.getPhotoPath();
    }

    /**
     * Retrieve the default language.
     * @return the default language.
     */
    public String getDefaultLanguage() {
        return user.getDefaultLanguage();
    }

    /**
     * Retrieve the last name.
     * @return the last name.
     */
    public String getLastName() {
        return user.getLastName();
    }

    /**
     * Retrieve the first name.
     * @return the firs name.
     */
    public String getFirstName() {
        return user.getFirstName();
    }

}
