package com.asylumproject.asylumproject.payload;

import org.springframework.security.core.GrantedAuthority;

public class UpdateUserAuthorities implements GrantedAuthority {

    private String authority;

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return this.authority;
    }
}
