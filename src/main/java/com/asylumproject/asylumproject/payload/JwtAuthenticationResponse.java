package com.asylumproject.asylumproject.payload;

/**
 * This class represents an object response that is sent to the client when a user logs into the system.
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 */
public class JwtAuthenticationResponse {

    private String accessToken;
    private String tokenType = "Bearer";
    private UserLoginInfo user;

    public JwtAuthenticationResponse(String accessToken, UserLoginInfo user){
        this.accessToken = accessToken;
        this.user = user;
    }

    public String getAccessToken(){
        return this.accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public void setUser(UserLoginInfo user) {
        this.user = user;
    }

    public UserLoginInfo getUser() {
        return user;
    }
}
