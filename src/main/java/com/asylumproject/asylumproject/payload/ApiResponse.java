package com.asylumproject.asylumproject.payload;

public class ApiResponse {

    private boolean success;
    private String message;

    /**
     * Api response constructor.
     * @param success true if successful, otherwise false.
     * @param message the message.
     */
    public ApiResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    /**
     * Validates if the request was successfully or not.
     * @return true if successful, otherwise false.
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Sets the api response success.
     * @param success true if successful, false otherwise.
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * Retrieve message.
     * @return the message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message.
     * @param message the message.
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
