package com.asylumproject.asylumproject.payload;

import javax.validation.constraints.NotBlank;

public class UpdatePasswordRequest {

    @NotBlank
    private String userName;

    private String oldPassword;

    @NotBlank
    private String newPassword;

    /**
     * Set the user name.
     * @param userName the user name to set.
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Set the new password.
     * @param newPassword the new password.
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * Set the old password.
     * @param oldPassword the old password to set.
     */
    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    /**
     * Retrieve the user name.
     * @return the user name.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Retrieve the new password.
     * @return the new password.
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * Retrieve the old password.
     * @return the old password.
     */
    public String getOldPassword() {
        return oldPassword;
    }
}


