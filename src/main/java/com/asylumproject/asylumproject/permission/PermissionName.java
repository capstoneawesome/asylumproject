package com.asylumproject.asylumproject.permission;

/**
     * Enum to the hold the four type of User permissions: SiteUser, Teacher, ContentCurator, SystemAdmin. Every user has
     * the SiteUser permission, and the other three can be added or removed.
     */
public enum PermissionName {

        /**
         * A User with this permission can: search for content, read a story in regular form, view interactive story,
         * change site language, change story language.
         */
        ROLE_SITE_USER,
        /**
         *  A User with this permission can: Apply a rating filter.
         */
        ROLE_TEACHER,
        /**
         *  A User with this permission can: Create content, rate a story, edit content, view traffic report for owned
         *  Stories, upload content elements, submit content, publish content, archive content, unarchive content, view
         *  archived content.
         */
        ROLE_CONTENT_CURATOR,
        /**
         *  A User with this permission can: Create a site backup, delete a user, add a user, edit a user, view traffic
         *  reports for the entire website, restore a site backup, delete content, archive content, unarchive content,
         *  view archived content, rate a Story, publish content.
         */
        ROLE_SYSTEM_ADMIN
}
