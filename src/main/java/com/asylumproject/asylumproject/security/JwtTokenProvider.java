package com.asylumproject.asylumproject.security;

import com.asylumproject.asylumproject.broker.AccountBroker;
import com.asylumproject.asylumproject.problemdomain.User;
import io.jsonwebtoken.impl.DefaultClock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.*;

import java.util.Date;
import java.util.Optional;
import java.util.function.Function;

/**
 * Handles all operations having to do with JWTs, including generating tokens, refreshing tokens, extracting claims, checking expiration, and validating JWTs.
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 */
@Component
public class JwtTokenProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenProvider.class);

    private AccountBroker accountBroker;

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationInMs}")
    private int jwtExpirationInMs;

    private Clock clock = DefaultClock.INSTANCE;

    /**
     * Instantiates a new JwtTokenProvider using the AccountBroker passed.
     * @param accountBroker the accountBroker to autowire
     */
    @Autowired
    public JwtTokenProvider(AccountBroker accountBroker) {
        this.accountBroker = accountBroker;
    }

    /**
     * Generate a token based on the user's authentication.
     * @param authentication the user's authentication.
     * @return the string token.
     */
    public String generateToken(Authentication authentication){
        UserDetails userPrincipal = (UserDetails) authentication.getPrincipal();

        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);

        return Jwts.builder()
                .setSubject(userPrincipal.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    /**
     * Validate if an existing token can be refreshed.
     * @param token the string token to validate.
     * @return true if it is refreshed, otherwise false.
     */
    public Boolean canTokenBeRefreshed(String token) {
        if (!isTokenExpired(token))
           return ((getExpirationDateFromToken(token).getTime()) - clock.now().getTime()) <= 604800000;
        return false;
    }

    /**
     * Validate if a toke ha expired.
     * @param token the token to validate.
     * @return true if valid, otherwise false.
     */
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(clock.now());
    }

    /**
     * Retrieve the expiration date of a token.
     * @param token the token.
     * @return the expiration date.
     */
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    /**
     * Retrieve the claim of a token.
     * @param token the token.
     * @param claimsResolver the claim resolver.
     * @param <T> the type of claim.
     * @return the type.
     */
    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    /**
     * Retrieve all claims from token.
     * @param token the token.
     * @return a list of claims.
     */
    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
    }

    /**
     * Refresh a token.
     * @param token the token to refresh.
     * @return the refreshed token.
     */
    public String refreshToken(String token) {
        final Date createdDate = clock.now();
        final Date expirationDate = calculateExpirationDate(createdDate);
        final Claims claims = getAllClaimsFromToken(token);
        claims.setIssuedAt(createdDate);
        claims.setExpiration(expirationDate);
        return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
    }

    /**
     * Calculate the expiration date based on the creation date.
     * @param createdDate the create date.
     * @return the expiration date.
     */
    private Date calculateExpirationDate(Date createdDate) {
        return new Date(createdDate.getTime() + jwtExpirationInMs);
    }

    /**
     * Retrieve a username based on a token.
     * @param token the token.
     * @return the matching username.
     */
    public String getUsernameFromJWT(String token){
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return claims.getSubject();
    }

    /**
     * Retrieve a user id based on a token.
     * @param token the token.
     * @return the matching user id.
     */
    public int getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();
        String username = claims.getSubject();
        Optional<User> user = accountBroker.getUserByUserName(username);
        return user.map(User::getID).orElse(-1);
    }

    /**
     * Validate a token based on user details.
     * @param authToken the token.
     * @param userDetails the user details.
     * @return true if validated, otherwise false.
     */
    public boolean validateToken(String authToken, UserDetails userDetails){
        try{
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            final String username = getUsernameFromJWT(authToken);
            return username.equals(userDetails.getUsername());
        } catch (SignatureException ex) {
            LOGGER.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            LOGGER.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            LOGGER.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            LOGGER.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            LOGGER.error("JWT claims string is empty.");
        }
        return false;
    }
}
