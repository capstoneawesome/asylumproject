package com.asylumproject.asylumproject.security;

import com.asylumproject.asylumproject.manager.UserManager;
import com.asylumproject.asylumproject.problemdomain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * This Filter is added before any other SpringSecurity Filter when doing authorization to API endpoints.
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 */
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger((JwtAuthenticationFilter.class));

    @Autowired
    private UserManager userManager;
    @Autowired
    private JwtTokenProvider tokenProvider;

    /**
     * Initializes a new JwtAuthenticationFilter
     */
    public JwtAuthenticationFilter(){

    }

    /**
    //@Autowired
    public JwtAuthenticationFilter(JwtTokenProvider tokenProvider, UserManager userManager) {
        this.tokenProvider = tokenProvider;
        this.userManager = userManager;
    }
    **/
    /**
     * Verifies that the JWT received in the request is valid, and sets the the verified authentication on the User.
     * @param request the client request received in the filter
     * @param response the response to send to the next filter in the Spring Security filter chain.
     * @param filterChain contains all of the Filters that Spring Security uses
     * @throws ServletException if the filter cannot be processed
     * @throws IOException if an IOException occurs in filter processing
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        try{
            String jwt = getJwtFromRequest(request);

            if (StringUtils.hasText(jwt)) {

                String username = tokenProvider.getUsernameFromJWT(jwt);

                if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                    Optional<User> userOpt = userManager.getUserByUserName(username);

                    if (userOpt.isPresent() && tokenProvider.validateToken(jwt, userOpt.get())) {
                        User user = userOpt.get();
                        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
                        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                        SecurityContextHolder.getContext().setAuthentication(authentication);
                    }
                }
            }
        } catch (Exception ex ){
            LOGGER.info("JWT invalid, user will be logged out");
        }

        filterChain.doFilter(request, response);
    }

    /**
     * Extracts the JWT from the Authorization header.
     * @param request the request to get the header from
     * @return the extracted JWT
     */
    private String getJwtFromRequest(HttpServletRequest request){
        String bearerToken = request.getHeader("Authorization");

        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")){
            return bearerToken.substring(7);
        }

        return null;
    }
}
