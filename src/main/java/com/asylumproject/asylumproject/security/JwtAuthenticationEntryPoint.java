package com.asylumproject.asylumproject.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Attaches to the SecurityConfig to handle unauthorized JWT usage.
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 */
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthenticationEntryPoint.class);

    /**
     * Commences the authentication scheme for the entrypoint.
     * @param request the request from the client
     * @param response the response to send to the client
     * @param authException if the authentication scheme cannot be commenced
     * @throws IOException if an IOException occurs during processing
     * @throws ServletException if a ServletException occurs during processing
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {

        LOGGER.error("Unauthorized access error. Message - {}", authException.getMessage());
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage());
    }
}
