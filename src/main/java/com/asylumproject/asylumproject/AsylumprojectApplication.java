package com.asylumproject.asylumproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class AsylumprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(AsylumprojectApplication.class, args);
    }

}

