package com.asylumproject.asylumproject.problemdomain;

import com.asylumproject.asylumproject.payload.SignUpRequest;
import com.asylumproject.asylumproject.permission.Permission;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.*;

/**
 * Represents a user, including all of the personal attributes associated with a user, and their permissions.
 * A User has one PermissionList object associated with it.
 */
@Entity
@SQLDelete(sql = "UPDATE user SET deleted = true WHERE user_id = ?", check = ResultCheckStyle.COUNT)
//@Where(clause = "deleted <> true")
public class User implements UserDetails
{

    //Attributes
//    @OneToMany(mappedBy = "creator", cascade = CascadeType.ALL)
//    private List<Content> contents;

//    @OneToMany(mappedBy = "actingUser", cascade = CascadeType.ALL)
//    private Set<Event> events;

    /**
     * The unique Id for a user in the system.
     * Valid range: 0 to 2^31 - 1
     * Cannot be null.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int userId;


    /**
     * The userId of the user that created this user.
     * Valid range: 0 to 2^31 - 1
     * Cannot be null.
     */
//    @NotNull
//    private int creator;

    @ManyToOne
    private User creator;

    /**
     * The first name of the user.
     * Can contain any characters.
     * 100 character length limit.
     * May be null only for a user that does not have more permissions than the SiteUserPermissionGroup class holds.
     */
    @NotBlank
    private String firstName;


    /**
     * The last name of the user.
     * Can contain any characters.
     * 100 character length limit.
     * May be null only for a user that does not have more permissions than the SiteUserPermissionGroup class holds.
     */
    @NotBlank
    private String lastName;


    /**
     * The email address of the user.
     * Can contain any characters.
     * 100 character length limit.
     * May be null only for a user that does not have more permissions than the SiteUserPermissionGroup class holds.
     */
    @Column(unique = true)
    @NotBlank
    private String emailAddress;


    /**
     * The phone number of the user.
     * Can contain only whole numbers and hyphens.
     * May be null only for a user that does not have more permissions than the SiteUserPermissionGroup class holds.
     */
    @NotBlank
    private String phoneNumber;


    /**
     * The default language set by the user.
     * Can be one of a selected number of website languages.
     * Cannot be null.
     */
    @NotBlank
    private String defaultLanguage;


    /**
     * The user name set by the user or the creator.
     * Can contain any characters.
     * 30 character length limit.
     * May be null only for a user that does not have more permissions than the SiteUserPermissionGroup class holds.
     */
    @Column(unique = true)
    @NotBlank
    private String userName;


    /**
     * The password set by the user.
     * Can contain any characters.
     * 150 character length limit.
     * May be null only for a user that does not have more permissions than the SiteUserPermissionGroup class holds.
     */
    @NotBlank
    private String password;

    private String resetToken;

    /**
     * The file path containing the location of the user's profile photo on the system.
     * Can contain any characters.
     * 500 character length limit.
     * May be null only for a user that does not have more permissions than the SiteUserPermissionGroup class holds.
     */
    @NotBlank
    private String photoPath;

    /**
     * The date the User object was created.
     * Must be less than or equal to the current date.
     * May be null only for a user that does not have more permissions than the SiteUserPermissionGroup class holds.
     */
    @Column(updatable=false)
    @CreationTimestamp
    private Timestamp createDateTime;



    @Column
    @UpdateTimestamp
    private Timestamp updateDateTime;

    /**
     * Determines if the User appears in Hibernate queries.
     * true = yes
     * false = no
     */
    @NotNull
    private boolean deleted;

    /**
     * Determines whether the User is able to login.
     * true = can log in
     * false = cannot log in
     */
    @NotNull
    private boolean enabled;


    /**
     * Holds all of the permissions for a User. Each permission entry contains the name of the permission and
     * whether it is enabled (TRUE) or disabled (FALSE).
     * Cannot be null;
     */
    @org.hibernate.annotations.Type(
            type = "org.hibernate.type.SerializableToBlobType",
            parameters = { @org.hibernate.annotations.Parameter( name = "classname", value = "java.util.HashMap" ) }
    )
    private Set<Permission> permissions;

    /**
     * Defines the "Roles" assigned to the User, that are used for authorization on URI endpoints.
     */
    @org.hibernate.annotations.Type(
            type = "org.hibernate.type.SerializableToBlobType",
            parameters = { @org.hibernate.annotations.Parameter( name = "classname", value = "java.util.List" ) }
    )
    private List<GrantedAuthority> authorities;

    //methods

    public User(){

    }

    public User(SignUpRequest signUpRequest) {
        this.firstName = signUpRequest.getFirstName();
        this.lastName = signUpRequest.getLastName();
        this.userName = signUpRequest.getUserName();
        this.emailAddress = signUpRequest.getEmail();
        this.password = signUpRequest.getPassword();
        this.defaultLanguage = signUpRequest.getDefaultLanguage();
//        this.creator = signUpRequest.getCreator() > 0? signUpRequest.getCreator(): 0;
        this.phoneNumber = signUpRequest.getPhoneNumber();
        this.photoPath = signUpRequest.getPhotoPath();
        this.permissions = signUpRequest.getPermissions();
        this.creator = signUpRequest.getCreator();
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public boolean isDeleted() {
        return deleted;
    }

    /**
     * Sets the User's deleted state. A User with an deleted state of "false" means they have been soft-deleted.
     * A User with an deleted state of "true" means they are visible to Hibernate queries.
     * @param deleted the deleted state to set
     */
    public void setDeleted(boolean deleted){
        this.deleted = deleted;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public String getCreatorUserName() {
        return creator.getUsername();
        //return creator == null? null: creator.getUsername();
    }

    /**
     * Sets the userId of the User.
     * @param id the id to set
     */
    public void setID(int id){
       this.userId = id;
    }

    /**
     * Sets the first name of the User.
     * @param firstName the first name to set
     */
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }

    /**
     * Sets the last name of the User.
     * @param lastName the last name to set
     */
    public void setLastName(String lastName){
        this.lastName = lastName;
    }

    /**
     * Sets the email address of the User.
     * @param email the email address to set
     */
    public void setEmail(String email){
        this.emailAddress = email;
    }

    /**
     * Sets the phone number of the User.
     * @param phoneNumber the phone number to set
     */
    public void setPhoneNumber(String phoneNumber){
        this.phoneNumber = phoneNumber;
    }

    /**
     * Sets the default language of the User.
     * @param language the language to set
     */
    public void setDefaultLanguage(String language){
        this.defaultLanguage = language;
    }

    /**
     * Sets the file path for the user's profile photo.
     * @param photoPath the file path to set
     */
    public void setUserPhoto(String photoPath){
        this.photoPath = photoPath;
    }

    /**
     * Sets the username of the User.
     * @param userName the username to set
     */
    public void setUserName(String userName){
        this.userName = userName;
    }

    /**
     * Sets the password of the User.
     * @param password the password to set
     */
    public void setPassword(String password){
        this.password = password;
    }

    /**
     * Sets the list of permissions for the User.
     * @param permissions the list of permissions to set
     */
    public void setPermissionList(Set<Permission> permissions){
        this.permissions = permissions;
    }

    /**
     * Sets the userId of the User that created this User.
     * @param creator the userId of the User that created this User
     */
//    public void setCreator(int creator){
//        this.creator = creator;
//    }


    /**
     * Gets the date and time on which the User was created.
     * @return the creation date
     */
    public Timestamp getCreateDateTime() {
        return createDateTime;
    }

    /**
     * Gets the userId of the User that created this User.
     * @return creator the userId of the User that created this User
     */
//    public int getCreator(){
//        return this.creator;
//    }

    /**
     * Gets the userId of this User.
     * @return the userId
     */
    public int getID(){
        return this.userId;
    }

    public String getName() {
        return firstName + " " + lastName;
    }

    /**
     * Gets the first name of this User.
     * @return the first name
     */
    public String getFirstName(){
        return this.firstName;
    }

    /**
     * Gets the last name of this User.
     * @return the last name
     */
    public String getLastName(){
        return this.lastName;
    }

    /**
     * Gets the email address of this User.
     * @return the email address
     */
    public String getEmail(){
        return this.emailAddress;
    }

    /**
     * Gets the phone number of this User.
     * @return the phone number
     */
    public String getPhoneNumber(){
       return this.phoneNumber;
    }

    /**
     * Gets the default language of this User.
     * @return the default language
     */
    public String getDefaultLanguage(){
        return this.defaultLanguage;
    }

    /**
     * Gets the file path pointing to the user's profile photo.
     * @return the photo path
     */
    public String getPhotoPath(){
        return this.photoPath;
    }

    /**
     * Gets the password of this User.
     * @return the password
     */
    @JsonIgnore
    public String getPassword(){
        return this.password;
    }

    /**
     * Gets the username of this User.
     * @return the username
     */
    @Override
    public String getUsername() {
        return this.userName;
    }

    /**
     * Gets the permissions HashMap associated with this User.
     * @return the permissions HashMap of this User
     */
    public Set<Permission> getPermissions(){
       return this.permissions;
    }

    /**
     * Returns the User's Deleted state. A User with an deleted state of "false" means they have been soft-deleted.
     * A User with an deleted state of "true" means they are visible to Hibernate queries.
     * @return the deleted state of the User
     */
    public boolean getDeleted(){
        return this.deleted;
    }

    /**
     * Retrieve the last date time the user information was updated.
     * @return the las date time updated.
     */
    public Timestamp getUpdateDateTime() {
        return updateDateTime;
    }

    public void setAuthorities(List<GrantedAuthority> authorities){
        this.authorities = authorities;
    }

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    /**
     * Gets the User's enabled state. A User that isn't enabled is unable to login.
     * This method is provided by Spring Security
     * @return the User's enabled state attribute
     */
    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }

    @JsonIgnore
    public String getResetToken() {
        return resetToken;
    }

}
