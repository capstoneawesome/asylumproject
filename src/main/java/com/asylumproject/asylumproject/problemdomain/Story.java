package com.asylumproject.asylumproject.problemdomain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class representing the Story object. A Story contains all of the ContentElements and necessary information for an asylum seeker's story.
 * @author CapstoneAwesome
 *
 */

@Entity
@SQLDelete(sql = "UPDATE content SET deleted = true WHERE content_id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted <> true")
public class Story extends Content
{

	@Column(insertable = false, updatable = false)
	private String dtype;

	@Override
	public boolean isDeleted() {
		return deleted;
	}

	@Override
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * Enumerated states to indicate Database routing.
	 * @author CapstoneAwesome
	 *
	 */
	public enum State
	{
		DRAFT,
		PREPUBLISHED,
		PUBLISHED,
		ARCHIVED
	};

	//Attributes

	/**
	 * Possible states: 'DRAFT', 'PREPUBLISHED', 'PUBLISHED', 'ARCHIVED'
	 */
	@Enumerated(EnumType.STRING)
	private State state;


	private String countryFull;

	/**
	 * List of professionally translated languages available for this story.
	 */
	//@ManyToMany(mappedBy = "stories", cascade = CascadeType.ALL)
//	private Set<Language> languages;

	public String getCountryFull() {
		return countryFull;
	}

	public void setCountryFull(String countryFull) {
		this.countryFull = countryFull;
	}


	public List<MapPoint> getMapPoints() {
		return mapPoints;
	}

	public void setMapPoints(List<MapPoint> mapPoints) {
		this.mapPoints = mapPoints;
	}

	/**
	 * A list of map point that belong to this Story.
	 */
	@OneToMany(mappedBy = "story", cascade = CascadeType.ALL)
	private List<MapPoint> mapPoints;

	
	/**
	 * The name of the asylum seeker who is the subject of this story.
	 */
	private String asylumSeekerName;


	public String getContentRating() {
		return contentRating;
	}

	public void setContentRating(String contentRating) {
		this.contentRating = contentRating;
	}

	/**
	 * Content rating which indicates subject matter which may be disturbing or inappropriate for some viewers.  
	 */
	@NotBlank
	private String contentRating;

	
	/**
	 * Country of origin for the asylum seeker in this story.
	 */
	@NotBlank
	private String countryOfOrigin;


	private boolean deleted;


	//Methods


	/**
	 * non-argument constructor
	 */
	public Story(){

		//super(languages);
}

//	public Story(Set<Language> languages){
//		this.languages = languages;
//	}

	/**
	 * Retrieves asylum seeker name.
	 * @return asylumSeekerName asylum seeker name
	 */
	public String getAsylumSeekerName() {
		return asylumSeekerName;
	}
	
	/**
	 * Sets asylum seeker name.
	 * @param asylumSeekerName asylum seeker name
	 */
	public void setAsylumSeekerName(String asylumSeekerName) {
		this.asylumSeekerName = asylumSeekerName;
	}

	/**
	 * Saves the content retrieved from the Content editor into this Story object.
	 * @param content the Story content that user has added
	 */
	public void setContent(Object content){

	}

	/**
	 * Returns the content that was saved into this Story object.
	 * @return the the content saved into the Story object.
	 */
	public Object getContent(){
		return null;
	}

	/**
	 * Retrieves the content rating for this story.
	 * @return contentRating rating indicating any potentially disturbing content
	 */
	public String getStoryRating() {
		return contentRating;
	}
	
	/**
	 * Sets the content rating for this story.
	 * @param contentRating rating indicating any potentially disturbing content
	 */
	public void setStoryRating(String contentRating) {
		this.contentRating = contentRating;
	}

	/**
	 * Retrieves the origin country of asylum seeker for this story.
	 * @return countryOfOrigin origin country of asylum seeker for this story
	 */
	public String getCountryOfOrigin() {
		return countryOfOrigin;
	}
	
	/**
	 * Sets the origin country of asylum seeker for this story.
	 * @param countryOfOrigin origin country of asylum seeker for this story
	 */
	public void setCountryOfOrigin(String countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}
	
	/**
	 * Retrieves list of professionally translated languages available for this story.
	 * @return languages list of professionally translated languages available for this story
	 */
//	public Set<ContentLanguage> getStoryLanguages() {
//		return languages;
//	}

	/*
	@Override
	public Set<Language> getLanguages() {
		Set<Language> languages = new HashSet<>();
	    for (MapPoint m : this.mapPoints){

	    	for (ContentElement cE: m.getContentElement()){

				languages.add(contentManager.getLcE.getLanguage());
			}
		}
	    return (Set<Language>) languages;
	}
	*/

	/**
	 * Appends a language to list of languages contained in Story object. 
	 */
	public void addStoryLanguage()
	{
		
	}
	
	/**
	 * Removes specified language from list of languages contained in the Story object.
	 */
	public void removeStoryLanguage()
	{
		
	}
	
	/**
	 * Retrieves the enumerated state of this Story object.
	 * @return state Enumerated value representing state
	 */
	public State getState()
	{
		return this.state;
	}
	
	/**
	 * Sets the state for this Story object.
	 * @param state Enumerated value representing state
	 */
	public void setState(State state)
	{
		this.state = state;
	}

	/**
	 * Opens up this saved story draft for drafting.
	 */
	public void continueDraft()
	{
		
	}
	
	/**
	 * Saves this story as a draft which can be continued later.
	 */
	public void saveDraft()
	{
		
	}
	
	/**
	 * Discards this story draft, permanently deleting it LOGICALLY. Called from a Delete request on the REST API.
	 */
	public void discardDraft()
	{
		
	}
	
	/**
	 * Submits this story for pre-publishing.
	 */
	public void submit()
	{
		
	}
	
	/**
	 * Allows rating of this story to be set in pre-publishing.
	 */
	public void rate()
	{
		
	}
	
	/**
	 * Publishes this story. This makes the story publicly visible.
	 */
	public void publish()
	{
		
	}
	
	/**
	 * Archives this story. An archived story is saved in the database but is not publicly visible.
	 */
	public void archive()
	{
		
	}
	
	/**
	 * Opens up the archived or published story for editing.
	 */
	public void edit()
	{
		
	}
	
	/**
	 * Deletes this story from the application and archive. Requires SysAdmin privileges.
	 */
	public void delete()
	{
		
	}


	public boolean addMapPoint(MapPoint newMapPoint) {
		if(newMapPoint == null)
			throw new NullPointerException();
		return mapPoints.add(newMapPoint);
	}

	public void addMapPointAt(int index, MapPoint newMapPoint) {
		if(newMapPoint == null)
			throw new NullPointerException();
		if(index < 0 || index > mapPoints.size())
			throw new IndexOutOfBoundsException();
		mapPoints.add(index, newMapPoint);
	}

	public boolean removeMapPoint(MapPoint mapPoint) {
		if(mapPoint == null)
			throw new NullPointerException();
		return mapPoints.remove(mapPoint);
	}

	public void moveMapPointTo(int finalPosition, MapPoint mapPoint) {
		if(finalPosition < 0 || finalPosition > mapPoints.size())
			throw new IndexOutOfBoundsException();
		if(mapPoint == null)
			throw new NullPointerException();
		mapPoints.add(finalPosition, mapPoint);
	}

	@JsonIgnore
	public String getDtype() {
		return dtype;
	}

	@JsonIgnore
	public void setDtype (String dtype) {
		this.dtype = dtype;
	}

}
