package com.asylumproject.asylumproject.problemdomain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import net.bytebuddy.build.ToStringPlugin;
import org.hibernate.annotations.*;
import org.springframework.boot.context.properties.bind.DefaultValue;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A class that represents a single point on the map, belonging to a Story.
 *
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 */
@Entity
@SQLDelete(sql = "UPDATE map_point SET deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT)
//@Where(clause = "deleted <> true")
public class MapPoint {

    @OneToMany(mappedBy = "mappoint", cascade = CascadeType.ALL)
    private List<ContentElement> contentElement;


    //Attributes
    @ManyToOne
    private Story story;

    /**
     * Id representing this MapPoint.
    */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    /****** do not remove the next commented lines **************
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "map_point_seq")
    @GenericGenerator(name = "map_point_seq",
            strategy = "com.asylumproject.asylumproject.config.StringPrefixTest",
            parameters = {
                    @Parameter(name = StringPrefixTest.INCREMENT_PARAM, value = "50"),
                    @Parameter(name = StringPrefixTest.VALUE_PREFIX_PARAMETER, value = "MP_"),
                    @Parameter(name = StringPrefixTest.NUMBER_FORMAT_PARAMETER, value = "%05d")
            })
    @Column(name = "id", updatable = false, nullable = false)
    private String id;
     ***********************************************************/

    /**
     * Contains two numbers: The longitude and the latitude of the coordinate.
     */
    @JsonDeserialize
    @org.hibernate.annotations.Type(
            type = "org.hibernate.type.SerializableToBlobType",
            parameters = { @org.hibernate.annotations.Parameter( name = "classname", value = "java.util.HashMap" ) }
    )
    private List<Double> coordinates;

    /**
     * The current zoom level of this MapPoint on the UI map.
     * Valid range: 1 to 15
     */
    @NotNull
    private double zoomLevel;

    @Column(updatable=false)
    @CreationTimestamp
    private Timestamp createDateTime;

    @Column
    @UpdateTimestamp
    private Timestamp updateDateTime;

    private boolean deleted;

    //Methods

    public MapPoint(){}

    public MapPoint(List<Double> coordinates, int zoomLevel) {
        this.coordinates = coordinates;
        this.zoomLevel = zoomLevel;
    }

    /**
     * Retrieve date date of map point creation.
     * @return date time of creation.
     */
    public Timestamp getCreateDateTime() {
        return createDateTime;
    }

    /**
     * Retrieve date time of last map point update.
     * @return date time of las update.
     */
    public Timestamp getUpdateDateTime() {
        return updateDateTime;
    }

    /**
     * Gets the coordinates associated with this MapPoint.
     * @return an ArrayList of Double containing the latitude and longitude of the MapPoint.
    */
    public List<Double> getCoordinates() {
        return coordinates;
    }


    /**
     * Sets the coordinates associated with this MapPoint.
     * @param coordinates the latitude and longitude to set
    */
    public void setCoordinates(List<Double> coordinates) {
        this.coordinates = coordinates;
    }

    /**
     * Gets the current zoom level of this MapPoint on the UI.
     * @return the current zoom level, from 1 - 15
     */
    public double getZoomLevel() {
        return zoomLevel;
    }

    /**
     * Sets the current zoom level of this MapPoint as it appears on the UI.
     * @param zoomLevel the current zoom level
     */
    public void setZoomLevel(double zoomLevel) {
        this.zoomLevel = zoomLevel;
    }

    public int getId() {
        return id;
    }


    public List<ContentElement> getContentElement() {
        return contentElement;
    }

    public void setContentElement(List<ContentElement> contentElement) {
        this.contentElement = contentElement;
    }

    public void addContentElement(ContentElement contentElement){
        this.contentElement.add(contentElement);
    }

    @JsonIgnore
    public Story getStory() {
        return story;
    }

    public void setStory(Story story) {
        this.story = story;
    }

    public void setCreateDateTime(Timestamp createDateTime) {
        this.createDateTime = createDateTime;
    }

    public void setUpdateDateTime(Timestamp updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
