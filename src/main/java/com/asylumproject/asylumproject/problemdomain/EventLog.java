package com.asylumproject.asylumproject.problemdomain;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
public class EventLog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int eventId;

    @Column(updatable = false)
    @CreationTimestamp
    private Timestamp dateTime;

    @ManyToOne
    @Where(clause = "deleted <> true || deleted = true")
    private User actingUser;

    private Event.Operation operation;

    @ManyToOne
    @Where(clause = "deleted <> true || deleted = true")
    private User user;

    @ManyToOne
    private Content content;

    @ManyToOne
    private ContentElement contentElement;

    @ManyToOne
    private MapPoint mapPoint;

    @ManyToOne
    private Tag tag;

    private String uuid;

    public EventLog() {}

    private EventLog (User actingUser, Event.Operation operation, User user, Content content,
                      ContentElement contentElement, MapPoint mapPoint, Tag tag, String uuid) {
        this.actingUser = actingUser;
        this.operation = operation;
        this.user = user;
        this.content = content;
        this.contentElement = contentElement;
        this.mapPoint = mapPoint;
        this.tag = tag;
        this.uuid = uuid == null ? UUID.randomUUID().toString() : uuid;
    }

    /**
     * Retrieve an Event Log instance based on provided event information.
     * @param actingUser the acting user.
     * @param operation the operation.
     * @param o the item being affected.
     * @param uuid the event unique identifier value.
     * @return an Event log instance.
     */
    public static EventLog getInstance(User actingUser, Event.Operation operation,
                                       Object o, String uuid) {
        if(o instanceof User)
            return new EventLog(actingUser, operation, (User)o, null, null, null, null, uuid);

        if(o instanceof Content)
            return new EventLog(actingUser, operation, null, (Content)o, null, null, null, uuid);

        if(o instanceof ContentElement)
            return new EventLog(actingUser, operation, null, null, (ContentElement)o, null, null, uuid);

        if(o instanceof MapPoint)
            return new EventLog(actingUser, operation, null, null, null, (MapPoint)o, null, uuid);

        if(o instanceof Tag)
            return new EventLog(actingUser, operation, null, null, null, null, (Tag) o, uuid);

        else
            return new EventLog(actingUser, operation, null, null, null,null,null, uuid);
    }

    /**
     * Retrieve the id.
     * @return the id.
     */
    public int getEventId() {
        return eventId;
    }

    /**
     * Retrieve the UUID.
     * @return the UUID.
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Retrieve the acting user.
     * @return the acting user.
     */
    public User getActingUser() {
        return actingUser;
    }

    /**
     * Retrieve the user.
     * @return the user or null if the event did not affect a user.
     */
    public User getUser() {
        return user;
    }

    /**
     * Retrieve the operation.
     * @return the operation.
     */
    public String getOperation() {
        return operation.getDisplayName();
    }

    /**
     * Retrieve the Content.
     * @return the content or null if the event did not affect a content.
     */
    public Content getContent() {
        return content;
    }

    /**
     * Retrieve the Content Element.
     * @return the content element or null if the event did not affect a content element.
     */
    public ContentElement getContentElement() {
        return contentElement;
    }

    /**
     * Retrieve the map point.
     * @return the map point or null if the event did not affect a map point.
     */
    public MapPoint getMapPoint() {
        return mapPoint;
    }

    /**
     * Retrieve the tag.
     * @return the tag or null if the event did not affect a tag.
     */
    public Tag getTag() {
        return tag;
    }

    /**
     * Retrieve the date and time.
     * @return the date and time.
     */
    public Timestamp getDateTime() {
        return dateTime;
    }

    /**
     * Retrieve the affected item.
     * @return the affected item.
     */
    public Object getAffectedItem() {
        if(user != null)
            return user;
        if(content != null)
            return content;
        if(contentElement != null)
            return contentElement;
        if(mapPoint != null)
            return mapPoint;
        if(tag != null)
            return tag;
        else
            return null;
    }

    /**
     * Validates if the affected item is a user.
     * @return true if the affected item is a user, otherwise false.
     */
    public boolean isUser()  {
        return user != null;
    }

    /**
     * Validates if the affected item is a content.
     * @return true if the affected item is a content, otherwise false.
     */
    public boolean isContent() {
        return content != null;
    }

    /**
     * Validates if the affected item is a content element.
     * @return true if the affected item is a content element, otherwise false.
     */
    public boolean isContentElement() {
        return contentElement != null;
    }

    /**
     * Validates if the affected item is a map point.
     * @return true if the affected item is a map point, otherwise false.
     */
    public boolean isMapPoint() {
        return mapPoint != null;
    }

    /**
     * Validates if the affected item is a tag.
     * @return true if the affected item is a tag, otherwise false.
     */
    public boolean isTag() {
        return tag != null;
    }

}
