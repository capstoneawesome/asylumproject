package com.asylumproject.asylumproject.problemdomain;

/**
 * Class representing an Event object.
 *
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 *
 */

public class Event
{

    /**
     * List of available list of event operations to log.
     */
    public enum Operation {
        CREATED("Created"),
        MODIFIED("Modified"),
        DELETED("Deleted"),
        PASSWORD_CHANGE("Password Change"),
        SIGN_IN("Sign In"),
        SIGN_OUT("Sign Out"),
        REQUEST_PASSWORD_RESET("Request Password Reset"),
        NAVIGATE("Navigate");

        private String displayName;

        Operation(String displayName) {
            this.displayName = displayName;
        }

        public String getDisplayName() {
            return displayName;
        }
    }

}

