package com.asylumproject.asylumproject.problemdomain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Set;

/**
 * Class representing a Language object.
 *
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 */
@Entity
@SQLDelete(sql = "UPDATE language SET deleted = true WHERE language_id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted <> true")
public class Language
{

    private boolean deleted;

    @ManyToMany(mappedBy = "languages")
    private List<Content> content;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int languageId;

    /**
     * Language description.
     * Language description would be the full name of language.
     */
    private String description;

    /**
     * Language code standard.
     * Language code would be ISO 639-1 standard.
     * For example english would be 'EN'.
     */
    @NotBlank
    private String code;

    private String name;


    //Methods

    public Language(){

    }

    /**
     * Class constructor for a language object.
     * @param code The ISO 639-3 language code
     */
    public Language(String code) {
        this.code = code;
    }

    /**
     * Set the name.
     * @param name the name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Retrieve the name.
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Retrieve the list of content elements.
     * @return the list of content elements.
     */
    @JsonIgnore
    public List<Content> getContent() {
        return content;
    }

    /**
     * Set the list of contents.
     * @param content the list of contents to set.
     */
    public void setContent(List<Content> content) {
        this.content = content;
    }

    /**
     * Retrieve the language id number.
     * @return the language id.
     */
    public int getLanguageId() {
        return languageId;
    }

    /**
     * Set the language id number.
     * @param languageId Language id number to set.
     */
    public void setLanguageId(int languageId) {
        this.languageId = languageId;
    }

    /**
     * Retrieve the language description.
     * @return the language description.
     */
    public String getLanguage() {
        return description;
    }

    /**
     * Sets the language description.
     * @param language Language description to set.
     */
    public void setLanguage(String language) {
        this.description = language;
    }

    /**
     * Retrieve the description.
     * @return the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description.
     * @param description the description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Retrieve the code.
     * @return the code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Set the code.
     * @param code the code to set.
     */
    public void setCode(String code) {
        this.code = code;
    }
}
