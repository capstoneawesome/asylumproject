package com.asylumproject.asylumproject.problemdomain;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class representing a Report object.
 *
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 */

@Entity
public class Report
{

    //Attributes

    /**
     * A unique report id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int reportId;

    /**
     * Report name to identify its purpose.
     */
    @NotBlank
    private String name;


    //Methods

    /**
     * Class default constructor.
     */
    public Report() {}

    /**
     * Class constructor for a Report object.
     * @param name the name of the report.
     */
    public Report(String name) {
        this.name = name;
    }

}
