package com.asylumproject.asylumproject.problemdomain;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;

/**
 * Class representing an FAQ content container. This class is a sub-class of Content and contains textual questions and answers.
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 *
 */

@Entity
@SQLDelete(sql = "UPDATE content SET deleted = true WHERE content_id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted <> true")
public class FAQ extends Content
{

	//Attributes

	/**
	 * List of content elements representing FAQ questions and
	 * List of content elements representing FAQ answers.
	 */
	@OneToMany(mappedBy = "faq")
	@NotBlank
	private Map<ContentElement, ContentElement> questionAnswers;

	private boolean deleted;

	//Methods

	/**
	 *
	 * @param questionAnswers
	 */
	public FAQ(@NotBlank HashMap<ContentElement, ContentElement> questionAnswers) {
//        super(languages);
        this.questionAnswers = questionAnswers;
	}

	public FAQ(){

//        super(languages);
    }


	/**
	 * Retrieves the list of questions contained in the FAQ and
	 * Retrieves the list of answers contained in the FAQ.
	 * @return questionAnswers the list of questions and answers contained in the FAQ
	 */
	public @NotBlank Map<ContentElement, ContentElement> getQuestionAnswers(){
		return questionAnswers;
	}


	/**
	 * Appends a question to the list of questions contained in FAQ object and
	 * Appends an answer to the list of questions contained in FAQ object.
	 *
	 * @param questionAnswers the question and answer to add
	 */
	public void setQuestionAnswers(@NotBlank Map<ContentElement, ContentElement> questionAnswers){
		this.questionAnswers = questionAnswers;

	}


	/**
	 * Removes a specified answer and question from the list of questions contained in FAQ object.
	 * @param QuestionAnswer the question and answer that will be removed from th list QuestionAnswers.
	 */
	public void removeQuestionAnswers(Text QuestionAnswer){

	}

}
