package com.asylumproject.asylumproject.problemdomain;

import com.asylumproject.asylumproject.broker.AccountBroker;
import com.asylumproject.asylumproject.manager.UserManager;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.sql.Timestamp;
import java.util.*;

/**
 * An abstract class representing a container which holds content elements.
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 *
 */
@Entity
@SQLDelete(sql = "UPDATE content SET deleted = true WHERE content_id = ?", check = ResultCheckStyle.COUNT)
//@Where(clause = "deleted <> true")
public abstract class Content
{
	//Attributes

	/*
	@OneToMany(mappedBy = "content", cascade = CascadeType.ALL)
	public Set<ContentLanguage> languages;

	 */
	@ManyToMany
	public Set<Language> languages;

	/**
	 * Unique id number identifying content container.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int contentId;

	@Column(insertable = false, updatable = false)
	private String dtype;
	
	/**
	 * User who created the content container. It must be an existing user.
	 */
	private int creator;

	@ManyToOne
	private User creatorUser;

	/**
	 * Date content container was created.
	 */
	@Column(updatable=false)
	@CreationTimestamp
	private Timestamp createDateTime;

	@Column
	@UpdateTimestamp
	private Timestamp updateDateTime;

	/**
	 * Date that the content container and contained elements can be made visible to public users.
	 * Not null or blank. availableDate should always be equals or grater than createDate.
	 */
	private Date availableDate;
	
	/**
	 * Date that the content container and contained elements can no longer be made visible to public users.
	 * Not null. If left blank then the content container is always visible.
	 * endDate should always be equals or grater than availableDate.
	 */
	private Date endDate;

	public Set<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(Set<Language> languages) {
		this.languages = languages;
	}


	/**
	 * Language of content container.
	 * according to ISO 639-1 standard (two-character value)
	 */
	//private String language;
	
	/**
	 * List of content elements contained in content container.
	 * List can be empty only if state = 'draft'. Otherwise cannot be empty.
	 */
	@OneToMany(mappedBy = "content", cascade = CascadeType.ALL)
	private List<ContentElement> contentElements;
	
	/**
	 * List of content tags associated with this container.
	 */
	@ManyToMany
	private List<Tag> tags;
	
	/**
	 * Description of content container.
	 * Maximum length of 500 characters.
	 */
	private String description;
	
	/**
	 * Content container title.
	 * Maximum length of 150 characters.
	 */
	private String title;


	/**
	 * Indicates whether the Content is visible to non-SysAdmins or not.
	 */
	private boolean deleted;

	//Methods

	public Content(){

	}

	public Content(Set<Language> languages){
		this.languages = languages;
	}

	/**
	 * Retrieves the content id.
	 * @return contentId content id
	 */
	public int getContentID() {
		return contentId;
	}
	
	/**
	 * Sets the content id.
	 * @param contentId content id
	 */
	public void setContentID(int contentId) {
		this.contentId = contentId;
	}
	
	/**
	 * Retrieves the user which created this content container.
	 * @return creator user which created this content container
	 */
	public int getCreator() {
		return creator;
	}

	/**
	 * Sets the creator of this content container.
	 * @param creator creator of this content container
	 */
	public void setCreator(int creator) {
		this.creator = creator;
	}

	public void setCreatorUser(User creatorUser) {
		this.creatorUser = creatorUser;
	}

	public User getCreatorUser() {
		return creatorUser;
	}

	/**
	 * Retrieves the date this content was created.
	 * @return createDateTime date this content was created
	 */
	public Timestamp getCreateDateTime() {
		return createDateTime;
	}

	/**
	 * Sets the date this content was created.
	 * @param createDateTime date this content was created
	 */
	public void setCreateDateTime(Timestamp createDateTime) {
		this.createDateTime = createDateTime;
	}

	/**
	 * Retrieves the date this content is available to be published.
	 * @return availableDate the date this content is available to be published
	 */
	public Date getAvailableDate() {
		return availableDate;
	}
	
	/**
	 * Sets the date this content is available to be published.
	 * @param availableDate the date this content is available to be published
	 */
	public void setAvailableDate(Date availableDate) {
		this.availableDate = availableDate;
	}

	/**
	 * Checks the availability of content based on date; returns true if available.
	 * @return true if available
	 */
	public Boolean isAvailable()
	{
		return true;
	}
	
	/**
	 * Retrieves the date this content is no longer available to be published.
	 * @return endDate the date this content is no longer available to be published
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the date this content is no longer available to be published.
	 * @param endDate the date this content is no longer available to be published
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	


	/**
	 * Retrieves a list of ContentElements contained in this content container.
	 * @return elements a list of ContentElements contained in this content container
	 */
	public List<ContentElement> getContentElements() {
		return contentElements;
	}
	
	/**
	 * Retrieves a specified ContentElement from this content container.
	 * @return element a specified ContentElement contained in this content container
	 */
	public ContentElement getContentElement() {
		return null;
	}
	
	/**
	 * Sets a list of ContentElements contained in this content container, replacing existing list.
	 * @param elements a list of ContentElements contained in this content container
	 */
	public void setContentElements(List<ContentElement> elements) {
		this.contentElements = elements;
	}
	
	/**
	 * Clears the list of content elements.
	 */
	public void clearContentElements() {
		
	}
	
	/**
	 * Retrieves the description of the content container.
	 * @return description a description of the content container
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description of the content container.
	 * @param description a description of the content container
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Retrieves the title of the content container.
	 * @return title the title of the content container
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Sets the title of the content container.
	 * @param title the title of the content container
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/*
	public Set<ContentLanguage> getLanguages() {
		return languages;
	}

	 */

	/*
	public void setLanguages(Set<ContentLanguage> languages) {
		this.languages = languages;
	}

	 */
	
	/**
	 * Retrieves the list of content tags for this container.
	 * @return tags the list of content tags for this container
	 */
	public List<Tag> getTags() {
		return tags;
	}
	
	/**
	 * Retrieves the list of content tags for this container replacing the existing list.
	 * @param tags the list of content tags for this container
	 */
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	
	/**
	 * Clears the list of content tags for this container.
	 */
	public void clearTags()
	{
		
	}
	
	/**
	 * Appends a ContentElement to the list of ContentElements contained in the content object.
	 * @param ce a content element
	 */
	public void addContentElement(ContentElement ce)
	{
		
	}
	
	/**
	 *Adds a collection of ContentElements to the list of ContentElements contained in the content object.
	 * @param col the collection of ContentElement to add
	 */
	public void addContentElementAll(Collection<ContentElement> col)
	{
		
	}
	
	/**
	 * Removes a ContentElement with specified id number from the list of ContentElements contained in the Content Object. 
	 * @param id id of content element to be removed
	 */
	public void removeContentElement(int id)
	{
		
	}
	
	/**
	 * Appends a content tag to the list of content tags contained in the content object.
	 * @param tag tag to be added
	 */
	public void addTag(Tag tag)
	{
		this.tags.add(tag);
	}
	
	/**
	 * Adds a collection of tags to the list of content tags contained in the content object.
	 * @param tags collection of tags to be added
	 */
	public void addTagAll(Collection<Tag> tags)
	{
		
	}
	
	/**
	 * Removes a specified content tag from the list of content tags contained in the Content Object. 
	 * @param tag tag to be removed
	 */
	public void removeTag(Tag tag)
	{
		Tag tagToRemove = null;
		for (Tag t: tags){
			if (t.getTagId() == tag.getTagId()){
				tagToRemove = t;
			}
		}
		tags.remove(tagToRemove);
	}
	
	/**
	 * Removes the content object from application.
	 */
	public void delete()
	{
		
	}

	/**
	 * Retrieves the last date time the content was updated.
	 * @return the last date time update.
	 */
	public Timestamp getUpdateDateTime() {
		return updateDateTime;
	}

	/**
	 * Sets the last date time the content was updated.
	 * @param updateDateTime the last date time update.
	 */
	public void setUpdateDateTime(Timestamp updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@JsonIgnore
	public String getDtype() {
		return dtype;
	}

	@JsonIgnore
	public void setDtype (String dtype) {
		this.dtype = dtype;
	}

	private int getContentId() {
		return contentId;
	}

	private void setContentId(int contentId) {
		this.contentId = contentId;
	}

}
