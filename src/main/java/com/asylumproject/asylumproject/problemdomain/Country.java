package com.asylumproject.asylumproject.problemdomain;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

/**
 * Class representing a Country object.
 *
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 */
@Entity
@SQLDelete(sql = "UPDATE country SET deleted = true WHERE code = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted <> true")
public class Country
{

    //Attributes

    /**
     * Unique country code as per ISO 3166-1 Alpha-2 code.
     */
    @Id
    private String code;

    /**
     * Official state name.
     */
    @NotBlank
    private String name;

    private boolean deleted;
    //Methods

    /**
     * Class constructor for a Country object.
     */
    public Country() {
    }

    /**
     * Class constructor for a Country object.
     * @param code country code.
     * @param name country name.
     */
    public Country(String code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * Retrieves the country id.
     * @return the country id.
     */
    public String getCountryCode() {
        return code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

}
