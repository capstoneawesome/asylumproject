package com.asylumproject.asylumproject.problemdomain;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

/**
 * Class representing Resource content container. This class is a sub-class of Content and contains textual resources and links.
 *
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 */

@Entity
@SQLDelete(sql = "UPDATE content SET deleted = true WHERE content_id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted <> true")
public class Resource extends Content
{

	//Attributes

	/**
	 * The type of resource being presented in this container.
	 */
	@NotBlank
	private String type;

    public Resource() {
//        super(languages);
    }

	private boolean deleted;
    //Methods

	/**
	 * Retrieves the resource type.
	 * @return type resource type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the resource type.
	 * @param type resource type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
}
