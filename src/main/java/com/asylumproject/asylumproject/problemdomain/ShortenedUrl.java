package com.asylumproject.asylumproject.problemdomain;

import net.bytebuddy.build.ToStringPlugin;

import javax.persistence.*;
import java.util.List;

@Entity
public class ShortenedUrl {

    private String full_url;
    private String shortUrl;

    @ElementCollection
    private List<Integer> tagIds;

    @Id
    private String randomString;


    public ShortenedUrl(){}

    public ShortenedUrl(String randomString, List<Integer> tagIds) {
        this.randomString = randomString;
        this.tagIds = tagIds;
    }

    public String getRandomString() {
        return randomString;
    }

    public void setRandomString(String randomString) {
        this.randomString = randomString;
    }


    public List<Integer> getTagIds() {
        return this.tagIds;
    }

    public void setTagIds(List<Integer> tagIds) {
        this.tagIds = tagIds;
    }


    public String getFull_url() {
        return full_url;
    }

    public void setFull_url(String full_url) {
        this.full_url = full_url;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

}
