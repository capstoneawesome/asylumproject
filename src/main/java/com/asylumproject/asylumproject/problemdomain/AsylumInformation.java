package com.asylumproject.asylumproject.problemdomain;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

/**
 * Class representing the AsylumInformation content container. AsylumInformation holds various ContentElements which provide information for
 * or about asylum seekers.
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 *
 */

@Entity
public class AsylumInformation extends Content
{

	//Attributes

	/**
	 * The type of asylum information being presented in this container.
	 * The available types are: 'about', 'rights' and 'process'
	 */
	@NotBlank
	private String type;

    public AsylumInformation() {
//        super(languages);
    }


    //Methods

	/**
	 * Retrieves the asylum information type.
	 * @return type Asylum information type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the asylum information type.
	 * @param type asylum information type.
	 */
	public void setType(String type) {
		this.type = type;
	}
}
