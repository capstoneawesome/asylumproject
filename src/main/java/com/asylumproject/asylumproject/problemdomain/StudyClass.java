package com.asylumproject.asylumproject.problemdomain;


import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/** 
 * A StudyClass object is created when a openStudyClass request is submitted to StudyClassManager. 
 * A StudyClass provides data being used in a teacher's(host's) session. 
 * It holds a list of students and a count of students so that the host can track the peers. 
 * 
 * @author CapstoneAwesome
 */

@Entity
@SQLDelete(sql = "UPDATE study_class SET deleted = true WHERE study_class_id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted <> true")
public class StudyClass
{

	//Attributes

	/**
	 * A unique id number identifying the teacher session.
	 * An id is alphanumeric.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String studyClassId;


	/**
	 * A list of students. It tracks students connected to a teacher's session.
	 * It must be greater than 0.
	 */
	//private List<String> studentList;

	private boolean deleted;

	/**
	 * A single integer number for total student number in a class.
	 * It must be greater than 0.
	 */
	@NotNull
	private int studentCount;


	/**
	 * A time limitation for a session created by a teacher.
	 * Time format is in milliseconds.
	 */
	@NotNull
	private long effectiveTime;


	/**
	 * A temporary URL that is shared with students to establish a connection to the teacher session.
	 * Once students have URL they are only able to access filtered contents
	 * Valid URL regular expression : "^(https?)://[-a-zA-Z0-9+&amp;@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&amp;@#/%=~_|]"
	 */
	@NotBlank
	private String url;


	/**
	 * A numbered code for a session created by teacher so that session allows only students who know the PIN.
	 * 6 digit numbers in string format. Since it is a temporary number for a session created by teacher, It must be discarded when a session is not available.
	 */
	@NotBlank
	private String pin;


	//Methods

	/**
     * Gets the study class id for a request by StudyClassManager.
	 * @return A study class id
	 */
	public String getStudyClassId() {
		return this.studyClassId;
	}
	
	/**
	 * Sets the study class id for when an object of studyClass is instantiated.
	 * @param studyClassId the id to set
	 */
	public void setStudyClassId(String studyClassId) {
		this.studyClassId = studyClassId;
	}
	
	/**
	 * Gets the list of students for a request by StudyClassManager.
	 * @return the list of student
	 */
//	public List<String> getStudentList() {
//		return this.studentList;
//	}
	
	/**
	 * Sets the list of students connected to a session.
	 * @param studentList the array list to set
	 */
//	public void setStudentList(ArrayList<String> studentList) {
//		this.studentList = studentList;
//	}
	
	/**
	 * Gets the total number of student in a class.
	 * @return An integer count number of student
	 */
	public int getStudentCount() {
		return this.studentCount;
	}
	
	/**
	 * Sets the total number of students connected to the session.
	 * @param count The total count of student to set
	 */
	public void setStudentCount(int count) {
		this.studentCount = count;
	}
	
	/**
	 * Gets the URL of a temporary web page created by a teacher.
	 * @return The URL of a temporary web page.
	 */
	public String getURL() {
		return this.url;
	}
	
	/**
	 * Sets a URL for when a teach creates an activity class.
	 * @param url the URL of a temporary web page when a teacher's session is available
	 */
	public void setURL(String url) {
		this.url = url;
	}
	
	/**
	 * Gets the PIN of a session created by a teacher.
	 * @return the PIN number of a session
	 */
	public String getPIN() {
		return this.pin;
	}
	
	/**
	 * Sets the pin number for when a teacher has an available session of activities. 
	 * @param pin the PIN number of session
	 */
	public void setPIN(String pin) {
		//further discussion is needed
	}
	
	/**
	 * Gets the time in milliseconds for available time of a session.
	 * @return The effective time
	 */
	public long getEffectiveTime() {
		return this.effectiveTime;
	}
	
	/**
	 * Sets the effective time for when a session is created by a teacher.
	 * @param time the effective time of a session
	 */
	public void setEffectiveTime(long time) {
		this.effectiveTime = time;
	}
}
