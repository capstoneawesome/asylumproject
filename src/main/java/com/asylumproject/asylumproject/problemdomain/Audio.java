package com.asylumproject.asylumproject.problemdomain;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * Class representing an Audio object that extends the Content Element class.
 * 
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 *
 */

@Entity
@SQLDelete(sql = "UPDATE content_element SET deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted <> true")
public class Audio extends ContentElement
{

	//Attributes

	/**
	 * Audio's length measured in seconds.
	 */
	@NotNull
	private double length;


	//Methods

	public Audio(){}

	/**
	 * Class constructor for an Audio object.
	 * @param language Language.
	 * @param description Description.
	 * @param length Audio's language.
	 */
	public Audio(String language, String description, double length) {
		super(language, description);
		this.length = length;
	}

	/**
	 * Retrieves the audio's length.
	 * @return the length
	 */
	public double getLength() {
		return length;
	}

	/**
	 * Sets the audio's length.
	 * @param length the length to set
	 */
	public void setLength(double length) {
		this.length = length;
	}
	
	

}
