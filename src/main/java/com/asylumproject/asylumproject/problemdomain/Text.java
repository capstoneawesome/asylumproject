package com.asylumproject.asylumproject.problemdomain;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * Class representing a Text object that extends the Content Element class.
 * 
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 *
 */

@Entity
@SQLDelete(sql = "UPDATE content_element SET deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted <> true")
public class Text extends ContentElement
{

	//Attributes

	/**
	 * Text's length.
	 */
	@NotNull
	private long length;


	//Methods

	public Text(){}

	@Override
	public String toString() {
		return "Text{" +
				"length=" + length +
				'}';
	}

	/**
	 * Class constructor for a Text object.
	 * @param language Language.
	 * @param description Description.
	 * @param length Length.
	 */
	public Text(String language, String description, long length, String filePath, long fileSize, String fileType, String state, String contentType) {
		super(language, description, filePath, fileSize, fileType, state, contentType);
		this.length = length;
	}

	/**
	 * Retrieves the text file's length.
	 * 
	 * @return the length
	 */
	public long getLength() {
		return length;
	}

	/**
	 * Sets the text file's length.
	 * 
	 * @param length the length to set
	 */
	public void setLength(long length) {
		this.length = length;
	}

	
}
