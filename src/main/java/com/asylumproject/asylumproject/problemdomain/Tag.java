package com.asylumproject.asylumproject.problemdomain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import javax.persistence.*;
import java.util.Set;

@Entity
@SQLDelete(sql = "UPDATE tag SET deleted = true WHERE tagId = ?", check = ResultCheckStyle.COUNT)
//@Where(clause = "deleted <> true")
public class Tag
{

    //Attributes

    /**
     * The tag id number.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int tagId;


    /**
     * The tag description.
     */
    private String tag;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @JsonIgnore
    public Set<Content> getContentSet() {
        return contentSet;
    }

    public void setContentSet(Set<Content> contentSet) {
        this.contentSet = contentSet;
    }

    private boolean deleted;

    @ManyToMany(mappedBy = "tags")
    private Set<Content> contentSet;

    //Methods

    public Tag(){}

    /**
     * Class constructor for a Tag object.
     * @param tagId the tag id number.
     * @param tag the tag description.
     */
    public Tag(int tagId, String tag) {
        this.tagId = tagId;
        this.tag = tag;
    }

    /**
     * Retrieve the tag id number.
     * @return the tag id.
     */
    public int getTagId() {
        return tagId;
    }

    /**
     * Set the tag id number.
     * @param tagId the tag id number to set.
     */
    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    /**
     * Retrieve the tag description.
     * @return the tag description.
     */
    public String getTag() {
        return tag;
    }

    /**
     * Set the tag description.
     * @param tag the tag description to set.
     */
    public void setTag(String tag) {
        this.tag = tag;
    }


}
