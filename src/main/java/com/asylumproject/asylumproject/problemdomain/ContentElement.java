package com.asylumproject.asylumproject.problemdomain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * Abstract class representing the Content Elements with in a Content object.
 * 
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 *
 */
@Entity
@SQLDelete(sql = "UPDATE content_element SET deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT)
//@Where(clause = "deleted <> true")
public class ContentElement
{

	//Attributes

	@ManyToOne
	@ColumnDefault("-1")
	private MapPoint mappoint;

	/**
	 * "@JsonIgnore" needed to prevent recursion with nested MapPoint
	 * @return
	 */
	@JsonIgnore
	public MapPoint getMappoint() {
		return mappoint;
	}

	public void setMappoint(MapPoint mappoint) {
		this.mappoint = mappoint;
	}

	//
	@ManyToOne
	private FAQ faq;

	//
	@ManyToOne
	private Content content;



//	private int mapPointID;
//
//	public int getMapPointID() {
//		return mapPointID;
//	}
//
//	public void setMapPointID(int mapPoint) {
//		this.mapPointID = mapPoint;
//	}

	/**
	 * Unique id number identifying content element.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(insertable = false, updatable = false)
	private String dtype;

	/**
	 * Language of content element.
	 * according to ISO 639-1 standard (two-character value)
	 */
	@NotBlank
	private String language;



	/**
	 * the filepath to the content element associated file.
	 * blank if no file is associated.
	 */
	@NotBlank
	private String filePath;

	private boolean deleted;

	/**
	 * the file size of the content element associated file.
	 * 0 if no file is associated.
	 */
	private long fileSize;

	@NotNull
	private String contentType;

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * Description of content container.
	 * Maximum length of 500 characters.
	 */
	private String description;

	/**
	 * Files could be of type: audio, video, image, text.
	 */
	@NotBlank
	private String fileType;

	/**
	 * Possible states: 'DRAFT', 'PRE-PUBLISHED', 'PUBLISHED', 'ARCHIVED'
	 */
	@NotBlank
	private String state;

	@Column(updatable=false)
	@CreationTimestamp
	private Timestamp createDateTime;

	@Column
	@UpdateTimestamp
	private Timestamp updateDateTime;

	//Methods

	public ContentElement(){}

	@Override
	public String toString() {
		return "ContentElement{" +
				"mappoint=" + mappoint +
				", faq=" + faq +
				", content=" + content +
				", id=" + id +
				", dtype='" + dtype + '\'' +
				", language='" + language + '\'' +
				", filePath='" + filePath + '\'' +
				", deleted=" + deleted +
				", fileSize=" + fileSize +
				", contentType='" + contentType + '\'' +
				", description='" + description + '\'' +
				", fileType='" + fileType + '\'' +
				", state='" + state + '\'' +
				", createDateTime=" + createDateTime +
				", updateDateTime=" + updateDateTime +
				'}';
	}

	/**
	 * Abstract constructor.
	 * @param language language.
	 * @param description description.
	 */
	public ContentElement(String language, String description) {
		super();
		this.language = language;
		this.description = description;
	}

	public ContentElement(String language, String description, String filePath, long fileSize, String fileType, String state, String contentType) {
		super();
		this.language = language;
		this.description = description;
		this.filePath = filePath;
		this.fileSize = fileSize;
		this.fileType = fileType;
		this.state = state;
		this.contentType = contentType;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * Retrieve date time of last content element update.
	 * @return date time of las update.
	 */
	public Timestamp getUpdateDateTime() {
		return updateDateTime;
	}

	/**
	 * Retrieve date time of content element creation.
	 * @return date time of creation.
	 */
	public Timestamp getCreateDateTime() {
		return createDateTime;
	}

	/**
	 * Retrieve id value.
	 * 
	 * @return the id.
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Sets the id value.
	 * @param id the id to set.
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Retrieves the language.
	 * @return the language.
	 */
	public String getLanguage() {
		return language;
	}
	
	/**
	 * Sets the language attribute.
	 * @param language the language to set.
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	
	/**
	 * Retrieves the file path.
	 * @return the filePath.
	 */
	public String getFilePath() {
		return filePath;
	}
	
	/**
	 * Sets the file path.
	 * @param filePath the filePath to set.
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	/**
	 * Retrieves the file size.
	 * @return the fileSize.
	 */
	public long getFileSize() {
		return fileSize;
	}
	
	/**
	 * Sets the file size.
	 * @param fileSize the fileSize to set.
	 */
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	
	/**
	 * Retrieves the description.
	 * @return the description.
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Retrieves the description.
	 * @param description the description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Retrieves the file type.
	 * @return the fileType.
	 */
	public String getFileType() {
		return fileType;
	}
	
	/**
	 * Sets the file type.
	 * @param fileType the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDtype() {
		return dtype;
	}
}
