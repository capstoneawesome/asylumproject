package com.asylumproject.asylumproject.problemdomain;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * Class representing an Audio object that extends the Content Element class.
 * 
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 *
 */

@Entity
@SQLDelete(sql = "UPDATE content_element SET deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted <> true")
public class Video extends ContentElement
{

	//Attributes

	/**
	 * Video's length.
	 */
	@NotNull
	public double length;


	//Methods

	public Video(){}

	/**
	 * Class constructor for a Video object.
	 * @param language Language.
	 * @param description Description.
	 * @param length Length.
	 */
	public Video(String language, String description, double length) {
		super(language, description);
		this.length = length;
	}

	/**
	 * Retrieves the video's length.
	 * 
	 * @return the length
	 */
	public double getLength() {
		return length;
	}

	/**
	 * Sets the video's length.
	 * 
	 * @param length the length to set
	 */
	public void setLength(long length) {
		this.length = length;
	}

	
}
