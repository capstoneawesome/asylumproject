package com.asylumproject.asylumproject.problemdomain;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Class representing an Image object that extends the Content Element class.
 * 
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 *
 */

@Entity
@SQLDelete(sql = "UPDATE content_element SET deleted = true WHERE id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted <> true")
public class Image extends ContentElement
{

	//Attributes

	/**
	 * Content Element's width in pixels.
	 */
	@NotNull
	private int width;


	/**
	 * Content Element's height in pixels.
	 */
	@NotNull
	private int height;


	/**
	 * Caption that describes the image.
	 * Maximum length 150 characters.
	 */
	@NotBlank
	private String caption;


	//Methods

	public Image(){}
	
	/**
	 * Class constructor for an Image object.
	 * @param language Language.
	 * @param description Description.
	 * @param width Image width.
	 * @param height Image Height.
	 */
	public Image(String language, String description, int width, int height) {
		super(language, description);
		this.width = width;
		this.height = height;
	}

	/**
	 * Retrieve the image's width.
	 * 
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Sets the image's width.
	 * 
	 * @param width the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * Retrieves the image's height.
	 * 
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Sets the image's height.
	 * 
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Retrieves the image's caption.
	 * 
	 * @return the caption
	 */
	public String getCaption() {
		return caption;
	}

	/**
	 * Sets the image's caption.
	 * 
	 * @param caption the caption to set
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}

	
}
