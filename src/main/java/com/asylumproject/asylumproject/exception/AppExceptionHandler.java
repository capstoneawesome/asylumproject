package com.asylumproject.asylumproject.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
public class AppExceptionHandler {

    @ExceptionHandler({ResourceNotFoundException.class, HttpClientErrorException.Forbidden.class})
    public ResponseEntity<Object> handleSpecificExceptions(Exception ex, WebRequest request){

        String errorMessageDesc = ex.getLocalizedMessage();

        if (errorMessageDesc == null){
            errorMessageDesc = ex.toString();
        }

        ErrorMessage errorMessage = new ErrorMessage(new Date(), errorMessageDesc);
        return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
