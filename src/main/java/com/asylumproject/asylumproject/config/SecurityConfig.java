package com.asylumproject.asylumproject.config;

import com.asylumproject.asylumproject.manager.UserManager;
import com.asylumproject.asylumproject.security.JwtAuthenticationEntryPoint;
import com.asylumproject.asylumproject.security.JwtAuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * This class handles the security managed by Spring Security, including authentication filters for log in and JWTs, and
 * API endpoint role authorization.
 * @author Yoon Jong Il, Amir Bozorgyamchi, Calder Trombley, Jorge Blanco, Jeffrey Borchert
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(SecurityConfig.class);


    @Autowired
    private UserManager userManager;
    private JwtAuthenticationEntryPoint unauthorizedHandler;


    /**
     * Instantiates a new SecurityConfig object with a UserManager and JwtAuthenticationEntryPoint objects for autowiring.
     * @param userManager the UserManager object to be autowired into the class
     * @param unauthorizedHandler the JwtAuthenticationEntryPoint object to be autowired into the class
     */
    @Autowired
    public SecurityConfig(UserManager userManager, JwtAuthenticationEntryPoint unauthorizedHandler){
        this.userManager = userManager;
        this.unauthorizedHandler = unauthorizedHandler;
    }

    /**
     * Returns a new JwtAuthenticationFilter Bean
     */
    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter(){
        return new JwtAuthenticationFilter();
    }

    /**
     * Used by Spring Security to obtain an AuthenticationManager instance.
     * @param authenticationManagerBuilder the builder that uses the UserManager instance for authentication
     * @throws Exception if the authenticationmanager builder fails
     */
    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception{
        authenticationManagerBuilder.userDetailsService(userManager)
                                    .passwordEncoder(passwordEncoder());
    }

    /**
     * Returns a new AuthenticationManagager bean
     */
    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception{
        return super.authenticationManagerBean();
    }

    /**
     * Handles HTTP 403 errors (access denied) for requests to unauthorized resources.
     * @return an AccessDeniedHandler to handle the 403 reponse
     */
    private AccessDeniedHandler accessDeniedHandler() {
        return (request, response, e) -> {
            e.printStackTrace();
            logger.debug("Returning HTTP 403 FORBIDDEN with message: \"{}\"", e.getMessage());
            response.sendError(HttpStatus.FORBIDDEN.value(), e.getMessage());
        };
    }

    /**
     * Return a BCryptPasswordEncoder used for hashing user passwords.
     */
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /**
     * Used to configure the HttpSecurity of Spring Security. This is the source of truth for determining which roles
     * are allowed to access which API endpoints.
     * @param http the instance of HttpSecurity to configure
     * @throws Exception if the HttpSecurity cannot be configured
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http.cors()
                .and()
                .csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(unauthorizedHandler)
                .and().exceptionHandling().accessDeniedHandler(accessDeniedHandler())
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()

                .authorizeRequests()
                .antMatchers("/", "/**/*.html", "/**/*.css","/**/*.js")
                .permitAll()

                .antMatchers("/static/**")
                .permitAll()

                .antMatchers(HttpMethod.GET, "/stories/**")
                .permitAll()
                .antMatchers(HttpMethod.POST, "/stories/filtered")
                .permitAll()

                .antMatchers(HttpMethod.GET, "/share/**")
                .permitAll()

                .antMatchers(HttpMethod.GET, "/map/**")
                .permitAll()

                .antMatchers(HttpMethod.PUT,"/api/auth/**")
                .hasAnyRole("TEACHER", "CONTENT_CURATOR", "SYSTEM_ADMIN")
                .antMatchers(HttpMethod.POST,"/api/auth/**")
                .permitAll()
                .antMatchers(HttpMethod.GET,"/api/auth/**")
                .permitAll()

                .antMatchers("/api/users/**")
                .hasAnyRole( "SYSTEM_ADMIN")

                .antMatchers(HttpMethod.GET, "/api/content/**")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/api/content/stories/archived")
                .hasAnyRole("CONTENT_CURATOR")
                .antMatchers(HttpMethod.GET, "/api/content/mappoints/{mapPointID}/elements/archived")
                .hasAnyRole("CONTENT_CURATOR")
                .antMatchers(HttpMethod.POST, "/api/content/**")
                .hasAnyRole("CONTENT_CURATOR", "SYSTEM_ADMIN")
                .antMatchers(HttpMethod.POST, "/api/content/{contentID}/mappoints/{mapPointID}/elements/text")
                .hasAnyRole("CONTENT_CURATOR")
                .antMatchers(HttpMethod.PUT, "/api/content/**")
                .hasAnyRole("CONTENT_CURATOR", "SYSTEM_ADMIN")
                .antMatchers(HttpMethod.DELETE, "/api/content/**")
                .hasAnyRole("CONTENT_CURATOR", "SYSTEM_ADMIN")
                .antMatchers("/api/content/filters/**")
                .hasAnyRole("TEACHER")

                .antMatchers("/api/admin/**")
                .hasAnyRole("SYSTEM_ADMIN")
                .antMatchers("/api/admin/reports/{contentId}")
                .hasAnyRole("CONTENT_CURATOR", "SYSTEM_ADMIN")
                .antMatchers("/api/admin/reports/{userId}")
                .hasAnyRole("CONTENT_CURATOR", "SYSTEM_ADMIN")
                .antMatchers("/api/admin/reports/email/**")
                .hasAnyRole("SYSTEM_ADMIN")
                .anyRequest()
                .authenticated();

        http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

}
