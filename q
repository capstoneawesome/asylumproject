[33mcommit 6172674b856550c162f68bd2dc1da9878a608b88[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31mheroku/master[m[33m)[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Apr 10 12:35:02 2020 -0600

     add antMatchers(/static/**).permitAll()

[33mcommit 800b4a80a0921def47c518b9090b816ef1f2fe28[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Apr 10 12:24:04 2020 -0600

    add 'homepage' to package.json'

[33mcommit 643f82727a21822f0ac0f22b3cfc248e76f065c5[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Apr 10 11:31:41 2020 -0600

    add missing js file

[33mcommit 7513398029fb950c8f189a92447b764e0ccc98a4[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Apr 10 11:21:12 2020 -0600

    change api path in mapwindow

[33mcommit 0516fac52ea8777f055a6e0d05630e2f514b0195[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Apr 10 11:04:44 2020 -0600

    change js path in index.html

[33mcommit 48aee19b1aa6f6676a24186fcbd0d94782b51361[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Apr 10 10:44:53 2020 -0600

    change css path

[33mcommit cc82b06360c0c0c4fbc263498154a406d9e728e3[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Apr 10 10:14:35 2020 -0600

    build react app and place output files in src/main/resources/static

[33mcommit efd4091d2909b5276aef67f284ef22be0bdd8442[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Apr 10 00:28:13 2020 -0600

    permit / to all users

[33mcommit 37672bede3e5d2cde545251cf56d99f8e95fe091[m[33m ([m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Thu Apr 9 21:48:24 2020 -0600

    Jongil
    done with comments

[33mcommit 303a14a3bd4b1f9f6a26a354144616a5155a2bf7[m
Merge: 40326d10 8325805a
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 9 21:37:52 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 40326d1074ab4977cf47d440f4acca35b902c896[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 9 21:37:44 2020 -0600

    Fix mail type

[33mcommit 8325805a4f4e7d2d4773edb81646b3925492f369[m
Merge: 59b09415 25b22444
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Apr 9 20:35:16 2020 -0600

    Merge branch 'master' of gitlab.com:capstoneawesome/asylumproject

[33mcommit 25b224444f01fb2b5926350285d4491bbee219e2[m
Merge: a5c9cce4 65973552
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Apr 9 20:10:01 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit a5c9cce4ffb31393245153ff7f9d29e205e8b49f[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Apr 9 20:09:36 2020 -0600

    merge changes

[33mcommit 6661fc84f4090e97c8ccc054999def543faecdc9[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Apr 9 20:04:36 2020 -0600

    fix incorrect signin issue

[33mcommit 65973552ceeea51de730b886b8e39a6da9e22cfd[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 9 19:53:18 2020 -0600

    Comments for documentation added

[33mcommit 4778645fd9933041889d91e274b4f518921d36c7[m
Merge: fe82e6ec 791e2b60
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 9 19:23:13 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 791e2b60c421c705e2c8b510fe474a012426778e[m
Merge: db854148 5d64ce13
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Apr 9 19:09:34 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit db854148033bdf07325dac5470efb882b5a5ecf4[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Apr 9 19:09:27 2020 -0600

    fix incorrect signin issue

[33mcommit 5d64ce13fb4896527a2e2ec464bcdf757590af8a[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Thu Apr 9 19:07:21 2020 -0600

    all comments

[33mcommit fe82e6ec69a157b9e8bf7714399529176067bd9d[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 9 19:07:18 2020 -0600

    initial commit

[33mcommit b630d0968a44f02d1c7b47b176421f7f34482725[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Thu Apr 9 19:06:56 2020 -0600

    all comments

[33mcommit 59b0941570b6a4415fa63786d74f399e3d341e6c[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Apr 9 17:37:15 2020 -0600

    css changes to some buttons including dashboard

[33mcommit 5f62d5e37f279cf473a1de2fc6b694f33baf7ea7[m
Merge: d4f3aec5 9319ed6b
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Apr 9 17:11:12 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit d4f3aec5c96251474ec72cb60bdf566a89ae91b4[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Apr 9 17:10:46 2020 -0600

    Add About Modal

[33mcommit 1c745553c056b2462eabae903435d683853e3c5a[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Apr 9 17:10:09 2020 -0600

    Add About Modal

[33mcommit 9319ed6b98f05c2b6daad9db8ad5f6b44f6783fb[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Apr 9 16:54:58 2020 -0600

    finished up comments and deleted unused pics and vids.

[33mcommit 5d2b9d48244d43c4fa5d3c164c2d2669973e2f1d[m
Merge: b2c8d7f5 36e36638
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Apr 9 16:24:14 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit b2c8d7f5f87da67372f990fe86048fce90d5208a[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Apr 9 16:24:09 2020 -0600

    add about modal

[33mcommit b8ab3e2009348341b19d24e2a418fbee47520a57[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Apr 9 16:23:04 2020 -0600

    add about modal

[33mcommit 36e36638a9ed5174606ae40ded6a38057d058bde[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Apr 9 16:18:33 2020 -0600

    some css stuff, an attribution control and lots of docstrings

[33mcommit e856b9f212538c6bfa45148f64be736a387bd1ce[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Thu Apr 9 15:48:09 2020 -0600

    Jongil
    working on user account

[33mcommit 37082e114d37b246f1f8ce803184fae6f598a2df[m
Merge: eb24f628 6487956c
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 9 14:01:05 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit eb24f62876dd8064990d04c103ed9d3858c0c92b[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 9 14:00:59 2020 -0600

    Reports front end and config updates

[33mcommit 6487956cfe11d0374b4a443702156e9c96e1866e[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Apr 9 11:42:32 2020 -0600

    shareable link does what it is supposed to. Seems like you have to be signed in though, which I would like to change. Also, seems like the backend is returning unpublished stories, which also has to change

[33mcommit cdfe61f5949ae1b8e79493d3e29af20754af27b3[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Apr 9 10:47:55 2020 -0600

    made the link generator stop generating so much, and check if response is defined before trying to use it

[33mcommit c4668b6064ee3cb955db0be7737255841a2352fd[m
Merge: 7ffb36e2 03de41cd
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Thu Apr 9 10:29:45 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 7ffb36e29b0506ea0b3e7a774e1baf7e4c1c1fab[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Thu Apr 9 10:29:27 2020 -0600

    contentController comments

[33mcommit 18c8e702454e8e9e8435e06b8a0f523ba25b52e5[m
Merge: 82223fd9 03de41cd
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 9 10:06:01 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 82223fd91e821bc2f04e4040c209ce110d4c86cf[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 9 10:05:43 2020 -0600

    Reports front end fixes

[33mcommit 03de41cda2eefbc981677ba1b04d2ff3739adef3[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Apr 9 00:37:34 2020 -0600

    user can get generated filter link and other bug fixes

[33mcommit 35a528d7af59b89b75febe642640aac60aaedecf[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Wed Apr 8 22:54:55 2020 -0600

    jongil
    working on user account

[33mcommit e0055284c550b45784b3e73e9948f08f0dd60f03[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Wed Apr 8 22:40:26 2020 -0600

    jongil
    working on user account

[33mcommit ac4675583ffc48574a860b2796d78f361773723a[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Wed Apr 8 21:09:26 2020 -0600

    Long filter tags auto-wrap, map points without media no longer display carousel, story list auto-wraps and correctly generates language names, languages filter properly

[33mcommit 08c051679fe7c3c4b413281e504cd4db22619703[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Wed Apr 8 20:13:12 2020 -0600

    jongil
    working on user account

[33mcommit fc85b5a334cdbfb9b3975b638e1e6c8e72fef03f[m
Merge: a4d96ffc 5c3ac173
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Apr 8 17:52:13 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit a4d96ffcc2210489f144c9e91b9e6bf602012f50[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Apr 8 17:52:07 2020 -0600

    bug fixes

[33mcommit 5c3ac17306ec31f2728d582492e4843e9c2bacde[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Wed Apr 8 17:45:40 2020 -0600

    Finished. Audio, Text, Image, and Video all work. Lots of updates to lots of stuff. Html parser added for story text

[33mcommit 5a4ce3ec48c80dbe5ebde01f8b220a8896c2f218[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Wed Apr 8 17:04:56 2020 -0600

    Jongil
    Working on user account

[33mcommit ee6cc95dd17a2ca2dc076479694b845c50fe6c20[m
Merge: 1314af1a 8759b231
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Wed Apr 8 15:11:02 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 1314af1a405cda6aba0fc53acb3f5cf9a448c0c3[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Wed Apr 8 15:10:55 2020 -0600

    Front end fixes (Story Events)

[33mcommit 8759b2316412c7430ab3d54982cec8e61122bf65[m
Merge: 2ddc958f d0adb2b4
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Apr 8 14:53:50 2020 -0600

    change file upload limit to 100MB

[33mcommit 2ddc958ffcfc677f9af68988321ab1d566bae2d2[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Apr 8 14:50:46 2020 -0600

    change file upload limit to 100MB

[33mcommit d0adb2b437eca09d0089e15ae0fc88762277ea24[m
Merge: afe4672c 7bc9d879
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Wed Apr 8 14:09:40 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit afe4672c32ef12228e6d92ce1b1f5d31ef1896d1[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Wed Apr 8 14:09:26 2020 -0600

    contentController comments

[33mcommit 7bc9d879bad8cd3e1682a6e8cdb600898e249b5a[m
Merge: b5f270ae 2ee1f166
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Apr 8 14:07:36 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit b5f270ae671a947f618470c340ca63b40273e226[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Apr 8 14:07:23 2020 -0600

    bug fixes

[33mcommit 2ee1f166db528feb18c9e60bce98cb7bb3df7d27[m
Merge: eb5a1064 4c65377e
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Wed Apr 8 13:17:59 2020 -0600

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       src/main/java/com/asylumproject/asylumproject/controller/ContentController.java

[33mcommit eb5a10640a70896a9197c5183f6a6bf4936beea1[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Wed Apr 8 13:17:10 2020 -0600

    contentController comments

[33mcommit 4c65377e143dacf5740484f4f9aa84fb57b7cdd4[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Wed Apr 8 01:23:23 2020 -0600

    trying to get the stories from the backend to run,but languages are messed up and the carousel can only handle a specific number of elements. Mostly there though, just have to convert the text from html better for the text elements

[33mcommit 87250fc65c11e3be362a7d3ff70ef5775631cb7f[m
Merge: acf6d54c 5734788c
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Apr 7 23:56:28 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit acf6d54cb0775be2c7c8baea7f63fca67a4f2819[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Apr 7 23:56:18 2020 -0600

    User manager bug fix

[33mcommit 81ddb7502017299060e5077dac6c72097381c005[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Apr 7 23:54:25 2020 -0600

    Reports and Database management front end touchups

[33mcommit 5734788c70fd56f283fd84c3b1a0b3f1a92cb983[m
Merge: 1d028668 ca35a77f
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Tue Apr 7 15:55:49 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 1d0286683ded9a9f114bd8da52289a9331181c78[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Tue Apr 7 15:55:43 2020 -0600

    fix map point picker zoom level

[33mcommit ca35a77f25e86a3559315804206f5aafefbb8065[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Apr 7 08:32:40 2020 -0600

    Reports front end fixes
    Updates event logger
    Started documentation

[33mcommit 111c6d66ca65fc5f4030b90d8ed1297a2d6f15c8[m
Merge: 1d5407ea 4b3b28aa
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Apr 7 08:14:59 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 1d5407eacc4910edbb1f359b74d8c293c91d07ed[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Apr 7 08:14:39 2020 -0600

    Reports front end fixes
    Updates event logger
    Started documentation

[33mcommit 4b3b28aa588c0688623019f284eecb2257c365ce[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Mon Apr 6 23:41:44 2020 -0600

    Added main menu

[33mcommit b1c94d6a83c35af59c100f75880f67e9d837a7fc[m
Merge: 83c0abc4 cf241a7b
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Apr 6 21:10:01 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 83c0abc42245c87dc33de5288dde1d1d07237e82[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Apr 6 21:09:50 2020 -0600

    fix country name bug

[33mcommit cf241a7b356acc9bb1008c837a87d1d62de0f88a[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Mon Apr 6 20:07:57 2020 -0600

    Fixed accordion bug that crashed the 3rd-party js if you tried to select accordion headers after an odd number of tag actions (wtf is that); made rating tags work as tag objects, made language selector and story text fully functional

[33mcommit 302da5846970e02a111feb228ab5eccff8873894[m
Merge: 444a010b 61211320
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Apr 6 19:31:40 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 444a010b863817fda9eb7f3e931c3071996265c3[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Apr 6 19:31:36 2020 -0600

    add comments

[33mcommit c68dbb333a24d50fa1bb99dd24b997edc528ea3c[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Apr 6 19:27:13 2020 -0600

    remove View folder and Filter classes

[33mcommit 61211320340014c6a1cdea3045647c8746c9e964[m
Merge: 580acaf8 48119ae8
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Apr 6 18:33:03 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 580acaf89cb6662ad0ea9edb10b174ef6c83acf2[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Apr 6 18:32:54 2020 -0600

    Javadoc comments added.

[33mcommit 48119ae8a37b38b99ac2c997007066cd99447608[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Apr 6 17:47:11 2020 -0600

    simplified shareable link endpoints

[33mcommit 02371a2f42c323ccfb8de399f3225295abe057cb[m
Merge: 0e77011f b001a8a2
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Apr 6 16:55:43 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 0e77011f95883f216742c8b705cd3e958ad2ae98[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Apr 6 16:55:37 2020 -0600

    remove method from shareable link code

[33mcommit b001a8a2b782eb5a41505fd905dfee419c1423c3[m
Merge: 1843f26a b7be8cd6
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Mon Apr 6 16:40:10 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 1843f26aa3acfd4ae0a1ace73cb2ba6deec9f928[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Mon Apr 6 16:38:48 2020 -0600

    languagdes

[33mcommit b7be8cd67d4d50ec4dde663c247548ea12d757a0[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Apr 6 15:23:26 2020 -0600

    Reports front end fixes
    Updates event logger
    Started documentation

[33mcommit f030bb25d3740d0700ffeea670438a64bf0bf9e4[m
Merge: a8da8a99 c86bac03
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Apr 6 15:15:33 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit a8da8a99d883f3607da98b62b477a7e9bd0d4f98[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Apr 6 15:15:06 2020 -0600

    Reports front end fixes
    Updates event logger
    Started documentation

[33mcommit 27aaf42c9ee0bd932c2ec4959d0225b142b92feb[m
Merge: 872222d4 c86bac03
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Apr 6 13:18:53 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 872222d430e20ffff2d7d57dce35d6a557c36145[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Apr 6 13:18:47 2020 -0600

    add pageloader to manage stories screen

[33mcommit c86bac03635c76ab3e725c8c10d3440fef923269[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Mon Apr 6 12:09:26 2020 -0600

    jongil
    working on evenhandler

[33mcommit 24059b639522d76466bc41c6d81a4a27e84f2343[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Mon Apr 6 01:03:10 2020 -0600

    FilterControl and story catalogue and a bunch of other changes

[33mcommit 639be7aa35a3b2fcc43c0d327310e31923587a52[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Mon Apr 6 00:17:27 2020 -0600

    jongil
    working on layout

[33mcommit aa956d39fd645de294087b20325f07328cb72092[m
Merge: d898922a a0dcc294
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sun Apr 5 22:36:22 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit d898922a04651b28b2e6a97b21434d06491e69d6[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sun Apr 5 22:36:11 2020 -0600

    Modified soft delete methods for User, Content, ContentElement, MapPoint, Tag

[33mcommit a0dcc29489deb64daf13a3c73f9953de73ac74d1[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 21:28:34 2020 -0600

    jongil
    working on layout

[33mcommit 70856cef601c7e056daf5d0bfd626bd7f55cab65[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 21:24:17 2020 -0600

    jongil
    working on layout

[33mcommit fd28b1edbcb8155617ed32917302d5a8d192a7bd[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 18:51:42 2020 -0600

    jongil
    working on layout

[33mcommit 87fe62702583863778a395f6998d66c60450cadb[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sun Apr 5 18:44:46 2020 -0600

    Fix for backend crashing bug

[33mcommit 04a29e634d5c899465d77b9331fe2537ab6fc501[m
Merge: 5d9f9435 5f519043
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sun Apr 5 18:38:00 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 5d9f9435dec24d60753495cac06cc152216b890f[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sun Apr 5 18:36:04 2020 -0600

    Reports front end fixes
    Updates event logger
    Started documentation

[33mcommit 5f51904333ee3b0c13edeb9ba76e1b5ea006f4a4[m
Merge: 1e86c41b eec82de8
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Apr 5 18:33:54 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 1e86c41ba54cd64913418a5a9c210dc172aa6afb[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Apr 5 18:33:49 2020 -0600

    Add new menu to side of content manager

[33mcommit 2281e3c65b1482032e10118a19b46a8a112bf84f[m
Merge: 3aab69a7 eec82de8
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sun Apr 5 18:31:39 2020 -0600

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       src/main/webapp/webapp/src/components/Admin/Adminboard/Report/Reports.jsx

[33mcommit 3aab69a7a6de9649c1bc0533f153642f6c1a4449[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sun Apr 5 18:29:54 2020 -0600

    Reports front end fixes
    Updates event logger
    Started documentation

[33mcommit eec82de8bb2ce39732b79ca5a5719e7ffe5720b9[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 17:59:50 2020 -0600

    jongil
    working on layout

[33mcommit fa23dc435e5683c56cc1c280361378f6dbc72335[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 17:45:36 2020 -0600

    jongil
    working on layout

[33mcommit 6fad25beed572247fa70af789cc45770332ddce3[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 17:34:05 2020 -0600

    jongil
    login button

[33mcommit 6880bff38a2250b023c35f6c2220fa3c199ecb45[m
Merge: 00477e05 33278875
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Apr 5 16:05:16 2020 -0600

    Add ErrorHandler.jsx, fix conflicts

[33mcommit 00477e059df04dde3a4774e21df401ef7d71e85d[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Apr 5 16:01:26 2020 -0600

    Add ErrorHandler.jsx

[33mcommit 33278875abf6b94e3f072800a744687602a342fb[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 15:59:39 2020 -0600

    jongil
    working on layout

[33mcommit e0610140305a8b40b4d85221fd96b1c5f6d76db9[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 14:40:50 2020 -0600

    jongil
    working on user list

[33mcommit 96ef5601dfa7aedcd05357dcd3844e90801652f5[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 13:48:50 2020 -0600

    jongil
    working on user list

[33mcommit 8a8eebaa83d6c442efe392df17b4d099cf6a5c8d[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 13:42:58 2020 -0600

    jongil
    working on user table

[33mcommit 114e66675e9ea1a7c07d167af81763076fbe86a0[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 12:24:54 2020 -0600

    jongil
    lunch time

[33mcommit 30b76431a3a46ec8df3c09728975bede5ed2b71c[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 12:05:36 2020 -0600

    jongil
    working on layout

[33mcommit e4a553240936d66c176b3693f0f21030f0aafc55[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 10:20:00 2020 -0600

    jongil
    working on layout

[33mcommit 44b9b4f2a1b78657193c6e58796ce034e8c3488b[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 09:42:02 2020 -0600

    jongil
    working on layout

[33mcommit a58851bf47ec324835d79f276a65f51e01bcdbd2[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 09:15:20 2020 -0600

    jongil
    working on layout

[33mcommit d0b339a89dadc30f03b3db9ec3ffac85c6b87270[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 07:57:09 2020 -0600

    jongil
    working on layout

[33mcommit 70d811d60f05fc81bd4431d2ba424854f952946b[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 07:39:37 2020 -0600

    jongil
    working on layout

[33mcommit 5d8d60fde631126c6c25974ee231703282b376ea[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 07:02:59 2020 -0600

    jongil
    working on layout

[33mcommit 14120f8c2cef651b392f97e5b9708709d7893f93[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 06:27:34 2020 -0600

    jongil
    working on layout

[33mcommit e15bd3e9701587b8b47671505b55846e1dbb18a8[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Sun Apr 5 05:35:11 2020 -0600

    Jongil
    working on layout

[33mcommit 09612d8adabf2f542acddad5bc7e54e1006e2292[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sat Apr 4 23:59:02 2020 -0600

    Add comments for all Content Manager components and files in src/libraries

[33mcommit 4aa878564e4e61fb82ac343705b17f521bfae61b[m
Merge: c2dd4c4e 73a26397
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sat Apr 4 22:25:04 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit c2dd4c4e7642897e0d9e73159de24f42aa8af6c8[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sat Apr 4 22:24:59 2020 -0600

    Add Shareable link back end code. Add jsdoc and jsdoc2md

[33mcommit 73a2639732e040b04e1eac2f02fd8f3b90ce8926[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Apr 4 16:49:03 2020 -0600

    Reports front end fixes
    Updates event logger
    Started documentation

[33mcommit 8d8a67e043d93496c86c37753b8cd948d81d945a[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Apr 4 14:19:32 2020 -0600

    Reports and Database UI fixes

[33mcommit be9e601b00cb6442277b6e86f057921764290237[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Apr 4 10:27:00 2020 -0600

    Reports front end fixes
    Updates event logger
    Started documentation

[33mcommit 25637734f2e34292a298a312a784f80b61d1f3b3[m
Merge: 3af22025 e1474430
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Apr 4 00:16:08 2020 -0600

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       src/main/java/com/asylumproject/asylumproject/controller/ContentController.java

[33mcommit 3af22025d49b711f5f7ad51648c633d8368e5cf4[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Apr 4 00:11:56 2020 -0600

    Reports front end fixes
    Updates event logger
    Started documentation

[33mcommit e147443093959d0da4c139f1d6ec14a60436b676[m
Merge: 5f8eff5f 75dd54ea
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Apr 3 17:48:01 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 5f8eff5f086da0dfdcac3ae30711993f6fe97d8d[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Apr 3 17:47:56 2020 -0600

    manage stories table working

[33mcommit f7913c83f799866658863d43a3c639ea0b518406[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Fri Apr 3 16:26:56 2020 -0600

    Reports front end fixes

[33mcommit 75dd54ea0ff873855ed3f71c85d43fdd9bdb979d[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Fri Apr 3 12:59:23 2020 -0600

    Sysadmin menu updates

[33mcommit 471e1993361e72fc83a1865b751aaaa8dd3ad73f[m
Merge: 7664db1f 52e19081
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Apr 3 00:45:31 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 7664db1f6c9556230fb82015e7933e4236dd8edb[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Apr 3 00:30:38 2020 -0600

    many content manager changes

[33mcommit 52e19081e92b9280f3c3782042fdfe0e35d4545d[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 2 23:15:35 2020 -0600

    Reports front end fixes

[33mcommit 941cae030952675a91de9c49a9ae5f16279b4b52[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 2 23:15:15 2020 -0600

    ignore .class files

[33mcommit 6a69551c3945b27b4bf1b9e144cf8942814830e1[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 2 23:14:10 2020 -0600

    Reports front end fixes

[33mcommit eecbc014c2e1783fd3100e52eabee01dba6bab8c[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 2 23:13:41 2020 -0600

    Logging event for ContentController

[33mcommit 08149548489b438f51bf53a003dfd5f2c33b0846[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 2 20:55:10 2020 -0600

    Reports front end fixes

[33mcommit 1044f5508fc74b7d50ee40c32cb7a803d08d6212[m
Merge: ef760c5a d425194a
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 2 20:23:08 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit ef760c5aa928d84a101d4b6ff9d674ef0593fdd1[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 2 20:23:01 2020 -0600

    Database backup fixes

[33mcommit d425194a5d2942129d50ed44f2992386f793662b[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Apr 2 18:27:35 2020 -0600

    minor changes in story and mappoint

[33mcommit 94185345abe1d1d013862cf0135636f3fe069800[m
Merge: 5f950e5b dab4bd48
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Apr 2 16:57:29 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 5f950e5ba0adfc2da97169918c479bd8be4c0d6b[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Apr 2 16:55:36 2020 -0600

    content manager changes

[33mcommit dab4bd485ed02d21f770b3d24fadf128e2ec551f[m
Merge: 36afab21 0a57ca48
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 2 16:38:38 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 36afab21d6f5420229794e222272f9d2e76525a4[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Apr 2 16:38:29 2020 -0600

    Database management UI

[33mcommit 0a57ca48ec417e2a204a46c5c7cc358bb74ffc04[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Apr 2 10:28:00 2020 -0600

    class file removal

[33mcommit 7b97055b52b4ab1fdaa94956a893033b5890f7c5[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Wed Apr 1 20:47:41 2020 -0600

    Backups UI implementation

[33mcommit 82444f1a95c41df976dc1914df4fb4803f44529d[m
Merge: bb1e5717 fa419d62
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Apr 1 16:33:53 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit bb1e571792ae042106f998acb60685634203135b[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Apr 1 16:33:47 2020 -0600

    Manage Stories table finished

[33mcommit fa419d624c84af1717c6315042da3a7d997679a8[m
Merge: c48000d8 6bedaad3
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Wed Apr 1 16:31:42 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit c48000d8dc1b860cfb1859113f33763bfb96c8d6[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Wed Apr 1 16:31:24 2020 -0600

    languagdes

[33mcommit 6bedaad36e00e74c44856b84b09504855c888edf[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Wed Apr 1 15:00:22 2020 -0600

    Reports front end fixes

[33mcommit 97d7f59d4429349ea12be2273227436d0e315b89[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Wed Apr 1 01:14:38 2020 -0600

    Backups front end added (work in progress)

[33mcommit 146d3fed8d39187df8d59767d1d56f2dce8148a2[m
Merge: dae12b30 b7a6d225
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Mar 31 19:07:36 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit dae12b30075a9d8c2c94a27006786608adb74873[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Mar 31 19:07:25 2020 -0600

    Reports front end fixes

[33mcommit b7a6d225789b7b45ea55c5839341d687fa648c72[m
Merge: a46a51c6 b30aad12
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Tue Mar 31 12:27:46 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit a46a51c64a82ff7460bb50f43658c057f9963f37[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Tue Mar 31 12:27:31 2020 -0600

    Fix getting available languages on Content

[33mcommit b30aad1254cffe42a307f1849a876b188558fd0d[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Mar 31 09:44:51 2020 -0600

    Reports front end fixes

[33mcommit bc3a07ad7e5412a946d938c5ddafe72adfe84dfa[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Mar 31 09:39:40 2020 -0600

    Reports front end fixes

[33mcommit c375752ce4c500a31dca98e47c2801044b316f9b[m
Merge: 1c9570d8 b9ca5eae
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Mar 31 08:52:00 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit b9ca5eaef9b36b1cfbd9461c29ce1665de547275[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Tue Mar 31 07:42:40 2020 -0600

    stories can be created in the content manager start to finish

[33mcommit 1c9570d8231cd0a44f9207e406e2bfee894e172c[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Mar 30 17:56:12 2020 -0600

    Reports front end fixes

[33mcommit 4f3cdfc98b36ce7a746a6364d9ff29dcb1b0257b[m
Merge: 9ccdcbbb 7199ba6b
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Mar 30 17:51:16 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 9ccdcbbba4cd1d2354e7ac04836441ecd9821911[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Mar 30 17:46:21 2020 -0600

    Reports front end fixes

[33mcommit 7199ba6b831631b4fc0057c5ff69bc70966cd662[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sat Mar 28 15:54:19 2020 -0600

    available languages added to content

[33mcommit c9c994d8258f7221073ab685757be9e67235bf0d[m
Merge: 757e5b6a 35b712ca
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sat Mar 28 15:08:09 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 757e5b6a16b4bb31e5ac535ed38893c8017784f6[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sat Mar 28 14:59:27 2020 -0600

    content changes

[33mcommit 35b712caebe2c2208316f10645e51357a47033f4[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Sat Mar 28 14:57:36 2020 -0600

    languagdes

[33mcommit a9a543fcf3eaed90b4bce8f409d293f76c6f342a[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Mar 27 16:28:27 2020 -0600

    deleting content content element implemented

[33mcommit 809cc4c7b08053ffd6245b8bbc69531107e7ccba[m
Merge: 846dd40c 0b8a545d
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Mar 26 18:09:10 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 846dd40c65d520cf19386069a7785c2269931f6d[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Mar 26 18:09:05 2020 -0600

    Add listPublishedStories()

[33mcommit 0b8a545d4b205d7186cd479c7dbe354d22e6136f[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Mar 26 18:03:45 2020 -0600

    Reports front end fixes

[33mcommit d202d6a3821c67ec845a6d1198479f0838e754cd[m
Merge: 59263664 58b5dcc6
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Mar 26 16:39:29 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 59263664282255d181fa39515f69acd502837873[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Mar 26 16:39:23 2020 -0600

    Add get all stories method to controller

[33mcommit 58b5dcc6839c5ba9c6e3f6fd372c7d0f15f7bbce[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Mar 26 13:06:28 2020 -0600

    more video bug fixes for various scenarios I found were broken

[33mcommit cda660fa05636f8a4643671f4275aad94f554dd6[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Mar 26 12:38:26 2020 -0600

    video now pauses when you close the carousel

[33mcommit b8ec306e51e03aa24383a93b29a1cb801683df5a[m
Merge: 360223c2 07a70631
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Mar 26 10:24:52 2020 -0600

    creation of map points and text elements in content manager

[33mcommit 360223c2c0a40b16c673134a477eb357df08b932[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Mar 26 10:04:59 2020 -0600

    creation of map points and text elements in content manager

[33mcommit e4d815058945c7181da4789d1b3c7e2ebef08172[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Mar 26 09:27:29 2020 -0600

    media carousel now opens and closes properly

[33mcommit 07a706319f36a31f3ecfd9a00baa95b383f468c1[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Mar 26 08:44:21 2020 -0600

    Reports front end fixes

[33mcommit 88c4c7ea54291d2d86ef9c00b0c6334b7a1a169a[m
Author: Jorge Blanco <jorge.blanco@edu.sait.ca>
Date:   Thu Mar 26 14:37:42 2020 +0000

    Delete AsylumprojectApplicationTests.class

[33mcommit 24258df5335fc5cd8b754b307f493cbf67735000[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Mar 26 08:31:56 2020 -0600

    images and videos added to story view

[33mcommit 3dfafeef598b5f56a35c3525063669fc70a0e2ff[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Wed Mar 25 19:15:57 2020 -0600

    Reports front end fixes

[33mcommit eed0374acae61d55923a44024cac9682736232e1[m
Merge: da141e57 087ef9af
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Mar 25 11:26:57 2020 -0600

    update .gitignore to exclude .gradle folder

[33mcommit 087ef9af2d1ba5cffb2474f8e77ed5db10d64895[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Wed Mar 25 11:15:40 2020 -0600

    added /build/classes/** to git ignore

[33mcommit da141e579c250a8a54c2670d56bfe21609f591f5[m
Merge: 2daf4023 2ccfbe7d
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Mar 25 11:02:42 2020 -0600

    Add updateContent to content controller

[33mcommit 2daf4023a8e2255a19a0368171acc506fa0079f1[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Mar 25 11:01:00 2020 -0600

    Add updateContent to content controller

[33mcommit 2ccfbe7dca451de58320569ef30bb05631cf7089[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Wed Mar 25 10:56:32 2020 -0600

    added .class files to gitignore cuz they were causing problems

[33mcommit 504686667b5dfac94ff6f89258897c6df14fdc6c[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Wed Mar 25 10:31:48 2020 -0600

    Updated Content Class added userCreator attribute
    Update Reports frontend

[33mcommit 71cd76ff71d1ee395b66a003aed5010971820056[m
Merge: f8596ac6 163a37ef
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Wed Mar 25 10:28:10 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 163a37efae6ddda2658f5455015fe392264700b8[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Wed Mar 25 10:22:21 2020 -0600

    adding in some previous csschanges -- ignore

[33mcommit f8596ac6a91c00687d83310cb49ae60883c4e9ab[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Wed Mar 25 10:18:50 2020 -0600

    Updated Content Class added userCreator attribute
    Update Reports frontend

[33mcommit 467dd543f4b0376b1e2fa2d9859c679086c984be[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Wed Mar 25 07:29:06 2020 -0600

    Update Reports frontend

[33mcommit d66ce534a82ed3ded942046c2d2ccb397d8589e8[m
Merge: 16e5e05d 11a28eb3
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Mar 24 23:08:55 2020 -0600

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       build/classes/java/main/com/asylumproject/asylumproject/broker/ContentBroker.class

[33mcommit 16e5e05d717fec3dd4a6ab942276b899e43def0a[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Mar 24 23:08:01 2020 -0600

    Update Reports frontend

[33mcommit 11a28eb3811a5fe474fc8729e2c550ddf0f443f3[m
Merge: 857e4e5e 800bde2e
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Tue Mar 24 21:36:47 2020 -0600

    Content element file downloads implemented

[33mcommit 857e4e5ed080ec2a05532b505ab7446f9effe0e9[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Tue Mar 24 21:28:59 2020 -0600

    Content element file downloads implemented

[33mcommit 800bde2e0c3a4a78af24b6f6ac1dea7c40d29897[m
Merge: c707efd5 48950f11
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Mar 24 18:35:15 2020 -0600

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       .gradle/6.0.1/executionHistory/executionHistory.bin
    #       .gradle/6.0.1/executionHistory/executionHistory.lock
    #       .gradle/6.0.1/fileHashes/fileHashes.bin
    #       .gradle/6.0.1/fileHashes/fileHashes.lock
    #       .gradle/6.0.1/javaCompile/classAnalysis.bin
    #       .gradle/6.0.1/javaCompile/javaCompile.lock
    #       .gradle/6.0.1/javaCompile/taskHistory.bin
    #       .gradle/buildOutputCleanup/buildOutputCleanup.lock
    #       .gradle/buildOutputCleanup/outputFiles.bin
    #       build/classes/java/main/com/asylumproject/asylumproject/broker/ContentBroker.class
    #       build/classes/java/main/com/asylumproject/asylumproject/controller/ContentController.class

[33mcommit c707efd514a603304891043056feffd55ac78d35[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Mar 24 18:34:08 2020 -0600

    Reports and Backups Generation (backend)

[33mcommit 48950f113c79ef551827484e7bd1194ce40e4701[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Tue Mar 24 11:04:09 2020 -0600

    big story update. Missing video and image components which require a rework

[33mcommit 7de41609bba826286dd606b8c5262a214e15a848[m
Merge: c83447ea d991f155
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Mar 23 14:43:02 2020 -0600

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       .gitignore

[33mcommit c83447eab01b9635aa3c5bb3a3f1d4e3827eefb8[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Mar 23 14:33:22 2020 -0600

    Reports and Backups Generation (backend)

[33mcommit 3eb5552d5286665d405cc5a14356d1ca005c05ea[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Mar 23 14:32:21 2020 -0600

    Reports and Backups Generation (backend)

[33mcommit dcb0d7cc435815400a2b9efdb41f6de372c8bb29[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Mar 23 14:29:51 2020 -0600

    Reports and Backups Generation (backend)

[33mcommit d991f1558efeebd3be1f120300eb03385a9f22b5[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 21:16:36 2020 -0600

    fix Story.java

[33mcommit 1a34871d54052c1ab5ed0a6ab799454df1793a5c[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 20:30:52 2020 -0600

    uploads to correct folders working

[33mcommit 9fbe2419b26a2ab627a827622db07f5a85538fba[m
Merge: e1a14784 146c0d80
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 18:01:02 2020 -0600

    content manager fixes

[33mcommit e1a14784dec61c900766be5806704e2dd60a700b[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 17:59:33 2020 -0600

    content manager fixes

[33mcommit d1e3f1984f386da03b3f68e7c11100263c2c83f4[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 17:59:08 2020 -0600

    content manager fixes

[33mcommit 146c0d80636040d40596016fd2d0b44ee1c18b6c[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Sun Mar 22 17:13:01 2020 -0600

    ContentController

[33mcommit 5ddcde4d11b7c9b023d2310a34c1c0c2cc162959[m
Merge: 9bd20393 eff4c8bf
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Sun Mar 22 16:21:05 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 9bd20393ce66250beeaa3d6156ba2ab980f8025d[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Sun Mar 22 16:16:07 2020 -0600

    ContentController

[33mcommit eff4c8bf9b9ac4dfe357f86b5b9e7c74f07cd62b[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 16:14:55 2020 -0600

    fix typo

[33mcommit 9c9f59bfbddcc8a3f2953a7f0e063bc0837aa14a[m
Merge: 5f3485ff c7206299
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 15:52:58 2020 -0600

    fix

[33mcommit 5f3485ffdab11369143d1bb63be41becd600f8e0[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 15:45:25 2020 -0600

    add gradle files

[33mcommit cbae6bb7a98fd3fcd87f63629f6df0587297501e[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 15:43:51 2020 -0600

    add gradle files

[33mcommit c7206299a5ecfe16caafcd694663399bc70c2091[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Sun Mar 22 15:43:05 2020 -0600

    ContentController

[33mcommit 887c2d636b6efb54e1b62d67eff8dea7bc78eeee[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 15:36:22 2020 -0600

    add missing story fields

[33mcommit f19e64c6b9a49752034f90e80ed4382a04d66e20[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 15:26:00 2020 -0600

    fix storyID issue

[33mcommit 645a62225deaee41dbeb8564629e1dfd7de0bf8b[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 15:18:39 2020 -0600

    change mappointrequest

[33mcommit 948d6a1c7de304121a88e7f515eab03e9072501a[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 15:11:29 2020 -0600

    add contentid param to uploads

[33mcommit 20bf10379ab95d2fed96e1b6a0de21a5b50e7515[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Sun Mar 22 13:04:32 2020 -0600

    ContentController

[33mcommit 8797955d75ea340dcf1a730de2ec650e93700fca[m
Merge: 6e531d11 8cb55c3c
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 12:59:47 2020 -0600

    file upload working

[33mcommit 6e531d1183c3ed1f91ee2a03f67eb919ea86da8e[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 12:56:49 2020 -0600

    file upload working

[33mcommit 17972e80cd50a903b00ad4a8de5ea79eb87a1469[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 22 12:52:18 2020 -0600

    file upload working

[33mcommit 8cb55c3cbfbdf45d40acf4b32b75a14a8318c22f[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 21 22:34:59 2020 -0600

    Reports and Backups Genearation (backend)

[33mcommit 7f5d6a83be4aec8b7d0acc576059a370029c2c8d[m
Merge: 035e23d4 9c9230fb
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Fri Mar 20 22:08:06 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 035e23d48b4dd1e59228d279e7eb8d745055a6a4[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Fri Mar 20 22:07:58 2020 -0600

    Reports and Backups Genearation (backend)

[33mcommit 9c9230fbb2c098fcb68901afddb709dd1a59ee73[m
Merge: 4140a0ef c2dca619
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Mar 20 22:01:13 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 4140a0efb65a86c7455f78a2409a6b43115a77a2[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Mar 20 22:00:52 2020 -0600

    Routing for content manager pages moved from App component to sub-components

[33mcommit c2dca619e96d9fb8693fc91d85e091181c59bedd[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Fri Mar 20 20:10:17 2020 -0600

    Reports and Backups Genearation (backend)

[33mcommit 27464de7365ee0bafd39705b52c6c66cb49ad9a7[m
Merge: cc63d473 1bfcfff2
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Fri Mar 20 19:30:12 2020 -0600

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       .gradle/6.0.1/executionHistory/executionHistory.bin
    #       .gradle/6.0.1/executionHistory/executionHistory.lock
    #       .gradle/6.0.1/fileHashes/fileHashes.bin
    #       .gradle/6.0.1/fileHashes/fileHashes.lock
    #       .gradle/6.0.1/javaCompile/classAnalysis.bin
    #       .gradle/6.0.1/javaCompile/javaCompile.lock
    #       .gradle/6.0.1/javaCompile/taskHistory.bin
    #       .gradle/buildOutputCleanup/buildOutputCleanup.lock

[33mcommit cc63d473a00c04f216ead073cce7becd1f3bffad[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Fri Mar 20 19:29:20 2020 -0600

    Reports and Backups Genearation (backend)

[33mcommit 1bfcfff2110f5d771f6c96b24cec1394ebb75e57[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Mar 20 18:40:11 2020 -0600

    fix Story json object

[33mcommit cf29a7be3f280e828b65f02c2c247cab14ac03ac[m
Merge: 05c94445 cd8aa37c
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Fri Mar 20 17:44:00 2020 -0600

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       .gradle/6.0.1/executionHistory/executionHistory.bin
    #       .gradle/6.0.1/executionHistory/executionHistory.lock
    #       .gradle/6.0.1/fileContent/fileContent.lock
    #       .gradle/6.0.1/fileHashes/fileHashes.bin
    #       .gradle/6.0.1/fileHashes/fileHashes.lock
    #       .gradle/6.0.1/fileHashes/resourceHashesCache.bin
    #       .gradle/6.0.1/javaCompile/classAnalysis.bin
    #       .gradle/6.0.1/javaCompile/jarAnalysis.bin
    #       .gradle/6.0.1/javaCompile/javaCompile.lock
    #       .gradle/6.0.1/javaCompile/taskHistory.bin
    #       .gradle/buildOutputCleanup/buildOutputCleanup.lock
    #       .gradle/buildOutputCleanup/cache.properties
    #       .gradle/buildOutputCleanup/outputFiles.bin

[33mcommit 05c944453568f67ad58c48ddac5c62130cf2b68e[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Fri Mar 20 17:43:30 2020 -0600

    ContentController

[33mcommit c4577bfc55b5d238e3075c70b6fe82ea2b98237d[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Fri Mar 20 17:39:29 2020 -0600

    ContentController

[33mcommit 5c74222638007b713bdc6c1aaa7831d01841156b[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Fri Mar 20 17:38:30 2020 -0600

    ContentController

[33mcommit cd8aa37c02f0958d88b366728005bfc1963df551[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Mar 20 17:21:33 2020 -0600

    fix contentmanager error

[33mcommit ce16ad7d46bfde0044aa1299d4623f591f5be100[m
Merge: 1ba628f2 54bd8a40
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Fri Mar 20 17:16:16 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 54bd8a40bcbec53a774b252139b61669f3936ee4[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Mar 20 17:16:00 2020 -0600

    fix story submit

[33mcommit 1ba628f2012ee0340f3b73425c2b355958aa6491[m
Merge: 719e996a be200b48
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Fri Mar 20 17:06:09 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit be200b48b7f69df6ec5e07dde9e7ee8d20e1994d[m
Author: Jorge Blanco <jorge.blanco@edu.sait.ca>
Date:   Fri Mar 20 23:05:51 2020 +0000

    Update build.gradle

[33mcommit 9bb4e6ec47b3a81b09bab34ba1f64edaa1a1e6a2[m
Merge: 04e7a75d f320e3d1
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Mar 20 16:47:13 2020 -0600

    Content Manager changes

[33mcommit 04e7a75dff5540437d70c180e0d65bec6a99504e[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Mar 20 16:38:20 2020 -0600

    Content Manager changes

[33mcommit 719e996a8db12434908d32bd12a116510ada81c3[m
Merge: 58321267 f320e3d1
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Fri Mar 20 16:37:35 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit f320e3d1c15658013b67e2d5b098eb53fea36046[m
Merge: 9630b694 71842dd8
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Mar 19 11:42:36 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 9630b694f5cc0a236c5b829b532b3065b7602a7b[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Thu Mar 19 11:42:26 2020 -0600

    Generate report and backup downloadable files

[33mcommit 71842dd8e43f5d38a169e1e5c8a1aedc7f9ec06b[m
Author: superjiy <superjiy@gmail.com>
Date:   Thu Mar 19 08:56:06 2020 -0600

    jongil
    permissions icons issues has been solved

[33mcommit 930e1a82cca2fcf9ce6fd1baa7437625567de957[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Wed Mar 18 14:17:31 2020 -0600

    Generate report and backup downloadable files

[33mcommit 710ea733e5203d06b941c715ed52fe59fd9c22c1[m
Author: superjiy <superjiy@gmail.com>
Date:   Wed Mar 18 11:42:39 2020 -0600

    jongil
    pushing stuffs

[33mcommit 297c038cd907b94b1bc39c38b69381f366318630[m
Author: superjiy <superjiy@gmail.com>
Date:   Tue Mar 17 15:50:17 2020 -0600

    jongil
    dashboard styling

[33mcommit a7f3bbfd36399187a5a4b3bd0bdeda03ae372199[m
Author: superjiy <superjiy@gmail.com>
Date:   Tue Mar 17 15:29:16 2020 -0600

    jongil
    working on menu and site routing

[33mcommit f8421c750f2d73adfbc7eb9fe95cdcd7235d3408[m
Author: superjiy <superjiy@gmail.com>
Date:   Tue Mar 17 13:38:52 2020 -0600

    jongil
    warning correction

[33mcommit 75813daa27addfc14889b2f5d7eb929835aa3501[m
Author: superjiy <superjiy@gmail.com>
Date:   Tue Mar 17 10:16:10 2020 -0600

    jongil
    scss settings have been changed

[33mcommit 53ebea20b300ce30188f4dbc29b2204462b90b5e[m
Author: superjiy <superjiy@gmail.com>
Date:   Tue Mar 17 10:15:05 2020 -0600

    jongil
    scss setting has been changed

[33mcommit 006da53f72a8edcfe625c4dc85a58002bd0072d4[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Mar 16 19:58:37 2020 -0600

    jongil
    working on user dashboard

[33mcommit 2ea20211bb74ceca236c93168096ae2958f96c37[m
Merge: 6047ca5e 8527514a
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Mar 16 17:53:25 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit 6047ca5e601dd378b60fb5588d7ea264eb8b4ef5[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Mar 16 17:53:18 2020 -0600

    Fixed User JSON

[33mcommit 8527514aa02a672d18bcb1ece4f432db1622aa42[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Mar 16 15:59:16 2020 -0600

    Jongil
    conditional routing is applied

[33mcommit c502d0a37a30657186de01f688fa8a8b8ca49fbd[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Mar 16 09:04:39 2020 -0600

    jongil
    redux-persist states have been modified

[33mcommit 96e461c5a7cf071414934c6b48d6ffd5332e4caf[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Mar 16 08:02:55 2020 -0600

    Connecting Reports back-end to front-end
    Creating PDF reports

[33mcommit 49b282cd98b9f2c3aa67650929be6ba82bb1fc52[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sun Mar 15 18:43:19 2020 -0600

    Connecting Reports back-end to front-end
    Creating PDF reports

[33mcommit 62f020ffb780919b08ff893a64da6d17134a97c2[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sun Mar 15 18:27:59 2020 -0600

    Connecting Reports back-end to front-end
    Creating PDF reports

[33mcommit a5aebc7e77f30d5f3c9aad52e547a3ea0e89eda1[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sun Mar 15 15:21:14 2020 -0600

    Connecting Reports back-end to front-end
    Creating PDF reports

[33mcommit 141a920d6ddd943bb672170e1b7334df856a638d[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 14 19:52:20 2020 -0600

    Connecting Reports back-end to front-end
    Creating PDF reports

[33mcommit 7495e03f1f5841fd31692c9a0922dd38224f7bc9[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 14 19:36:57 2020 -0600

    Connecting Reports back-end to front-end
    Creating PDF reports

[33mcommit 53dd1a62ac777a18057fc1c2b02d8848181ddcd5[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 14 15:26:36 2020 -0600

    Connecting Reports back-end to front-end

[33mcommit 393ac5ad778573a3e4db159c217bd46d8fed5f8b[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 14 15:26:21 2020 -0600

    Connecting Reports back-end to front-end

[33mcommit 05f9faf51a318a80fedb70790c2d7ecb36983401[m
Merge: c8851feb 2abc5e69
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 14 15:02:03 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit c8851feb9f4677b7c6542fff96186902469b7407[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 14 15:01:53 2020 -0600

    Connecting Reports back-end to front-end

[33mcommit eedb981ad368c60135eef27c2ed34dc917dbf34d[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 14 14:52:13 2020 -0600

    Connecting Reports back-end to front-end

[33mcommit 2abc5e69cb8b7b6cf6b69606a48c26edb62b305c[m
Author: superjiy <superjiy@gmail.com>
Date:   Sat Mar 14 11:46:46 2020 -0600

    Jongil
    user information nullification has been added to signout

[33mcommit d4abe4732329f8e7d5fc6a83474da82b4b039396[m
Author: superjiy <superjiy@gmail.com>
Date:   Sat Mar 14 11:11:35 2020 -0600

    Jongil
    Refresh issue has been solved

[33mcommit beeec4cfd6fba0578b80d9d01e5454a39dd96700[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Mar 11 20:30:11 2020 -0600

    add sweetalert

[33mcommit e5461b49b24e212a626bc1c70626e01b9b377ddc[m
Author: superjiy <superjiy@gmail.com>
Date:   Tue Mar 10 09:37:49 2020 -0600

    jongil
    init page to /map

[33mcommit 9a1ebb9b221b38598ad769bdbafb668b06adddb1[m
Merge: a50d92ee 30255f28
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Mar 10 08:40:29 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit a50d92ee2351f47846182fd155ee65772a03c1cf[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Mar 10 08:40:21 2020 -0600

    Connecting Reports back-end to front-end

[33mcommit 30255f280a95b8850473d62230426f4abaee8822[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Mar 9 20:02:56 2020 -0600

    Add @SQLDelete to all contentelement classes

[33mcommit 58321267910d0138ba9864f12869012d06b045cd[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Mon Mar 9 13:14:42 2020 -0600

    ContentController

[33mcommit 6bba645e9ebc17265b3210ba2f384e90445414fc[m
Merge: 11599906 2e157530
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Mar 9 13:07:31 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 1159990621b55a4d30af3041ecc0410f45a9ccfa[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Mar 9 13:07:25 2020 -0600

    content controller

[33mcommit 2e1575300bbf1fd507ab6548f66d878f255f05ee[m
Merge: d5ca7fef a2c00a9b
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Mon Mar 9 13:05:42 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit d5ca7fef68a1b564f2e39bccc8d37c8eb0d57143[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Mon Mar 9 13:02:46 2020 -0600

    ContentController

[33mcommit a2c00a9bec7f7892fd39ad45aae6e4d9da580619[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Mar 9 12:40:23 2020 -0600

    Connecting Reports back-end to front-end

[33mcommit 04e3f62cabaf185f4003cafb1ffb44f4dde97932[m
Merge: ebf9ff02 7607e8e8
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Mar 9 12:18:20 2020 -0600

    Merge remote-tracking branch 'origin/master'

[33mcommit ebf9ff023afc29d87a161c2714d281fe63243898[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Mar 9 12:18:06 2020 -0600

    Connecting Reports back-end to front-end

[33mcommit 7607e8e816ff9dbe6717173f554843651b965420[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Mar 9 11:47:48 2020 -0600

    add spring upload limits

[33mcommit 33b443f906f85abe47b5314db4c85f5f3bb1f641[m
Merge: 63354e10 5656a359
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Mar 9 11:43:23 2020 -0600

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 63354e1002d357cdda66090e7ec0fd48e8fa8078[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Mar 9 11:43:18 2020 -0600

    multiple content manager updates

[33mcommit 5656a3599748c8049cf1914135c187a139be353a[m
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Mon Mar 9 11:39:59 2020 -0600

    ContentController

[33mcommit abad66e262de8982c5c4975e29afc0c68e83997f[m
Merge: 5f3696e9 bc5f3901
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Mon Mar 9 11:29:32 2020 -0600

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       src/main/java/com/asylumproject/asylumproject/broker/ContentElementBroker.java
    #       src/main/java/com/asylumproject/asylumproject/controller/ContentController.java
    #       src/main/java/com/asylumproject/asylumproject/manager/ContentElementManager.java

[33mcommit bc5f3901b4153f33edf79b44b2fc5a5c57b00a33[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sun Mar 8 19:13:50 2020 -0600

    Connecting Reports back-end to front-end

[33mcommit a31ac033df105d4302bfd69575c2f00a2b4bcf09[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sun Mar 8 17:46:34 2020 -0600

    Connecting Reports back-end to front-end

[33mcommit 5f3696e9926b30563d5d93c95ae8fb0e54b70bd1[m
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Sun Mar 8 14:21:38 2020 -0600

    ContentController

[33mcommit 3126be5aabfa0f2fe9de0711413dc33fcc4d1234[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 7 23:18:18 2020 -0700

    Retrieving Story information for Reporting

[33mcommit 29b123fda0ca179e2c9a43ec0748462efc3c5900[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 7 22:54:34 2020 -0700

    First attempt to connect Reports front-end with back-end

[33mcommit dd3b1ee81695a3909957773c13200c5f9993ea82[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 7 21:49:37 2020 -0700

    Reports user interface

[33mcommit f7fa59f955e3842971c094869c042f7d6cc33485[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 7 20:20:11 2020 -0700

    Reports user interface

[33mcommit f1657f2808cd17daca41dc9f0803f1f9864ea3ea[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 7 20:00:43 2020 -0700

    Reports user interface

[33mcommit 345fca5d3ae6f0c8ab1dac9adddb0877452bcacd[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 7 16:28:36 2020 -0700

    Reports user interface

[33mcommit e3d07d7c30246f283b6c1905e6e6c7cbc0cbda8e[m
Merge: 64ca1155 5394586b
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 7 15:53:17 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 5394586b6c8c60ad4b095e4495e1997ad5b056dd[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sat Mar 7 14:02:37 2020 -0700

    fix typo

[33mcommit d03a79a855a468d99ebe08d78b3f99b6cd6b9483[m
Merge: 350c9e63 48838ab9
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sat Mar 7 14:00:00 2020 -0700

    fix signout.jsx conflict

[33mcommit 48838ab9d9c99f637b555a67df740868ed6f50b7[m[33m ([m[1;32mjwt_testing[m[33m)[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sat Mar 7 13:52:17 2020 -0700

    Add refresh token functionality

[33mcommit 64ca1155667df47e4cfdbe5b9d485a99d8a2afb8[m
Merge: 7acb6720 350c9e63
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 7 13:40:38 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 7acb6720e4d6b6d077910d129f61de2a8df9a4bf[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Mar 7 13:40:29 2020 -0700

    Reports user interface

[33mcommit 350c9e63d56de3cd36d50f6415266de86bd850b1[m
Author: superjiy <superjiy@gmail.com>
Date:   Fri Mar 6 16:06:53 2020 -0700

    jongil
    working on site routing

[33mcommit 65d5d9c183aa158e84cda3a9933c5a67c5d23c9d[m
Author: superjiy <superjiy@gmail.com>
Date:   Thu Mar 5 20:18:11 2020 -0700

    jongil
    working on site routing

[33mcommit eb1cb795b0b4d2354b03ef56f88dbcfe1b74e6b2[m
Merge: 8bf42534 7dcbd4e1
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Mar 5 19:47:40 2020 -0700

    fix merge conflict

[33mcommit 8bf42534749f4a6562a263fd716fbb227fb9e188[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Mar 5 19:15:05 2020 -0700

    fix issue with audio and video format

[33mcommit 7dcbd4e1058cd51b66536d89f40cbd8be914eaf5[m
Author: superjiy <superjiy@gmail.com>
Date:   Thu Mar 5 17:58:31 2020 -0700

    jongil
    working on conditional site routing

[33mcommit 453eaaa6ddbbbb8ba2ba2c2248370ba17573be44[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Thu Mar 5 10:56:46 2020 -0700

    Added Reports UI (draft)

[33mcommit 521ac523453f420dfe3bd53da6d697ac2031dc30[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Thu Mar 5 10:23:56 2020 -0700

    Added Reports UI (draft)

[33mcommit 139ce53a94400886f5e3986a1b81a8833faf7c32[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Wed Mar 4 10:48:29 2020 -0700

    Added Reports UI (draft)

[33mcommit d66152e4c571f74c28a2043f11dfe2e6e29b1897[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Mar 3 13:52:38 2020 -0700

    Added Reports UI (draft)

[33mcommit d01266d3241cf15d8f121e1c07c39ebc07c54266[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Mar 3 12:07:14 2020 -0700

    Added Reports UI (draft)

[33mcommit b5d1524f144de533f06fe9fe959bb9b4124e1a47[m
Merge: d3373e13 fd773677
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Mar 3 09:33:44 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit d3373e13f004cfd6611b8608005739c9fd61e35a[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Mar 3 09:33:38 2020 -0700

    Added event logger to ContentController
    Added Reports UI (draft)

[33mcommit 8bea58f013be3d8e179a3b842ddc36b6e260d24b[m
Merge: ed14f036 fd773677
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Mar 2 17:40:38 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit fd77367773ce8bc4c69bf86ff5289f3f8717ef72[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Mon Mar 2 17:40:01 2020 -0700

    added CoordinatePicker component for use in story creation

[33mcommit 92b70d5039422cffcc60b802240f492af5bfb5e5[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Mar 2 14:55:10 2020 -0700

    Added Reports UI (draft)

[33mcommit c84f3504c1c1d313a0b3cdfbfa6f47a6c69bd4ca[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Mar 2 14:52:50 2020 -0700

    Added Reports UI (draft)

[33mcommit bfc1ac26f265c1239e6c9196a59172bdb78157f3[m
Merge: a0f1499d 0f3ca180
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Mar 2 13:29:42 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit a0f1499d96d083482044cd34e74cd6e250fccc95[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Mar 2 13:29:36 2020 -0700

    Added Reports UI (draft)

[33mcommit ed14f0365b60e821d16fcd182ab6d81a9866a712[m
Merge: 0e532369 0f3ca180
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Mar 2 12:55:55 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 0f3ca180454c33a1da6e661bffe5c9cc13178059[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Mon Mar 2 12:36:12 2020 -0700

    Expanded markers, when clicked, now open 'story mode' and move to position and zoom level. No close story control though so you'll have to refresh the page :P

[33mcommit 0e5323692e7477982834278f5819171a0ce85190[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Mar 2 12:34:47 2020 -0700

    Fix issue with audio and video length format

[33mcommit 7fcb02946c5ffff588bb2ca316aad7ee79eb3f88[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Mar 2 11:19:04 2020 -0700

    Started Report UI

[33mcommit 451815de80e37b10450aa405df85ab3b957e38e7[m
Merge: 360a64a1 9f8b594b
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Mar 2 10:47:41 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 360a64a1514e98a222e5b1c8e1883fd4679c7cde[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Mar 2 10:47:15 2020 -0700

    Started Report UI

[33mcommit 9f8b594b03260336a83d381c95a5258724870f80[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Sun Mar 1 16:57:04 2020 -0700

    styled and animated expanded markers. This is the easy flexbox backup design because my initial svg design was a css nightmare and I gave up.

[33mcommit ff854a3730411023a7935282785bf688e7a06cbe[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Mar 1 09:19:37 2020 -0700

    Add @PathVariable to audio and video getters

[33mcommit 3ebc273cde2ef7b2f6b62cdd0afd16f1dae627fa[m
Merge: 4077d519 ce17ccbc
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Sun Mar 1 00:01:23 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 4077d5190011de504eba9b2a52d1ad46b6f16c7a[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Sun Mar 1 00:01:10 2020 -0700

    ContentController

[33mcommit ce17ccbc588a6c8c56fd3d163c60eea01b9f0954[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Sat Feb 29 15:59:00 2020 -0700

    Changed story and marker attributes to more closely match the details section of story creator. This is my proposal for info displayed on the expanded marker

[33mcommit 7ebf90bd53ab6b280b77a1a33071837015c1f4e7[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Sat Feb 29 14:29:17 2020 -0700

    Map ready to build markers based on list fetched from backend, multiple test markers added, some code cleanup

[33mcommit 5201395261eb73838441dc5d1b3dd3a10e584b82[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sat Feb 29 12:30:40 2020 -0700

    fix UI update for image elements

[33mcommit 5410d1de736753ab1cf939c21b8decc8bfdc6dd3[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sat Feb 29 11:38:47 2020 -0700

    Fix order of parameters in db method

[33mcommit ddd4882f6fd8f9bd2bf04dab1235e7456b7e0527[m
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Sat Feb 29 11:04:08 2020 -0700

    ContentController

[33mcommit 4fd09cd943b56da06fe942d8f898f27004100617[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Thu Feb 27 12:33:40 2020 -0700

    Add null length to image

[33mcommit dcac4309f2b68537d653efb5400ce2acf88d30c0[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Thu Feb 27 12:31:47 2020 -0700

    Add null length to image

[33mcommit e2c7863322262f1dab790ab735a5388dbec6ab88[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Thu Feb 27 12:25:54 2020 -0700

    fix mappoint url

[33mcommit 847dedae7d941c98967ba1487180614ac631048b[m
Merge: f10ffe9e dc4f1c2e
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Thu Feb 27 12:12:41 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit f10ffe9e52af4e964aaf649c6336d09db36db291[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Thu Feb 27 12:12:30 2020 -0700

    fix css import issues

[33mcommit dc4f1c2e4861b72cd073c6ce32613c04ee57c341[m
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Thu Feb 27 12:11:12 2020 -0700

    ContentController

[33mcommit c8a6a54de7d7f6e584ecbe35308ee2a131add3d8[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Thu Feb 27 11:15:50 2020 -0700

    change mappoint url on frontend

[33mcommit fb548d221d8ab4f6a0672b2400cbb9b26e3b5ace[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Thu Feb 27 10:57:47 2020 -0700

    Add Spring file upload limit

[33mcommit 1e09c0635a8d806f9ba1725e985afa7fa4bd1533[m
Merge: 0a645932 e747e63b
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Thu Feb 27 10:48:01 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 0a645932002cad4b79e652c70f9d7f46e27dcddf[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Thu Feb 27 10:47:52 2020 -0700

    Add contentmanager steps

[33mcommit e747e63bc9f9d2cee5cc8644830d1be418a97c6f[m
Merge: 10588407 1a602a5e
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Thu Feb 27 10:47:10 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 10588407661d5de511d97a0304d1b77c9b7dd60a[m
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Thu Feb 27 10:46:46 2020 -0700

    ContentController

[33mcommit 1a602a5e4c821efa3937ae37700bdcc1f23b2490[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Wed Feb 26 21:16:24 2020 -0700

    Initial unstyled expanded marker

[33mcommit 88b79df5b347ca5a3c5f21b1da1fedfa91dcba07[m
Merge: 16ad1c56 f446ab62
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Feb 26 15:48:31 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 16ad1c56158878d71c257d4eb9b45a1bd5294583[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Feb 26 15:46:13 2020 -0700

    Update story creation steps

[33mcommit f446ab627f89299e5bb23f4b5727bdda3ed32ad3[m
Merge: 33493204 7e746e37
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Wed Feb 26 12:48:26 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 334932044e30a9c5b11f4fe404bf2518fe8d4c08[m
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Wed Feb 26 12:48:13 2020 -0700

    ContentController

[33mcommit 7e746e37fe2099b664a4cb13dd36915678517342[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Tue Feb 25 13:34:09 2020 -0700

    Content Manager updates

[33mcommit 51f418b98455901c405c8907d2c3fc3b36bfe80b[m
Merge: 0ac078a2 eb45cf3e
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Feb 25 10:59:01 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 0ac078a230366527051f25358e8e998e10180bf7[m
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Feb 25 10:58:46 2020 -0700

    ContentController

[33mcommit eb45cf3ef4e23c000e1559008fcedd655e37c5e7[m
Merge: d657aed2 49409b86
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Tue Feb 25 10:57:35 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit d657aed286f6b8fc7a0306af78004e74b349d69d[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Tue Feb 25 10:57:16 2020 -0700

    Add create map point button

[33mcommit 49409b8687699b2f5a79d00393c7af141da3c632[m
Merge: f9ff3d9a 7ce10d17
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Feb 25 09:03:33 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit f9ff3d9a76d1ef2c5802a74427769bbbd348b84c[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Feb 25 09:03:25 2020 -0700

    Logging events updated

[33mcommit 7ce10d1747ef1178f3ccb0313833149dc88922f7[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Tue Feb 25 08:58:25 2020 -0700

    jongil
    a little change on css

[33mcommit ad3eb394f2830003599edf978cbe145cf9a30018[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Feb 25 08:46:34 2020 -0700

    Logging events

[33mcommit 152809a180d35a8a3ea3210d74bbad026908ce51[m
Merge: 2733c510 a3c5cdae
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Tue Feb 25 08:35:30 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 2733c5109407b18263781986bacbe48f53def97e[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Tue Feb 25 08:32:38 2020 -0700

    Add transitions to Content Manager

[33mcommit a3c5cdae2b2323636d1b9014d0c07d64f3919b4f[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Mon Feb 24 21:05:41 2020 -0700

    Implemented map marker, fixed div sizing errors, fixed map window size. Really went through all the divs to solve the chaos

[33mcommit a70a18dc7861eb5185c12201c97c6cee30457eff[m
Author: Jongil Yoon <superjiy@gmail.com>
Date:   Mon Feb 24 19:01:37 2020 -0700

    jongil
    permission issue is solved

[33mcommit 3d0e60a19c9c3e6abaeaa82cb6727d5f94d7c643[m
Merge: fcd14d4d 02c93e3f
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Feb 24 17:04:58 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit fcd14d4d95ba8c672d6345bf545cbcbb2942ba8f[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Feb 24 17:04:49 2020 -0700

    content manager edits

[33mcommit 57ed9b2ed6e69a18149fbaab5f95f9aa50755582[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Feb 24 17:04:33 2020 -0700

    content manager edits

[33mcommit 02c93e3f366449bfc14ca7fba495075816462cfa[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Feb 24 12:07:15 2020 -0700

    Logging events

[33mcommit f5e78d58d3ca32d0bd583d2670358fabe6b3eef5[m
Merge: 7ce6637c b4a3fe80
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Feb 24 09:06:54 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 7ce6637c091f02638eee2e564e3c51327ff6626c[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Feb 24 09:06:38 2020 -0700

    Logging events

[33mcommit a3a5201bb6237dc682ead1f76892ba12ea013f5c[m
Merge: 4f0d50c4 b4a3fe80
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Feb 23 07:54:17 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit b4a3fe8003ee77f0dbb62b06ff50490dccc8c8e8[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Sat Feb 22 20:47:50 2020 -0700

    fixed map interactivity event handler issue and added search by location control

[33mcommit 4f0d50c4ab4ef088b808a9f5425225ae588be753[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Feb 21 19:18:51 2020 -0700

    add attributes to contentelement upload

[33mcommit 4164da038995ff97c2b69a61f76c5e568f3e4268[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Thu Feb 20 19:24:41 2020 -0700

    ContentController

[33mcommit bfc569b556d6519ba6466eadd821e66050b31abd[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Feb 20 15:17:45 2020 -0700

    fix menu error

[33mcommit 246b51ec07363adcfb4644bb1ec4fcc74a64e7c8[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Tue Feb 18 21:03:11 2020 -0700

    Update Content Manager files

[33mcommit 549e50128b3da04ecdc98c06fb99503acbde1c0c[m
Merge: a8559b4b 58825a0f
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Feb 16 16:53:08 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit a8559b4b7e692e349c29b92effe13e652cef8a48[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Feb 16 16:53:00 2020 -0700

    Add upload preview for video and audio

[33mcommit 58825a0f53f16003ef61d9bb020893570cfb40ae[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Fri Feb 14 11:54:24 2020 -0700

    Added EventManager to UserController and AuthenticationController.

[33mcommit 8866355323a77b5a5f99cb256cff870ef527e0ae[m
Merge: bd1da874 ffacd9e0
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Fri Feb 14 11:04:47 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit bd1da874e3e541eab9317be22ccf3c840c34acc3[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Fri Feb 14 11:03:44 2020 -0700

    Added EventManager to UserController to log user events.

[33mcommit ffacd9e0aa3b339b96f1f69e79368c46e24c1f00[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Thu Feb 13 11:32:44 2020 -0700

    'mappoint'

[33mcommit 0fb73879563346243fa5ed1718f0e49f1ba0779a[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Thu Feb 13 11:08:48 2020 -0700

    add mapPointID to upload

[33mcommit 22eb8cc4e4a82d1a03d8cd719fcd170ab491d40a[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Thu Feb 13 10:22:08 2020 -0700

    fix element upload

[33mcommit 6ca0e23737ec2b1a7a0e049766a78b4e2ab69221[m
Merge: d22fd064 d72b8ab1
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Thu Feb 13 10:17:03 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit d22fd064c3a9d3ea352bb560dc3bb7f1bb0fbd67[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Thu Feb 13 10:16:50 2020 -0700

    ContentController

[33mcommit d72b8ab1e95a16eb8411ef8e39d74d6e6956948d[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Feb 13 10:08:41 2020 -0700

    Language quick fix

[33mcommit 72b4f63e12d4a38d540edff9cfd449286d74d7ce[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Feb 13 08:34:00 2020 -0700

    temp user details button and some cleanup

[33mcommit ae10ecef6abdea40b83121738ad3b22bee0f5bdf[m
Merge: 3e62903d 3d58ae33
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Feb 12 20:39:13 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 3e62903d5659d9e4e1533995e4b0b96b8b971092[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Feb 12 20:39:07 2020 -0700

    Add delete buttons to content element list, highlight active button, select category

[33mcommit 3d58ae337f41608b112190afb72a1e138aec8bb9[m
Merge: 7da39132 2dd13325
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Wed Feb 12 10:09:08 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 7da39132d98ea3ca1769bf823131893c06ce27a1[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Wed Feb 12 10:08:59 2020 -0700

    createDate and UpdateDate added to support logging functionality

[33mcommit 2dd133259f386e4404dc27bbe386d0b29fedef5c[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Tue Feb 11 16:50:38 2020 -0700

    Add more attributes to element upload

[33mcommit 5646c32f48f22da39dfd23c3628bc3ef5795ac76[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Tue Feb 11 15:29:23 2020 -0700

    change file upload name

[33mcommit ab5964ddf4e08ba877029bb15a40870e67440bd3[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Tue Feb 11 15:29:02 2020 -0700

    change file upload name

[33mcommit 00fd894cdd344452a26e91eff1c25e8cd314bc1d[m
Merge: b5ad603b 1d708449
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Feb 11 12:40:31 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit b5ad603b171e28bd50e28e55251a96c4da6af78a[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Feb 11 12:40:21 2020 -0700

    createDate and UpdateDate added to support logging functionality

[33mcommit 1d708449aa375d937d6148dca30d28e4b0c8ded9[m
Merge: 446bf72f 60a2fc6e
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Feb 11 12:26:26 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 446bf72ffca32034559fc787dfde051906ecc437[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Feb 11 12:26:13 2020 -0700

    ContentController

[33mcommit 60a2fc6e541e714a01cd5f42c06580edd9af1de0[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Tue Feb 11 11:49:04 2020 -0700

    removed 69xxx

[33mcommit f21b40fe2527b2ad57d3a519f0c2cf3ed4f9f5e4[m
Merge: 2b9b101b add5f3de
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Feb 11 11:40:30 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 2b9b101badd28f8ec160f997ec355b21163cdfd8[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Feb 11 11:40:17 2020 -0700

    ContentController

[33mcommit add5f3de4df490cb48effe5796d9ab46eb9d6669[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Tue Feb 11 10:57:24 2020 -0700

    found and shot the comma

[33mcommit a90e752378995139f9791cb5785a8d31f87eea5a[m
Merge: a1c31eea de5995f8
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Tue Feb 11 10:28:59 2020 -0700

    Merge branch 'master' of gitlab.com:capstoneawesome/asylumproject

[33mcommit a1c31eeada6d4f8bec3bc4156f73b1352db03c48[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Tue Feb 11 10:28:41 2020 -0700

    SignOut hotfix 1.189 The good enough for now update

[33mcommit de5995f8a5f2400eb1f0f596a9863e7b004fbe13[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Feb 11 09:50:50 2020 -0700

    createDate and UpdateDate added to support logging functionality

[33mcommit 82455fb0d868fc4d0d36a6e53ba4346b1b7ae82b[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Feb 11 09:25:37 2020 -0700

    createDate and UpdateDate added to support logging functionality

[33mcommit 4c4926197e5090fb8854b114f7337776dda80a8c[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Feb 11 08:20:48 2020 -0700

    setup for JavaMailService
    Request reset password
    Mail templates

[33mcommit 70d32af49994319e760abc92f976bcef26297d52[m
Merge: 2a782bfb a68ec6d7
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Feb 11 08:18:16 2020 -0700

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       src/main/resources/application.properties

[33mcommit a68ec6d7533ab21fc598f767361807e7508c1a06[m
Merge: 3e8333df 5cef6e46
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Tue Feb 11 08:15:35 2020 -0700

    Merge branch 'master' of gitlab.com:capstoneawesome/asylumproject

[33mcommit 5cef6e461442e139a73a50d5fedd0c7cd546c1bd[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Feb 11 08:11:54 2020 -0700

    ContentController

[33mcommit 3e8333dfed76e1c949dbd71dee9348fadf26ab1c[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Tue Feb 11 08:07:45 2020 -0700

    just a small change to App

[33mcommit ab1e44404991e112aa7108980ae7be44ac2bd82e[m
Merge: c098ec76 c1d73d98
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Feb 11 08:06:59 2020 -0700

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       src/main/resources/application.properties

[33mcommit 2a782bfbfa1acba3c8eaa8337d75a0398b66773e[m
Merge: 914d6e76 c1d73d98
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Feb 11 08:05:50 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 914d6e768d607bcdc085c73ac20c461e96654a06[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Feb 11 08:05:41 2020 -0700

    setup for JavaMailService
    Request reset password
    Mail templates

[33mcommit c098ec767f6bffe168095c27c0b1900abb041e54[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Feb 11 08:02:03 2020 -0700

    ContentController

[33mcommit c1d73d98909234e69ead6a0013953f1eed45eff7[m
Merge: 3e060ce9 6f93b9ce
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Tue Feb 11 07:50:51 2020 -0700

    Merge branch 'master' of gitlab.com:capstoneawesome/asylumproject

[33mcommit 3e060ce945cea95312ba8dc983cc27eb1c0e6d4e[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Tue Feb 11 07:50:45 2020 -0700

    some stuff

[33mcommit 6f93b9ce8c5ba96944d6b7763326addd41dbf766[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Feb 10 22:35:50 2020 -0700

    Add Content Elements box

[33mcommit 939dc421bcfea86b8d36849fa0758de5e797ffab[m
Merge: 05517a05 6374df16
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Feb 10 20:07:59 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 05517a05ded0aecc323e25018e9e6f6547aa3f59[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Feb 10 20:07:35 2020 -0700

    Add image caption

[33mcommit 20880de6e5c42e3b5f0e941d5fb5669c97b4677b[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Feb 10 20:06:49 2020 -0700

    setup for JavaMailService
    Request reset password
    Mail templates

[33mcommit d9e94aa9c8be514abc3d58de17783a7f6149cc4c[m
Merge: c4c7a91a 6374df16
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Feb 10 15:31:02 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit c4c7a91a19442750c6fcf3ff1747e3c606a7dd2c[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Feb 10 15:30:50 2020 -0700

    setup for JavaMailService
    Request reset password
    Mail templates

[33mcommit 6374df16fbd053fe57862606ef33e4f899332a0e[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Feb 10 14:44:19 2020 -0700

    jongil
    Can NOT figure out the permission checkbox issue

[33mcommit 6a1f605f682a1fd07618e42343df50d9029b05f8[m
Merge: d6a79446 a7bbcd0e
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Mon Feb 10 13:28:52 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit d6a79446bf79a09a2b884522feef3aeea7c0405b[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Mon Feb 10 13:28:46 2020 -0700

    Add 'no file selected image'

[33mcommit a7bbcd0ef1965a8ee47620f5ddad3af2534842df[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Mon Feb 10 12:38:02 2020 -0700

    added temp sign in and sign out to be replaced by a user display at some point

[33mcommit dc653d77db5024b26bb4d000f1fe6e2ce02349aa[m
Merge: 723179dc 80389a3d
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Feb 10 11:41:19 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 723179dc6d18298ed0c27e450593a78f4a4b2336[m
Merge: 143e19a0 0d3d9808
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Feb 10 11:38:50 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 80389a3dbbb9fd137099b4c817427e1a7e4e94ab[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Mon Feb 10 11:38:41 2020 -0700

    animated site menu navigation

[33mcommit 143e19a0c880d88ffe75b551662b886d3bc7f60f[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Feb 10 11:38:33 2020 -0700

    setup for JavaMailService
    Request reset password
    Mail templates

[33mcommit 0d3d98082ce1f24d7d0c8153400b6121378530b6[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Mon Feb 10 10:12:00 2020 -0700

    animated menu no page transistions yet

[33mcommit e76b7aea9bfa9a5e7c7a5e22736b508e1b7f2385[m
Author: superjiy <superjiy@gmail.com>
Date:   Sat Feb 8 15:21:33 2020 -0700

    jongil
    working on permission checkbox

[33mcommit daaaf386f51f99bae89421fae930bd974ef1e022[m
Author: superjiy <superjiy@gmail.com>
Date:   Sat Feb 8 15:20:55 2020 -0700

    jongil
    working on permission checkbox

[33mcommit 01b732e969e3508ea5cdf33c71e458c8eca59d0a[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Sat Feb 8 12:25:51 2020 -0700

    added react-spring you gotta npmInstall just committing to avoid a package.json collision

[33mcommit 5de0b16ea15b85953ffad6f17378c0b2f6744d7b[m
Author: superjiy <superjiy@gmail.com>
Date:   Fri Feb 7 16:01:12 2020 -0700

    jongil
    working on checkbox

[33mcommit d130c921511bb83e79f8ddc4d904e723d3064065[m
Author: superjiy <superjiy@gmail.com>
Date:   Fri Feb 7 11:05:01 2020 -0700

    Jongil
    check point
    working on user administration

[33mcommit 27f1d979b04d7da019e3e973b549ed0de7a50810[m
Author: superjiy <superjiy@gmail.com>
Date:   Thu Feb 6 20:50:08 2020 -0700

    jongil
    iso language module and moment time module have been installed
    working on user registration and view

[33mcommit 8a2e8ebd171d4f5bb0b398d81819c02d094ace9b[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Feb 6 19:46:30 2020 -0700

    Change syntax to react style

[33mcommit 530ee515551f666d789fb936ccc84a4163a09f66[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Feb 6 16:06:09 2020 -0700

    zoom control is functional

[33mcommit 97a518493789efc03e65a20ff06947367340d8ad[m
Merge: 58b8118d 40631aef
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Thu Feb 6 15:17:08 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 58b8118d00ee956619e775d8cd9b94d84700398e[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Thu Feb 6 15:17:00 2020 -0700

    setup for JavaMailService added

[33mcommit 40631aefd6deefdc7e273b3259a1980acb7f3f6c[m
Author: superjiy <superjiy@gmail.com>
Date:   Thu Feb 6 15:04:23 2020 -0700

    jongil
    Whole structure of 'Admin page' has been changed
    Please save your code first and pull the new changes

[33mcommit e9bdf0207335696a24b14fbec330e4c389b5c613[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Feb 6 12:50:44 2020 -0700

    basic map zoom control as react component

[33mcommit 47f65d2bffc573abaa378e6a36a81835a0287a69[m
Merge: ec590548 f5641e81
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Feb 6 12:49:15 2020 -0700

    Merge branch 'master' of gitlab.com:capstoneawesome/asylumproject

[33mcommit f5641e81b8b388a0a5aba5edf216eafcd5c0df62[m
Merge: 84f454bf fe00d42d
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Thu Feb 6 11:45:44 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 84f454bf5ff58c193e3e5c56351157cd0a52214f[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Thu Feb 6 11:45:24 2020 -0700

    setup for JavaMailService added

[33mcommit fe00d42dbd877e63a445ebdd0b96b59a520e26db[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Thu Feb 6 11:43:37 2020 -0700

    Add @Enumerated to Story state

[33mcommit ec59054879609868124de4c497a9410e1812455a[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Feb 6 11:37:39 2020 -0700

    d

[33mcommit b2289131ca9177ec3d0cfddd41870b81047ed2d7[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Thu Feb 6 11:29:12 2020 -0700

    composite primary key class has been added.

[33mcommit fa40bef716fc9dbee8ba5c9fe9ad60f8cbf06e57[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Thu Feb 6 11:28:32 2020 -0700

    composite primary key class has been added.

[33mcommit dba597183d677619057d8394b0adcda3d8c69246[m
Merge: 4ca31b25 a7f8bb5a
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Thu Feb 6 11:04:58 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 4ca31b25a902acfe1cbd9b29c783a217603448bc[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Thu Feb 6 11:04:41 2020 -0700

    composite primary key class has been added.

[33mcommit a7f8bb5a8c8e6706773c972b11d2631ff0623e82[m
Merge: e68fc52b 5054d4a6
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Thu Feb 6 08:17:11 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit e68fc52b37717fd9328852d683d1698354e20967[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Thu Feb 6 08:17:05 2020 -0700

    updates in AuthenticationController

[33mcommit 5054d4a6b1e8e9cdcde322b11704c670f86b362f[m
Merge: 33abb977 90079464
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Feb 5 22:08:33 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 33abb9776ebbb80d59fd36b9225c6d91eb6d7727[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Feb 5 22:08:21 2020 -0700

    Add modal and preview image for element upload

[33mcommit 9007946485105ae8c4c4aca50de2906aac726ded[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Wed Feb 5 20:43:33 2020 -0700

    Row style updates on click

[33mcommit 932fed200b159c1454ab561436f4be4a04d2bda6[m
Merge: 7b78b338 2d9e2684
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Wed Feb 5 19:13:20 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 7b78b3385f5756392fe2e2797e739d555714b8c6[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Wed Feb 5 19:12:48 2020 -0700

    composite primary key class has been added.

[33mcommit 2d9e268471a60c97903e2a6487f91d181b54027a[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Wed Feb 5 17:33:14 2020 -0700

    user table now sends user up to parent with callback

[33mcommit d2d46777770ba5d39d2e2e10ba0db674c2f3827e[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Wed Feb 5 10:08:02 2020 -0700

    rename UploadElemen t to .jsx'

[33mcommit e4b8a8792a861a36ff8cc0dfdc1bf0f718e618a6[m
Merge: 177dcfc6 a3f99912
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Tue Feb 4 20:54:34 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit 177dcfc6edbac336f8ddd86eca7f6fc3ea14f245[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Tue Feb 4 20:54:10 2020 -0700

    Add UploadElement.js to Editor folder

[33mcommit a3f99912382dd184066b6533da8c573e571f0c50[m
Author: superjiy <superjiy@gmail.com>
Date:   Tue Feb 4 19:17:36 2020 -0700

    jongil
    redux has been applied

[33mcommit 8133fe7e92323d4336773c093a4a232c82ced9fb[m
Merge: c0a7f5ca efab186f
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Feb 4 12:05:05 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit c0a7f5ca57014c1a1a1f0e8a8533d293d7f224e4[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Feb 4 12:04:50 2020 -0700

    updates in AuthenticationController

[33mcommit efab186fa69729274f45099cd01666d3b7a13528[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Tue Feb 4 10:54:23 2020 -0700

    table click sets selectedUser state(edit)

[33mcommit 83431e361e1fe353040b02b777b9909669ec39f6[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Tue Feb 4 10:50:39 2020 -0700

    table click sets selectedUser state

[33mcommit c2cb0475ec631380528385ff047dca2e59f6039a[m
Author: superjiy <superjiy@gmail.com>
Date:   Tue Feb 4 09:20:32 2020 -0700

    jongil
    webapp folder has been restructured!!!!!!!!!!!!!!!!!!!!!!!1

[33mcommit ea84eeca7b8fc5d83b3ae667987a68d805529a17[m
Merge: 43e9c57a 48c45aa1
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Feb 4 08:17:07 2020 -0700

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       src/main/java/com/asylumproject/asylumproject/controller/AuthenticationController.java

[33mcommit 43e9c57a1c88b181d665eb798a5bb97fe5580002[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Feb 4 08:14:05 2020 -0700

    updates in useController

[33mcommit 48c45aa14e0f0d44eae1a69460f968eb96a48593[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Feb 3 20:00:34 2020 -0700

    jongil

[33mcommit 5e60213c529422f42222c688ff35127642a0ded4[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Feb 3 19:45:55 2020 -0700

    jongil
    CSS layout has been modified

[33mcommit 9adc6ba34cd06803503525c08be699b51b4c4224[m
Merge: b52b59e7 f4faa563
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Feb 3 15:31:15 2020 -0700

    Merge branch 'master' of https://gitlab.com/capstoneawesome/asylumproject

[33mcommit b52b59e7d259f812930e418d04a0bb78cf956d0e[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Feb 3 15:30:44 2020 -0700

    Add InternalAuthenticationServiceException catch block, to catch invalid username and return 401 unauthorized

[33mcommit f4faa563c2470fbde5f1fd60b75ccc2cdd3a8df9[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Feb 3 12:59:49 2020 -0700

    jongil
    system administration page UI

[33mcommit 7ef41cedc7d25642eadc5b8298bf212fd9385d81[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Feb 3 11:58:58 2020 -0700

    jongil
    house keeping
    move on to administration page

[33mcommit fca51a6ac2fee2ce3cd2effefcd469bd0dba525f[m
Merge: 47bf1e76 a31e87af
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Feb 3 11:58:07 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 47bf1e7665000b91805def4417747a271077826f[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Feb 3 11:57:59 2020 -0700

    updates in useController

[33mcommit a31e87affd74d74cff25c9fb4be81b92cb0226fa[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Feb 3 10:27:15 2020 -0700

    jongil
    
    extra commit

[33mcommit 1ad9fe52db266256e5e0b236d90b71679d533d73[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Feb 3 10:26:37 2020 -0700

    jongil
    
    video-react react-youtube

[33mcommit 2f0c2024190c3ed5c93ab76eb3209b9b764f5bbe[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Feb 3 10:26:00 2020 -0700

    jongil
    
    minor error fix

[33mcommit 25db6f5e64571b09e62aec1815184a414948ef4b[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Mon Feb 3 08:06:15 2020 -0700

    Users and Usertable converted to hooks and table builds itself

[33mcommit f87b4b82b0739db861e54e3ca19548c6eb4f0428[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Sun Feb 2 23:41:09 2020 -0700

    Adding hibernate associations to entities.

[33mcommit 193dd899e965a43b1eb85bc1d71e9a88b5dc317d[m
Merge: 42187292 3457baac
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sun Feb 2 20:48:22 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 421872922a8d6ee5d9f72abeeb378611e5fe4759[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sun Feb 2 20:48:14 2020 -0700

    updates in useController

[33mcommit 3457baacdb0116b565a261fc9c17b519874530a1[m
Author: superjiy <superjiy@gmail.com>
Date:   Sun Feb 2 16:48:13 2020 -0700

    jongil
    controls have been changed

[33mcommit fcccabb2d3985ce6405a9d5d00368df5993bc7dd[m
Author: superjiy <superjiy@gmail.com>
Date:   Sun Feb 2 13:32:08 2020 -0700

    jongil
    gsap, and dat javascript libraries have been installed

[33mcommit 340dfa094e6977216f8850faf3eb7e6c8136b969[m
Merge: ab3a47f7 8258fe5e
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Feb 1 22:03:10 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit ab3a47f713347077ce58ee9b10f93bcab65cedc8[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Feb 1 22:03:00 2020 -0700

    updates in useController

[33mcommit 8258fe5e34a9ddd6f2290ed7b69df963c5528c9e[m
Author: superjiy <superjiy@gmail.com>
Date:   Sat Feb 1 15:13:26 2020 -0700

    jongil
    clickable icon is working!

[33mcommit f9539115f6c3afb550d2e375330ab7ce755e9b8e[m
Author: superjiy <superjiy@gmail.com>
Date:   Sat Feb 1 14:59:27 2020 -0700

    jongil
    jongil icon is now clickable
    open console log to be able to check message

[33mcommit 39e9fa93dfb542b9e2779987ebcf3c16917e4e56[m
Author: superjiy <superjiy@gmail.com>
Date:   Sat Feb 1 14:20:11 2020 -0700

    jongil
    icon placement in canvas

[33mcommit 3b3fd630c1ece9b6616441e471d168257375eb17[m
Author: superjiy <superjiy@gmail.com>
Date:   Sat Feb 1 13:53:27 2020 -0700

    jongil
    skybox has been implemented

[33mcommit 4d2ed270541f6b84b837310448cf76df0b9e9b81[m
Author: superjiy <superjiy@gmail.com>
Date:   Sat Feb 1 13:15:45 2020 -0700

    jongil
    importing image is succeed

[33mcommit 2856a7cf61756194deed83710a8b43a9b38ee32e[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Fri Jan 31 18:51:00 2020 -0700

    change user password updated

[33mcommit 8805cdbaca25262f0e2a68ac0d7fe24df47a9233[m
Author: superjiy <superjiy@gmail.com>
Date:   Fri Jan 31 16:23:23 2020 -0700

    jongil
    play with 3d

[33mcommit 550d38885f21fa9b280e32ba2b9202de68060891[m
Author: superjiy <superjiy@gmail.com>
Date:   Fri Jan 31 16:04:43 2020 -0700

    jongil
    play with 3d

[33mcommit fe875c4826d94922971e09b9ff0380339ec65874[m
Author: superjiy <superjiy@gmail.com>
Date:   Thu Jan 30 12:47:38 2020 -0700

    jongil
    redux applied

[33mcommit 73bff7b3b7bd9c3fbfa9c8da6903889bdab3520c[m
Author: superjiy <superjiy@gmail.com>
Date:   Wed Jan 29 21:06:54 2020 -0700

    jongil
    redux has been installed

[33mcommit 3908ad2c53717fac3958c0ccf1ceb0684c90a103[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Jan 29 20:16:23 2020 -0700

    User has enabled and deleted attributes now.
    The Active attribute is removed.
    
    Enabled is from Spring Security, determines if the user can log in.
    Deleted attribute is a soft delete.
    
    You will see the annotations added to User that make the soft delete happen. (@SQLDelete)

[33mcommit 5d4de6e1b75c0b04efa2ec00cce9ae85f42ffd64[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Wed Jan 29 14:38:45 2020 -0700

    Updated user API added functionality:
    - check existing username
    - check existing email
    - change password

[33mcommit 7f7e7602d55b2d17421d12bcf608b2310823ed5f[m
Author: superjiy <superjiy@gmail.com>
Date:   Tue Jan 28 19:06:21 2020 -0700

    jongil
    basic admin stuff has been implemented

[33mcommit 9f6b4f7af01eb4a8e144495a2442169fa8d66bf7[m
Author: superjiy <superjiy@gmail.com>
Date:   Tue Jan 28 15:24:18 2020 -0700

    jongil
    basic registration request can get through

[33mcommit 0489c39dd9ce67a31c7d2e96827e30918f7d6d6d[m
Author: superjiy <superjiy@gmail.com>
Date:   Tue Jan 28 10:33:35 2020 -0700

    jongil
    1. react-bulma-components has been removed
    2. font awesome has been applied

[33mcommit 5009d8cf673e600d4e487bc507057bac0670f439[m
Author: superjiy <superjiy@gmail.com>
Date:   Tue Jan 28 10:15:38 2020 -0700

    jongil
    house keeping

[33mcommit 2a8023e262a1f973cbc5df9ad78468cf230e34c2[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Jan 28 08:09:45 2020 -0700

    removed user registration from AuthenticationController

[33mcommit b1b876dc94dddc9325b42020daffa94026fc55c1[m
Author: superjiy <superjiy@gmail.com>
Date:   Tue Jan 28 08:05:48 2020 -0700

    jongil
    hamburger button is working

[33mcommit 0ddcc42416b478f277dc0977e5888d4c3bcfc86f[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Jan 27 22:17:29 2020 -0700

    /api/users api updated (UserController)

[33mcommit 8bd71d720a8e501ac7830933d09eff841e75e4db[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Mon Jan 27 20:57:22 2020 -0700

    user response update

[33mcommit 7f3c8151161fbefc95fc074c8b67e9290dd52c8d[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Jan 27 20:02:03 2020 -0700

    Jongil
    double checked

[33mcommit 82c605a4b5f1c2a8f4f9d65bf9b2b091b841f3da[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Jan 27 19:57:12 2020 -0700

    jongil
    house keeping

[33mcommit 80bb4ca4527523eeb1742cf3460f8d4c02c7a092[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Jan 27 18:37:23 2020 -0700

    jongil
    need to fix naming for users

[33mcommit 2f966556be4e5e3d4fab3470430266ecce38373b[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Jan 27 17:14:04 2020 -0700

    jongil
    UI house keeping

[33mcommit 820784a9fce83134925f6ab6d48c33dbecc44d49[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Jan 27 17:06:54 2020 -0700

    jongil
    done with aboutus

[33mcommit 9a839f9ccad392db8466181c065476fd34572a12[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Jan 27 16:56:22 2020 -0700

    jongil
    aboutus is fixed

[33mcommit cc777729a0f1d53466a440fd16bd872f4ed58ad1[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Jan 27 16:27:36 2020 -0700

    jongil
    working on basic css stuff

[33mcommit f48dfa90b1df131682ab4f77674eb324c7476e1b[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Jan 27 16:01:41 2020 -0700

    jongil
    admin page small changes

[33mcommit 79de7b35c2ef441af03f9ab4e1ac7e4f6ca9b6bf[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Jan 27 15:54:42 2020 -0700

    jongil
    working on admin page

[33mcommit 96988a00e6509c4504eb6a97d0331407bfa84870[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Jan 27 15:18:54 2020 -0700

    jongil
    admin page is initiated

[33mcommit 374499ea365621e3c44710d93bd849489abd4fb1[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Jan 27 14:49:26 2020 -0700

    jongil
    authentication house keeping

[33mcommit 030a3bab8da5b0357eed18da771d5edc7c119eb8[m
Author: superjiy <superjiy@gmail.com>
Date:   Mon Jan 27 13:43:19 2020 -0700

    jongil
    
    basic user authentication stuff done
    please refer to SignIn and Editor components

[33mcommit 6ae6a1c4019df536b59aa5c53a1d4e71d693b731[m
Author: superjiy <superjiy@gmail.com>
Date:   Sun Jan 26 16:49:57 2020 -0700

    jongil
    controlling object

[33mcommit 5cea4e57cbe94873284e23247a34936747cf6812[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Jan 25 23:14:35 2020 -0700

    Auth update

[33mcommit ed659045c1e823e5a7b343a52bb07cedef660753[m
Merge: f7359688 125ba42a
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sat Jan 25 21:43:32 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit f73596881a09659a1431fb518b59dad57bb33630[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sat Jan 25 21:43:15 2020 -0700

    Add Roles.
    PermissionName enum names change to start with ROLE_ instead of PERMISSION_

[33mcommit 125ba42a4d459ea2fb74d216b3d0bb05ae83e7e7[m
Author: superjiy <superjiy@gmail.com>
Date:   Sat Jan 25 14:36:22 2020 -0700

    jongil
    login request-response data fetching

[33mcommit aebfa3b8c8c6b67679da7329c5e782be72502e7a[m
Author: superjiy <superjiy@gmail.com>
Date:   Sat Jan 25 11:54:05 2020 -0700

    Jongil
    small fixes

[33mcommit b4cdbfa9e502df567c928a336decc95b168b27d6[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Jan 25 11:26:32 2020 -0700

    LoginRequest updated

[33mcommit 341f904b46e15a4dfa3822379e5b1b4b2ce66633[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Sat Jan 25 09:28:38 2020 -0700

    LoginRequest updated

[33mcommit 86959fa45c5430f49956ce687bd740b4b7b6a33d[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Fri Jan 24 16:06:44 2020 -0700

    change editor.jsx

[33mcommit 643a658e2c5561e0b71f21884315cfedb7ac5335[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Fri Jan 24 16:04:24 2020 -0700

    change editor.jsx

[33mcommit fe51a33b01d6e9fea7625c33c5e2fad1329007e3[m
Merge: 377886c4 08073de8
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Fri Jan 24 16:00:50 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 377886c469f1275e5ed02030a6905e7cb883cc4b[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Fri Jan 24 16:00:36 2020 -0700

    change editor.jsx

[33mcommit 08073de8da8444009e09adf118327d446802c86b[m
Merge: bb13c4b6 7803c2af
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Fri Jan 24 15:59:48 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit bb13c4b65619854a1db859fca3a768f12e19fc24[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Fri Jan 24 15:59:12 2020 -0700

    Added permissions attribute to the Signin api response

[33mcommit 7803c2af3de8e5edfa1c0ee1f843e2595f76fa8d[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Fri Jan 24 15:40:50 2020 -0700

    moar table

[33mcommit 4f4ed480a5623a81b6ad57527763ce8b120b5a03[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Fri Jan 24 15:24:33 2020 -0700

    table test beginning

[33mcommit 6825c0f417df53f204580b33ec75aa6e2af3c0a3[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Fri Jan 24 14:20:15 2020 -0700

    try again

[33mcommit c0fee9867a9f97193285546ceef9f6044d3cf5ef[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Fri Jan 24 14:04:28 2020 -0700

    added initial user window and table component

[33mcommit c8439cbe74f47880cc92ad5a28640a3bda41b82e[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Fri Jan 24 13:49:40 2020 -0700

    change users

[33mcommit 8835b31b3262dc05f1f46b1b42a7c8a43fb8b717[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Fri Jan 24 13:25:50 2020 -0700

    change username

[33mcommit 4f0b8709cb80637a8ac36fb3858080aa9994e499[m
Merge: 747bc841 f7825baf
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Fri Jan 24 13:16:11 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 747bc84148cf09ccac486b9cc3301b673de44daf[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Fri Jan 24 13:15:53 2020 -0700

    add crossorigin

[33mcommit f7825bafe9746dc2f64e3067af2722ae60076157[m
Author: superjiy <superjiy@gamil.com>
Date:   Fri Jan 24 13:15:30 2020 -0700

    jongil
    jwt testing

[33mcommit 01c364720e71ab05041611cb5cc20a7f56e4d451[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Fri Jan 24 13:11:51 2020 -0700

    Remove permitAll() for POST on /api/content/**

[33mcommit cd2b3ba2f8f9034d6f43b025021467b7e0254a0d[m
Merge: f4032321 28e10341
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Fri Jan 24 11:59:26 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit f403232105ec6ea6734fc76e904373263947a8b0[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Fri Jan 24 11:59:04 2020 -0700

    Remove PDFDocletgenerator.bat
    Editing Permissions now works.

[33mcommit 28e103419fed8567eadf15c6d3e3792e972cc5c7[m
Author: superjiy <superjiy@gamil.com>
Date:   Fri Jan 24 11:03:00 2020 -0700

    jongil
    3D rendering

[33mcommit 0cfceea7b4776366296570a3538584e6dfbc9fb4[m
Author: superjiy <superjiy@gamil.com>
Date:   Fri Jan 24 10:08:33 2020 -0700

    jongil
    footer has been implemented

[33mcommit 0cd52a3d117e70be6e4a911eca9b32b673503e34[m
Author: superjiy <superjiy@gamil.com>
Date:   Fri Jan 24 09:57:10 2020 -0700

    jongil
    working on image handling

[33mcommit c78fbce104bae0e77a27e8f48518a1801424829f[m
Author: superjiy <superjiy@gamil.com>
Date:   Fri Jan 24 09:20:08 2020 -0700

    jongil
    Navbar implemented

[33mcommit 23367873e0da5aa13b4a6dc368bafa358644f67f[m
Merge: fdf8b977 ccfb2945
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Jan 24 09:13:40 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit fdf8b9770fd01969cac8996b20588d97eb199c50[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Jan 24 09:13:20 2020 -0700

    JWT login verification working.

[33mcommit ccfb2945d4c25fe8f0c66d5b1c476c12787c5d15[m
Author: superjiy <superjiy@gamil.com>
Date:   Fri Jan 24 08:39:59 2020 -0700

    Jongil
    react-bulma-component
    __variables.sass bug fix

[33mcommit 515e20442b767a0776eac5bbdd64a15cd07bd1b2[m
Merge: 9dbaa0ef 40d8044c
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Jan 23 21:50:31 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 9dbaa0ef20de113695adbfd41a43f9fb68ca2288[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Jan 23 21:50:13 2020 -0700

    JWT token now generated from login and returned in response!

[33mcommit 40d8044cf0298ac97dd83f2dc8b1b36277e46b9f[m
Author: superjiy <superjiy@gamil.com>
Date:   Thu Jan 23 20:20:14 2020 -0700

    jongil
    react-bulma-component
    path issue

[33mcommit dcffc7c939baa13ef7ff9767d3fddbfbddbcceac[m
Author: superjiy <superjiy@gamil.com>
Date:   Thu Jan 23 19:36:00 2020 -0700

    jongil
    bulma
    react-bulma-components
    node-sass
    have been installed

[33mcommit 813945674b1f60a2bc91c82fab8e4a45881d9b3b[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Jan 23 18:33:38 2020 -0700

    map cleanup

[33mcommit cbe065e2601296903f5e5066778b8b97a068a338[m
Author: superjiy <superjiy@gamil.com>
Date:   Thu Jan 23 12:38:32 2020 -0700

    jongil
    simple navigation

[33mcommit 9c36444c9bb89b9e2ee43ab16ad4ad2972abfd21[m
Author: superjiy <superjiy@gamil.com>
Date:   Thu Jan 23 12:30:36 2020 -0700

    jongil
    
    house keeping

[33mcommit 64342ffe430fb9d98036b78c2a508dff7f27ea2f[m
Merge: 15824c7d 8da47fc9
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Thu Jan 23 11:46:50 2020 -0700

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       src/main/java/com/asylumproject/asylumproject/dataaccess/PermissionDB.java

[33mcommit 15824c7da223628175057a11d827532a8c8b4f85[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Thu Jan 23 11:46:22 2020 -0700

    update signuprequest

[33mcommit 8da47fc98d05fc93fcf5ab403ae1029ce8d9192f[m
Author: superjiy <superjiy@gamil.com>
Date:   Thu Jan 23 11:39:04 2020 -0700

    jong
    
    reat routing

[33mcommit 2a0af3423fb5976e84905ffffe18083adf356771[m
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Thu Jan 23 11:13:39 2020 -0700

    updating data access classes.

[33mcommit cf47f69bd1e404e3c49e6b44e766e533990bd86f[m
Merge: f8adb359 4df71dcb
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Jan 23 10:59:58 2020 -0700

    Merge branch 'master' of gitlab.com:capstoneawesome/asylumproject

[33mcommit f8adb3592b089bc2794d2bc4821c918990b6f030[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Thu Jan 23 10:59:30 2020 -0700

    homepage set

[33mcommit 4df71dcbac57e6c9169bbb0677050c8e04c21b84[m
Merge: c2120f0d 2caa62b3
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Thu Jan 23 10:58:34 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit c2120f0d32fa6f49652ed88bb7a55f63e120e4d1[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Thu Jan 23 10:58:06 2020 -0700

    Controllers update

[33mcommit 2caa62b321e2d62be76669a110788cad9bc290c3[m
Merge: aa934bf1 b8245d0e
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Jan 22 18:41:03 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit aa934bf1932a954fbd337ab41104bca525288a85[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Jan 22 18:40:39 2020 -0700

    Update authentication classes.

[33mcommit b8245d0e7f369ea0d82570bb8b94f1d90c5082b5[m
Author: superjiy <superjiy@gamil.com>
Date:   Wed Jan 22 13:59:12 2020 -0700

    Jongil
    done with demonstration

[33mcommit 88886b13f0d0767e83e8a3c94013d4cf8bff4293[m
Merge: d379cadc 5fccbc52
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Wed Jan 22 11:36:40 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 5fccbc520677b7b7e1e8797dfe29651e8e250ef8[m
Author: superjiy <superjiy@gamil.com>
Date:   Tue Jan 21 19:23:18 2020 -0700

    Jongil
    extra

[33mcommit d4996cab4794917acfa17732970a0c9d3cb0802b[m
Author: superjiy <superjiy@gamil.com>
Date:   Tue Jan 21 19:21:59 2020 -0700

    Jongil
    CORS issue has been solved

[33mcommit 9fd784a36fd5e09dbf6ab0bf62ae5a50e1195b1a[m
Merge: dcabbf31 51a7bda1
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Tue Jan 21 16:21:42 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit dcabbf31f90650cd29df78cc772fd66ac7ecdcec[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Tue Jan 21 16:21:09 2020 -0700

    add getter and setter to state in Text class

[33mcommit 51a7bda10006415285eaf45861cf418022d6a90e[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Tue Jan 21 14:51:24 2020 -0700

    cleanup

[33mcommit d379cadcf485e9d4b2304542f2282c1e5ed5c527[m
Merge: 720b71da 95388f74
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Jan 21 14:14:06 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 95388f74f48412a04767514238b96204e8d0574e[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Tue Jan 21 11:46:26 2020 -0700

    fixed import

[33mcommit 720b71da0352e299fba730659d90cb6e74c3b586[m
Merge: 50a0a5c9 c85e1251
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Jan 21 11:45:28 2020 -0700

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       src/main/java/com/asylumproject/asylumproject/problemdomain/Resource.java
    #       src/main/java/com/asylumproject/asylumproject/problemdomain/Story.java
    #       src/main/java/com/asylumproject/asylumproject/problemdomain/Text.java

[33mcommit 50a0a5c9c72c157ec01f6d82c4b0bfc47d8637fe[m
Merge: cad8c768 6505907c
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Jan 21 11:39:22 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit c85e1251dfe2f74160282f9415ce12919aa84bfa[m
Merge: 21578347 6505907c
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Jan 21 11:33:31 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 2157834754bf48a0f748739b655e17f5671faaea[m
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Jan 21 11:33:15 2020 -0700

    updating problem domain classes.

[33mcommit 6505907cf9861736f006957c8bc33dc5490221ac[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Tue Jan 21 11:21:49 2020 -0700

    updated gradle to run npm tasks

[33mcommit 4a0bf0500003eb726282f843bc9379c046770dd6[m
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Jan 21 11:06:00 2020 -0700

    I have updated language class.

[33mcommit 7357a4dfa032086a3901175d9669b93d806425e6[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Tue Jan 21 10:42:28 2020 -0700

    build.gradle test

[33mcommit d938bd35a9afe7d4f30b65047b8bf86a14419b3b[m
Merge: c0153629 96a66afc
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Jan 21 10:05:24 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit c0153629de02520536f6b2cbc89c734a60c9cd14[m
Author: 774570 <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Jan 21 10:02:52 2020 -0700

    I have updated language class.

[33mcommit cad8c7688d49ad6c499968b8fc1b483c0e01af58[m
Merge: 7ac9c631 96a66afc
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Jan 21 08:02:10 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 96a66afc5c486cce92bc8ca19515d00cf690e9f6[m
Author: superjiy <superjiy@gamil.com>
Date:   Mon Jan 20 19:51:16 2020 -0700

    jongil_In http header, there is a problem with content-type.
    It seems to be related to mode: no-cors option.
    I was not able to find a solution for this issue

[33mcommit c08754aba9b35d0deb77c25fa4bf6319dc373abf[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Jan 20 18:10:00 2020 -0700

    change mysql props

[33mcommit 5f51636dfd7402797f07d8b7e8e9052968ac3bed[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Jan 20 18:08:54 2020 -0700

    Add permitAll() for text elements.
    Update Content Controller.
    Update Text class.

[33mcommit afb7085d1376d6ed1144f306803cbe4e7e613ac9[m
Merge: d6019d4d ac9252e0
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Jan 20 17:37:20 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit ac9252e0b558ce570284f8aa6de6c43cb8bd59cf[m
Author: superjiy <superjiy@gamil.com>
Date:   Mon Jan 20 17:36:16 2020 -0700

    jongil_401 error issue is ongoing
    application.properties file has been modified
    more information, please refer to database connection

[33mcommit d6019d4dcd1dd875699b3cd290fa66736db19e6c[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Mon Jan 20 17:36:04 2020 -0700

    Add permitAll() for ContentController

[33mcommit de207206932400ac3d6ea2eb4d2dd394b360c86c[m
Author: superjiy <superjiy@gamil.com>
Date:   Mon Jan 20 14:24:21 2020 -0700

    jongil_connectivity between react and spring boot

[33mcommit 216534a8eb73acb2c10a2b5e73007a49660e4850[m
Author: superjiy <superjiy@gamil.com>
Date:   Mon Jan 20 13:14:03 2020 -0700

    jongil_basic quilljs is up and running

[33mcommit a1cc6a477eccda7cc08a8615dd70ad48ef4504af[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Mon Jan 20 13:10:04 2020 -0700

    Some of the changes didn't commit for some reason

[33mcommit 67803e6f156bd37da139d81eff9bc086f4f11db9[m
Author: JBorchelli <jeffrey.borchert@edu.sait.ca>
Date:   Mon Jan 20 13:06:10 2020 -0700

    Map Stuff

[33mcommit 7ac9c63139391215d35971f39d258284695a62ae[m
Merge: ae222712 59ea8614
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Mon Jan 20 10:41:48 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 59ea8614d7e4928d704ef7b1edc0213219462d3e[m
Author: superjiy <superjiy@gamil.com>
Date:   Sun Jan 19 16:53:16 2020 -0700

    jongil_Editor component init

[33mcommit ae222712af0ad801de9f0f319627c0fd9589359e[m
Merge: b0685fab 88add17c
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Fri Jan 17 13:47:18 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 88add17c617464317970d0d7cb0fc82f7249ad75[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Fri Jan 17 13:30:28 2020 -0700

    Minor fixes.

[33mcommit 2250bbff7aa6dc3c1b968a5eee5595bc537c144a[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Fri Jan 17 13:08:19 2020 -0700

    Final variables changed to uppercase.

[33mcommit 9e5719ee5f3891d3f060d247ef1355f566427472[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Jan 17 09:21:42 2020 -0700

    Add setter.

[33mcommit d0b4101e296810738a0c80c760337933ca41587c[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Fri Jan 17 09:16:58 2020 -0700

    Add missing javadoc params.
    Fix ReportBroker.

[33mcommit b0685fabb391dc56c037a9b73f7741c73470ef18[m
Merge: 2d080968 f01b1e79
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Thu Jan 16 18:56:35 2020 -0700

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       src/main/java/com/asylumproject/asylumproject/manager/BackupManager.java

[33mcommit f01b1e79133034e0834ccde14f7d5a43c54eda7f[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Thu Jan 16 17:23:51 2020 -0700

    updates to fix Sequence Diagrams

[33mcommit 2d08096800c1c2d03a8604ac9d32e70eec4a7e72[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Thu Jan 16 11:56:26 2020 -0700

    Updated problem domain classes with attribute constraints

[33mcommit 629269147047ff315bb73aeb2c41952817ada543[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Wed Jan 15 16:44:11 2020 -0700

    update StudyClassController, UserController, StudyClass

[33mcommit dcf3958f9cd561981673ef19e2f40d9b4b309c8d[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Wed Jan 15 10:57:45 2020 -0700

    updated controller classes

[33mcommit 4c110014cd83e1fd1bb401055c2bdf1f2565caaf[m
Merge: 41b91528 82da8be2
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Jan 14 13:59:27 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 41b915282e954dcd93b619a41e8a2c227f9a3b77[m
Author: blajor <46553507+blajor@users.noreply.github.com>
Date:   Tue Jan 14 13:59:07 2020 -0700

    Updated problem domain classes with attribute constraints

[33mcommit 82da8be238346c76481c5e5cf2cfbd2be6a9ec8e[m
Author: JBorchelli <jeffborch@gmail.com>
Date:   Tue Jan 14 09:50:35 2020 -0700

    Updated app and background data dictionary

[33mcommit b022230fcd9b08fbcc74db3ea4df038c5ec33233[m
Merge: d2cd5889 3ebb5947
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Tue Jan 14 09:49:22 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit d2cd5889439eee37919d89519a46b405b26c21b7[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Tue Jan 14 09:48:57 2020 -0700

    Updated dataaccess classes to interfaces.

[33mcommit 7cf9ee5dcd0cb245d58461deedecc91dcf4c22fa[m
Merge: 48a0e2c4 3ebb5947
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Jan 14 09:28:17 2020 -0700

    Merge remote-tracking branch 'origin/master'
    
    # Conflicts:
    #       src/main/resources/application.properties

[33mcommit 48a0e2c4e5649dd4beb2679b65221d7e785eaf57[m
Author: blajor <jorge.blanco@edu.sait.ca>
Date:   Tue Jan 14 09:27:24 2020 -0700

    Updated problem domain classes with attribute constraints

[33mcommit 3ebb59476e678e38c880fc611639cdd565fb52da[m
Author: JBorchelli <jeffborch@gmail.com>
Date:   Tue Jan 14 09:17:22 2020 -0700

    Updated data dictionary

[33mcommit 71c6e7bf486d35d720e8e118b7fff54bbe3e5f90[m
Merge: 6ae51025 49cef459
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Jan 14 08:52:35 2020 -0700

    Merge remote-tracking branch 'origin/master'

[33mcommit 6ae510255ec5e99f0667cd05944ed182ac08cf53[m
Author: amir <amir.bozorgyamchi@edu.sait.ca>
Date:   Tue Jan 14 08:52:00 2020 -0700

    I have updated language class.

[33mcommit 49cef459dca8f04fb063a0ac56cdcdc74bd05b23[m
Author: JBorchelli <jeffborch@gmail.com>
Date:   Tue Jan 14 08:36:16 2020 -0700

    Updated app and background data dictionary

[33mcommit 50b15dcaf1433ccb1445a18e120c9583fe8bcc0e[m
Author: JBorchelli <jeffborch@gmail.com>
Date:   Mon Jan 13 10:54:20 2020 -0700

    added webapp

[33mcommit 22afbb022a796cfe7bd87d1f5f2ac8ee0f586033[m
Author: runeman2@gmail.com <runeman2@gmail.com>
Date:   Mon Jan 13 10:48:04 2020 -0700

    Add text to home page response.

[33mcommit cb9c3fc3a4b1569075847cf64ad6458277f8d596[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sun Jan 12 19:40:40 2020 -0700

    Add text to home page response.

[33mcommit 54e870a157479bc66dbc838dab5baa2a4b39ba09[m
Author: redlac <Calder.Trombley@edu.sait.ca>
Date:   Sat Jan 11 19:34:59 2020 -0700

    Initial commit
